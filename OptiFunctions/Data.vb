﻿
Imports System.Net
Imports System.Security.Cryptography
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.IO
Imports System.Windows
Imports System.Globalization
Imports System.Threading.Tasks
Imports System.Math
Imports System
Imports System.Data.Odbc
Imports System.Data.OleDb
Imports System.Runtime.InteropServices
'Imports Microsoft.Office.Interop.Excel

Namespace Data

    Public Interface DataBase

        Sub ArrayToTable(source As DatTable, tableName As String)

        Function TableToArray(sqlCommandStr As String) As DatTable

        Function getDataTable(ByRef sqlCommandStr As String, ByRef tableName As String) As DataTable

        Sub execSqlCommand(sqlCommandStr As String)

        Sub beginTransaction()
        Sub commitTransaction()

        Sub openConn()
            
        Sub closeConn()

        Function getSchema() As DataTable
        Function getSchema(ByRef collectionName As String) As DataTable
    End Interface

    'Kopfzeilen abfangen !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    Public Structure DatTable
        Dim numberOfRows As Integer
        Dim numberOfColumns As Integer
        Dim cells As String(,)
    End Structure

    Public Interface DatSource
        Sub ArrayToTable(source As DatTable)
        Function TableToArray() As DatTable
        Function TableToDataTable() As DataTable
    End Interface

    Public Class CSV_File
        Implements DatSource

        Private fileName As String
        Private strm As Stream
        Private wStream As Stream

        Private reader As StreamReader
        Private writer As StreamWriter
        Private csvDelimiter As String = ","

        Private isHeadline As Boolean = True
        Private headLine As String()


        Public Sub New(ByRef fN As String)
            fileName = fN
            strm = Nothing
        End Sub

        Public Sub New(ByRef rs As Stream)
            strm = rs
            fileName = Nothing
        End Sub

        Public Sub setIsHeadLine(isHL As Boolean)
            isHeadline = isHL
        End Sub

        Public Sub setHeadline(ByRef hL As String())
            headLine = hL
        End Sub

        Public Sub setCSVDelimier(ByRef cD As String)
            csvDelimiter = cD
        End Sub

        Public Function getCSVDelimier()
            getCSVDelimier = csvDelimiter
        End Function

        Public Function getHeadLine() As String()
            getHeadLine = headLine
        End Function

        Public Function getIsHeadline()
            getIsHeadline = isHeadline
        End Function

        Public Sub OpenForReading()
            If fileName IsNot Nothing Then
                reader = New StreamReader(fileName)
            ElseIf strm IsNot Nothing Then
                reader = New StreamReader(strm)
            End If
        End Sub

        Public Sub OpenForWriting(ByVal isAppend As Boolean)
            If fileName IsNot Nothing Then
                writer = New StreamWriter(fileName, isAppend)
            ElseIf strm IsNot Nothing Then
                writer = New StreamWriter(strm)
            End If
        End Sub

        Public Sub CloseDoc()

            If reader IsNot Nothing Then
                reader.Close()
            End If
            If writer IsNot Nothing Then
                writer.Close()
            End If

        End Sub

        Public Sub ArrayToTable(source As DatTable) Implements DatSource.ArrayToTable

            Dim i, j As Integer
            Dim currLine As String

            For j = 0 To source.numberOfRows - 1

                currLine = ""

                For i = 0 To source.numberOfColumns - 1

                    currLine = currLine + source.cells(j, i) + csvDelimiter

                Next
                writer.WriteLine(currLine)

            Next

            writer.Flush()

        End Sub

        Public Function TableToDataTable() As DataTable Implements DatSource.TableToDataTable
            Dim maxColumn As Integer
            Dim resultTab As DataTable
            Dim i As Integer
            Dim j As Integer
            Dim numberOfCols As Integer
            Dim currLine As String
            Dim lineArr As String()
            Dim isReadHeadLine As Boolean
            Dim currSplitLine As String()

            resultTab = New DataTable

            'resultTab.numberOfColumns = 0

            isReadHeadLine = False

            i = 0

            While Not reader.EndOfStream()

                currLine = reader.ReadLine()

                If isHeadline And Not isReadHeadLine Then
                    headLine = Split(currLine, csvDelimiter)
                    isReadHeadLine = True
                    For j = 0 To headLine.Length - 1
                        resultTab.Columns.Add(headLine(j))
                        numberOfCols = numberOfCols + 1
                    Next
                Else
                    currSplitLine = Split(currLine, csvDelimiter)
                    If Not isReadHeadLine Then
                        For j = 0 To currSplitLine.Length - 1
                            resultTab.Columns.Add()
                            numberOfCols = numberOfCols + 1
                        Next
                        isReadHeadLine = True
                    End If

                    maxColumn = currSplitLine.GetLength(0)
                    If maxColumn > numberOfCols Then
                        For j = numberOfCols To maxColumn - 1
                            resultTab.Columns.Add()
                            numberOfCols = numberOfCols + 1
                        Next

                    End If

                    If currLine <> "" Then
                        ReDim Preserve lineArr(i)

                        lineArr(i) = currLine

                        i = i + 1
                    End If
                End If
            End While

            'resultTab.numberOfRows = i

            If i > 0 Then
                'ReDim resultTab.cells(resultTab.numberOfRows - 1, resultTab.numberOfColumns - 1)

                For i = 0 To UBound(lineArr)

                    currSplitLine = Split(lineArr(i), csvDelimiter)
                    Dim currRow As DataRow
                    currRow = resultTab.NewRow()

                    For j = 0 To UBound(currSplitLine)

                        currRow(j) = currSplitLine(j)
                        'resultTab.cells(i, j) = currSplitLine(j)

                    Next
                    resultTab.Rows.Add(currRow)

                Next
            End If
            'TableToArray = resultTab
            Return resultTab

        End Function

        Public Function TableToArray() As DatTable Implements DatSource.TableToArray

            Dim maxColumn As Integer
            Dim resultTab As DatTable
            Dim i As Integer
            Dim j As Integer
            Dim currLine As String
            Dim lineArr As String()
            Dim isReadHeadLine As Boolean
            Dim currSplitLine As String()

            resultTab = New DatTable

            resultTab.numberOfColumns = 0

            isReadHeadLine = False

            i = 0

            While Not reader.EndOfStream()

                currLine = reader.ReadLine()

                If isHeadline And Not isReadHeadLine Then
                    headLine = Split(currLine, csvDelimiter)
                    isReadHeadLine = True
                Else
                    currSplitLine = Split(currLine, csvDelimiter)
                    maxColumn = currSplitLine.GetLength(0)
                    If maxColumn > resultTab.numberOfColumns Then
                        resultTab.numberOfColumns = maxColumn
                    End If

                    If currLine <> "" Then
                        ReDim Preserve lineArr(i)

                        lineArr(i) = currLine

                        i = i + 1
                    End If
                End If
            End While

            resultTab.numberOfRows = i

            If i > 0 Then
                ReDim resultTab.cells(resultTab.numberOfRows - 1, resultTab.numberOfColumns - 1)

                For i = 0 To UBound(lineArr)

                    currSplitLine = Split(lineArr(i), csvDelimiter)

                    For j = 0 To UBound(currSplitLine)

                        resultTab.cells(i, j) = currSplitLine(j)

                    Next

                Next
            End If
            TableToArray = resultTab
        End Function


        Public Function TableToArray(ByVal startRow As Integer, ByVal startColumn As Integer, ByVal numberOfColumns As Integer)

            Dim maxColumn As Integer
            Dim resultTab As DatTable
            Dim i As Integer
            Dim j As Integer
            Dim currLine As String
            Dim currSplitLine As String()
            Dim initArr(numberOfColumns, 0) As String

            resultTab = New DatTable

            resultTab.numberOfColumns = numberOfColumns

            i = 0

            While Not reader.EndOfStream()

                currLine = reader.ReadLine()

                If currLine <> "" Then


                    If i >= startRow Then
                        ReDim Preserve initArr(numberOfColumns, i - startRow)

                        For j = 0 To resultTab.numberOfColumns - 1
                            initArr(j, i - startRow) = ""
                        Next

                        If (i Mod 100) = 0 Then
                            Console.WriteLine("Line " + CStr(i) + " read")
                        End If

                        currSplitLine = Split(currLine, csvDelimiter)

                        If UBound(currSplitLine) < numberOfColumns + startColumn Then
                            maxColumn = UBound(currSplitLine)
                        Else
                            maxColumn = numberOfColumns + startColumn
                        End If

                        For j = startColumn To maxColumn
                            initArr(j - startColumn, i - startRow) = currSplitLine(j)
                        Next
                    End If

                    i = i + 1
                End If
            End While

            resultTab.numberOfRows = i - startRow

            ReDim resultTab.cells(resultTab.numberOfRows - 1, resultTab.numberOfColumns - 1)
            For j = 0 To resultTab.numberOfRows - 1

                For i = 0 To resultTab.numberOfColumns - 1

                    resultTab.cells(j, i) = initArr(i, j)

                Next
            Next

            TableToArray = resultTab
        End Function
    End Class

    Public Class SQLTable
        Implements DatSource

        Private queryTxt As String
        Private tableName As String

        Private usedDb As DataBase

        Sub New(db As DataBase)
            usedDb = db
        End Sub

        Sub setTablenameForWriting(ByRef tabName As String)
            tableName = tabName
        End Sub

        Sub setQueryForReading(ByRef qTxt As String)
            queryTxt = qTxt
        End Sub

        Sub ArrayToTable(source As DatTable) Implements DatSource.ArrayToTable

            usedDb.ArrayToTable(source, tableName)

        End Sub

        Function TableToArray() As DatTable Implements DatSource.TableToArray
            TableToArray = usedDb.TableToArray(queryTxt)

        End Function
        Public Function TableToDataTable() As DataTable Implements DatSource.TableToDataTable


        End Function

    End Class

    Public Class ODBCDatabase
        Implements DataBase

        Private usedConnStr As String
        Private usedConnection As System.Data.Odbc.OdbcConnection
        Private usedTransaction As OdbcTransaction
        Private usC As CultureInfo

        Public Sub New(connStr As String)
            usedConnStr = connStr
            usedConnection = New System.Data.Odbc.OdbcConnection(usedConnStr)

            usedTransaction = Nothing
            usC = CultureInfo.CreateSpecificCulture("en-US")

        End Sub

        Public Sub openConn() Implements DataBase.openConn
            usedConnection.Open()

        End Sub
        Public Sub closeConn() Implements DataBase.closeConn
            usedConnection.Close()

        End Sub

        Public ReadOnly Property dbName As String
            Get
                Return usedConnection.Database
            End Get
        End Property

        Public Sub ArrayToTable(source As DatTable, tableName As String) Implements DataBase.ArrayToTable
            Dim i, j As Integer


            For i = 0 To source.numberOfRows - 1


                Dim sqlWriteQuery As String = "INSERT INTO " + tableName + " VALUES ("

                For j = 0 To source.numberOfColumns - 1 'columnNames.Length - 1
                    sqlWriteQuery += source.cells(i, j)
                    If j < source.numberOfColumns - 1 Then
                        sqlWriteQuery += ","
                    End If
                Next

                sqlWriteQuery += ")"

                execSqlCommand(sqlWriteQuery)

            Next
        End Sub

        Public Sub execSqlCommand(sqlCommandStr As String) Implements DataBase.execSqlCommand

            Dim queryCommand As New OdbcCommand(sqlCommandStr, usedConnection)
            If usedTransaction IsNot Nothing Then
                queryCommand.Transaction = usedTransaction
            End If
            queryCommand.CommandTimeout = 30000

            Try
                queryCommand.ExecuteNonQuery()
            Catch ex As Exception
                Dim i
                i = 1
                If usedConnection.State <> ConnectionState.Open And usedTransaction Is Nothing Then

                    usedConnection = New System.Data.Odbc.OdbcConnection(usedConnStr)
                    usedConnection.Open()
                    queryCommand = New OdbcCommand(sqlCommandStr, usedConnection)
                    queryCommand.ExecuteNonQuery()
                Else
                    usedTransaction = Nothing
                    usedConnection = New System.Data.Odbc.OdbcConnection(usedConnStr)
                    usedConnection.Open()
                    Throw ex

                End If
            End Try

        End Sub

        Public Function TableToArray(sqlCommandStr As String) As DatTable Implements DataBase.TableToArray
            Dim queryCommand As New OdbcCommand(sqlCommandStr, usedConnection)
            Dim myDataTable As New DataTable
            Dim iColumns As Integer
            Dim iRows As Integer


            queryCommand.CommandTimeout = 300

            If usedTransaction IsNot Nothing Then
                queryCommand.Transaction = usedTransaction
            End If

            Try
                myDataTable.Load(queryCommand.ExecuteReader())
            Catch ex As Exception
                Dim i
                i = 1
                If usedConnection.State <> ConnectionState.Open And usedTransaction Is Nothing Then
                    usedConnection = New System.Data.Odbc.OdbcConnection(usedConnStr)
                    usedConnection.Open()
                    queryCommand = New OdbcCommand(sqlCommandStr, usedConnection)
                    myDataTable.Load(queryCommand.ExecuteReader())
                Else
                    usedTransaction = Nothing
                    Throw ex
                End If
            End Try

            'On Error Resume Next
            Dim resultTab As DatTable

            resultTab.numberOfRows = myDataTable.Rows.Count
            resultTab.numberOfColumns = myDataTable.Columns.Count

            ReDim resultTab.cells(resultTab.numberOfRows - 1, resultTab.numberOfColumns)

            If resultTab.numberOfRows > 0 Then
                iRows = 0
                For Each row As DataRow In myDataTable.Rows

                    For iColumns = 0 To resultTab.numberOfColumns - 1
                        resultTab.cells(iRows, iColumns) = Convert.ToString(row.ItemArray(iColumns), usC)
                    Next
                    iRows = iRows + 1
                Next
            End If

            TableToArray = resultTab
        End Function

        Sub beginTransaction() Implements DataBase.beginTransaction

            Try
                usedTransaction = usedConnection.BeginTransaction()
            Catch ex As Exception
                usedConnection = New System.Data.Odbc.OdbcConnection(usedConnStr)
                usedConnection.Open()
                usedTransaction = usedConnection.BeginTransaction()
            End Try

        End Sub
        Sub commitTransaction() Implements DataBase.commitTransaction

            If usedConnection.State = ConnectionState.Open Then
                usedTransaction.Commit()
            End If

            usedTransaction = Nothing
        End Sub


        Public Function getDataTable(ByRef sqlCommandStr As String, ByRef tableName As String) As DataTable Implements DataBase.getDataTable
            Dim queryCommand As New OdbcCommand(sqlCommandStr, usedConnection)
            Dim myDataTable As New DataTable(tableName)

            queryCommand.CommandTimeout = 300

            If usedTransaction IsNot Nothing Then
                queryCommand.Transaction = usedTransaction
            End If

            Try
                myDataTable.Load(queryCommand.ExecuteReader())
            Catch ex As Exception
                Dim i
                i = 1
                If usedConnection.State <> ConnectionState.Open And usedTransaction Is Nothing Then
                    usedConnection = New System.Data.Odbc.OdbcConnection(usedConnStr)
                    usedConnection.Open()
                    queryCommand = New OdbcCommand(sqlCommandStr, usedConnection)
                    myDataTable.Load(queryCommand.ExecuteReader())
                Else
                    usedTransaction = Nothing
                    Throw ex
                End If
            End Try

            Return myDataTable

        End Function

        Public Function getSchema() As DataTable Implements DataBase.getSchema
            Return usedConnection.GetSchema()
        End Function

        Public Function getSchema(ByRef collectionName As String) As DataTable Implements DataBase.getSchema
            Return usedConnection.GetSchema(collectionName)
        End Function


    End Class

    Public Class OleDatabase
        Implements DataBase



        Private usedConnection As System.Data.OleDb.OleDbConnection
        Private usedTransaction As OleDbTransaction
        Private usC As CultureInfo

        Public Sub New(connStr As String)
            usedConnection = New System.Data.OleDb.OleDbConnection(connStr)

            usedTransaction = Nothing
            usC = CultureInfo.CreateSpecificCulture("en-US")
        End Sub

        Public Sub openConn() Implements DataBase.openConn
            usedConnection.Open()
        End Sub
        Public Sub closeConn() Implements DataBase.closeConn
            usedConnection.Close()

        End Sub

        Public ReadOnly Property dbName As String
            Get
                Return usedConnection.Database
            End Get

        End Property

        Public Sub ArrayToTable(source As DatTable, tableName As String) Implements DataBase.ArrayToTable
            Dim i, j As Integer

            For i = 0 To source.numberOfRows - 1


                Dim sqlWriteQuery As String = "INSERT INTO [" + tableName + "] VALUES ("

                For j = 0 To source.numberOfColumns - 1
                    sqlWriteQuery += source.cells(i, j)
                    If j < source.numberOfColumns - 1 Then
                        sqlWriteQuery += ","
                    End If
                Next

                sqlWriteQuery += ")"

                Dim queryCommandInsert As New OleDbCommand(sqlWriteQuery, usedConnection)
                queryCommandInsert.ExecuteNonQuery()

            Next
        End Sub

        Public Function TableToArray(sqlCommandStr As String) As DatTable Implements DataBase.TableToArray
            Dim queryCommand As New OleDbCommand(sqlCommandStr, usedConnection)
            Dim myDataTable As New DataTable
            Dim iColumns As Integer
            Dim iRows As Integer
            queryCommand.CommandTimeout = 300

            myDataTable.Load(queryCommand.ExecuteReader())


            Dim resultTab As DatTable

            resultTab.numberOfRows = myDataTable.Rows.Count
            resultTab.numberOfColumns = myDataTable.Columns.Count

            ReDim resultTab.cells(resultTab.numberOfRows - 1, resultTab.numberOfColumns)

            If resultTab.numberOfRows > 0 Then
                iRows = 0
                For Each row As DataRow In myDataTable.Rows

                    For iColumns = 0 To resultTab.numberOfColumns - 1
                        resultTab.cells(iRows, iColumns) = Convert.ToString(row.ItemArray(iColumns), usC)
                    Next
                    iRows = iRows + 1
                Next
            End If

            TableToArray = resultTab
        End Function

        Sub execSqlCommand(sqlCommandStr As String) Implements DataBase.execSqlCommand
            Dim queryCommandPeriod As New OleDbCommand(sqlCommandStr, usedConnection)
            queryCommandPeriod.ExecuteNonQuery()
        End Sub
        Sub beginTransaction() Implements DataBase.beginTransaction

            usedTransaction = usedConnection.BeginTransaction()
        End Sub
        Sub commitTransaction() Implements DataBase.commitTransaction

            usedTransaction.Commit()
        End Sub

        Public Function getDataTable(ByRef sqlCommandStr As String, ByRef tableName As String) As DataTable Implements DataBase.getDataTable

            Dim queryCommand As New OleDbCommand(sqlCommandStr, usedConnection)
            Dim myDataTable As New DataTable

            queryCommand.CommandTimeout = 300

            myDataTable.Load(queryCommand.ExecuteReader())

            Return myDataTable
        End Function

        Public Function getSchema() As DataTable Implements DataBase.getSchema
            Return usedConnection.GetSchema()
        End Function

        Public Function getSchema(ByRef collectionName As String) As DataTable Implements DataBase.getSchema
            Return usedConnection.GetSchema(collectionName)
        End Function
    End Class

    Public Class Excel_Document
        Implements DatSource

        Protected fileName As String
        Protected xlApp As Microsoft.Office.Interop.Excel.Application
        Protected currDoc As Microsoft.Office.Interop.Excel.Workbook
        Protected isCSV As Boolean
        Protected reader As StreamReader
        Protected csvDelimiter = ","

        Protected usedSheetName As String
        'Protected usedStartRow As Integer
        'Protected usedStartColumn As Integer
        'Protected usedNumberOfRows As Integer
        'Protected usedNumberOfColumns As Integer
        Protected ulCorner As String
        Protected lrCorner As String
        Protected isAreaSet As Boolean
        Protected isSQLSet As Boolean

        Protected isO As Boolean

        Sub New(ByRef fN As String)
            fileName = fN
            isO = False

        End Sub

        Public Sub setCSVParameters(ByRef csvDelim As String, ByVal iCSV As Boolean)
            isCSV = iCSV
            csvDelimiter = csvDelim
        End Sub

        'Public Sub setSheetData(ByRef sheetName As String, ByVal startRow As Integer, ByVal startColumn As Integer, ByVal numberOfRows As Integer, ByVal numberOfColumns As Integer)
        '    usedSheetName = sheetName
        '    usedStartRow = startRow
        '    usedStartColumn = startColumn
        '    usedNumberOfRows = numberOfRows
        '    usedNumberOfColumns = numberOfColumns
        'End Sub

        Public Sub setSheetData(ByRef sheetName As String, ByRef ulC As String, ByRef lrC As String)
            usedSheetName = sheetName
            ulCorner = ulC
            lrCorner = lrC
            isAreaSet = True
            isSQLSet = False
        End Sub

        Public Function TableToArray() As DatTable Implements DatSource.TableToArray 'ByRef sheetName As String, ByVal startRow As Integer, ByVal startColumn As Integer, ByVal numberOfRows As Integer, ByVal numberOfColumns As Integer)

            Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
            Dim isAllEmpty As Boolean
            Dim maxRow As Integer
            Dim maxColumn As Integer

            Dim currVal As Object
            Dim resultTab As DatTable
            Dim i As Integer
            Dim j As Integer
            Dim currLine As String
            Dim currSplitLine As String()
            resultTab = New DatTable

            'ReDim resultTab.cells(numberOfRows - 1, numberOfColumns - 1)

            'For i = 0 To numberOfRows - 1

            '    For j = 0 To numberOfColumns - 1
            '        resultTab.cells(i, j) = ""
            '    Next
            'Next

            'If isCSV Then

            '    maxColumn = startColumn + numberOfColumns - 1

            '    maxRow = startRow + numberOfRows - 1

            '    reader.BaseStream.Seek(0, SeekOrigin.Begin)
            '    i = 0
            '    isAllEmpty = True
            '    While i <= maxRow
            '        currLine = reader.ReadLine()
            '        If i >= startRow Then
            '            'If (i Mod 10) = 0 Then
            '            '    Console.WriteLine("Line " + CStr(i) + " read")
            '            'End If
            '            currSplitLine = Split(currLine, csvDelimiter)
            '            isAllEmpty = True

            '            For j = startColumn To maxColumn
            '                resultTab.cells(i - startRow, j - startColumn) = currSplitLine(j)
            '            Next
            '        End If
            '        i = i + 1

            '    End While

            'Else
            xlWorkSheet = currDoc.Worksheets(usedSheetName) '(sheetName)
            Dim xCells As Microsoft.Office.Interop.Excel.Range
            Dim lastCell As Microsoft.Office.Interop.Excel.Range

            Dim maxRowFound As Integer
            Dim maxColFound As Integer

            'maxColumn = startColumn + numberOfColumns - 1
            xCells = xlWorkSheet.Cells
            lastCell = xCells.SpecialCells(Microsoft.Office.Interop.Excel.XlCellType.xlCellTypeLastCell)

            maxRowFound = lastCell.Row
            maxColFound = lastCell.Column

            ' maxRow = startRow + numberOfRows - 1

            If maxRow > maxRowFound - 1 Then
                maxRow = maxRowFound - 1
            End If

            If maxColumn > maxColFound - 1 Then
                maxColumn = maxColFound - 1
            End If
            If maxRow >= xCells.Rows.Count Then
                maxRow = xCells.Rows.Count
            End If

            If maxColumn >= xCells.Columns.Count Then
                maxColumn = xCells.Columns.Count - 1
            End If
            Dim startRow As Integer
            Dim startColumn As Integer
            Dim vals As Object(,) 
            Dim lastCellName As String = lastCell.AddressLocal(ReferenceStyle:=Microsoft.Office.Interop.Excel.XlReferenceStyle.xlA1) 'xlWorkSheet.Cells.SpecialCells(Microsoft.Office.Interop.Excel.XlCellType.xlCellTypeLastCell).AddressLocal(ReferenceStyle:=Microsoft.Office.Interop.Excel.XlReferenceStyle.xlA1)

            vals = xlWorkSheet.Range(ulCorner, lastCellName).Value '("A" + Convert.ToString(startRow + 1), lastCell).Value '//get_Range("A" + Convert.ToString(startRow + 1) + ":CB" + Convert.ToString(maxRow + 1))


            startRow = vals.GetLowerBound(0)
            startColumn = vals.GetLowerBound(1)

            maxRow = vals.GetUpperBound(0)
            maxColumn = vals.GetUpperBound(1)

            resultTab.numberOfRows = maxRow - startRow + 1
            resultTab.numberOfColumns = maxColumn - startColumn + 1

            ReDim resultTab.cells(resultTab.numberOfRows - 1, resultTab.numberOfColumns - 1)
            For i = startRow To maxRow 'startRow To maxRow

                For j = startColumn To maxColumn 'startColumn To maxColumn
                    currVal = vals(i, j) '.Value

                    If currVal IsNot Nothing Then

                        resultTab.cells(i - startRow, j - startColumn) = currVal.ToString() '(i - startRow, j - startColumn) = currVal.ToString()
                    Else
                        resultTab.cells(i - startRow, j - startColumn) = "" '(i - startRow, j - startColumn) = ""
                    End If
                Next
            Next

            TableToArray = resultTab

            'Marshal.ReleaseComObject(xlWorkSheet)
            'Marshal.ReleaseComObject(xCells)
            'Marshal.ReleaseComObject(lastCell)
        End Function

        Public Sub OpenDoc()
            xlApp = New Microsoft.Office.Interop.Excel.Application
            xlApp.DisplayAlerts = False
            xlApp.AskToUpdateLinks = False
            currDoc = xlApp.Workbooks.Open(fileName, Local:=True)
            isCSV = False
            isO = True
        End Sub

        Public Sub OpenTextDoc()
            xlApp = New Microsoft.Office.Interop.Excel.Application
            xlApp.Workbooks.OpenText(fileName, DataType:=1, Other:=True, OtherChar:=";", DecimalSeparator:=",", ThousandsSeparator:=".")
            currDoc = xlApp.ActiveWorkbook
            isCSV = False
            isO = True
        End Sub

        Public Sub OpenDocAsCSV(ByRef cD As String)

            reader = New StreamReader(fileName)
            csvDelimiter = cD

            isCSV = True
            isO = True
        End Sub

        Public Sub CloseDoc()

            If isO Then

                If isCSV Then
                    reader.Close()
                Else
                    xlApp.DisplayAlerts = False
                    currDoc.Close()
                    Marshal.ReleaseComObject(currDoc)
                    'xlApp.DisplayAlerts = True
                    xlApp.Quit()
                    Marshal.ReleaseComObject(xlApp)
                    currDoc = Nothing
                    xlApp = Nothing
                End If

                isO = False
            End If

        End Sub

        Public Sub ArrayToTable(source As DatTable) Implements DatSource.ArrayToTable

        End Sub

        'Public Function TableToArray() As DatTable Implements DatSource.TableToArray
        '    TableToArray = TableToArray(usedSheetName, usedStartRow, usedStartColumn, usedNumberOfRows, usedNumberOfColumns)
        'End Function

        Public Function TableToDataTable() As DataTable Implements DatSource.TableToDataTable

            Return Nothing

        End Function

        Public ReadOnly Property IsOpen As Boolean
            Get
                Return isO
            End Get
        End Property
    End Class


    Public Class Excel_Document2007
        Implements DatSource

        Public Enum XLS_Type
            XLS
            XLSX
            'CSV
        End Enum

        '"varchar(255)"
        Protected colTypeStrs As String() = {"varchar(255)", "integer", "float"}

        Public Enum COL_Type
            STR_COL = 0
            INT_COL = 1
            DOUBLE_COL = 2
        End Enum

        Public Structure ColumnSpec
            Dim ColumnName As String
            Dim ColumnType As COL_Type
        End Structure


        Protected fileName As String
        Protected xlApp As Microsoft.Office.Interop.Excel.Application
        Protected currDoc As Microsoft.Office.Interop.Excel.Workbook

        Protected xlsDb As OleDatabase
        Protected xlsxTypeString As String = "Excel 12.0 Xml"
        Protected xlsTypeString As String = "Excel 8.0"

        Protected Const connStrBaseWrite As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Chr(34) & "{0}" & Chr(34) & ";Extended Properties='{2};HDR={1}'"
        Protected Const connStrBaseRead As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Chr(34) & "{0}" & Chr(34) & ";Extended Properties='{2};HDR={1};IMEX=1;MAXSCANROWS=1;READONLY=TRUE'" ''MAXSCANROWS=0;

        Protected Const readEntireSheetBase As String = "select * from [{0}]"
        'Protected Const readAreaFromSheetBase As String = "select * from [{0}${1}:{2}]"
        Protected Const sheetNameWithRangeTemplate As String = "{0}${1}:{2}"
        Protected Const createSheetTemplate As String = "create table [{0}] ({1})"

        Protected usedSheetName As String

        Protected isAreaSet As Boolean
        Protected ulCorner As String
        Protected lrCorner As String
        Protected isWrte As Boolean
        Protected isRCSheet As Boolean

        Protected isSQLSet As Boolean
        Protected usedSQLStatement As String

        Protected usedColSpecs As ColumnSpec()
        Sub New(ByRef fN As String, ByVal isHeader As Boolean, ByVal isWrite As Boolean, ByVal xlst As XLS_Type)

            'Dim olecon As OleDbConnection = New OleDbConnection
            'Dim olecmd As OleDbCommand = New OleDbCommand
            'Dim FilePath As String = "C:\"
            'Dim FileName As String = "EmployeeDatabase3.xlsx"
            ''Excel 12.0 Xml
            ''~~> Construct your connection string
            'Dim connstring As String = "Provider=Microsoft.ACE.OLEDB.12.0;" &
            '                           "Data Source=" & FilePath & FileName & ";" &
            '                           "Extended Properties='Excel 8.0;HDR=FALSE;'"

            'olecon.ConnectionString = connstring
            'olecon.Open()

            'olecmd.Connection = olecon

            ''~~> Command to create the table
            'olecmd.CommandText = "CREATE TABLE Sheet1 (Sno Int, " &
            '                                           "Employee_Name VARCHAR, " &
            '                                           "Company VARCHAR, " &
            '                                           "Date_Of_joining DATE, " &
            '                                           "Stipend DECIMAL, " &
            '                                           "Stocks_Held DECIMAL)"
            'olecmd.ExecuteNonQuery()

            ''~~> Adding Data
            'olecmd.CommandText = "INSERT INTO Sheet1 (Sno, Employee_Name, Company,Date_Of_joining,Stipend,Stocks_Held) values " &
            '                      "('1', 'Siddharth Rout', 'Defining Horizons', '20/7/2014','2000.75','0.01')"
            'olecmd.ExecuteNonQuery()

            ''~~> Close the connection
            'olecon.Close()


            Dim hdr As String
            Dim xlsType As String

            If xlst = XLS_Type.XLS Then
                xlsType = xlsTypeString
            ElseIf xlst = XLS_Type.XLSX Then
                xlsType = xlsxTypeString
            End If
            If isHeader Then
                hdr = "yes"
            Else
                hdr = "no"
            End If

            isWrte = isWrite
            'isRCSheet = isRCS
            If isWrte Then
                xlsDb = New OleDatabase(String.Format(connStrBaseWrite, fN, hdr, xlsType))
            Else
                xlsDb = New OleDatabase(String.Format(connStrBaseRead, fN, hdr, xlsType))
            End If

            isAreaSet = False

        End Sub

        Public Sub setSheetDataForCreate(ByRef sheetName As String, ByRef colSpecs As ColumnSpec())
            usedColSpecs = colSpecs
            usedSheetName = sheetName
            isAreaSet = False
            isSQLSet = False
        End Sub

        Public Sub setSheetData(ByRef sheetName As String, ByRef ulC As String, ByRef lrC As String)
            usedSheetName = sheetName
            ulCorner = ulC
            lrCorner = lrC
            isAreaSet = True
            isSQLSet = False
        End Sub

        Public Sub setSheetDataSQL(ByRef sqlStatement As String)
            usedSQLStatement = sqlStatement
            isSQLSet = True
            isAreaSet = False
        End Sub

        Public Sub setSheetData(ByRef sheetName As String)
            usedSheetName = sheetName
            isAreaSet = False
            isSQLSet = False
        End Sub

        Public Property IsRecreateSheet
            Get
                IsRecreateSheet = isRCSheet
            End Get
            Set(value)
                isRCSheet = value
            End Set
        End Property


        Public Sub OpenDoc()



            xlsDb.openConn()


        End Sub

        Public Sub CloseDoc()
            xlsDb.closeConn()
        End Sub

        Public Function getWorkSheets() As String()

            Dim schema As DataTable
            Dim tabNames As String()
            tabNames = Nothing
            schema = xlsDb.getSchema("Tables")

            For i = 0 To schema.Rows.Count - 1
                If schema.Rows(i).ItemArray(schema.Columns.IndexOf("TABLE_TYPE")).ToString = "TABLE" Then
                    ReDim Preserve tabNames(i)
                    tabNames(i) = schema.Rows(i).ItemArray(schema.Columns.IndexOf("TABLE_NAME")).ToString()
                End If
            Next

            Return tabNames
        End Function

        Public Sub ArrayToTable(source As DatTable) Implements DatSource.ArrayToTable

            Dim i, j As Integer
            Dim maxColSpecI As Integer
            Dim schema As DataTable
            Dim colSpecStr As String
            Dim createCommandStr As String
            'Dim finalSheetStr As String

            'If isAreaSet Then
            '    finalSheetStr = String.Format(sheetNameWithRangeTemplate, usedSheetName, ulCorner, lrCorner)
            'Else
            '    finalSheetStr = usedSheetName
            'End If


            If isRCSheet Then

                schema = xlsDb.getSchema("Tables")

                For i = 0 To schema.Rows.Count - 1
                    If schema.Rows(i).ItemArray(schema.Columns.IndexOf("TABLE_TYPE")).ToString = "TABLE" And schema.Rows(i).ItemArray(schema.Columns.IndexOf("TABLE_NAME")).ToString() = usedSheetName Then
                        xlsDb.execSqlCommand("drop table " + usedSheetName)
                    End If
                Next

                '{

                '    if (dt.Rows[i].ItemArray[dt.Columns.IndexOf("TABLE_TYPE")].ToString() == "TABLE" &&
                '        !dt.Rows[i].ItemArray[dt.Columns.IndexOf("TABLE_NAME")].ToString().Contains("$"))
                '    {
                '        comboBox1.Items.Add(dt.Rows[i].ItemArray[dt.Columns.IndexOf("TABLE_NAME")]);
                '    }
                colSpecStr = ""
                maxColSpecI = UBound(usedColSpecs)
                For i = 0 To maxColSpecI
                    colSpecStr = colSpecStr & usedColSpecs(i).ColumnName & " " & colTypeStrs(usedColSpecs(i).ColumnType)
                    If i < maxColSpecI Then
                        colSpecStr = colSpecStr & ","
                    End If
                Next

                createCommandStr = String.Format(createSheetTemplate, usedSheetName, colSpecStr)

                xlsDb.execSqlCommand(createCommandStr)

            End If

            xlsDb.ArrayToTable(source, usedSheetName)
        End Sub

        Public Function TableToArray() As DatTable Implements DatSource.TableToArray

            Dim usedSqlCmd As String
            Dim finalSheetStr As String

            If isSQLSet Then
                TableToArray = xlsDb.TableToArray(usedSQLStatement)
            Else
                If isAreaSet Then
                    ' usedSqlCmd = String.Format(readAreaFromSheetBase, usedSheetName, ulCorner, lrCorner)
                    finalSheetStr = String.Format(sheetNameWithRangeTemplate, usedSheetName, ulCorner, lrCorner)
                Else
                    finalSheetStr = usedSheetName
                End If

                usedSqlCmd = String.Format(readEntireSheetBase, finalSheetStr)
                TableToArray = xlsDb.TableToArray(usedSqlCmd)
            End If

        End Function

        Public Function TableToDataTable() As DataTable Implements DatSource.TableToDataTable

            Return Nothing
        End Function
    End Class
End Namespace