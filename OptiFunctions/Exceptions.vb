﻿Namespace Exceptions

    Public Class OptimizerException
        Inherits Exception

        Public Enum OptimizerExceptionType
            InputFileNotFound
            ColumnNotFound
            'configErrFileNotFound
            'configErrErreneousContent
            'configErrWrongNumberOfArguments
            'dataInputErr
            'notEnoughInitialInventoryErr
            'notEnoughWarehouseCapacityForInitialStockErr
            'OPLWriteErr
            'OptimizationPrepareErr
            'OptimizationServiceErr
            'VoidSetOfRelevantDemandsErr
            'VoidSetOfRelevantDemandsWithArticlesToBuyWarning
            'SimulationNotExistErr
            'NotSinglePlantErr
            'PostProcessingErr
            'WritingCustomerResultsErr
            'WritingExtendedDataErr
            'IllegalCommandLineParameter
            'ErrorWhileTriggeringQlikviewServerTask
            'UnknownErrorDuringOptimization
            'InfeasibilityDuringOptimization
        End Enum

        'Public Const configErrFileNotFound As Integer = 1

        'Public Const configErrErreneousContent As Integer = 2

        'Public Const configErrWrongNumberOfArguments As Integer = 3

        'Public Const dataInputErr As Integer = 4

        ''Public Const createStructuresErr As Integer = 5

        'Public Const notEnoughInitialInventoryErr As Integer = 5

        'Public Const notEnoughWarehouseCapacityForInitialStockErr As Integer = 6

        'Public Const OPLWriteErr As Integer = 7

        'Public Const OptimizationPrepareErr As Integer = 8

        'Public Const OptimizationServiceErr As Integer = 9

        'Public Const VoidSetOfRelevantDemandsErr As Integer = 10

        'Public Const VoidSetOfRelevantDemandsWithArticlesToBuyWarning As Integer = 20

        'Public Const SimulationNotExistErr As Integer = 11

        'Public Const NotSinglePlantErr As Integer = 12

        'Public Const PostProcessingErr As Integer = 13

        'Public Const WritingCustomerResultsErr As Integer = 14

        'Public Const WritingExtendedDataErr As Integer = 15

        'Public Const IllegalCommandLineParameter As Integer = 16

        'Public Const ErrorWhileTriggeringQlikviewServerTask As Integer = 17

        'Public Const UnknownErrorDuringOptimization As Integer = 18

        'Public Const InfeasibilityDuringOptimization As Integer = 19

        Private details As String

        Private code As OptimizerExceptionType 'Integer

        Public Sub New(generalReason As OptimizerExceptionType, detailedExplanation As String)

            details = detailedExplanation
            code = generalReason
        End Sub


        Public Function getDetails() As String
            getDetails = details
        End Function

        Public Function getGeneralReason() As OptimizerExceptionType

            getGeneralReason = code

        End Function
    End Class

End Namespace