﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.IO;
using Optimizer_Data;
namespace GUI_SNP_Optimization
{
    public partial class RunParameterSelection : Form
    {
        protected string optimizationBasePath;
        protected string resultsSubDir;
        protected string settingsFileName;
        protected CultureInfo currC;
        protected DataTable optRunConfigTab;
        protected  List<Form1.OptimizationRun> currentOptimizationRuns;
        protected Dictionary<string, Form1.OptimizationRunConfig> currentRunConfigs;
        public RunParameterSelection()
        {
            InitializeComponent();
        }
        protected string[] buildTitles = { "Ausbaustufe 1", "AusbauStufe 2a" };
        protected Form1.OptimizationRunConfig selectedRunConfig;
        public void setParams(string oBPath,CultureInfo c,string rSubDir,string settingsFName, List<Form1.OptimizationRun> optRuns,string [] oRunIdentifierNames)
        {
            currC = c;
            optimizationBasePath = oBPath;
            resultsSubDir = rSubDir;
            settingsFileName = settingsFName;
            currentOptimizationRuns =optRuns;
            optRunIdentifierNames = oRunIdentifierNames;
        }

        private void loadOptimizationRunConfigs(Dictionary<string, Form1.OptimizationRunConfig> runConfigs, List<Form1.OptimizationRun> optRuns)
        {
            Form1.OptimizationRunConfig currConfig;
            string optimizationFullFilename;
            //Dictionary<string, OptimizationRunConfig> oRunConfigs;

            //oRunConfigs = new Dictionary<string, OptimizationRunConfig>();
            runConfigs.Clear();
            foreach (Form1.OptimizationRun oRun in optRuns)
            {
                currConfig = new Form1.OptimizationRunConfig();
                optimizationFullFilename = optimizationBasePath + oRun.OptFolderName + Path.DirectorySeparatorChar + resultsSubDir + Path.DirectorySeparatorChar + oRun.RunFolderName + Path.DirectorySeparatorChar + settingsFileName;
                currConfig.loadConfig(optimizationFullFilename);
                runConfigs.Add(oRun.RunFolderName, currConfig);
            }

            //return oRunConfigs;
        }

        protected string ISO_8601_FORMAT_STRING = "yyyy-MM-ddTHH-mm-ss";

        private static string[] optimizationSettingsColumns = {
                                                        "MinimumProductionQuantity",
                                                        "SplitQuantity",
                                                        "UseBaskets",
                                                        "CapacityFactorForMachines",
                                                        "OnlyProductionOptimization",
                                                        "MinumInventoryFactor",
                                                        "ExpansionStage",
                                                        "FixPlants",
                                                        "FixationThreshold",
                                                        "UseFixationsExlusions",
                                                        "MaxGap"
                                               };
        protected string[] optRunIdentifierNames;
        private void visualizeOptimizationRunConfigs()
        {
            string[] currDataRow;
            Form1.OptimizationRunConfig currRunConfig;

            optRunConfigTab.Rows.Clear();
            foreach (Form1.OptimizationRun run in currentOptimizationRuns)
            {
                currDataRow = new string[15];
                currDataRow[0] = run.OptFolderName;//reducedOptRunDirCandidate.Substring(0, atPos);
                currDataRow[1] = run.TimeStamp.ToString(ISO_8601_FORMAT_STRING);//reducedOptRunDirCandidate.Substring(atPos + 1, byPos - atPos - 1);
                currDataRow[2] = run.UserName;//reducedOptRunDirCandidate.Substring(byPos + 2, reducedOptRunDirCandidate.Length - byPos - 2);
                currDataRow[3] = run.ScenName;
                currRunConfig = currentRunConfigs[run.RunFolderName];

                currDataRow[4] = Convert.ToString(currRunConfig.minProdQty, currC);

                currDataRow[5] = Convert.ToString(currRunConfig.splitQty, currC);

                //currDataRow[5] = Convert.ToString(currRunConfig.isAllowSplittingIfInFixations, currC); ;

                currDataRow[6] = Convert.ToString(currRunConfig.isUseBaskets, currC);

                currDataRow[7] = Convert.ToString(currRunConfig.capacityFactorForMachines, currC);

                currDataRow[8] = Convert.ToString(currRunConfig.isOnlyProductionOptimization, currC);

                currDataRow[9] = Convert.ToString(currRunConfig.minInventoryFactor, currC);
                currDataRow[10] = buildTitles[(Int32)currRunConfig.expansionStage];
                currDataRow[11] = Convert.ToString(currRunConfig.isFixPlants, currC);
                currDataRow[12] = Convert.ToString(currRunConfig.fixationThreshold, currC);
                currDataRow[13] = Convert.ToString(currRunConfig.isUseFixationsExlusions, currC);
                currDataRow[14] = Convert.ToString(currRunConfig.maxGap, currC);
                optRunConfigTab.Rows.Add(currDataRow);
            }
        }
        private void RunParameterSelection_Load(object sender, EventArgs e)
        {
            int i;

            currentRunConfigs =new Dictionary<string,Form1.OptimizationRunConfig> ();

            optRunConfigTab = new DataTable();
            
            for (i = 0; i < optRunIdentifierNames.Length; i++)
                optRunConfigTab.Columns.Add(optRunIdentifierNames[i]);

            for (i = 0; i < optimizationSettingsColumns.Length; i++)
                optRunConfigTab.Columns.Add(optimizationSettingsColumns[i]);

            dGVOptimizationRunConfigs.DataSource = optRunConfigTab;
            loadOptimizationRunConfigs(currentRunConfigs, currentOptimizationRuns);
            visualizeOptimizationRunConfigs();
            selectedRunConfig = null;
        }

        public Form1.OptimizationRunConfig selectRunConfig()
        {
            this.ShowDialog();
            return selectedRunConfig;
        }

        private void btnLoadSettingsFromOptimizationRun_Click(object sender, EventArgs e)
        {
            DataRowView dRV;
            string key;

            if (dGVOptimizationRunConfigs.CurrentRow != null)
            {
                dRV = dGVOptimizationRunConfigs.CurrentRow.DataBoundItem as DataRowView;

                if (dRV != null)
                {
                    key = dRV.Row.ItemArray[0].ToString() + "@" + dRV.Row.ItemArray[1].ToString() + "@" + dRV.Row.ItemArray[2].ToString() + "@" + dRV.Row.ItemArray[3].ToString();
                    selectedRunConfig = currentRunConfigs[key];
                }
            }
            this.Close();
        }

        private void btnBackWithoutAcceptance_Click(object sender, EventArgs e)
        {
            selectedRunConfig = null;
            this.Close();
        }
    }
}
