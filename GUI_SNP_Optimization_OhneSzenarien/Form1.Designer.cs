﻿namespace GUI_SNP_Optimization
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.browseFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.gpExecution = new System.Windows.Forms.GroupBox();
            this.tCProgress = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lbOptimizationRunning = new System.Windows.Forms.Label();
            this.pgbLoading = new System.Windows.Forms.ProgressBar();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tbLogOutput = new System.Windows.Forms.TextBox();
            this.btnStopAction = new System.Windows.Forms.Button();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tCInput = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cbOfflineMode = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbTranspCostVar = new System.Windows.Forms.TextBox();
            this.tbTranspCostFix = new System.Windows.Forms.TextBox();
            this.cBExternalDistanceData = new System.Windows.Forms.CheckBox();
            this.cBCheckCountryMissingTransportConnectionsCustomer = new System.Windows.Forms.CheckBox();
            this.lbInvalidSaleplan = new System.Windows.Forms.Label();
            this.btnEditBaseData = new System.Windows.Forms.Button();
            this.tbEnde = new System.Windows.Forms.TextBox();
            this.tbBeginn = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cLBCheckResult = new System.Windows.Forms.CheckedListBox();
            this.lbBaseData = new System.Windows.Forms.ListBox();
            this.btnDataLoad = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label26 = new System.Windows.Forms.Label();
            this.cbUseFixationsExclusions = new System.Windows.Forms.CheckBox();
            this.tbFixationThreshold = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cbFixPlants = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cmBBuild = new System.Windows.Forms.ComboBox();
            this.cbAdditionalRunWithFixations = new System.Windows.Forms.CheckBox();
            this.btnSelectSettingsFromPastSimulationRun = new System.Windows.Forms.Button();
            this.btnRunOptimization = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tbMinInvFactor = new System.Windows.Forms.TextBox();
            this.tbCapFactor = new System.Windows.Forms.TextBox();
            this.tbSplittingQty = new System.Windows.Forms.TextBox();
            this.tbMinProdQty = new System.Windows.Forms.TextBox();
            this.lbBasketUsage = new System.Windows.Forms.Label();
            this.cbBasketUsage = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbIsOnlyProductionOptimization = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dGVOptimizationRuns = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnDataFolderOpen = new System.Windows.Forms.Button();
            this.btnResultVisualization = new System.Windows.Forms.Button();
            this.btnInputOpen = new System.Windows.Forms.Button();
            this.btnResultsOpen = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dGVOptResults = new System.Windows.Forms.DataGridView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lbUsedBaseDataFolder = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cbDataLoaded = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lbExistingFolders = new System.Windows.Forms.ListBox();
            this.tbOptimizationName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lbCulture = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbMaxGap = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.gpExecution.SuspendLayout();
            this.tCProgress.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tCInput.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGVOptimizationRuns)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGVOptResults)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpExecution
            // 
            this.gpExecution.Controls.Add(this.tCProgress);
            this.gpExecution.Controls.Add(this.btnStopAction);
            this.gpExecution.Location = new System.Drawing.Point(13, 441);
            this.gpExecution.Name = "gpExecution";
            this.gpExecution.Size = new System.Drawing.Size(1113, 166);
            this.gpExecution.TabIndex = 13;
            this.gpExecution.TabStop = false;
            this.gpExecution.Text = "Ausführung";
            this.gpExecution.Enter += new System.EventHandler(this.gpExecution_Enter);
            // 
            // tCProgress
            // 
            this.tCProgress.Controls.Add(this.tabPage3);
            this.tCProgress.Controls.Add(this.tabPage4);
            this.tCProgress.Location = new System.Drawing.Point(12, 19);
            this.tCProgress.Name = "tCProgress";
            this.tCProgress.SelectedIndex = 0;
            this.tCProgress.Size = new System.Drawing.Size(1098, 110);
            this.tCProgress.TabIndex = 54;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lbOptimizationRunning);
            this.tabPage3.Controls.Add(this.pgbLoading);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1090, 84);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Fortschritt";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lbOptimizationRunning
            // 
            this.lbOptimizationRunning.AutoSize = true;
            this.lbOptimizationRunning.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbOptimizationRunning.Location = new System.Drawing.Point(339, 55);
            this.lbOptimizationRunning.Name = "lbOptimizationRunning";
            this.lbOptimizationRunning.Size = new System.Drawing.Size(17, 17);
            this.lbOptimizationRunning.TabIndex = 42;
            this.lbOptimizationRunning.Text = "V";
            // 
            // pgbLoading
            // 
            this.pgbLoading.Location = new System.Drawing.Point(342, 18);
            this.pgbLoading.Name = "pgbLoading";
            this.pgbLoading.Size = new System.Drawing.Size(733, 25);
            this.pgbLoading.TabIndex = 41;
            this.pgbLoading.Click += new System.EventHandler(this.pgbLoading_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label11.Location = new System.Drawing.Point(7, 55);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 17);
            this.label11.TabIndex = 39;
            this.label11.Text = "Optimierung";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label3.Location = new System.Drawing.Point(7, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 17);
            this.label3.TabIndex = 38;
            this.label3.Text = "Laden der Stammdaten";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tbLogOutput);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1090, 84);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Protokoll";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tbLogOutput
            // 
            this.tbLogOutput.BackColor = System.Drawing.SystemColors.Window;
            this.tbLogOutput.Location = new System.Drawing.Point(0, 0);
            this.tbLogOutput.Multiline = true;
            this.tbLogOutput.Name = "tbLogOutput";
            this.tbLogOutput.ReadOnly = true;
            this.tbLogOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbLogOutput.Size = new System.Drawing.Size(1087, 84);
            this.tbLogOutput.TabIndex = 1;
            // 
            // btnStopAction
            // 
            this.btnStopAction.Location = new System.Drawing.Point(489, 135);
            this.btnStopAction.Name = "btnStopAction";
            this.btnStopAction.Size = new System.Drawing.Size(138, 23);
            this.btnStopAction.TabIndex = 9;
            this.btnStopAction.Text = "Ausführung abbrechen";
            this.btnStopAction.UseVisualStyleBackColor = true;
            this.btnStopAction.Click += new System.EventHandler(this.btnStopAction_Click);
            // 
            // pbLogo
            // 
            this.pbLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbLogo.Image")));
            this.pbLogo.InitialImage = ((System.Drawing.Image)(resources.GetObject("pbLogo.InitialImage")));
            this.pbLogo.Location = new System.Drawing.Point(969, 2);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(157, 32);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogo.TabIndex = 14;
            this.pbLogo.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tCInput);
            this.groupBox1.Location = new System.Drawing.Point(13, 147);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1113, 288);
            this.groupBox1.TabIndex = 53;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Eingabe";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // tCInput
            // 
            this.tCInput.Controls.Add(this.tabPage1);
            this.tCInput.Controls.Add(this.tabPage2);
            this.tCInput.Controls.Add(this.tabPage6);
            this.tCInput.Location = new System.Drawing.Point(7, 19);
            this.tCInput.Name = "tCInput";
            this.tCInput.SelectedIndex = 0;
            this.tCInput.Size = new System.Drawing.Size(1106, 263);
            this.tCInput.TabIndex = 32;
            this.tCInput.SelectedIndexChanged += new System.EventHandler(this.tCInput_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cbOfflineMode);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.tbTranspCostVar);
            this.tabPage1.Controls.Add(this.tbTranspCostFix);
            this.tabPage1.Controls.Add(this.cBExternalDistanceData);
            this.tabPage1.Controls.Add(this.cBCheckCountryMissingTransportConnectionsCustomer);
            this.tabPage1.Controls.Add(this.lbInvalidSaleplan);
            this.tabPage1.Controls.Add(this.btnEditBaseData);
            this.tabPage1.Controls.Add(this.tbEnde);
            this.tabPage1.Controls.Add(this.tbBeginn);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.cLBCheckResult);
            this.tabPage1.Controls.Add(this.lbBaseData);
            this.tabPage1.Controls.Add(this.btnDataLoad);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1098, 237);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Stammdaten";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cbOfflineMode
            // 
            this.cbOfflineMode.AutoSize = true;
            this.cbOfflineMode.Location = new System.Drawing.Point(719, 214);
            this.cbOfflineMode.Name = "cbOfflineMode";
            this.cbOfflineMode.Size = new System.Drawing.Size(91, 17);
            this.cbOfflineMode.TabIndex = 52;
            this.cbOfflineMode.Text = "Offline Modus";
            this.cbOfflineMode.UseVisualStyleBackColor = true;
            this.cbOfflineMode.CheckedChanged += new System.EventHandler(this.cbOfflineMode_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(544, 149);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(221, 13);
            this.label15.TabIndex = 51;
            this.label15.Text = "Variabler Anteil pro Kilometer Transportkosten";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(629, 128);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(138, 13);
            this.label12.TabIndex = 50;
            this.label12.Text = "Fixer Anteil Transportkosten";
            // 
            // tbTranspCostVar
            // 
            this.tbTranspCostVar.Location = new System.Drawing.Point(772, 147);
            this.tbTranspCostVar.Name = "tbTranspCostVar";
            this.tbTranspCostVar.Size = new System.Drawing.Size(100, 20);
            this.tbTranspCostVar.TabIndex = 49;
            this.tbTranspCostVar.TextChanged += new System.EventHandler(this.tbTranspCostVar_TextChanged);
            // 
            // tbTranspCostFix
            // 
            this.tbTranspCostFix.Location = new System.Drawing.Point(772, 123);
            this.tbTranspCostFix.Name = "tbTranspCostFix";
            this.tbTranspCostFix.Size = new System.Drawing.Size(100, 20);
            this.tbTranspCostFix.TabIndex = 48;
            this.tbTranspCostFix.TextChanged += new System.EventHandler(this.tbTranspCostFix_TextChanged);
            // 
            // cBExternalDistanceData
            // 
            this.cBExternalDistanceData.AutoSize = true;
            this.cBExternalDistanceData.Location = new System.Drawing.Point(720, 192);
            this.cBExternalDistanceData.Name = "cBExternalDistanceData";
            this.cBExternalDistanceData.Size = new System.Drawing.Size(183, 17);
            this.cBExternalDistanceData.TabIndex = 47;
            this.cBExternalDistanceData.Text = "Externe Distanzdaten verwenden";
            this.cBExternalDistanceData.UseVisualStyleBackColor = true;
            this.cBExternalDistanceData.CheckedChanged += new System.EventHandler(this.cBExternalDistanceData_CheckedChanged);
            // 
            // cBCheckCountryMissingTransportConnectionsCustomer
            // 
            this.cBCheckCountryMissingTransportConnectionsCustomer.AutoSize = true;
            this.cBCheckCountryMissingTransportConnectionsCustomer.Location = new System.Drawing.Point(720, 168);
            this.cBCheckCountryMissingTransportConnectionsCustomer.Name = "cBCheckCountryMissingTransportConnectionsCustomer";
            this.cBCheckCountryMissingTransportConnectionsCustomer.Size = new System.Drawing.Size(321, 17);
            this.cBCheckCountryMissingTransportConnectionsCustomer.TabIndex = 46;
            this.cBCheckCountryMissingTransportConnectionsCustomer.Text = "Land bei fehlenden Transportbeziehungen zum Kunden prüfen";
            this.cBCheckCountryMissingTransportConnectionsCustomer.UseVisualStyleBackColor = true;
            this.cBCheckCountryMissingTransportConnectionsCustomer.CheckedChanged += new System.EventHandler(this.cBCheckCountryMissingTransportConnectionsCustomer_CheckedChanged);
            // 
            // lbInvalidSaleplan
            // 
            this.lbInvalidSaleplan.AutoSize = true;
            this.lbInvalidSaleplan.Location = new System.Drawing.Point(142, 175);
            this.lbInvalidSaleplan.Name = "lbInvalidSaleplan";
            this.lbInvalidSaleplan.Size = new System.Drawing.Size(87, 13);
            this.lbInvalidSaleplan.TabIndex = 45;
            this.lbInvalidSaleplan.Text = "lbInvalidSaleplan";
            // 
            // btnEditBaseData
            // 
            this.btnEditBaseData.Location = new System.Drawing.Point(288, 121);
            this.btnEditBaseData.Name = "btnEditBaseData";
            this.btnEditBaseData.Size = new System.Drawing.Size(243, 23);
            this.btnEditBaseData.TabIndex = 44;
            this.btnEditBaseData.Text = "Stammdatenelement ansehen und bearbeiten";
            this.btnEditBaseData.UseVisualStyleBackColor = true;
            this.btnEditBaseData.Click += new System.EventHandler(this.btnEditBaseData_Click);
            // 
            // tbEnde
            // 
            this.tbEnde.BackColor = System.Drawing.SystemColors.Window;
            this.tbEnde.Location = new System.Drawing.Point(142, 148);
            this.tbEnde.Name = "tbEnde";
            this.tbEnde.ReadOnly = true;
            this.tbEnde.Size = new System.Drawing.Size(126, 20);
            this.tbEnde.TabIndex = 41;
            // 
            // tbBeginn
            // 
            this.tbBeginn.BackColor = System.Drawing.SystemColors.Window;
            this.tbBeginn.Location = new System.Drawing.Point(142, 122);
            this.tbBeginn.Name = "tbBeginn";
            this.tbBeginn.ReadOnly = true;
            this.tbBeginn.Size = new System.Drawing.Size(126, 20);
            this.tbBeginn.TabIndex = 40;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 151);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(118, 13);
            this.label14.TabIndex = 39;
            this.label14.Text = "Ende Planungszeitraum";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 13);
            this.label8.TabIndex = 38;
            this.label8.Text = "Beginn Planungszeitraum";
            // 
            // cLBCheckResult
            // 
            this.cLBCheckResult.ColumnWidth = 230;
            this.cLBCheckResult.FormattingEnabled = true;
            this.cLBCheckResult.Items.AddRange(new object[] {
            "Produktionsstandorte",
            "Lagercodes",
            "Historische Produktiontonnagen",
            "Arbeitspläne",
            "Schichten",
            "Maschinen",
            "Produkte",
            "Historische Transporte Nord-Ost-Europa",
            "Historische Transporte Süd-West-Europa",
            "Frachtkosten",
            "Anfangslagerbestand",
            "Zuordnung Verladeort zu Produktionsort",
            "Verkaufsplan"});
            this.cLBCheckResult.Location = new System.Drawing.Point(283, 16);
            this.cLBCheckResult.MultiColumn = true;
            this.cLBCheckResult.Name = "cLBCheckResult";
            this.cLBCheckResult.Size = new System.Drawing.Size(694, 94);
            this.cLBCheckResult.TabIndex = 37;
            this.cLBCheckResult.SelectedIndexChanged += new System.EventHandler(this.cLBCheckResult_SelectedIndexChanged);
            // 
            // lbBaseData
            // 
            this.lbBaseData.FormattingEnabled = true;
            this.lbBaseData.Location = new System.Drawing.Point(12, 16);
            this.lbBaseData.Name = "lbBaseData";
            this.lbBaseData.Size = new System.Drawing.Size(266, 95);
            this.lbBaseData.TabIndex = 36;
            this.lbBaseData.SelectedIndexChanged += new System.EventHandler(this.lbBaseData_SelectedIndexChanged);
            // 
            // btnDataLoad
            // 
            this.btnDataLoad.Location = new System.Drawing.Point(413, 170);
            this.btnDataLoad.Name = "btnDataLoad";
            this.btnDataLoad.Size = new System.Drawing.Size(203, 23);
            this.btnDataLoad.TabIndex = 35;
            this.btnDataLoad.Text = "Stammdaten laden";
            this.btnDataLoad.UseVisualStyleBackColor = true;
            this.btnDataLoad.Click += new System.EventHandler(this.btnDataLoad_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tbMaxGap);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.cbUseFixationsExclusions);
            this.tabPage2.Controls.Add(this.tbFixationThreshold);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.cbFixPlants);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.cmBBuild);
            this.tabPage2.Controls.Add(this.cbAdditionalRunWithFixations);
            this.tabPage2.Controls.Add(this.btnSelectSettingsFromPastSimulationRun);
            this.tabPage2.Controls.Add(this.btnRunOptimization);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.tbMinInvFactor);
            this.tabPage2.Controls.Add(this.tbCapFactor);
            this.tabPage2.Controls.Add(this.tbSplittingQty);
            this.tabPage2.Controls.Add(this.tbMinProdQty);
            this.tabPage2.Controls.Add(this.lbBasketUsage);
            this.tabPage2.Controls.Add(this.cbBasketUsage);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.cbIsOnlyProductionOptimization);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1098, 237);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Optimierung";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(719, 107);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(193, 13);
            this.label26.TabIndex = 59;
            this.label26.Text = "Fixierungs-Ausschlussmatrix verwenden";
            // 
            // cbUseFixationsExclusions
            // 
            this.cbUseFixationsExclusions.AutoSize = true;
            this.cbUseFixationsExclusions.Location = new System.Drawing.Point(1012, 107);
            this.cbUseFixationsExclusions.Name = "cbUseFixationsExclusions";
            this.cbUseFixationsExclusions.Size = new System.Drawing.Size(15, 14);
            this.cbUseFixationsExclusions.TabIndex = 58;
            this.cbUseFixationsExclusions.UseVisualStyleBackColor = true;
            // 
            // tbFixationThreshold
            // 
            this.tbFixationThreshold.Location = new System.Drawing.Point(565, 69);
            this.tbFixationThreshold.Name = "tbFixationThreshold";
            this.tbFixationThreshold.Size = new System.Drawing.Size(133, 20);
            this.tbFixationThreshold.TabIndex = 56;
            this.tbFixationThreshold.Text = "1";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(359, 72);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(206, 13);
            this.label25.TabIndex = 57;
            this.label25.Text = "Mindestpalettenzahl für Produkt in Periode";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(720, 73);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 13);
            this.label21.TabIndex = 55;
            this.label21.Text = "Werke fixieren";
            // 
            // cbFixPlants
            // 
            this.cbFixPlants.AutoSize = true;
            this.cbFixPlants.Location = new System.Drawing.Point(1013, 73);
            this.cbFixPlants.Name = "cbFixPlants";
            this.cbFixPlants.Size = new System.Drawing.Size(15, 14);
            this.cbFixPlants.TabIndex = 54;
            this.cbFixPlants.UseVisualStyleBackColor = true;
            this.cbFixPlants.CheckedChanged += new System.EventHandler(this.cbFixPlants_CheckedChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(16, 73);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(66, 13);
            this.label20.TabIndex = 53;
            this.label20.Text = "Ausbaustufe";
            // 
            // cmBBuild
            // 
            this.cmBBuild.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmBBuild.FormattingEnabled = true;
            this.cmBBuild.Location = new System.Drawing.Point(219, 69);
            this.cmBBuild.Name = "cmBBuild";
            this.cmBBuild.Size = new System.Drawing.Size(133, 21);
            this.cmBBuild.TabIndex = 52;
            this.cmBBuild.SelectedIndexChanged += new System.EventHandler(this.cmBBuild_SelectedIndexChanged);
            // 
            // cbAdditionalRunWithFixations
            // 
            this.cbAdditionalRunWithFixations.AutoSize = true;
            this.cbAdditionalRunWithFixations.Location = new System.Drawing.Point(518, 165);
            this.cbAdditionalRunWithFixations.Name = "cbAdditionalRunWithFixations";
            this.cbAdditionalRunWithFixations.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbAdditionalRunWithFixations.Size = new System.Drawing.Size(180, 17);
            this.cbAdditionalRunWithFixations.TabIndex = 51;
            this.cbAdditionalRunWithFixations.Text = "Zusätzlicher Lauf mit Fixierungen";
            this.cbAdditionalRunWithFixations.UseVisualStyleBackColor = true;
            // 
            // btnSelectSettingsFromPastSimulationRun
            // 
            this.btnSelectSettingsFromPastSimulationRun.Location = new System.Drawing.Point(311, 113);
            this.btnSelectSettingsFromPastSimulationRun.Name = "btnSelectSettingsFromPastSimulationRun";
            this.btnSelectSettingsFromPastSimulationRun.Size = new System.Drawing.Size(230, 23);
            this.btnSelectSettingsFromPastSimulationRun.TabIndex = 50;
            this.btnSelectSettingsFromPastSimulationRun.Text = "Einstellung aus Optimierungslauf übernehmen";
            this.btnSelectSettingsFromPastSimulationRun.UseVisualStyleBackColor = true;
            this.btnSelectSettingsFromPastSimulationRun.Click += new System.EventHandler(this.btnSelectSettingsFromPastSimulationRun_Click);
            // 
            // btnRunOptimization
            // 
            this.btnRunOptimization.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRunOptimization.Location = new System.Drawing.Point(325, 158);
            this.btnRunOptimization.Name = "btnRunOptimization";
            this.btnRunOptimization.Size = new System.Drawing.Size(186, 29);
            this.btnRunOptimization.TabIndex = 44;
            this.btnRunOptimization.Text = "Optimierung";
            this.btnRunOptimization.UseVisualStyleBackColor = true;
            this.btnRunOptimization.Click += new System.EventHandler(this.btnDataLoad_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(360, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 13);
            this.label4.TabIndex = 45;
            this.label4.Text = "Faktor für Mindestbestände";
            // 
            // tbMinInvFactor
            // 
            this.tbMinInvFactor.Location = new System.Drawing.Point(566, 9);
            this.tbMinInvFactor.Name = "tbMinInvFactor";
            this.tbMinInvFactor.Size = new System.Drawing.Size(133, 20);
            this.tbMinInvFactor.TabIndex = 44;
            this.tbMinInvFactor.Text = "1";
            this.tbMinInvFactor.TextChanged += new System.EventHandler(this.tbMinInvFactor_TextChanged);
            // 
            // tbCapFactor
            // 
            this.tbCapFactor.Location = new System.Drawing.Point(566, 40);
            this.tbCapFactor.Name = "tbCapFactor";
            this.tbCapFactor.Size = new System.Drawing.Size(133, 20);
            this.tbCapFactor.TabIndex = 37;
            this.tbCapFactor.Text = "1";
            this.tbCapFactor.TextChanged += new System.EventHandler(this.tbCapFactor_TextChanged);
            // 
            // tbSplittingQty
            // 
            this.tbSplittingQty.Location = new System.Drawing.Point(221, 38);
            this.tbSplittingQty.Name = "tbSplittingQty";
            this.tbSplittingQty.Size = new System.Drawing.Size(133, 20);
            this.tbSplittingQty.TabIndex = 34;
            this.tbSplittingQty.TextChanged += new System.EventHandler(this.tbSplittingQty_TextChanged);
            // 
            // tbMinProdQty
            // 
            this.tbMinProdQty.Location = new System.Drawing.Point(221, 9);
            this.tbMinProdQty.Name = "tbMinProdQty";
            this.tbMinProdQty.Size = new System.Drawing.Size(133, 20);
            this.tbMinProdQty.TabIndex = 31;
            this.tbMinProdQty.Text = "0";
            this.tbMinProdQty.TextChanged += new System.EventHandler(this.tbMinProdQty_TextChanged);
            // 
            // lbBasketUsage
            // 
            this.lbBasketUsage.AutoSize = true;
            this.lbBasketUsage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBasketUsage.Location = new System.Drawing.Point(719, 42);
            this.lbBasketUsage.Name = "lbBasketUsage";
            this.lbBasketUsage.Size = new System.Drawing.Size(122, 13);
            this.lbBasketUsage.TabIndex = 43;
            this.lbBasketUsage.Text = "Warenkörbe verwenden";
            // 
            // cbBasketUsage
            // 
            this.cbBasketUsage.AutoSize = true;
            this.cbBasketUsage.Location = new System.Drawing.Point(1012, 42);
            this.cbBasketUsage.Name = "cbBasketUsage";
            this.cbBasketUsage.Size = new System.Drawing.Size(15, 14);
            this.cbBasketUsage.TabIndex = 42;
            this.cbBasketUsage.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(719, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 13);
            this.label2.TabIndex = 40;
            this.label2.Text = "Nur Produktionsoptimierung";
            // 
            // cbIsOnlyProductionOptimization
            // 
            this.cbIsOnlyProductionOptimization.AutoSize = true;
            this.cbIsOnlyProductionOptimization.Location = new System.Drawing.Point(1012, 13);
            this.cbIsOnlyProductionOptimization.Name = "cbIsOnlyProductionOptimization";
            this.cbIsOnlyProductionOptimization.Size = new System.Drawing.Size(15, 14);
            this.cbIsOnlyProductionOptimization.TabIndex = 39;
            this.cbIsOnlyProductionOptimization.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(360, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "Faktor für Maschinenkapazitäten";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Mindestlosgröße (in Schichten)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(200, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Mindestlaufzeit für Splitting (in Schichten)";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBox7);
            this.tabPage6.Controls.Add(this.groupBox5);
            this.tabPage6.Controls.Add(this.groupBox3);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1098, 237);
            this.tabPage6.TabIndex = 3;
            this.tabPage6.Text = "Ergebnis";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.dGVOptimizationRuns);
            this.groupBox7.Location = new System.Drawing.Point(514, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(485, 95);
            this.groupBox7.TabIndex = 50;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Durchgeführte Simulationen";
            // 
            // dGVOptimizationRuns
            // 
            this.dGVOptimizationRuns.AllowUserToAddRows = false;
            this.dGVOptimizationRuns.AllowUserToDeleteRows = false;
            this.dGVOptimizationRuns.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dGVOptimizationRuns.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dGVOptimizationRuns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVOptimizationRuns.Location = new System.Drawing.Point(6, 19);
            this.dGVOptimizationRuns.MultiSelect = false;
            this.dGVOptimizationRuns.Name = "dGVOptimizationRuns";
            this.dGVOptimizationRuns.ReadOnly = true;
            this.dGVOptimizationRuns.Size = new System.Drawing.Size(466, 70);
            this.dGVOptimizationRuns.TabIndex = 46;
            this.dGVOptimizationRuns.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dGVOptimizationRuns_CellContentClick_1);
            this.dGVOptimizationRuns.SelectionChanged += new System.EventHandler(this.dGVOptimizationRuns_SelectionChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnDataFolderOpen);
            this.groupBox5.Controls.Add(this.btnResultVisualization);
            this.groupBox5.Controls.Add(this.btnInputOpen);
            this.groupBox5.Controls.Add(this.btnResultsOpen);
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(481, 89);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Datenanzeige";
            // 
            // btnDataFolderOpen
            // 
            this.btnDataFolderOpen.Location = new System.Drawing.Point(242, 60);
            this.btnDataFolderOpen.Name = "btnDataFolderOpen";
            this.btnDataFolderOpen.Size = new System.Drawing.Size(226, 23);
            this.btnDataFolderOpen.TabIndex = 20;
            this.btnDataFolderOpen.Text = "Datenordner öffnen";
            this.btnDataFolderOpen.UseVisualStyleBackColor = true;
            this.btnDataFolderOpen.Click += new System.EventHandler(this.folderOpen);
            // 
            // btnResultVisualization
            // 
            this.btnResultVisualization.Location = new System.Drawing.Point(7, 60);
            this.btnResultVisualization.Name = "btnResultVisualization";
            this.btnResultVisualization.Size = new System.Drawing.Size(228, 23);
            this.btnResultVisualization.TabIndex = 19;
            this.btnResultVisualization.Text = "Ergebnis-Visualisierung";
            this.btnResultVisualization.UseVisualStyleBackColor = true;
            this.btnResultVisualization.Click += new System.EventHandler(this.btnResultVisualization_Click);
            // 
            // btnInputOpen
            // 
            this.btnInputOpen.Location = new System.Drawing.Point(7, 19);
            this.btnInputOpen.Name = "btnInputOpen";
            this.btnInputOpen.Size = new System.Drawing.Size(228, 23);
            this.btnInputOpen.TabIndex = 16;
            this.btnInputOpen.Text = "Eingabedaten öffnen";
            this.btnInputOpen.UseVisualStyleBackColor = true;
            this.btnInputOpen.Click += new System.EventHandler(this.folderOpen);
            // 
            // btnResultsOpen
            // 
            this.btnResultsOpen.Location = new System.Drawing.Point(241, 19);
            this.btnResultsOpen.Name = "btnResultsOpen";
            this.btnResultsOpen.Size = new System.Drawing.Size(228, 23);
            this.btnResultsOpen.TabIndex = 17;
            this.btnResultsOpen.Text = "Ergebnisordner öffnen";
            this.btnResultsOpen.UseVisualStyleBackColor = true;
            this.btnResultsOpen.Click += new System.EventHandler(this.folderOpen);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dGVOptResults);
            this.groupBox3.Location = new System.Drawing.Point(6, 108);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1089, 126);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ergebnisse";
            // 
            // dGVOptResults
            // 
            this.dGVOptResults.AllowUserToAddRows = false;
            this.dGVOptResults.AllowUserToDeleteRows = false;
            this.dGVOptResults.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dGVOptResults.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dGVOptResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVOptResults.Location = new System.Drawing.Point(7, 19);
            this.dGVOptResults.Name = "dGVOptResults";
            this.dGVOptResults.ReadOnly = true;
            this.dGVOptResults.Size = new System.Drawing.Size(1076, 101);
            this.dGVOptResults.TabIndex = 11;
            this.dGVOptResults.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dGVOptResults_CellContentClick);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lbUsedBaseDataFolder);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.cbDataLoaded);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.lbExistingFolders);
            this.groupBox4.Controls.Add(this.tbOptimizationName);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Location = new System.Drawing.Point(13, 41);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1112, 100);
            this.groupBox4.TabIndex = 54;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Optimierungsakte";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // lbUsedBaseDataFolder
            // 
            this.lbUsedBaseDataFolder.AutoSize = true;
            this.lbUsedBaseDataFolder.Location = new System.Drawing.Point(429, 46);
            this.lbUsedBaseDataFolder.Name = "lbUsedBaseDataFolder";
            this.lbUsedBaseDataFolder.Size = new System.Drawing.Size(14, 13);
            this.lbUsedBaseDataFolder.TabIndex = 57;
            this.lbUsedBaseDataFolder.Text = "S";
            this.lbUsedBaseDataFolder.Click += new System.EventHandler(this.lbUsedBaseDataFolder_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(333, 46);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(99, 13);
            this.label16.TabIndex = 56;
            this.label16.Text = "Stammdatenordner:";
            // 
            // cbDataLoaded
            // 
            this.cbDataLoaded.AutoCheck = false;
            this.cbDataLoaded.AutoSize = true;
            this.cbDataLoaded.Location = new System.Drawing.Point(231, 45);
            this.cbDataLoaded.Name = "cbDataLoaded";
            this.cbDataLoaded.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbDataLoaded.Size = new System.Drawing.Size(96, 17);
            this.cbDataLoaded.TabIndex = 55;
            this.cbDataLoaded.Text = "Daten geladen";
            this.cbDataLoaded.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label9.Location = new System.Drawing.Point(550, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(207, 17);
            this.label9.TabIndex = 54;
            this.label9.Text = "Bestehende Optimierungsakten";
            // 
            // lbExistingFolders
            // 
            this.lbExistingFolders.FormattingEnabled = true;
            this.lbExistingFolders.Location = new System.Drawing.Point(759, 18);
            this.lbExistingFolders.Name = "lbExistingFolders";
            this.lbExistingFolders.Size = new System.Drawing.Size(228, 69);
            this.lbExistingFolders.TabIndex = 53;
            this.lbExistingFolders.SelectedIndexChanged += new System.EventHandler(this.lbExistingFolders_SelectedIndexChanged);
            // 
            // tbOptimizationName
            // 
            this.tbOptimizationName.Location = new System.Drawing.Point(224, 15);
            this.tbOptimizationName.Name = "tbOptimizationName";
            this.tbOptimizationName.Size = new System.Drawing.Size(313, 20);
            this.tbOptimizationName.TabIndex = 52;
            this.tbOptimizationName.TextChanged += new System.EventHandler(this.tbOptimizationName_TextChanged_1);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label10.Location = new System.Drawing.Point(18, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(173, 17);
            this.label10.TabIndex = 51;
            this.label10.Text = "Aktuelle Optimierungsakte";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(337, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 16);
            this.label13.TabIndex = 55;
            this.label13.Text = "Benutzer:";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(415, 19);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(39, 16);
            this.lblUserName.TabIndex = 56;
            this.lblUserName.Text = "Test";
            // 
            // lbCulture
            // 
            this.lbCulture.AutoSize = true;
            this.lbCulture.Location = new System.Drawing.Point(767, 19);
            this.lbCulture.Name = "lbCulture";
            this.lbCulture.Size = new System.Drawing.Size(48, 13);
            this.lbCulture.TabIndex = 57;
            this.lbCulture.Text = "lbCulture";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(677, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 58;
            this.label7.Text = "Ländereinstellung:";
            // 
            // tbMaxGap
            // 
            this.tbMaxGap.Location = new System.Drawing.Point(683, 187);
            this.tbMaxGap.Name = "tbMaxGap";
            this.tbMaxGap.Size = new System.Drawing.Size(60, 20);
            this.tbMaxGap.TabIndex = 61;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(517, 190);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(105, 13);
            this.label17.TabIndex = 60;
            this.label17.Text = "Maximaler Gap (in %)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1134, 612);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lbCulture);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.gpExecution);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pbLogo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Netzwerkoptimierung 1.1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gpExecution.ResumeLayout(false);
            this.tCProgress.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tCInput.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dGVOptimizationRuns)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dGVOptResults)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog browseFolder;
        private System.Windows.Forms.GroupBox gpExecution;
        private System.Windows.Forms.Button btnStopAction;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tCProgress;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox tbLogOutput;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ProgressBar pgbLoading;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox lbExistingFolders;
        private System.Windows.Forms.TextBox tbOptimizationName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.CheckBox cbDataLoaded;
        private System.Windows.Forms.Label lbUsedBaseDataFolder;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbOptimizationRunning;
        private System.Windows.Forms.Label lbCulture;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabControl tCInput;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lbInvalidSaleplan;
        private System.Windows.Forms.Button btnEditBaseData;
        private System.Windows.Forms.TextBox tbEnde;
        private System.Windows.Forms.TextBox tbBeginn;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckedListBox cLBCheckResult;
        private System.Windows.Forms.ListBox lbBaseData;
        private System.Windows.Forms.Button btnDataLoad;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnRunOptimization;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbMinInvFactor;
        private System.Windows.Forms.TextBox tbCapFactor;
        private System.Windows.Forms.TextBox tbSplittingQty;
        private System.Windows.Forms.TextBox tbMinProdQty;
        private System.Windows.Forms.CheckBox cbBasketUsage;
        private System.Windows.Forms.CheckBox cbIsOnlyProductionOptimization;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView dGVOptimizationRuns;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnInputOpen;
        private System.Windows.Forms.Button btnResultsOpen;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dGVOptResults;
        private System.Windows.Forms.Button btnSelectSettingsFromPastSimulationRun;
        private System.Windows.Forms.Label lbBasketUsage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cbAdditionalRunWithFixations;
        private System.Windows.Forms.Button btnResultVisualization;
        private System.Windows.Forms.CheckBox cBCheckCountryMissingTransportConnectionsCustomer;
        private System.Windows.Forms.CheckBox cBExternalDistanceData;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cmBBuild;
        private System.Windows.Forms.TextBox tbFixationThreshold;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox cbUseFixationsExclusions;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox cbFixPlants;
        private System.Windows.Forms.Button btnDataFolderOpen;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbTranspCostVar;
        private System.Windows.Forms.TextBox tbTranspCostFix;
        private System.Windows.Forms.CheckBox cbOfflineMode;
        private System.Windows.Forms.TextBox tbMaxGap;
        private System.Windows.Forms.Label label17;
    }
}

