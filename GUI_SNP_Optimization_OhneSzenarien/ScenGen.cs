﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

using Optimizer_Data;
using Logging;
using Optimizer_Exceptions;
using SNP_Optimizer;
using SNP_WEPA;

namespace GUI_SNP_Optimization
{
  
 //protected SQLLiteDataBase usedInMemDB;
    public partial class ScenGen : Form
    {
        public class Scenario
        {
            public enum ScenType
            {
                LOCATIONS=0,
                TOTAL=1
            }
            public string Name;

            public ScenType Type;

            public string BaseDataFolder;

            public Int32 NumberOfReplications;

            public bool IsImported=false;

            public string Key {
                get {return Name+BaseDataFolder;}
            }
        }

        

        protected class DemandEntry
        {
            public string period;
            public double value;
        }

        protected class LocationEntry
        {
            public string location;
            public double prob;
        }

        protected class TotalDemandEntry
        {
            public string CustomerChain;
            public string Product;
            public List<DemandEntry> demands;
            public string Key
            {
                get {return CustomerChain +Product; }
            }
            public TotalDemandEntry()
            {
                demands = new List<DemandEntry>();
            }
        }

        protected class LocationDistributionEntry
        {
            public string CustomerChain;
            public string Product;
            public List<LocationEntry> locProbs;

            public string Key
            {
                get { return CustomerChain + Product; }
            }

            public LocationDistributionEntry()
            {
                locProbs = new List<LocationEntry>();
            }
        }

        protected Distribution usedGlobalDemandDeviationDistribution;

        public Distribution GlobalDemandDeviationDistribution 
        {
            set { usedGlobalDemandDeviationDistribution = value; }
            get { return usedGlobalDemandDeviationDistribution; }
        }

        Dictionary<string, TotalDemandEntry> totalDemands;
        Dictionary<string, LocationDistributionEntry> locDistributions;

        //protected SQLLiteDataBase usedInMemDB;
        protected string loadedDataPath;

        protected bool isReloadData;

        protected string[] scenarioTypes = {"Standorte Kundenkette","Gesamtnachfrage" };
        protected string[] distributionTypes = {"Normalverteilung","Gleichverteilung" };

        protected string productTabName = "IN_PRODUCT";
       
        protected string customerTabName = "IN_CUSTOMER";
        
        protected string periodTabName = "IN_PERIOD";
        
        protected string demandTabName = "IN_DEMAND";
        
        protected string aggrCustChainSqlCmd = "insert into IN_CUSTOMERCHAIN_DEMAND " +
                                                "select CustomerChain," +
                                                "ProductIndex," +
                                                "PeriodIndex," +
                                                "Sum(DemandQuantity) as ChainDemandQuantity," +
                                                "ScenarioIndex " +
                                                "from IN_DEMAND demTab " +
                                                "join IN_CUSTOMER custTab on demTab.CustomerIndex=custTab.CustomerIndex " +
                                                "where ScenarioIndex=0 " +
                                                "group by CustomerChain, ProductIndex,PeriodIndex,ScenarioIndex " +
                                                "order by CustomerChain, ProductIndex,PeriodIndex ";

        protected string fractionsSqlCmd = "insert into IN_DEMAND_FRACTIONS " +
                                           "Select customerTotalDemTab.CustomerIndex," +
                                           "custTab.CustomerChain," +
                                           "customerTotalDemTab.ProductIndex," +
                                           "TotalCustomerDemandQuantity/(case when TotalChainDemandQuantity=0 then 1 else TotalChainDemandQuantity end) as CustomerDemandFraction " +
                                           "from (" +
                                                        "Select CustomerIndex," +
                                                               "ProductIndex," +
                                                               "Sum(DemandQuantity) TotalCustomerDemandQuantity " +
                                                               "from IN_DEMAND demTab " +
                                                               "where demTab.ScenarioIndex=0 " +
                                                               "group by CustomerIndex,ProductIndex " +
                                                ") customerTotalDemTab " +
                                          "join IN_CUSTOMER custTab on customerTotalDemTab.CustomerIndex=custTab.CustomerIndex " +
                                          "join (" +
                                                                "select CustomerChain," +
                                                                       "ProductIndex," +
                                                                       "Sum(DemandQuantity) as TotalChainDemandQuantity " +
                                                                        "from IN_DEMAND demTab " +
                                                                       "join IN_CUSTOMER custTab on demTab.CustomerIndex=custTab.CustomerIndex " +
                                                                       "where ScenarioIndex=0 " +
                                                                        "group by CustomerChain, ProductIndex " +
                                              ")" +
                                         "customerChainTotalDemTab on custTab.CustomerChain=customerChainTotalDemTab.CustomerChain and customerTotalDemTab.ProductIndex=customerChainTotalDemTab.ProductIndex";

        protected string customerChainDemandTabName = "IN_CUSTOMERCHAIN_DEMAND";
       
        protected string demandFractionsTabName = "IN_DEMAND_FRACTIONS";
       
        protected static string customerfileName = "customers.csv";
        protected static string productfileName = "products.csv";
        protected static string periodfileName = "periods.csv";
        protected static string transportplantfileName = "transportsplant.csv";
        protected static string transportcustomerfileName = "transportscustomer.csv";
        protected static string demandScenFileNamePattern = "demands_{0}_{1}_{2}.csv";
        protected static string rawDemandFileName = "demands.csv";
        protected static string scenfileName = "scenarios.csv";

        

        protected string CSV_SEMCOL_DELIMITER = ";";
        protected string CSV_SPACE_DELIMITER = " ";

        protected string[,] productTabCSVSpec = { 
                                               {"ProductName",CSV_File.STRING_TYPE,""},
                                               {"ProductIndex",CSV_File.INT_TYPE,""}
                                               };

        protected string[,] customersTabCSVSpec = { 
                                               {"CustomerName",CSV_File.STRING_TYPE,""},
                                               {"CustomerIndex",CSV_File.INT_TYPE,""},
                                               {"Country",CSV_File .STRING_TYPE ,""},
                                               {"PostalCode",CSV_File .STRING_TYPE ,""}
                                               };

        protected string[,] demandTabCSVSpec = { 
                                               {"CustomerIndex",CSV_File.INT_TYPE,""},
                                               {"ProductIndex",CSV_File.INT_TYPE,""},
                                               {"PeriodIndex",CSV_File.INT_TYPE,""},
                                               {"DemandQty",CSV_File.DOUBLE_TYPE,""},
                                               {"ScenarioIndex",CSV_File.INT_TYPE,""}
                                               };

        protected string[,] demandTabOutputCSVSpec = { 
                                               {"CustomerIndex",CSV_File.INT_TYPE,""},
                                               {"ProductIndex",CSV_File.INT_TYPE,""},
                                               {"PeriodIndex",CSV_File.INT_TYPE,""},
                                               {"DemandQty",CSV_File.DOUBLE_TYPE,""}
                                               };

        protected string[,] periodsTabCSVSpec = { 
                                               {"PeriodName",CSV_File.STRING_TYPE,""},
                                               {"PeriodIndex",CSV_File.INT_TYPE,""}
                                               };

        protected string[,] scenariosTabCSVSpec = { 
                                               {"ScenarioName",CSV_File.STRING_TYPE,""},
                                               {"ScenarioType",CSV_File.INT_TYPE,""},
                                               {"BaseDataFolder",CSV_File.STRING_TYPE,""},
                                               {"NumberOfReplications",CSV_File .INT_TYPE,""}
                                               };

        protected Dictionary<string,Scenario> currScens;

        protected string locDistrSQLCmd = "Select CustomerChain,ProductIndex,CustomerIndex,CustomerDemandFraction from IN_DEMAND_FRACTIONS order by CustomerIndex,ProductIndex,CustomerIndex"; //CustomerChain,
        protected string totalDemSQLCmd = "select CustomerChain,ProductIndex,PeriodIndex,DemandQuantity from IN_CUSTOMERCHAIN_DEMAND order by ProductIndex,PeriodIndex";//CustomerChain,

        protected Random usedRG;

        public enum DistributionType
        {
            NORMAL=0,
            UNIFORM=1
        }

        public ScenGen()
        {
            InitializeComponent();
            currScens = new Dictionary<string, Scenario>();
            totalDemands = new Dictionary<string, TotalDemandEntry>();
            locDistributions = new Dictionary<string, LocationDistributionEntry>();
            usedRG = new Random();
        }

        //public ScenGen(SQLLiteDataBase uDB)
        //    : this()
        //{
        //    usedInMemDB = uDB;
        //}
        public void generateScenarios( string lDP, bool iRD)
        {
            loadedDataPath = lDP + Path.DirectorySeparatorChar;
            isReloadData = iRD;
            loadDLProperties();
            if (isReloadData)
                importLoadedDemandData();
            loadScenGenBaseData();
            loadScens();
            //this.ShowDialog();
        }
        public void setDistribution(DistributionType dT,double stdDev,double mean)
        {
            NormalDistribution currNDT;
            UniformDistribution currUDT;
            double currHWDTH;
            switch (dT)
            {
                case DistributionType.NORMAL:
                     currNDT=new NormalDistribution ();
                    currNDT.Mean =mean;
                    currNDT .STDev =stdDev ;
                    usedGlobalDemandDeviationDistribution =currNDT ;
                    break;
                case DistributionType .UNIFORM :
                    currUDT=new UniformDistribution ();
                    currHWDTH=Math.Sqrt (3)*stdDev ;
                    currUDT .LBound =mean-currHWDTH ;
                    currUDT .UBound =mean+currHWDTH ;
                    usedGlobalDemandDeviationDistribution =currUDT;
                    break;
                default:
                    usedGlobalDemandDeviationDistribution =null;
                    break;
            }
        }

        public void loadTotalDemands()
        {
            DataTable tDTab;
            string currKey;
            string currProd,currCustChain,currPeriod;

           // tDTab = usedInMemDB.getData(totalDemSQLCmd);
            TotalDemandEntry tDE;
            DemandEntry currDE;

            totalDemands.Clear();
            //foreach (DataRow r in tDTab.Rows)
            //{
            //    currPeriod =Convert.ToString ((Int32) r[2],CultureInfo .InvariantCulture );
            //    currCustChain = (string)r[0];
            //    currProd = Convert.ToString((Int32)r[1], CultureInfo.InvariantCulture);
                
            //    currKey=currCustChain +currProd;
            //    if (totalDemands.ContainsKey(currKey))
            //    {
            //        tDE = totalDemands[currKey];
            //    }
            //    else
            //    {
            //        tDE = new TotalDemandEntry();
            //        tDE.CustomerChain = currCustChain;
            //        tDE.Product = currProd;
            //        totalDemands.Add(tDE.Key, tDE);
            //    }

            //    currDE = new DemandEntry();
            //    currDE.period = currPeriod;
            //    currDE.value = (double)r[3];
            //    tDE.demands.Add(currDE);
            //}
        }

        public void loadLocFractions()
        {
            DataTable tDTab;
            string currKey;
            string currProd, currCustChain, currLocation;

            //tDTab = usedInMemDB.getData(locDistrSQLCmd );
            LocationDistributionEntry  lDE;
            LocationEntry currLE;
            locDistributions.Clear();
            //foreach (DataRow r in tDTab.Rows)
            //{
            //    currLocation = Convert.ToString ((Int32)r[2],CultureInfo .InvariantCulture );//(string)r[2];
            //    currCustChain = (string)r[0];
            //    currProd = Convert.ToString((Int32)r[1], CultureInfo.InvariantCulture);

            //    currKey = currCustChain + currProd;

            //    if (locDistributions.ContainsKey(currKey))
            //    {
            //        lDE = locDistributions[currKey];
            //    }
            //    else
            //    {
            //        lDE = new LocationDistributionEntry();
            //        lDE.CustomerChain = currCustChain;
            //        lDE.Product = currProd;
            //        locDistributions.Add(lDE.Key, lDE);
            //    }

            //    currLE = new LocationEntry();
            //    currLE.location  = currLocation;
            //    currLE.prob  = (double)r[3];
            //    lDE.locProbs.Add(currLE);
            //}
        }

        public void loadScenGenBaseData()
        {
            loadTotalDemands();
            loadLocFractions();
        }

        //DataTable locDistribution;
        //DataTable totalDemands;
        public void loadScens()
        {
            string fSFN;
            Scenario currScen;
            DataTable scenData;
            CSV_File scenFile;
            currScens.Clear();
            fSFN=loadedDataPath + scenfileName;
            if (File.Exists(fSFN))
            {
                scenFile = new CSV_File(fSFN);
                scenFile.setInputColSpecsByStrings(scenariosTabCSVSpec);
                scenFile.CSVDelimiter = CSV_SEMCOL_DELIMITER;
                scenFile.openForReading();
                scenFile.IsHeadLine = true;
                scenData = scenFile.getData();
                scenFile.closeDoc();
                currScens.Clear();

                foreach (DataRow r in scenData.Rows)
                {
                    currScen = new Scenario();
                    currScen.Name = (string) r[scenariosTabCSVSpec[0,0]];
                    currScen.Type = (Scenario.ScenType)r[scenariosTabCSVSpec[1, 0]];
                    currScen.BaseDataFolder = (string)r[scenariosTabCSVSpec[2, 0]];
                    currScen.NumberOfReplications = (Int32)r[scenariosTabCSVSpec[3, 0]];
                    currScens.Add(currScen.Key,currScen);
                }
            }
        }

        //public void visualizeScens()
        //{
        //    DataTable vScenTab;
        //    int i;
        //    string[] currLine;
        //    vScenTab = new DataTable();
        //    currLine = new string[2];

        //    vScenTab.Columns.Add("Name");
        //    vScenTab.Columns.Add("Typ");

        //    foreach (KeyValuePair <string,Scenario> sKVP in currScens)
        //    {
        //        currLine[0]=sKVP.Value .Name ;
        //        currLine[1]=scenarioTypes [(Int32) sKVP.Value.Type ];
        //        vScenTab.Rows.Add(currLine);
        //    }

        //    dGVScens.DataSource = vScenTab;
        //}


        public Scenario getScenario(String scenName)
        {   
            return currScens[scenName+baseDataFolderName ];
        }

        public void saveScens()
        {
            string fSFN;
            DataRow currRow;
            DataTable scenData;
            CSV_File scenFile;
            fSFN = loadedDataPath + scenfileName;

            scenFile = new CSV_File(fSFN);

            scenFile.setInputColSpecsByStrings(scenariosTabCSVSpec);
            scenFile.setOutputColSpecsByStrings(scenariosTabCSVSpec);
            scenData = scenFile.getEmptyTableFromOutputColSpecs();

            foreach (KeyValuePair<string,Scenario> currScenKV in currScens)
            {
                currRow = scenData.NewRow();
                currRow.BeginEdit();
                currRow[scenariosTabCSVSpec[0, 0]] = currScenKV.Value.Name;
                currRow[scenariosTabCSVSpec[1, 0]] = currScenKV.Value.Type;
                currRow[scenariosTabCSVSpec[2, 0]] = currScenKV.Value.BaseDataFolder;
                currRow[scenariosTabCSVSpec[3, 0]] = currScenKV.Value.NumberOfReplications;
                currRow.EndEdit();
                scenData.Rows.Add(currRow);
            }
            scenFile.CSVDelimiter = CSV_SEMCOL_DELIMITER;
            scenFile.IsHeadLine = true;
            scenFile.openForWriting(false);
            scenFile.writeData(scenData);
            scenFile.closeDoc();
        }

        protected string dataLoadPropertiesFileName = "dataLoadProperties.csv";
        protected string baseDataFolderName;
        private void loadDLProperties()
        {  
            CSV_File dlPropertiesFile;

            DataTable dlProperties;

            dlPropertiesFile = new CSV_File(loadedDataPath + dataLoadPropertiesFileName);
            dlPropertiesFile.CSVDelimiter = CSV_SEMCOL_DELIMITER;
            dlPropertiesFile.openForReading();
            dlPropertiesFile.IsHeadLine = true;

            dlProperties = dlPropertiesFile.getData();
            baseDataFolderName = (string) dlProperties.Rows[0][0];
            
            dlPropertiesFile.closeDoc();
        }
        public string getScenDemandFileName(string scenName,Int32 replication)
        {
            return String.Format(demandScenFileNamePattern, scenName, baseDataFolderName,Convert.ToString (replication,CultureInfo .InvariantCulture));
        }
        protected double usedDemandQuantumSize = 1;

        public void makeScenario(string name,Scenario .ScenType scenType,Int32 numberOfReplications)
        {
            int i;
            Scenario newScen;
            DataTable demScenarioData;
            DataRow currDemRow;
            CSV_File demScenFile;
            string demScenFFN;
            LocationDistributionEntry lDE;
            double currRV;
            double currTotalDemand;
            double currCumProb;
            double demandQuantum;
            double [] locDemands;
            Int32 currentReplication;

            if (!currScens.ContainsKey(name + baseDataFolderName))
            {   
                newScen = new Scenario();
                newScen.Name = name;
                newScen.Type = scenType;//(Scenario.ScenType)i;
                newScen.BaseDataFolder = baseDataFolderName;
                newScen.NumberOfReplications = numberOfReplications;
                currScens.Add(newScen.Key,newScen);

                //Generate Data
                //scenFolderPath = loadedDataPath;
                //if (!Directory.Exists(scenFolderPath))
                //{
                //    Directory.CreateDirectory(scenFolderPath);
                //}
                for (currentReplication = 0; currentReplication < newScen.NumberOfReplications; currentReplication++)
                {
                    demScenFFN = loadedDataPath + Path.DirectorySeparatorChar + getScenDemandFileName(name,currentReplication);
                    demScenFile = new CSV_File(demScenFFN);
                    demScenFile.setInputColSpecsByStrings(demandTabCSVSpec);
                    demScenFile.setOutputColSpecsByStrings(demandTabOutputCSVSpec);
                    demScenarioData = demScenFile.getEmptyTableFromOutputColSpecs();

                    if (newScen.Type == Scenario.ScenType.LOCATIONS)
                    {
                        foreach (KeyValuePair<string, TotalDemandEntry> tDEKV in totalDemands)
                        {
                            foreach (DemandEntry dE in tDEKV.Value.demands)
                            {
                                lDE = locDistributions[tDEKV.Key];
                                //Zufallszahl ziehen und zuweisen

                                /*
                                 protected string[,] demandTabCSVSpec = { 
                                                   {"CustomerIndex",CSV_File.INT_TYPE,""},
                                                   {"ProductIndex",CSV_File.INT_TYPE,""},
                                                   {"PeriodIndex",CSV_File.INT_TYPE,""},
                                                   {"DemandQty",CSV_File.DOUBLE_TYPE,""},
                                                   {"ScenarioIndex",CSV_File.INT_TYPE,""}
                                                   };
                                */
                                currTotalDemand = dE.value;
                                locDemands = new double[lDE.locProbs.Count];
                                for (i = 0; i < locDemands.Length; i++)
                                    locDemands[i] = 0;
                                while (currTotalDemand > 0)
                                {
                                    if (currTotalDemand > usedDemandQuantumSize)
                                        demandQuantum = usedDemandQuantumSize;
                                    else
                                        demandQuantum = currTotalDemand;
                                    currRV = usedRG.NextDouble();
                                    currCumProb = 0;


                                    i = 0;
                                    foreach (LocationEntry lE in lDE.locProbs)
                                    {
                                        currCumProb += lE.prob;
                                        if (currRV <= currCumProb)
                                        {
                                            locDemands[i] += demandQuantum;
                                            break;
                                        }
                                        i++;
                                    }

                                    currTotalDemand -= demandQuantum;
                                }

                                i = 0;
                                foreach (LocationEntry lE in lDE.locProbs)
                                {
                                    if (locDemands[i] > 0)
                                    {
                                        currDemRow = demScenarioData.NewRow();
                                        currDemRow.BeginEdit();
                                        currDemRow[demandTabCSVSpec[0, 0]] = lE.location;
                                        currDemRow[demandTabCSVSpec[1, 0]] = tDEKV.Value.Product;
                                        currDemRow[demandTabCSVSpec[2, 0]] = dE.period;
                                        currDemRow[demandTabCSVSpec[3, 0]] = Math.Ceiling(locDemands[i]);
                                        currDemRow.EndEdit();
                                        demScenarioData.Rows.Add(currDemRow);
                                    }
                                    i++;
                                }

                            }
                        }
                    }
                    else if (newScen.Type == Scenario.ScenType.TOTAL)
                    {
                        foreach (KeyValuePair<string, TotalDemandEntry> tDEKV in totalDemands)
                        {
                            foreach (DemandEntry dE in tDEKV.Value.demands)
                            {
                                lDE = locDistributions[tDEKV.Key];
                                double rV;

                                rV = usedGlobalDemandDeviationDistribution.getNext();
                                if (rV < 0)
                                    rV = 0;
                                //Zufallszahl ziehen und Gesamtbedarf ableiten
                                currTotalDemand = dE.value * rV;
                                if (currTotalDemand > 0)
                                {
                                    foreach (LocationEntry lE in lDE.locProbs)
                                    {
                                        currDemRow = demScenarioData.NewRow();
                                        currDemRow.BeginEdit();
                                        currDemRow[demandTabCSVSpec[0, 0]] = lE.location;
                                        currDemRow[demandTabCSVSpec[1, 0]] = tDEKV.Value.Product;
                                        currDemRow[demandTabCSVSpec[2, 0]] = dE.period;
                                        currDemRow[demandTabCSVSpec[3, 0]] = Math.Ceiling(currTotalDemand * lE.prob);
                                        currDemRow.EndEdit();
                                        demScenarioData.Rows.Add(currDemRow);
                                    }
                                }
                            }
                        }
                    }
                    demScenFile.OutputCultureForConversions = CultureInfo.InvariantCulture;
                    demScenFile.InputCultureForConversions = CultureInfo.InvariantCulture;
                    demScenFile.CSVDelimiter = CSV_SPACE_DELIMITER;
                    demScenFile.IsHeadLine = false;
                    demScenFile.openForWriting(true);

                    demScenFile.writeData(demScenarioData);
                    demScenFile.closeDoc();
                }
                saveScens();
            }
            else
            {
                //Fehler
            }
        }

        public void clearTable(string tableName)
        {
            string sqlCmdStr = "delete from " + tableName;
            //usedInMemDB.execSqlCommand(sqlCmdStr);
        }

        public void clearAllDemandTables()
        {
            clearTable(customerTabName);
            clearTable(productTabName);
            clearTable(periodTabName);
            clearTable(demandTabName);
            clearTable(customerChainDemandTabName);
            clearTable(demandFractionsTabName);
        }

        private void importLoadedDemandData()
        {
            CSV_File productFile, customersFile, demandFile, periodsFile;
            DataTable productsTable, customersTable, demandTable, periodsTable;
            DataRow currDataRow;
            int i;
            string[] customerNameComponents;

            clearAllDemandTables();

            productFile = new CSV_File(loadedDataPath + productfileName);
            productFile.openForReading();
            productFile.IsHeadLine = false;
            productFile.CSVDelimiter = CSV_SEMCOL_DELIMITER;
            productFile.InputCultureForConversions = CultureInfo.InvariantCulture;
            productFile.setInputColSpecsByStrings(productTabCSVSpec);
            productsTable = productFile.getData();
            productFile.closeDoc();

            customersFile = new CSV_File(loadedDataPath + customerfileName);
            customersFile.openForReading();
            customersFile.IsHeadLine = false;
            customersFile.CSVDelimiter = CSV_SEMCOL_DELIMITER;
            customersFile.InputCultureForConversions = CultureInfo.InvariantCulture;
            customersFile.setInputColSpecsByStrings(customersTabCSVSpec);
            customersTable = customersFile.getData();
            customersFile.closeDoc();

            periodsFile = new CSV_File(loadedDataPath + periodfileName);
            periodsFile.openForReading();
            periodsFile.IsHeadLine = false;
            periodsFile.CSVDelimiter = CSV_SEMCOL_DELIMITER;
            periodsFile.InputCultureForConversions = CultureInfo.InvariantCulture;
            periodsFile.setInputColSpecsByStrings(periodsTabCSVSpec);
            periodsTable = periodsFile.getData();
            periodsFile.closeDoc();

            for (i = 0; i < customersTable.Rows.Count; i++)
            {
                currDataRow = customersTable.Rows[i];
                currDataRow.BeginEdit();
                customerNameComponents = ((string)currDataRow[0]).Split(SNP_DataProvider.KEY_COMPONENT_SEPARATOR[0]);
                currDataRow[0] = customerNameComponents[0];
                if (customerNameComponents.Length > 1)
                {
                    currDataRow[2] = customerNameComponents[1];
                    currDataRow[3] = customerNameComponents[2];
                }
                else
                {//For compatibility to old names without separators
                    currDataRow[2] = "";
                    currDataRow[3] = "";
                }
                currDataRow.EndEdit();
            }

            demandFile = new CSV_File(loadedDataPath + rawDemandFileName);
            demandFile.openForReading();
            demandFile.IsHeadLine = false;
            demandFile.CSVDelimiter = CSV_SPACE_DELIMITER;
            demandFile.setInputColSpecsByStrings(demandTabCSVSpec);
            demandFile.InputCultureForConversions = CultureInfo.InvariantCulture;
            demandTable = demandFile.getData();
            demandFile.closeDoc();

            for (i = 0; i < productsTable.Rows.Count; i++)
            {
                currDataRow = productsTable.Rows[i];
                currDataRow.BeginEdit();
                currDataRow[1] = i;
                currDataRow.EndEdit();
            }

            for (i = 0; i < customersTable.Rows.Count; i++)
            {
                currDataRow = customersTable.Rows[i];
                currDataRow.BeginEdit();
                currDataRow[1] = i;
                currDataRow.EndEdit();
            }

            for (i = 0; i < periodsTable.Rows.Count; i++)
            {
                currDataRow = periodsTable.Rows[i];
                currDataRow.BeginEdit();
                currDataRow[1] = i;
                currDataRow.EndEdit();
            }

            for (i = 0; i < demandTable.Rows.Count; i++)
            {
                currDataRow = demandTable.Rows[i];
                currDataRow.BeginEdit();
                currDataRow[4] = 0;
                currDataRow.EndEdit();
            }
            //usedInMemDB.writeData(productsTable, productTabName, true);
            //usedInMemDB.writeData(customersTable, customerTabName, true);
            //usedInMemDB.writeData(periodsTable, periodTabName, true);
            //usedInMemDB.writeData(demandTable, demandTabName, true);
            //usedInMemDB.execSqlCommand(aggrCustChainSqlCmd);
            //usedInMemDB.execSqlCommand(fractionsSqlCmd);
            // setScenSelection(null);
        }
        /*
        private void loadScens()
        {

        }*/
        
        private void ScenGen_Load(object sender, EventArgs e)
        {
            //if (isReloadData)
            //    importLoadedDemandData();
            //loadScenGenBaseData();
            //loadScens();

            //loadDLProperties();
            //cmBScenType.Items.Clear();
            //foreach (string sT in scenarioTypes)
            //{
            //    cmBScenType.Items.Add(sT);
            //}
            //cmBScenType.SelectedIndex = (Int32) Scenario .ScenType .TOTAL;
            //cmBDistributionType.Items.Clear();
            //foreach (string dT in distributionTypes)
            //{
            //    cmBDistributionType.Items.Add(dT);
            //}
            //cmBDistributionType.SelectedIndex = (Int32) DistributionType.UNIFORM;
           
           // visualizeScens();
        }

        public List<Scenario> getAllScenarios()
        {
            List<Scenario> scenList;
            scenList = new List<Scenario>();

            foreach (KeyValuePair<string, Scenario> sKVP in currScens)
            {
                if (sKVP.Value .BaseDataFolder ==baseDataFolderName )
                    scenList.Add(sKVP.Value);
            }

            return scenList;
        }

        //public DataTable getScenTabForVisualization()
        //{
        //    DataTable vScenTab;
        //    string[] currLine;
        //    vScenTab = new DataTable();
        //    currLine = new string[2];

        //    vScenTab.Columns.Add("Name");
        //    vScenTab.Columns.Add("Typ");

        //    foreach (KeyValuePair<string, Scenario> sKVP in currScens)
        //    {
        //        currLine[0] = sKVP.Value.Name;
        //        currLine[1] = scenarioTypes[(Int32)sKVP.Value.Type];
        //        vScenTab.Rows.Add(currLine);
        //    }

        //    return vScenTab;
        //}

        //public DataTable getVScenTypes()
        //{
        //}

        //public DataTable get

        protected Scenario.ScenType usedScenType;
        private void cmBScenType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            string typeStr;

            typeStr = cmBScenType.SelectedItem .ToString ();
            for (i = 0; i < scenarioTypes.Length; i++)
            {
                if (scenarioTypes[i] == typeStr)
                    break;
            }

            usedScenType = (Scenario.ScenType )i;

            if (usedScenType == Scenario.ScenType.LOCATIONS)
            {
                cmBDistributionType.Enabled = false;
                tbStdDev.Enabled = false;
            }
            else if (usedScenType == Scenario.ScenType.TOTAL)
            {
                cmBDistributionType.Enabled = true;
                tbStdDev.Enabled = true;
            }
        }

        protected DistributionType usedDistrType;
        private void cmBDistributionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            string typeStr;

            typeStr = cmBDistributionType.SelectedItem .ToString ();
            for (i = 0; i < distributionTypes .Length ;i++) //di .Length; i++)
            {
                if (distributionTypes[i] == typeStr)
                    break;
            }

            usedDistrType = (DistributionType)i;
            isMakeNewDistr = true;
        }

        
        protected const int MAX_UNIF_PCT = 57;
        protected double usedStdDev;
        private void tbStdDev_TextChanged(object sender, EventArgs e)
        {
            int stdDevPct;
            
            try
            {
                stdDevPct = Convert.ToInt32(tbStdDev.Text, CultureInfo.InvariantCulture);
            }
            catch (Exception exc)
            {
                tbStdDev.Text = "";
                return;
            }

            //if (usedDistrType == DistributionType.UNIFORM && stdDevPct > MAX_UNIF_PCT)
            //{
            //    tbStdDev .Text ="";
            //    return;
            //}

            usedStdDev =((double)stdDevPct) /100;
            isMakeNewDistr = true;
        }
        protected bool isMakeNewDistr = true;
        private void btnCreateScen_Click(object sender, EventArgs e)
        {
            if (isMakeNewDistr)
            {
                //setDistribution(usedDistrType, usedStdDev,used);
                isMakeNewDistr = false;
            }
            //makeScenario(tbScenName.Text, usedScenType);
            //visualizeScens();
        }
    }
}
