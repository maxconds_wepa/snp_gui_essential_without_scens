﻿namespace GUI_SNP_Optimization
{
    partial class ScenGen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmBScenType = new System.Windows.Forms.ComboBox();
            this.btnCreateScen = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dGVScens = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.cmBDistributionType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbStdDev = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbScenName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dGVScens)).BeginInit();
            this.SuspendLayout();
            // 
            // cmBScenType
            // 
            this.cmBScenType.FormattingEnabled = true;
            this.cmBScenType.Location = new System.Drawing.Point(16, 24);
            this.cmBScenType.Name = "cmBScenType";
            this.cmBScenType.Size = new System.Drawing.Size(272, 21);
            this.cmBScenType.TabIndex = 0;
            this.cmBScenType.SelectedIndexChanged += new System.EventHandler(this.cmBScenType_SelectedIndexChanged);
            // 
            // btnCreateScen
            // 
            this.btnCreateScen.Location = new System.Drawing.Point(16, 88);
            this.btnCreateScen.Name = "btnCreateScen";
            this.btnCreateScen.Size = new System.Drawing.Size(128, 24);
            this.btnCreateScen.TabIndex = 1;
            this.btnCreateScen.Text = "Szenario erstellen";
            this.btnCreateScen.UseVisualStyleBackColor = true;
            this.btnCreateScen.Click += new System.EventHandler(this.btnCreateScen_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(16, 128);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(128, 24);
            this.button2.TabIndex = 2;
            this.button2.Text = "Alle Szenarien löschen";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(168, 88);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(232, 24);
            this.button3.TabIndex = 3;
            this.button3.Text = "Optimierung mit ausgewähltem Szenario";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Typ";
            // 
            // dGVScens
            // 
            this.dGVScens.AllowUserToAddRows = false;
            this.dGVScens.AllowUserToDeleteRows = false;
            this.dGVScens.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVScens.Location = new System.Drawing.Point(440, 24);
            this.dGVScens.Name = "dGVScens";
            this.dGVScens.ReadOnly = true;
            this.dGVScens.Size = new System.Drawing.Size(272, 134);
            this.dGVScens.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(440, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Bestehende Szenarien";
            // 
            // cmBDistributionType
            // 
            this.cmBDistributionType.FormattingEnabled = true;
            this.cmBDistributionType.Location = new System.Drawing.Point(296, 24);
            this.cmBDistributionType.Name = "cmBDistributionType";
            this.cmBDistributionType.Size = new System.Drawing.Size(136, 21);
            this.cmBDistributionType.TabIndex = 7;
            this.cmBDistributionType.SelectedIndexChanged += new System.EventHandler(this.cmBDistributionType_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(296, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Verteilung der Abweichung";
            // 
            // tbStdDev
            // 
            this.tbStdDev.Location = new System.Drawing.Point(296, 64);
            this.tbStdDev.Name = "tbStdDev";
            this.tbStdDev.Size = new System.Drawing.Size(128, 20);
            this.tbStdDev.TabIndex = 9;
            this.tbStdDev.TextChanged += new System.EventHandler(this.tbStdDev_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(296, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Standardabweichung in %";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Name";
            // 
            // tbScenName
            // 
            this.tbScenName.Location = new System.Drawing.Point(16, 64);
            this.tbScenName.Name = "tbScenName";
            this.tbScenName.Size = new System.Drawing.Size(272, 20);
            this.tbScenName.TabIndex = 11;
            // 
            // ScenGen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 170);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbScenName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbStdDev);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmBDistributionType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dGVScens);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnCreateScen);
            this.Controls.Add(this.cmBScenType);
            this.Name = "ScenGen";
            this.Text = "Szenariogenerator";
            this.Load += new System.EventHandler(this.ScenGen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dGVScens)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmBScenType;
        private System.Windows.Forms.Button btnCreateScen;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dGVScens;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmBDistributionType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbStdDev;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbScenName;
    }
}