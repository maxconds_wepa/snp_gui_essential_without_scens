﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using Optimizer_Data;
using Logging;
using Optimizer_Exceptions;
using SNP_Optimizer;
using SNP_WEPA;
using EuropaMap;

namespace GUI_SNP_Optimization
{
    public partial class Form1 : Form, Logger, SNP_DataProvider.IProgressLogger 
    {
        public enum OptimizationRunState
        {
            STOPPED,
            RUNNING,
            COMPLETED
        }

        public enum ActionType
        {
            DATA_LOAD,
            OPTIMIZATION
        }

        public enum BuildType
        {
            BUILD_1=0,
            BUILD_2A=1
        }

        protected string[] buildTitles = { "Ausbaustufe 1", "AusbauStufe 2a" };


        protected int maxNumberOfThreads = 1;

        protected string generalCSVDelimiter = ";";
        protected double defaultFixedTransportCostPart = 0;
        protected double defaultVariableTransportCostPart = 1.8;
        protected double defaultMinLotSize = 0.5;
        protected double defaultMinRunTime = 3;
        protected double defaultFactorMinInventory = 0.5;
        protected double defaultFactorResourceCapacities = 1;
        protected double defaultMaxGap = 0.5;
        protected bool defaultIsUseBaskets = true;
        protected bool defaultIsOnlyProductionOptimization = false;
        protected bool defaultIsFixPlants = false;
        protected bool defaultUseFixationsExlusions = true;
        protected bool defaultIsUseExternalData = false;
        protected bool defaultCheckCountryForTransportRelations = true;
        protected bool defaultIsOfflineMode = true;
        protected string optimizationBasePath = "C:\\PM\\SNP-WEPA\\Optimizations\\";
      
        protected string baseDataMainPath = "C:\\Max-Con\\WEPA\\BaseData\\";
        
        protected string optimizationFolderPath;
        protected string optimizationBaseDataFolderName = "BaseData";
        protected string optimizationBaseDataFolderPath;
        protected string currentUserName;
        protected CultureInfo currC;
      
        protected LoggerCollection usedLoggers;
        protected bool isActionRunning;
        protected Thread currActionThread;
        protected ActionType currentActionType;

        protected const string optimizerFileName = "WEPA_Modellierung.exe";
        protected const string mergerFileName = "WEPA_merge.exe";
        protected string guiCfgFileName = "guicfg.csv";
        
        protected const string fixResultFileNameGER = "fixierung_output_gui.csv";
        protected const string fixResultFileNameENGL = "fixierung_output_gui.csv";
        protected const string optResultFileNameGER = "optimierung_output_gui.csv";
        protected const string optResultFileNameENGL = "optimierung_output_gui.csv";
        protected const double dummyCost = 1000000;

        protected delegate void progressCallBackMethod(int progressPercentage);
        protected delegate void logMethod(LoggerMsgType type, string msg, int msgId);
        protected delegate void loadResultsMethod(bool isOptimization, bool isGerman,bool isSelected,bool isQuietError);
        private delegate void enableSetProc(bool v);
        private delegate void textSetProc(string t);
        protected delegate void feedBackOptimizerProc(OptimizationRunState oRSt, Int32 r);
        
        protected delegate void returnVoidWithoutParams();

        protected const string ResultsSubDir = "Results";
        protected const string usedOptimizationFolderBase = "";
        protected string usedOptimizationFolder;
        protected string ISO_8601_FORMAT_STRING  = "yyyy-MM-ddTHH-mm-ss";
        protected bool isRunOptimization = false;
        protected bool baseDataFolderOk;
        protected const int EXCEL_POLLING_INTERVAL = 1;
        protected bool isDataLoaded;
        protected bool isOptimizationGiven;
        protected bool isBaseDataElementSelected;
        protected string selectedBaseDataFolder;

        protected string networkplanningBaseDirectory = "SNP";
        protected string baseDataFolder;
        protected string loadedDataPath;
        protected string resultsBasePath;
        protected string loadedDataSubDirectory = "Data";

        protected BaseData_CheckResult cR;

        protected static string[,] displayAndFileNameBases = { 
                                                    {"Produktionsstandorte","Plantlocations"}, 
                                                    {"Lagercodes","WareHouseCodes"}, 
                                                    {"Historische Produktionstonnagen","Produktionstonnage"},
                                                    {"Arbeitspläne und Stammdaten","Arbeitspläne und Stammdaten"},
                                                    {"Zusätzliche Arbeitspläne für Verkaufsartikel","Fehlende_AP_für_SalesArtikel"},
                                                    {"Schichten","Schichten"},
                                                    {"Historische Transporte Nord-Ost-Europa","HistoricalTransports_NorthEastern_Europe"},
                                                    {"Historische Transporte Süd-West-Europa","HistoricalTransports_SouthWestern_Europe"},
                                                    {"Frachtkosten","Freight_cost_matrix"},
                                                    {"Produktionskosten","Produktionskosten"},
                                                    {"Anfangslagerbestand","SSI Datei_Lagerbestand"},
                                                    {"Mindestbestand","Mindestbestand"},
                                                    {"Zuordnung Verladeort zu Produktionsort","Zuordnung_Ladeort_zu_Produktionsort"},
                                                    {"Verkaufsplan","sales plan"},
                                                    {"Lagerkapazitäten","LagerKapazitäten"},
                                                    {"Fixierungsmatrix","fixierung"},
                                                    {"Ausschlussmatrix","ausschluss"}
                                                  };

        protected string whseSouthWestEuropeFilename;// = "WareHouseCodes.xlsx";

        protected string whseCapFileName;

        protected string locationTranslationFilename;// = "Plantlocations.xlsx";

        protected static string tonnageFileNameBase = "Produktionstonnage";
        protected string tonnageFileName;// = "Produktionstonnages.xlsx";

        protected static string baseDataAndWorkingPlansFileNameBase = "Arbeitspläne und Stammdaten";
        protected string baseDataAndWorkingPlansFileName;// = "WorkingPlansAndBaseData.xlsx";

        protected static string additionalBaseDataAndWorkingPlansFileNameBase = "Fehlende_AP_für_SalesArtikel";
        protected string additionalBaseDataAndWorkingPlansFileName;
       //protected static string demandFileName = "sales plan 01-03.xlsx";
        protected static string salesPlanFileNameBase = "sales plan";// "sales plan {0}-{1}.xlsx";
        protected string salesPlanFileName;

        protected static string shiftsFileNameBase = "Schichten";
        protected string shiftsFileName;

        protected static string transpFileNameBase = "HistoricalTransports_NorthEastern_Europe";
        protected string transpFileName;// = ".xlsx";

        protected static string transpSouthWestEuropeFileNameBase = "HistoricalTransports_SouthWestern_Europe";
        protected string transpSouthWestEuropeFileName;// = "HistoricalTransports_SouthWestern_Europe.xlsx";

        protected static string interPlantTranspFileNameBase = "Freight_cost_matrix";
        protected string interPlantTranspFileName;

        protected static string costPerShiftFileNameBase = "Produktionskosten";
        protected string costPerShiftFileName;

        protected static string costPerTonFileNameBase = "Produktionskosten";
        protected string costPerTonFileName = "Produktionskosten.xlsx";
       // protected static string customerChainTransportCustomerRelationsFileName = "Kunde-Kette.xlsx";
        //protected static string tourChainFilename = "Aktuelle Frachten Eurolog 2014-12_mit Kette.xlsx";

        protected static string iniStocksFilenameBase = "SSI Datei_Lagerbestand";
        protected string iniStocksFilename;

        protected static string minimumStocksFilenameBase = "Mindestbestand";
        protected string minimumStocksFilename;

        protected static string transportStartSubstituteFileNameBase = "Zuordnung_Ladeort_zu_Produktionsort.xlsx";
        protected string transportStartSubstituteFileName;

        protected static string plantfileName = "plants.csv";
        protected static string customerfileName = "customers.csv";
        protected static string resourcefileName = "resources.csv";
        protected static string productfileName = "products.csv";
        protected static string periodfileName = "periods.csv";
        protected static string processfileName = "processes.csv";
        protected static string capacityfileName = "capacities.csv";
        protected static string transportplantfileName = "transportsplant.csv";
        protected static string transportcustomerfileName = "transportscustomer.csv";
        protected static string demandfileName = "demands.csv";
        protected static string palletspertonfileName = "pallets_per_ton.csv";
        protected static string basketsfileName = "baskets.csv";
        protected static string histProdFileName = "histProduction.csv";
        protected static string iniStFileName = "startBestand.csv";
        protected static string warehouseCapacitiesFileName = "lagerkapazitaeten.csv";
        protected static string articleNamesFileName = "artikelnamen.csv";
        protected static string productsNotConsideredFileName = "ignorierteProdukte.csv";
        protected static string processesNotConsideredFileName = "ignorierteProzesse.csv";
        protected static string resourcesNotConsieredFileName = "ignorierteMaschinen.csv";
        protected static string missingTransportsFileName = "fehlendeKundentransporte.csv";
        protected static string customerProductsInfeasibleBasketsFileName = "kundenProdukteUnzulaessigeWarenkoerbe.csv";
        protected static string fixationExclusionProductionProductsFileName ="fixierungausschlussproduktionsprodukte.csv";
        protected static string fixationExclusionTransportationProductsFileName = "fixierungausschlusstransportprodukte.csv";
        protected static string customerFixationsFileName = "customer_fixation_groups.csv";
        protected static string customerProductRelatedTransportationFixationsFileName = "customer_product_related_transports.csv";
        protected static string productionDetailsResultFileName = "optimierung_output_detail_produktion.csv";
        protected static string productionDetailsResultFileNameReplication = "optimierung_output_detail_produktion_{0}.csv";

        protected static string outputSeparator = " ";
        string dataLoadPropertiesFileName = "dataLoadProperties.csv";
        protected static string[] dataLoadPropertiesColumns = { "Stammdatenordner" };
        
        private static string[] MONTHS = { "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember" };

        private static string[] optimizationSettingsColumns = {
                                                        "MinimumProductionQuantity",
                                                        "SplitQuantity",
                                                        "UseBaskets",
                                                        "CapacityFactorForMachines",
                                                        "OnlyProductionOptimization",
                                                        "MinumInventoryFactor"
                                               };

        private string excTitle = "Ausnahme";
        //protected static string whseSouthWestEuropeFilename = "WareHouseCodes.xlsx";
        //protected static string locationTranslationFilename = "locsFinal_Troyes.xlsx";//"locsFinal.xlsx";//"locsFinal_Troyes.xlsx";//"locsFinal.xlsx";
        //protected static string tonnageFileName = "Produktionstonnage 2013+2014 Werk Automat Artikel.xlsx";
        //protected static string baseDataAndWorkingPlansFileName = "Arbeitspläne und Stammdaten 2015-04-20.xlsx";// "Arbeitspläne und Stammdaten 2015-03-09angepasst.xlsx";//Arbeitspläne und Stammdaten 2015-04-20.xlsx
        //protected static string additionalBaseDataAndWorkingPlansFileName = "Fehlende_AP_für_SalesArtikel_2015_01_16.xlsx";
        //protected static string demandFileName = "sales plan 01-03.xlsx";//"sales plan 04-06.xlsx";//"sales plan 03-05.xlsx";    
        //protected static string shiftsFileName = "Schichten_angepasst_Troyes.xlsx";//"Schichten_angepasst_Troyes_1Cap206C22.xlsx";//"Schichten_angepasst_Troyes.xlsx";//"Schichten_angepasst_Troyes.xlsx";//"Schichten.xlsx";
        //protected static string transpFileName = "12 - Dezember kumuliert nur Detail-Daten.xlsx";
        //protected static string transpSouthWestEuropeFileName = "12 - Dezember SüdWest kumuliert nur Daten_mit Kette.xlsx";
        ////"12 - Dezember SüdWest kumuliert nur Daten.xlsx";
        //protected static string interPlantTranspFileName = "FD-GR-2100_EN_Freight_cost_matrix_2014_mit_Troyes.xlsx";//"FD-GR-2100_EN_Freight_cost_matrix_2014_mit_Troyes.xlsx";//"FD-GR-2100_EN_Freight_cost_matrix_2014.xlsx";
        //protected static string costPerShiftFileName = "Produktionskosten_2015-01-16.xlsx";
        //protected static string costPerTonFileName = "Produktionskosten_2015-02-09.xlsx";
        //protected static string customerChainTransportCustomerRelationsFileName = "Kunde-Kette.xlsx";
        //protected static string tourChainFilename = "Aktuelle Frachten Eurolog 2014-12_mit Kette.xlsx";




        //protected static string whseSouthWestEuropeFilename = "WareHouseCodes.xlsx";
        //protected static string locationTranslationFilename = "locsFinal_Troyes.xlsx";//"locsFinal_Troyes.xlsx";//"locsFinal.xlsx";
        //protected static string tonnageFileName = "Produktionstonnage 2013+2014 Werk Automat Artikel.xlsx";
        //protected static string baseDataAndWorkingPlansFileName = "Arbeitspläne und Stammdaten 2015-03-09angepasst.xlsx";
        //protected static string additionalBaseDataAndWorkingPlansFileName = "Fehlende_AP_für_SalesArtikel_2015_01_16.xlsx";
        //protected static string demandFileName = "sales plan 04-06.xlsx";//"sales plan 03-05.xlsx";    
        //protected static string shiftsFileName = "Schichten_angepasst_Troyes.xlsx";//"Schichten_angepasst_Troyes.xlsx";//"Schichten.xlsx";
        //protected static string transpFileName = "12 - Dezember kumuliert nur Detail-Daten.xlsx";
        //protected static string transpSouthWestEuropeFileName = "12 - Dezember SüdWest kumuliert nur Daten_mit Kette.xlsx";
        ////"12 - Dezember SüdWest kumuliert nur Daten.xlsx";
        //protected static string interPlantTranspFileName = "FD-GR-2100_EN_Freight_cost_matrix_2014_mit_Troyes.xlsx";//"FD-GR-2100_EN_Freight_cost_matrix_2014_mit_Troyes.xlsx";//"FD-GR-2100_EN_Freight_cost_matrix_2014.xlsx";
        //protected static string costPerShiftFileName = "Produktionskosten_2015-01-16.xlsx";
        //protected static string costPerTonFileName = "Produktionskosten_2015-02-09.xlsx";
        //protected static string customerChainTransportCustomerRelationsFileName = "Kunde-Kette.xlsx";
        //protected static string tourChainFilename = "Aktuelle Frachten Eurolog 2014-12_mit Kette.xlsx";
        public bool isQuietMessages;

        protected string dataLoadTaskName = "Stammdaten laden";
        protected string runOptimizationTaskName = "Optimierung";
        protected string runOptimizationWithFixationTaskName = "Optimierung mit Fixierungen";
        protected string mergeTaskName = "Ergebnisse aus Optimierung und Fixierung zusammenführen";

        public const int NUMBER_DIGITS_TO_DISPLAY=2;
        public const string CURRENCY_SYMBOL = " €";
        protected bool isReadyToClose;
        protected Mutex clMut;
        protected bool isCloseRequested;

        string[,] dataTypeDBTable ={ {"Produktionsstandorte","LocationName,Country,PostalCode,WarehouseCapacity","IN_PLANTLOCATIONS","plantlocations.xlsx"},
                    {"Lagercodes","WarehouseCode,Country,PostalCode,LocationName","IN_WAREHOUSE_CODE_SW","warehousecodessw.xlsx"},
                    {"Historische Produktiontonnagen","MAC,Product_Id","IN_HISTORICAL_PRODUCTION","historicalProduction.xlsx"},
                    {"Arbeitspläne","Product_Id,MAC,TonPerShift","IN_ROUTING","routings.xlsx"},
                    {"Schichten","Period,MAC,NumberOfShifts","IN_SHIFTS","shifts.xlsx"},
                    {"Maschinen","MAC,Location,CostPerHour","IN_MACHINES","machines.xlsx"},
                    {"Produkte","Product_Id,Product_Description,KgPerPallet","IN_PRODUCT_INFO","products.xlsx"},
                    {"Historische Transporte Nord-Ost-Europa","Transport_Type,Tour_Id,Product_Id,SourceCountry,SourcePostalCode,SourceLocationName,DestCountry,DestPostalCode,DestLocationName,Customer,NumberOfPallets,Price","IN_HISTORICAL_TRANSPORTS_NE","transportsNE.xlsx"},
                    {"Historische Transporte Süd-West-Europa","WarehouseCode,DateOfExpedition,DestCountry,DestPostalCode,Customer,Product_Id,NumberOfPallets,Price","IN_HISTORICAL_TRANSPORTS_SW","transportsSW.xlsx"},
                    {"Frachtkosten","Plant_Start,Plant_End,Cost","IN_INTERCOMPANY_TRANSPORTS","ictransports.xlsx"},
                    {"Anfangslagerbestand","Product_Id,LocationName,Quantity","IN_INITIAL_STOCK","initialstocks.xlsx"},
                    {"Zuordnung Verladeort zu Produktionsort","SourceLocationName,ProductionLocationName","IN_SOURCELOCATION_PRODUCTIONLOCATION","slpl.xlsx"},
                        {"Verkaufsplan","Product_Id,Customer,Period,Country,MAC,Quantity","IN_DEMAND","demands.xlsx"}
                                   };

        //protected SQLLiteDataBase usedInMemDB;
        protected string productTabName = "IN_PRODUCT";
        protected string[,] productTabSpec = { { "Product", "text"},
                                                  { "ProductIndex", "int"}
                                                  };
        protected string customerTabName = "IN_CUSTOMER";
        protected string[,] customerTabSpec = { {"CustomerChain", "text"},
                                                  { "CustomerIndex", "int"},
                                                  {"Country","text"},
                                                  {"PostalCode","text"}
                                                  };
        protected string periodTabName = "IN_PERIOD";
        protected string[,] periodTabSpec = { { "Period", "text"},
                                                  { "PeriodIndex", "int"}
                                                  };

        protected string demandTabName = "IN_DEMAND";
        protected string[,] demandTabSpec = { {"CustomerIndex", "int"},
                                                {"ProductIndex", "int"},
                                                  { "PeriodIndex", "int"},
                                                  { "DemandQuantity", "double"},
                                                  { "ScenarioIndex", "int"}
                                                  };

        protected String productionDetailsTabName = "IN_PRODUCTION_DETAIL";

        protected String[,] productionDetailTabSpec ={
                                                         {"OptimizationFolder","text"},
                                                         {"ScenarioName","text"},
                                                         {"ReplicationNumber","int"},
                                                         {"Artikelnummer","text"},
                                                         {"Kunden","text"},
                                                         {"Monat","text"},
                                                         {"Werk","text"},
                                                         {"Anlage","text"},
                                                         {"Menge","double"},
                                                         {"Anzahl Schichten","double"},
                                                         {"Produktionskosten","double"}
                                                    };

        protected String productionSummTabName = "IN_PRODUCTION_SUMMARY";

        protected String[,] productionSummTabSpec ={
                                                         {"OptimizationFolder","text"},
                                                         {"ScenarioName","text"},
                                                         {"Artikelnummer","text"},
                                                         {"Kunden","text"},
                                                         {"Monat","text"},
                                                         {"Werk","text"},
                                                         {"Anlage","text"},
                                                         {"Menge","double"},
                                                         {"Anzahl Schichten","double"},
                                                         {"Produktionskosten","double"}
                                                    };

        /*
        protected string[,] detailedProductionResultsCSVSpec ={
                                                                  {"Artikelnummer",CSV_File.STRING_TYPE,""},
                                                                  {"Kunden",CSV_File.STRING_TYPE,""},
                                                                  {"Monat",CSV_File.STRING_TYPE,""},
                                                                  {"Werk",CSV_File.STRING_TYPE,""},
                                                                  {"Anlage",CSV_File.STRING_TYPE,""},
                                                                  {"Menge",CSV_File.DOUBLE_TYPE,""},
                                                                  {"Anzahl Schichten",CSV_File.INT_TYPE,""},
                                                                  {"Produktionskosten (Summe)",CSV_File.DOUBLE_TYPE,""}
                                                              };
        */

        protected string aggrCustChainSqlCmd = "insert into IN_CUSTOMERCHAIN_DEMAND " +
                                                "select CustomerChain,"+
                                                "ProductIndex,"+
                                                "PeriodIndex,"+
                                                "Sum(DemandQuantity) as ChainDemandQuantity,"+
                                                "ScenarioIndex "+
                                                "from IN_DEMAND demTab "+
                                                "join IN_CUSTOMER custTab on demTab.CustomerIndex=custTab.CustomerIndex " +
                                                "where ScenarioIndex=0 "+
                                                "group by CustomerChain, ProductIndex,PeriodIndex,ScenarioIndex "+
                                                "order by CustomerChain, ProductIndex,PeriodIndex ";

        protected string fractionsSqlCmd = "insert into IN_DEMAND_FRACTIONS " +
                                           "Select customerTotalDemTab.CustomerIndex," +
                                           "custTab.CustomerChain," +
                                           "customerTotalDemTab.ProductIndex," +
                                           "TotalCustomerDemandQuantity/(case when TotalChainDemandQuantity=0 then 1 else TotalChainDemandQuantity end) as CustomerDemandFraction " +
                                           "from (" +
                                                        "Select CustomerIndex," +
                                                               "ProductIndex," +
                                                               "Sum(DemandQuantity) TotalCustomerDemandQuantity " +
                                                               "from IN_DEMAND demTab " +
                                                               "where demTab.ScenarioIndex=0 " +
                                                               "group by CustomerIndex,ProductIndex " +
                                                ") customerTotalDemTab " +
                                          "join IN_CUSTOMER custTab on customerTotalDemTab.CustomerIndex=custTab.CustomerIndex " +
                                          "join (" +
                                                                "select CustomerChain," +
                                                                       "ProductIndex," +
                                                                       "Sum(DemandQuantity) as TotalChainDemandQuantity " +
                                                                        "from IN_DEMAND demTab " +
                                                                       "join IN_CUSTOMER custTab on demTab.CustomerIndex=custTab.CustomerIndex " +
                                                                       "where ScenarioIndex=0 " +
                                                                        "group by CustomerChain, ProductIndex " +
                                              ")" +
                                         "customerChainTotalDemTab on custTab.CustomerChain=customerChainTotalDemTab.CustomerChain and customerTotalDemTab.ProductIndex=customerChainTotalDemTab.ProductIndex";

        protected string customerChainDemandTabName = "IN_CUSTOMERCHAIN_DEMAND";
        protected string[,] customerChainDemandTabSpec = { {"CustomerChain", "text"},
                                                {"ProductIndex", "int"},
                                                  { "PeriodIndex", "int"},
                                                  { "DemandQuantity", "double"},
                                                  { "ScenarioIndex", "int"}
                                                  };

        protected string demandFractionsTabName = "IN_DEMAND_FRACTIONS";
        protected string[,] demandFractionsTabSpec = { {"CustomerIndex", "int"},
                                                        {"CustomerChain", "text"},
                                                    {"ProductIndex", "int"},
                                                  { "CustomerDemandFraction", "double"}
                                                  };

        protected string CSV_SPACE_DELIMITER = " ";
        protected string CSV_SEMCOL_DELIMITER = ";";
        protected string[,] productTabCSVSpec = { 
                                               {"ProductName",CSV_File.STRING_TYPE,""},
                                               {"ProductIndex",CSV_File.INT_TYPE,""}
                                               };

        protected string[,] customersTabCSVSpec = { 
                                               {"CustomerName",CSV_File.STRING_TYPE,""},
                                               {"CustomerIndex",CSV_File.INT_TYPE,""},
                                               {"Country",CSV_File .STRING_TYPE ,""},
                                               {"PostalCode",CSV_File .STRING_TYPE ,""}
                                               };

        protected string[,] demandTabCSVSpec = { 
                                               {"CustomerIndex",CSV_File.INT_TYPE,""},
                                               {"ProductIndex",CSV_File.INT_TYPE,""},
                                               {"PeriodIndex",CSV_File.INT_TYPE,""},
                                               {"DemandQty",CSV_File.DOUBLE_TYPE,""},
                                               {"ScenarioIndex",CSV_File.INT_TYPE,""}
                                               };

        protected string[,] periodsTabCSVSpec = { 
                                               {"PeriodName",CSV_File.STRING_TYPE,""},
                                               {"PeriodIndex",CSV_File.INT_TYPE,""}
                                               };

        protected string[,] detailedProductionResultsCSVSpec ={
                                                                  {"Artikelnummer",CSV_File.STRING_TYPE,""},
                                                                  {"Kunden",CSV_File.STRING_TYPE,""},
                                                                  {"Monat",CSV_File.STRING_TYPE,""},
                                                                  {"Werk",CSV_File.STRING_TYPE,""},
                                                                  {"Anlage",CSV_File.STRING_TYPE,""},
                                                                  {"Menge",CSV_File.STRING_TYPE,""},
                                                                  {"Anzahl Schichten",CSV_File.STRING_TYPE,""},
                                                                  {"Produktionskosten (Summe)",CSV_File.STRING_TYPE,""}
                                                              };

        /*
        protected String productionSummTabName = "IN_PRODUCTION_SUMMARY";

        protected String[,] productionSummTabSpec ={
                                                         {"OptimizationFolder","text"},
                                                         {"ScenarioName","text"},
                                                         {"Artikelnummer","text"},
                                                         {"Kunden","text"},
                                                         {"Monat","text"},
                                                         {"Werk","text"},
                                                         {"Anlage","text"},
                                                         {"Menge","double"},
                                                         {"Anzahl Schichten","double"},
                                                         {"Produktionskosten","double"}
                                                    };
        */
        protected string[] baseSQLTabGlobalSpec = {"baseTab",
                                                            "true",
                                                            "Select OptimizationFolder,ScenarioName,Artikelnummer,Kunden,Monat,Werk,Anlage,Menge, Anzahl Schichten as Schichten,Produktionskosten" 
                                                            +" from IN_PRODUCTION_SUMMARY prdTab"
                                                            +"group by OptimizationFolder,ScenarioName,Werk,Anlage",
                                                        "false",
                                                        "",
                                                        "false",
                                                        "false",
                                                        "",
                                                        "SG1"
                                                            };

        protected string[,] baseSQLTabColumnsSpec = { 
                                                              { "OptimizationFolder", "text",  "true","true","false","true","true","true","Product","",""},
                                                              { "ScenarioName", "text",  "true","true","false","true","true","true","Product","",""},
                                                              { "Artikelnummer", "text",  "true","true","false","true","true","true","Product","",""},
                                                              { "Kunden", "text",  "true","true","false","true","true","true","Product","",""},
                                                              { "Monat", "text",  "true","true","false","true","true","true","Product","",""},
                                                              { "Werk", "text",  "true","true","false","true","true","true","Product","",""},
                                                              { "Anlage", "text",  "true","true","false","true","true","true","Product","",""},
                                                              { "Menge", "text",  "true","true","false","true","true","true","Product","",""},
                                                              { "Schichten", "text",  "true","true","false","true","true","true","Product","",""},
                                                              { "Produktionskosten", "text",  "true","true","false","true","true","true","Product","",""}
                                                            };

        protected string[,] baseSQLTabJoins = { };

        protected string[] globalAggrUtilizationSQLTabGlobalSpec = {"baseTab",
                                                            "false",
                                                            "",
                                                        "true",
                                                        "baseTab",
                                                        "true",
                                                        "false",
                                                        "",
                                                        "SG1"
                                                            };
        /*
        currColSpec.ColumnName = colSpecs[i, 0];
                        currColSpec.SQL_Type = colSpecs[i, 1];

                        currColSpec.IsVisibleColumn = Convert.ToBoolean(colSpecs[i, 2]);

                        currColSpec.IsDimensionColumn = Convert.ToBoolean(colSpecs[i, 3]);

                        currColSpec.IsNonDimensionColumn = Convert.ToBoolean(colSpecs[i, 4]);

                        currColSpec.IsSortColumn = Convert.ToBoolean(colSpecs[i, 5]);

                        currColSpec.IsFilteredColumn = Convert.ToBoolean(colSpecs[i, 6]);

                        currColSpec.IsFilterEntryColumn = Convert.ToBoolean(colSpecs[i, 7]);

                        currColSpec.IsEnabled = true;

                        currColSpec.NewColumnName = colSpecs[i, 8];

                        if (colSpecs[i, 9] == ColumnSpec.AGGRTYPE_AVG_STR)
                            currColSpec.AggrType = ColumnSpec.ColAggrType.AVG;
                        else if (colSpecs[i, 9] == ColumnSpec.AGGRTYPE_COUNT_STR)
                            currColSpec.AggrType = ColumnSpec.ColAggrType.COUNT;
                        else if (colSpecs[i, 9] == ColumnSpec.AGGRTYPE_SUM_STR)
                            currColSpec.AggrType = ColumnSpec.ColAggrType.SUM;
                        else if (colSpecs[i, 9] == ColumnSpec.AGGRTYPE_MIN_STR)
                            currColSpec.AggrType = ColumnSpec.ColAggrType.MIN;
                        else if (colSpecs[i, 9] == ColumnSpec.AGGRTYPE_MAX_STR)
                            currColSpec.AggrType = ColumnSpec.ColAggrType.MAX;
                        else
                            currColSpec.AggrType = ColumnSpec.ColAggrType.NONE;

                        if (colSpecs[i, 10] != "")
                            currColSpec.DateTimeFormat = colSpecs[i, 10];
         */
        
        protected string[,] globalAggrUtilizationSQLTabColumnsSpec = { 
                                                              { "OptimizationFolder", "text",  "false","false","false","false","false","false","OptimizationFolder","",""},
                                                              { "ScenarioName",       "text",  "false","false","false","false","false","false","ScenarioName","",""},
                                                              { "Artikelnummer",      "text",  "false","false","false","false","false","false","Artikelnummer","",""},
                                                              { "Kunden",             "text",  "false","false","false","false","false","false","Kunden","",""},
                                                              { "Monat",              "text",  "false","false","false","false","false","false","Monat","",""},
                                                              { "Werk",               "text",  "true","true","false","true","true","false","Werk","",""},
                                                              { "Anlage",             "text",  "true","true","false","true","true","false","Anlage","",""},
                                                              { "Menge",              "text",  "true","false","true","false","false","false","Menge",FilterTableCollection.ColumnSpec.AGGRTYPE_SUM_STR,""},
                                                              { "Schichten",          "text",  "true","false","true","false","false","false","Schichten",FilterTableCollection.ColumnSpec.AGGRTYPE_SUM_STR,""},
                                                              { "Produktionskosten",  "text",  "true","false","true","false","false","false", "Produktionskosten",FilterTableCollection.ColumnSpec.AGGRTYPE_SUM_STR,""}
                                                            };

        protected string[,] globalAggrUtilizationSQLTabJoins = { };
        /*
        protected String productionDetailsTabName = "IN_PRODUCTION_DETAIL";

        protected String[,] productionDetailTabSpec ={
                                                         {"OptimizationFolder","text"},
                                                         {"ScenarioName","text"},
                                                         {"ReplicationNumber","int"},
                                                         {"Artikelnummer","text"},
                                                         {"Kunden","text"},
                                                         {"Monat","text"},
                                                         {"Werk","text"},
                                                         {"Anlage","text"},
                                                         {"Menge","double"},
                                                         {"Anzahl Schichten","double"},
                                                         {"Produktionskosten","double"}
                                                    };
        */
        public class OptimizationRunConfig
        {
            public static string[] optimizationSettingsHeader = {
                                                        "MinimumProductionQuantity",
                                                        "SplitQuantity",
                                                        "AllowSplittingIfInFixations",
                                                        "UseBaskets",
                                                        "CapacityFactorForMachines",
                                                        "OnlyProductionOptimization",
                                                        "MinumInventoryFactor",
                                                        "GermanOutput",
                                                        "ExpansionStage",
                                                        "FixPlants",
                                                        "FixationThreshold",
                                                        "UseFixationsExlusions",
                                                        "MaxGap"
                                               };

            //public bool isFixedProductionQties;
            public double minProdQty;
            public double splitQty;
            public bool isAllowSplittingIfInFixations;
            public double capacityFactorForMachines;
            public double minInventoryFactor;
            public bool isOnlyProductionOptimization;
            public bool isUseBaskets;
            public bool isUseGermanCulture;
            public bool isFixPlants;
            public bool isUseFixationsExlusions;
            public double fixationThreshold;
            public double maxGap;
            public BuildType expansionStage;
            protected string csvDelim = ";";
            protected string usCultureName = "en-US";
            


            public void saveConfig(string fileName)
            {
                CSV_File optimizationRunSettingsFile;
                DataTable confData;
                int i;
                string[] paramValues;
                CultureInfo usC;
                

                usC=CultureInfo.CreateSpecificCulture (usCultureName );

                confData = new DataTable();

                for (i = 0; i < optimizationSettingsHeader.Length; i++)
                    confData.Columns.Add(optimizationSettingsHeader[i]);

                paramValues = new string[optimizationSettingsHeader.Length];

                paramValues[0] = Convert.ToString(minProdQty, usC);

                paramValues[1] = Convert.ToString(splitQty, usC);

                if (isAllowSplittingIfInFixations)
                    paramValues[2] = "1";
                else
                    paramValues[2] = "0";

                if (isUseBaskets)
                    paramValues[3] = "1";
                else
                    paramValues[3] = "0";

                paramValues[4] = Convert.ToString(capacityFactorForMachines, usC);

                if (isOnlyProductionOptimization)
                    paramValues[5] = "1";
                else
                    paramValues[5] = "0";

                paramValues[6] = Convert.ToString(minInventoryFactor, usC);
                //optimizationRunSettingsFile.writeData (
                if (isUseGermanCulture)
                    paramValues[7] = "1";
                else
                    paramValues[7] = "0";

                paramValues[8] = Convert.ToString((Int32)expansionStage, usC);

                if (isFixPlants)
                    paramValues[9] = "1";
                else
                    paramValues[9] = "0";

                paramValues[10] = Convert.ToString(fixationThreshold, usC);

                if (isUseFixationsExlusions)
                    paramValues[11] = "1";
                else
                    paramValues[11] = "0";

                paramValues[12] = Convert.ToString(maxGap, usC);

                confData.Rows.Add(paramValues);
                optimizationRunSettingsFile = new CSV_File(fileName);
                
                optimizationRunSettingsFile.HeadLine = optimizationSettingsHeader;
                optimizationRunSettingsFile.IsHeadLine = true;
                optimizationRunSettingsFile.openForWriting(false);
                optimizationRunSettingsFile.writeData(confData);
                optimizationRunSettingsFile.closeDoc();
            }
            public void loadConfig(string fileName)
            {
                CSV_File currOptrunConfigFile;
                DataTable configData;
                CultureInfo usC;

                usC = CultureInfo.CreateSpecificCulture(usCultureName);

                if (File.Exists(fileName))
                {
                    using (currOptrunConfigFile = new CSV_File(fileName))
                    {

                        currOptrunConfigFile.IsHeadLine = true;
                        currOptrunConfigFile.openForReading();
                        configData = currOptrunConfigFile.getData();
                        currOptrunConfigFile.closeDoc();

                        if (configData.Rows[0].ItemArray.Length >= 7)
                        {
                            minProdQty = Convert.ToDouble(configData.Rows[0].ItemArray[0],usC);

                            splitQty = Convert.ToDouble(configData.Rows[0].ItemArray[1], usC);

                            if (configData.Rows[0].ItemArray[2].ToString() == "1")
                                isAllowSplittingIfInFixations = true;
                            else
                                isAllowSplittingIfInFixations = false;

                            if (configData.Rows[0].ItemArray[3].ToString() == "1")
                                isUseBaskets = true;
                            else
                                isUseBaskets = false;

                            capacityFactorForMachines = Convert.ToDouble(configData.Rows[0].ItemArray[4], usC);

                            if (configData.Rows[0].ItemArray[5].ToString() == "1")
                                isOnlyProductionOptimization = true;
                            else
                                isOnlyProductionOptimization = false;

                            minInventoryFactor = Convert.ToDouble(configData.Rows[0].ItemArray[6], usC);

                            if (configData.Rows[0].ItemArray.Length > 7)
                            {
                                if (configData.Rows[0].ItemArray[7].ToString() == "1")
                                    isUseGermanCulture = true;
                                else
                                    isUseGermanCulture = false;
                            }
                            else
                                isUseGermanCulture = false;

                            if (configData.Rows[0].ItemArray.Length > 8)
                            {
                                expansionStage = (BuildType)Convert.ToInt32(configData.Rows[0].ItemArray[8], usC);
                            }
                            else
                                expansionStage = BuildType.BUILD_1;

                            if (configData.Rows[0].ItemArray.Length > 9)
                            {
                                if (configData.Rows[0].ItemArray[9].ToString() == "1")
                                    isFixPlants = true;
                                else
                                    isFixPlants = false;
                            }
                            else
                                isFixPlants = false;

                            if (configData.Rows[0].ItemArray.Length > 10)
                            {
                                fixationThreshold = Convert.ToDouble(configData.Rows[0].ItemArray[10].ToString(), usC);
                            }
                            else
                                fixationThreshold = 0;

                            if (configData.Rows[0].ItemArray.Length > 11)
                            {
                                if (configData.Rows[0].ItemArray[11].ToString() == "1")
                                    isUseFixationsExlusions = true;
                                else
                                    isUseFixationsExlusions = false;
                            }
                            else
                            {
                                isUseFixationsExlusions = false;
                            }

                            if (configData.Rows[0].ItemArray.Length > 12)
                            {
                                maxGap = Convert.ToDouble(configData.Rows[0].ItemArray[12].ToString(), usC);
                            }
                            else
                                maxGap = 0;
                        }
                    }
                }
            }
        }

        protected enum File_State
        {
            NOT_EXISTS,
            EXISTS,
            AMBIGUOUS
        }

        protected class FileCheckResult
        {
            public File_State fS;
            public string fileName;
        }

        protected class BaseData_CheckResult
        {
            public FileCheckResult[] fCR;
            public enum SalesPlanCheckResult {
                            OK,
                            WRONG_FORMAT
            }
            public SalesPlanCheckResult salesPlanNameStatus;
            public string usedDemandFileName;
            public int startMonth;
            public int endMonth;
        };

        public void progressCallBack(int progressPercentage)
        {
            
            if (pgbLoading .InvokeRequired )
            {
             //   dGVOptResults.Invoke(new loadResultsMethod(fillResults), isFixed, isGerman);

                pgbLoading.Invoke(new progressCallBackMethod(progressCallBack), progressPercentage);
            }
            else
            {
                if (progressPercentage > 100)
                pgbLoading.Value  = 100;
                else
                pgbLoading.Value = progressPercentage;
            }
        }


        protected FileCheckResult[] fileCheck(string baseDir)
        {
            int i;
            string reducedFileCandidate;
            FileCheckResult [] fCR;



            fCR = new FileCheckResult[displayAndFileNameBases.GetLength(0)];//[baseFileNameList .Length ];

           
            //     protected static string whseSouthWestEuropeFilename = "WareHouseCodesSouthWestEurope.xlsx";
            // protected static string locationTranslationFilename = "Plantlocations.xlsx";
            // protected static string tonnageFileName = "Produktionstonnages.xlsx";
            // protected static string baseDataAndWorkingPlansFileName = "WorkingPlansAndBaseData.xlsx";
            // protected static string additionalBaseDataAndWorkingPlansFileName = "AdditionalWorkingPlans.xlsx";
            ////protected static string demandFileName = "sales plan 01-03.xlsx";
            // protected static string demandFileNameBase = "sales plan";// "sales plan {0}-{1}.xlsx";
            // protected static string shiftsFileName = "Shifts.xlsx";
            // protected static string transpFileName = "HistoricalTransports_NorthEastern_Europe.xlsx";
            // protected static string transpSouthWestEuropeFileName = "HistoricalTransports_SouthWestern_Europe.xlsx";

            for (i = 0; i < fCR.Length; i++)
            {
                fCR[i] = new FileCheckResult();
                fCR[i].fS = File_State.NOT_EXISTS;
            }
            foreach (string fileCandidate in Directory.GetFiles(baseDir))
            {
                reducedFileCandidate =trimLongPath (fileCandidate ).ToUpper ();

                for (i = 0; i < fCR.Length; i++)
                {
                    if (reducedFileCandidate.Contains(displayAndFileNameBases[i,1].ToUpper ()))//(baseFileNameList[i]))
                    {
                        if (fCR[i].fS == File_State.NOT_EXISTS)
                        {
                           fCR[i].fS = File_State.EXISTS;
                           fCR[i].fileName = reducedFileCandidate;
                           break;
                        }
                        else if (fCR[i].fS == File_State.EXISTS)
                        {
                            fCR[i].fS = File_State.AMBIGUOUS;
                            break;
                        }
                    }
                }
            }

            return fCR;
        }

        public Form1()
        {
            

            InitializeComponent();
            
        }

        protected class OptimizationResult
        {
            public string FolderName;
            public double ProductionCost;
            public double InterCompanyTranportCost;
            public double CustomerTransportCost;
            public double InventoryCost;
            public double TotalCost
            {
                get
                {
                    return ProductionCost + InterCompanyTranportCost + CustomerTransportCost + InventoryCost;
                }
            }
            public double AverageCapacityUtilization;
            public double MedianOfCapacityUtilization;
        }

        protected string trimNumericString(string numStr)
        {
            string trNumStr;

            trNumStr = numStr.Trim();
            string spaceChar=" ";
            if (trNumStr.Contains(spaceChar))
                return (trNumStr.Substring(0, trNumStr.IndexOf(spaceChar)));

            return trNumStr.Trim();
        }

        protected OptimizationResult loadOptResult(string usedResultFileName,bool isGerman)
        {
            string csvDelim = ";";
            CultureInfo convC;
            DataTable data;
            OptimizationResult currResult;
            CSV_File resultData = new CSV_File(usedResultFileName);
            resultData.IsHeadLine = true;
            resultData.CSVDelimiter = csvDelim;
            resultData.openForReading();

            data= resultData.getData();
            
            resultData.closeDoc();

            if (isGerman)
                convC = CultureInfo.CreateSpecificCulture("de-DE");
            else
                convC = CultureInfo.CreateSpecificCulture("en-US");

            currResult = new OptimizationResult();
            currResult.FolderName = getPathFromLongFileName ( usedResultFileName);
            currResult.ProductionCost = Convert.ToDouble(trimNumericString(data.Rows[0].ItemArray[0].ToString()), convC);
            currResult.InterCompanyTranportCost = Convert.ToDouble(trimNumericString(data.Rows[0].ItemArray[1].ToString()), convC);
            currResult.CustomerTransportCost = Convert.ToDouble(trimNumericString(data.Rows[0].ItemArray[2].ToString()), convC);
            currResult.InventoryCost = Convert.ToDouble(trimNumericString(data.Rows[0].ItemArray[3].ToString()), convC);
            currResult.AverageCapacityUtilization = Convert.ToDouble(trimNumericString(data.Rows[0].ItemArray[5].ToString()), convC);
            currResult.MedianOfCapacityUtilization = Convert.ToDouble(trimNumericString(data.Rows[0].ItemArray[6].ToString()), convC);

            return currResult;
        }

        protected DataTable displayResultsTab;
       // protected DataTable currOptResultsTab, currOptFixedResultsTab, currSelectedOptResultsTab;
        protected OptimizationResult currOptResult, currOptFixedResult, currSelectedOptResult;

        protected string[] resultTypeNames = { "Optimal", "Optimal mit Fixierungen","Differenz"};

        protected string[] resultsColumnNames ={   
                                                   "Optimierungsakte",
                                                   "Zeit",
                                                   "Benutzername",
                                                   "Szenario",
                                                   "Type",
                                                    "Produktionskosten",
                                                    "IC-Transportkosten",	
                                                    "Kundentransportkosten",	
                                                    "Lagerkosten gesamt",
                                                    "Gesamtkosten",
                                                    "mittlere Kapazitätsauslastung",
                                                    "Median Kapazitätsauslastung"
                                                };

        protected OptimizationRun currOptimizationRun,selectedOptimizationRun;
          //currDataRow = new string[10];
          //      currDataRow[0] = run.OptFolderName;//reducedOptRunDirCandidate.Substring(0, atPos);
          //      currDataRow[1] = run.TimeStamp.ToString(ISO_8601_FORMAT_STRING);//reducedOptRunDirCandidate.Substring(atPos + 1, byPos - atPos - 1);
          //      currDataRow[2] = run.UserName;//reducedOptRunDirCandidate.Substring(byPos + 2, reducedOptRunDirCandidate.Length - byPos - 2);

        protected void addResultRow(OptimizationResult optResult, OptimizationRun run,bool isFixed)
        {
            string[] resultRow;

            resultRow = new string[resultsColumnNames.Length];
            resultRow[0] = run.OptFolderName;
            resultRow[1] = run.TimeStamp.ToString(ISO_8601_FORMAT_STRING);
            resultRow[2] = run.UserName;
            resultRow[3] = run.ScenName;
            if (!isFixed)
                resultRow[4] = resultTypeNames[0];
            else
                resultRow[4] = resultTypeNames[1];

            resultRow[5] = optResult.ProductionCost.ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);  //Convert.ToString(Math.Round(optResult.ProductionCost, NUMBER_DIGITS_TO_DISPLAY), currC) + CURRENCY_SYMBOL;
            resultRow[6] = optResult.InterCompanyTranportCost.ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);//Convert.ToString(Math.Round(optResult.InterCompanyTranportCost, NUMBER_DIGITS_TO_DISPLAY), currC) + CURRENCY_SYMBOL;
            resultRow[7] = optResult.CustomerTransportCost.ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);//Convert.ToString(Math.Round(optResult.CustomerTransportCost, NUMBER_DIGITS_TO_DISPLAY), currC) + CURRENCY_SYMBOL;
            resultRow[8] = optResult.InventoryCost.ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);//Convert.ToString(Math.Round(optResult.InventoryCost, NUMBER_DIGITS_TO_DISPLAY), currC) + CURRENCY_SYMBOL;
            resultRow[9] = optResult.TotalCost.ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);//Convert.ToString(Math.Round(optResult.TotalCost, NUMBER_DIGITS_TO_DISPLAY), currC) + CURRENCY_SYMBOL;
            resultRow[10] = optResult.AverageCapacityUtilization.ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);//Convert.ToString(Math.Round(optResult.AverageCapacityUtilization, NUMBER_DIGITS_TO_DISPLAY), currC);
            resultRow[11] = optResult.MedianOfCapacityUtilization.ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);//Convert.ToString(Math.Round(optResult.MedianOfCapacityUtilization, NUMBER_DIGITS_TO_DISPLAY), currC);

            displayResultsTab.Rows.Add(resultRow);
        }

        protected void addResultDeltaRow(OptimizationResult result1, OptimizationResult result2)
        {
            string[] resultRow;

            resultRow = new string[resultsColumnNames.Length];
            resultRow[0] = "";
            resultRow[1] = "";
            resultRow[2] = "";
            resultRow[3] = "";
            resultRow[4] = resultTypeNames[2];
            resultRow[5] = (result1.ProductionCost- result2.ProductionCost).ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);  //Convert.ToString(Math.Round(optResult.ProductionCost, NUMBER_DIGITS_TO_DISPLAY), currC) + CURRENCY_SYMBOL;
            resultRow[6] = (result1.InterCompanyTranportCost-result2.InterCompanyTranportCost).ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);//Convert.ToString(Math.Round(optResult.InterCompanyTranportCost, NUMBER_DIGITS_TO_DISPLAY), currC) + CURRENCY_SYMBOL;
            resultRow[7] = (result1.CustomerTransportCost - result2.CustomerTransportCost).ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);//Convert.ToString(Math.Round(optResult.CustomerTransportCost, NUMBER_DIGITS_TO_DISPLAY), currC) + CURRENCY_SYMBOL;
            resultRow[8] = (result1.InventoryCost-result2.InventoryCost).ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);//Convert.ToString(Math.Round(optResult.InventoryCost, NUMBER_DIGITS_TO_DISPLAY), currC) + CURRENCY_SYMBOL;
            resultRow[9] = (result1.TotalCost-result2.TotalCost).ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);//Convert.ToString(Math.Round(optResult.TotalCost, NUMBER_DIGITS_TO_DISPLAY), currC) + CURRENCY_SYMBOL;
            resultRow[10] = (result1.AverageCapacityUtilization - result2.AverageCapacityUtilization).ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);//Convert.ToString(Math.Round(optResult.AverageCapacityUtilization, NUMBER_DIGITS_TO_DISPLAY), currC);
            resultRow[11] = (result1.MedianOfCapacityUtilization-result2.MedianOfCapacityUtilization).ToString("N" + NUMBER_DIGITS_TO_DISPLAY.ToString(), currC);//Convert.ToString(Math.Round(optResult.MedianOfCapacityUtilization, NUMBER_DIGITS_TO_DISPLAY), currC);

            displayResultsTab.Rows.Add(resultRow);
        }

        protected void visualizeResults()
        {
            displayResultsTab.Clear();
            
            if (currOptResult!= null && ((currSelectedOptResult !=null && currSelectedOptResult.FolderName ==currOptResult.FolderName)|| currSelectedOptResult ==null))
            {
                if (currOptFixedResult != null)
                    addResultRow(currOptFixedResult, currOptimizationRun, true);
                 addResultRow(currOptResult, currOptimizationRun,false);
                
                if (currOptFixedResult !=null)
                {
                    addResultDeltaRow (currOptFixedResult ,currOptResult );
                }
            }
            else
            {
                if (currOptResult !=null)
                {
                    addResultRow(currOptResult, currOptimizationRun,false);   
               }

                if (currSelectedOptResult != null)
                {
                    addResultRow(currSelectedOptResult, selectedOptimizationRun, false);
                }

                if (currOptResult !=null && currSelectedOptResult !=null)
                    addResultDeltaRow (currOptResult ,currSelectedOptResult );
            }
        }

        public void fillResults(bool isFixed,bool isGerman,bool isSelected,bool isQuietError)
        {
            string usedResultFileName;

            if (dGVOptResults.InvokeRequired)
            {
                dGVOptResults.Invoke(new loadResultsMethod(fillResults), isFixed, isGerman,isSelected,isQuietError );
            }
            else
            {
                try
                {
                    if (isFixed)
                    {
                        if (isGerman)
                            usedResultFileName = fixResultFileNameGER;
                        else
                            usedResultFileName = fixResultFileNameENGL;
                    }
                    else
                    {
                        if (!isSelected)
                        {
                            //isFixed = false;
                            currOptFixedResult = null;
                            currSelectedOptResult = null;
                        }
                        if (isGerman)
                            usedResultFileName = optResultFileNameGER;
                        else
                            usedResultFileName = optResultFileNameENGL;
                    }

                   

                    try
                    {
                       

                        if (isSelected)
                        {
                            usedResultFileName = selectedOptimizationFolder + Path.DirectorySeparatorChar + usedResultFileName;//Environment.CurrentDirectory + Path.DirectorySeparatorChar + ResultsSubDir + Path.DirectorySeparatorChar +  usedResultFileName;
                            currSelectedOptResult = loadOptResult(usedResultFileName,isGerman );
                        }
                        else if (isFixed)
                        {
                            usedResultFileName = usedOptimizationFolder + Path.DirectorySeparatorChar + usedResultFileName;
                            currOptFixedResult = loadOptResult(usedResultFileName, isGerman);
                        }
                        else
                        {
                            usedResultFileName = usedOptimizationFolder + Path.DirectorySeparatorChar + usedResultFileName;
                            //Environment.CurrentDirectory + Path.DirectorySeparatorChar + ResultsSubDir + Path.DirectorySeparatorChar +  usedResultFileName;
                            currOptResult = loadOptResult(usedResultFileName, isGerman);
                            
                        }

                        
                    }
                    catch (Exception e)
                    {
                        currSelectedOptResult = null;
                        if (!isQuietError)
                            throw e;
                    }

                    visualizeResults();
                    //CSV_File resultData = new CSV_File(usedResultFileName);
                    //resultData.IsHeadLine=true;
                    //resultData.CSVDelimiter = csvDelim;
                    //resultData.openForReading();

                    //if (!isFixed)
                    //    currOptResultsTab = resultData.getData();
                    //else
                    //    currOptFixedResultsTab = resultData.getData();//resultData.TableToDataTable();

                    //resultData.closeDoc();
                   
                   // optResults.setTable(currDTab);

                    //dGVOptResults.DataSource = optResults.getFilteredData();
                }
                catch (Exception e)
                {
                    if (!isQuietError)
                    {
                        MessageBox.Show("Fehler beim Laden der Ergebnisse" + e.Message, "",
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.None);
                    }
                }

            }


        }
        public void outputMessage(LoggerMsgType type, string msg, int msgId)
        {
            if (tbLogOutput.InvokeRequired)
            {
                tbLogOutput.Invoke(new logMethod(outputMessage), type, msg, msgId);
            }
            else
            {
                tbLogOutput.AppendText("\n");
                tbLogOutput.AppendText(msg);
            }
        }
       

        private void tbDblQty_TextChanged(object sender, EventArgs e)
        {
            TextBox tbSender;

            tbSender = (TextBox)sender;

            try
            {
                if (tbSender.Text.Trim().Length > 0)
                    Convert.ToDouble(tbSender.Text, currC);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Keine reelle Zahl.", "",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.None);

                tbSender.Text = "";
            }
        }

        private BaseData_CheckResult checkBaseDataFolder(string folderName)
        {
            BaseData_CheckResult cR;
            string currReducedFileCandidate;
            int i;
            string spNameSecondPart;
            string startMStr, endMStr;
            int dashIndex;
            int dotIndex;
            cR = new BaseData_CheckResult();

            cR.fCR  = fileCheck(baseDataMainPath +Path.DirectorySeparatorChar + folderName);

             //protected static string[] fileNameBases = { "WareHouseCodes", 
             //                                       "Plantlocations", 
             //                                       "Produktionstonnage",
             //                                       "Arbeitspläne und Stammdaten",
             //                                       "Fehlende_AP_für_SalesArtikel",
             //                                       "sales plan",
             //                                       "Schichten",
             //                                       "HistoricalTransports_NorthEastern_Europe",
             //                                       "HistoricalTransports_SouthWestern_Europe",
             //                                       "Freight_cost_matrix",
             //                                       "Produktionskosten",
             //                                       "SSI Datei_Lagerbestand",
             //                                       "Mindestbestand",
             //                                       "Zuordnung_Ladeort_zu_Produktionsort"
             //                                     };

            if (cR.fCR[13].fS == File_State.EXISTS)
            {
              
                cR.startMonth = -1;
                cR.endMonth = -1;

                spNameSecondPart = cR.fCR [13].fileName.Substring(displayAndFileNameBases[13,1].Length).Trim();//(fileNameBases [13].Length  ).Trim ();
                dashIndex = spNameSecondPart.IndexOf("-");
                dotIndex = spNameSecondPart.IndexOf(".");

                if (dotIndex != -1)
                    spNameSecondPart = spNameSecondPart.Substring(0, dotIndex);

                if (spNameSecondPart.Length == 5 && dashIndex == 2)
                {
                    startMStr = spNameSecondPart.Substring(0, 2);
                    endMStr = spNameSecondPart.Substring(dashIndex + 1, 2);

                    try
                    {
                        cR.startMonth = Convert.ToInt32(startMStr) - 1;
                        cR.endMonth = Convert.ToInt32(endMStr) - 1;
                    }
                    catch (Exception e)
                    {
                        cR.startMonth = -1;
                        cR.endMonth = -1;
                    }
                }

                if (cR.startMonth >= 0 && cR.startMonth <= 11 && cR.endMonth >= 0 && cR.endMonth <= 11)
                {
                    cR.salesPlanNameStatus = BaseData_CheckResult.SalesPlanCheckResult.OK;
                }
                else
                    cR.salesPlanNameStatus = BaseData_CheckResult.SalesPlanCheckResult.WRONG_FORMAT;
            }
            

            //foreach (string dirCandidate in Directory.GetFiles (optimizationBasePath))
            //{
            //    currReducedFileCandidate = trimLongPath(dirCandidate);//dirCandidate.Substring(dirCandidate.LastIndexOf(Path.DirectorySeparatorChar)+1);

            //    if (currReducedFileCandidate == locationTranslationFilename)
            //        cR.fileExistence[0] = true;
            //   else if (currReducedFileCandidate ==whseSouthWestEuropeFilename)
            //       cR.fileExistence[1] = true;
            //   else if (currReducedFileCandidate == tonnageFileName)
            //       cR.fileExistence[2] = true;
            //   else if (currReducedFileCandidate == baseDataAndWorkingPlansFileName)
            //       cR.fileExistence[3] = true;
            //   else if (currReducedFileCandidate == additionalBaseDataAndWorkingPlansFileName)
            //       cR.fileExistence[4] = true;
            //    else if (currReducedFileCandidate == shiftsFileName)
            //        cR.fileExistence[5] = true;
            //    else if (currReducedFileCandidate == transpFileName)
            //        cR.fileExistence[6] = true;
            //    else if (currReducedFileCandidate == transpSouthWestEuropeFileName)
            //        cR.fileExistence[7] = true;
            //   else if (currReducedFileCandidate.Contains(demandFileNameBase))
            //   {
            //       if (cR.fileExistence[8])
            //           cR.salesPlan_Status = BaseData_CheckResult.SalesPlanCheckResult.MULTIPLE_SALEPLANS;
            //       else
            //       {
            //           string spNameSecondPart;
            //           string startMStr, endMStr;
            //           cR.fileExistence[8] = true;
            //           int dashIndex;
            //           int dotIndex;
            //           cR.startMonth = -1;
            //           cR.endMonth = -1;
                      
            //           spNameSecondPart = currReducedFileCandidate.Substring(demandfileName.Length);
            //           dashIndex = spNameSecondPart.IndexOf("-");
            //           dotIndex=spNameSecondPart .IndexOf(".");

            //           if (dotIndex!=-1)
            //                spNameSecondPart= spNameSecondPart.Substring (0,dotIndex );

            //           if (spNameSecondPart.Length == 5 && dashIndex==2)
            //           {
            //               startMStr = spNameSecondPart.Substring(0, 2);
            //               endMStr = spNameSecondPart.Substring(dashIndex + 1, 2);

            //               try
            //               {
            //                   cR.startMonth = Convert.ToInt32 (startMStr)-1;
            //                   cR.endMonth = Convert.ToInt32 (endMStr)-1;
            //               }
            //               catch (Exception e)
            //               {
            //                   cR.startMonth=-1;
            //                   cR.endMonth=-1;
            //               }
            //           }

            //           if (cR.startMonth >= 0 && cR.startMonth <= 11 && cR.endMonth >= 0 && cR.endMonth <= 11)
            //           {
            //               cR.salesPlan_Status = BaseData_CheckResult.SalesPlanCheckResult.OK;
            //               cR.usedDemandFileName = currReducedFileCandidate;
            //           }
            //           else
            //               cR.salesPlan_Status = BaseData_CheckResult.SalesPlanCheckResult.WRONG_FORMAT;
            //       }
            //   } 
               
            //}

            return cR;
        }

        //private double getFolderContentSize(string folderPath)
        //{
        //    int i;
        //    string size;
        //    string[] filenames;
        //    FileInfo currFileInfo;
        //    filenames= Directory.GetFiles(folderPath, "*.*", SearchOption.AllDirectories);

        //    size = 0;
        //    for (i = 0; i < filenames.Length; i++)
        //    {
        //        currFileInfo = new FileInfo(filenames[i]);

        //        size += currFileInfo.Length;
        //    }

        //    return size;
        //}

        private OptimizationFolderInfo isOptimizationFolder(string folderName)
        {
            string folderPath = optimizationBasePath + Path.DirectorySeparatorChar + folderName;
            OptimizationFolderInfo cI;
            DirectoryInfo currDirInfo;
            FileInfo [] files;
            int i;

            cI = new OptimizationFolderInfo();

            try
            {
                if (Directory.Exists(folderPath) &&
                    Directory.Exists(folderPath + Path.DirectorySeparatorChar + optimizationBaseDataFolderName) &&
                    Directory.Exists(folderPath + Path.DirectorySeparatorChar + ResultsSubDir) &&
                    Directory.Exists(folderPath + Path.DirectorySeparatorChar + loadedDataSubDirectory)
                    )
                    //return true;
                    cI.IsOptimizationFolder = true;
                else
                    cI.IsOptimizationFolder = false;
                    //return false;
                if (cI.IsOptimizationFolder)
                {
                    //cI.FolderContentSize = getFolderContentSize(folderPath);
                    cI.FolderName = folderName;

                    currDirInfo = new DirectoryInfo(folderPath);
                    files = currDirInfo.GetFiles("*.*", SearchOption.AllDirectories);
                    cI.FolderContentSize = 0;
                    for (i = 0; i < files.Length; i++)
                    {
                        cI.FolderContentSize += files[i].Length;
                    }
                    cI.creationTime = currDirInfo.CreationTime;
                    cI.lastWriteTime = currDirInfo.LastWriteTime;
                }
            }
            catch (Exception e)
            {
                GUI_ExceptionHandler(new OptimizerException(OptimizerException.OptimizerExceptionType.UnknownError, e.Message));
                cI.IsOptimizationFolder = false;
                //return false;
            }

            return cI;
        }

        private bool makeOptimizationFolder(string folderName)
        {
            string folderPath = optimizationBasePath + Path.DirectorySeparatorChar + folderName;
            try
            {

                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                if (!Directory.Exists(folderPath + Path.DirectorySeparatorChar + optimizationBaseDataFolderName))
                {
                    Directory.CreateDirectory(folderPath + Path.DirectorySeparatorChar + optimizationBaseDataFolderName);
                }

                if (!Directory.Exists(folderPath + Path.DirectorySeparatorChar + ResultsSubDir))
                {
                    Directory.CreateDirectory(folderPath + Path.DirectorySeparatorChar + ResultsSubDir);
                }

                if (!Directory.Exists(folderPath + Path.DirectorySeparatorChar + loadedDataSubDirectory))
                {
                    Directory.CreateDirectory(folderPath + Path.DirectorySeparatorChar + loadedDataSubDirectory);
                }
                loadOptFolderList(false);

                return true;
            }
            catch (Exception e)
            {
                GUI_ExceptionHandler(new OptimizerException(OptimizerException.OptimizerExceptionType.UnknownError, e.Message));
                return false;
            }

            
        }
        protected class OptimizationFolderInfo
        {
            public string FolderName;
            public long FolderContentSize;
            public bool IsOptimizationFolder;
            public DateTime creationTime;
            public DateTime lastWriteTime;
        }
        private List<OptimizationFolderInfo> existingOptimizationFolders()
        {
            List<OptimizationFolderInfo> optimizationFolders;
            string reducedDirCandidate;
            optimizationFolders = null;
            OptimizationFolderInfo currOFI;

            try
            {
                optimizationFolders = new List<OptimizationFolderInfo>();

                foreach (string dirCandidate in Directory.GetDirectories(optimizationBasePath))
                {
                    reducedDirCandidate = trimLongPath(dirCandidate);//dirCandidate.Substring(dirCandidate.LastIndexOf(Path.DirectorySeparatorChar)+1);

                    currOFI = isOptimizationFolder(reducedDirCandidate);

                    if (currOFI.IsOptimizationFolder)
                    {
                        optimizationFolders.Add(currOFI );
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
               // GUI_ExceptionHandler(new OptimizerException(OptimizerException.OptimizerExceptionType.UnknownError, e.Message));
                //Application.Exit();

            }
           return optimizationFolders;
        }

        protected bool isOptFoldersExceedCriticalSize=false;

        protected long maxDiskSpaceForOptFolders = 1 * 1000 * 1000;// 1024 * 1024 * 500;

        protected delegate void returnVoidBoolParam(bool b);
        private void loadOptFolderList(bool isCheckSize)
        {
            long totalDiskSpaceUsedForOptFolders;
            OptimizationFolderInfo oldestFolder;
            List<OptimizationFolderInfo> optFolders;
            DialogResult isDeleteFolder;
            bool isAbort=false;

            if (lbExistingFolders.InvokeRequired)
                lbExistingFolders.Invoke(new returnVoidBoolParam(loadOptFolderList), isCheckSize);
            else
            {
                try
                {

                    do
                    {
                        isOptFoldersExceedCriticalSize = false;
                        optFolders = existingOptimizationFolders(); ;
                        usedOptimizationFolder = "";
                        lbExistingFolders.Items.Clear();
                        totalDiskSpaceUsedForOptFolders = 0;
                        oldestFolder = null;
                        foreach (OptimizationFolderInfo folder in optFolders)
                        {
                            lbExistingFolders.Items.Add(folder.FolderName);
                            totalDiskSpaceUsedForOptFolders += folder.FolderContentSize;
                            if (oldestFolder == null || oldestFolder.creationTime > folder.creationTime)
                                oldestFolder = folder;
                        }


                        if (oldestFolder != null)
                        {
                            if (isCheckSize && totalDiskSpaceUsedForOptFolders > maxDiskSpaceForOptFolders)
                            {
                                isOptFoldersExceedCriticalSize = true;
                                isDeleteFolder = MessageBox.Show("Optimierungsakten benötigen " +
                                                                    totalDiskSpaceUsedForOptFolders.ToString("N", currC) +
                                                                    " Bytes aber es sind maximal " +
                                                                    maxDiskSpaceForOptFolders.ToString("N", currC) + " erlaubt. Möchten Sie den ältesten Ordner " +
                                oldestFolder.FolderName + " erstellt am " + oldestFolder.creationTime.ToString(ISO_8601_FORMAT_STRING) + " löschen?", "",
                                                      MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.None);

                                if (isDeleteFolder == DialogResult.No)
                                    isAbort = true;
                                else
                                {
                                    Directory.Delete(optimizationBasePath + Path.DirectorySeparatorChar + oldestFolder.FolderName, true);
                                }
                            }
                        }
                    }
                    while (!isAbort && isOptFoldersExceedCriticalSize);
                }
                catch (Exception e)
                {
                    GUI_ExceptionHandler(new OptimizerException(OptimizerException.OptimizerExceptionType.UnknownError, e.Message));
                    Close();
                }
            }

        }

        protected Dictionary<string, int> baseDataFoldersDb;
        protected const string sql_ROOT_ALL="Select * from IN_BASEDATA_ROOT";
        private void getBaseDataFoldersFromDB()
        {
            DataTable bdFolderTab;
            int i;

            baseDataFoldersDb=new Dictionary<string,int> ();

            bdFolderTab = usedDB.getData(sql_ROOT_ALL);

            for (i=0;i<bdFolderTab .Rows.Count ;i++)
               baseDataFoldersDb.Add (bdFolderTab .Rows [i].ItemArray [0].ToString().Trim (),Convert.ToInt32 (bdFolderTab .Rows [i].ItemArray [3].ToString()));
        }


        private void loadBaseDataFolderList()
        {
            string [] baseDataFolders;
            //list<string> baseDataFolders = existingBaseDataFolders();s

            try
            {
                //getBaseDataFoldersFromDB();
                //foreach (KeyValuePair<string, int> bF in baseDataFoldersDb)
                  //  lbBaseData.Items.Add(bF.Key);
                baseDataFolders = Directory.GetDirectories(baseDataMainPath);
                foreach (string dirCandidate in baseDataFolders)
                {
                    lbBaseData.Items.Add(trimLongPath(dirCandidate));
                }
            }
            catch (Exception e)
            {
                GUI_ExceptionHandler(new OptimizerException(OptimizerException.OptimizerExceptionType.UnknownError, e.Message));
                Application.Exit ();
            }
        }
        protected string distanceDataPath;
        private void loadGUI_CFG()
        {
            CultureInfo usC;
            DataTable guiCfg;
            CSV_File guiCfgFile;

           // try
           // {
                usC = CultureInfo.CreateSpecificCulture("en-US");

                guiCfgFile = new CSV_File(guiCfgFileName);
                guiCfgFile.IsHeadLine = true;
                guiCfgFile.openForReading();
                guiCfgFile.CSVDelimiter = ";";
                guiCfg = guiCfgFile.getData();
                guiCfgFile.closeDoc();
                if (guiCfg.Columns.Count != 5)
                    throw new OptimizerException(OptimizerException.OptimizerExceptionType.ConfigurationError, "Falsche Anzahl Spalten in Konfigurationsdatei.");

                baseDataMainPath = Path.GetFullPath(guiCfg.Rows[0].ItemArray[0].ToString().Trim());//makeRelativePathAbsolute(guiCfg .Rows [0].ItemArray [0].ToString ().Trim ());
                optimizationBasePath = Path.GetFullPath(guiCfg.Rows[0].ItemArray[1].ToString().Trim());//makeRelativePathAbsolute ( guiCfg.Rows [0].ItemArray [1].ToString ().Trim());
                maxDiskSpaceForOptFolders = Convert.ToInt64(guiCfg.Rows[0].ItemArray[2].ToString().Trim(), usC);
                maxNumberOfThreads = Convert.ToInt32(guiCfg.Rows[0].ItemArray[3].ToString().Trim(), usC);
                distanceDataPath = Path.GetFullPath(guiCfg.Rows[0].ItemArray[4].ToString().Trim());
            //}
          


        }
        Dictionary<string, OptimizationRunConfig> currentRunConfigs;
      
        protected void setDefaultInputValues()
        {
            tbMinProdQty.Text = Convert.ToString(defaultMinLotSize, currC);
            tbSplittingQty.Text = Convert.ToString(defaultMinRunTime, currC);
            tbMinInvFactor.Text = Convert.ToString(defaultFactorMinInventory, currC);
            tbCapFactor.Text = Convert.ToString(defaultFactorResourceCapacities, currC);
            cbBasketUsage.Checked  = defaultIsUseBaskets;
            cbFixPlants.Checked = defaultIsFixPlants;
            cbUseFixationsExclusions.Checked = defaultUseFixationsExlusions;
            cbIsOnlyProductionOptimization.Checked  = defaultIsOnlyProductionOptimization;
            allowExternalDistanceDataInput(defaultIsUseExternalData);
            tbTranspCostFix.Text = Convert.ToString(defaultFixedTransportCostPart, currC);
            tbTranspCostVar.Text = Convert.ToString(defaultVariableTransportCostPart, currC);
            tbMaxGap.Text = Convert.ToString(defaultMaxGap, currC);
            cbOfflineMode.Checked = defaultIsOfflineMode;
            cBCheckCountryMissingTransportConnectionsCustomer.Checked = defaultCheckCountryForTransportRelations;
        }
        //protected DataTable optRunConfigTab;
        protected DataTable optRunTab;
        //protected ODBCDataBase usedDB;
        //public void createTableFromSpec(string tableName, string[,] tabSpec)
        //{
        //    int i, tL;
        //    string sqlCmdStr = "create table " + tableName + " (";
        //    tL = tabSpec.GetLength(0);
        //    for (i = 0; i < tL; i++)
        //    {
        //        sqlCmdStr += tabSpec[i, 0] + " " + tabSpec[i, 1];
        //        if (i < tL - 1)
        //            sqlCmdStr += ",";
        //    }
        //    sqlCmdStr += ")";

        //    usedInMemDB.execSqlCommand(sqlCmdStr);
        //}

        //public void clearTable(string tableName)
        //{
        //    string sqlCmdStr = "delete from " + tableName;
        //    usedInMemDB.execSqlCommand(sqlCmdStr);
        //}

        //public void dropTable(string tableName)
        //{
        //    string sqlCmdStr = "drop table " + tableName;
        //    usedInMemDB.execSqlCommand(sqlCmdStr);
        //}

        //public void createAllTables()
        //{
        //    createTableFromSpec(customerTabName, customerTabSpec);
        //    createTableFromSpec(productTabName, productTabSpec);
        //    createTableFromSpec(periodTabName, periodTabSpec);
        //    createTableFromSpec(demandTabName, demandTabSpec);
        //    createTableFromSpec(customerChainDemandTabName, customerChainDemandTabSpec);
        //    createTableFromSpec(demandFractionsTabName, demandFractionsTabSpec);
        //    createTableFromSpec(productionDetailsTabName, productionDetailTabSpec);
        //    createTableFromSpec(productionSummTabName, productionSummTabSpec);
        //}

        //public void clearAllDemandTables()
        //{
        //    clearTable(customerTabName);
        //    clearTable(productTabName);
        //    clearTable(periodTabName);
        //    clearTable(demandTabName);
        //    clearTable(customerChainDemandTabName);
        //    clearTable(demandFractionsTabName);
        //}

        protected GoogleDistanceMatrixSource distanceSource;

        private FilterTableCollection dataFTC;
        /*
        public string TableName;

        public bool IsUseSQLCmd;
        public string BaseSQLCmd;

        public bool IsUseCalculatedTable;
        public string CalcTableName;

        public bool IsUseDimensions;

        public bool IsDistinctRows;
        public string addFilterExpression = "";
        */

        //protected string[] fullDemandTabGlobalSpec2 = { "fullDemTab" ,
        //                                                 "true",
        //                                                 "Select Product,Period,Customer,Country || ' '|| PostalCode as Location,DemandQuantity, demTab.PeriodIndex,demTab.ScenarioIndex " +
        //                                                 "from IN_DEMAND demTab " +
        //                                                 "join IN_PRODUCT prdTab on demTab.ProductIndex=prdTab.ProductIndex " +
        //                                                 "join IN_CUSTOMER cTab on cTab.CustomerIndex=demTab.CustomerIndex " +
        //                                                 "join IN_PERIOD perTab on perTab.PeriodIndex=demTab.PeriodIndex",
        //                                                 "false",
        //                                                 "",
        //                                                 "false",
        //                                                 "false",
        //                                                 ""
        //                                             };
        /*
        public class ColumnSpec
        {
            public enum ColAggrType
            {
                SUM,
                AVG,
                COUNT,
                MIN,
                MAX,
                NONE
            }
            public string ColumnName;
            public string SQL_Type;
            public bool isVisibleColumn;
            public bool isDimensionColumn;
            public bool isNonDimensionColumn;
            public bool isSortColumn;
            public bool isFilteredColumn;
            public bool isFilterEntryColumn;
            public string NewColumnName;
            public ColAggrType AggrType;
             public string DateTimeFormat = "%Y-%m-%dT%H:%M:%S";
            public bool isZeroForNull = false;
           
           
        }*/
      
        protected string[,] fullDemandTabColumnSpec={
                                                        {"Product",FilterTableCollection .TableSpec .SQL_TEXT  ,"true","false","false","true","true","true","Product","",""},
                                                        {"Period",FilterTableCollection .TableSpec .SQL_TEXT ,"true","false","false","true","true","true","Period","",""},
                                                        {"Customer",FilterTableCollection .TableSpec .SQL_TEXT ,"true","false","false","true","true","true","Customer","",""},
                                                        {"Location",FilterTableCollection .TableSpec .SQL_TEXT ,"true","false","false","true","true","true","Location","",""},
                                                        {"DemandQuantity",FilterTableCollection .TableSpec .SQL_DOUBLE ,"true","false","false","true","true","true","DemandQuantity","",""},
                                                        {"PeriodIndex",FilterTableCollection .TableSpec .SQL_INTEGER  ,"true","false","false","true","true","true","PeriodIndex","",""},
                                                        {"ScenarioIndex",FilterTableCollection .TableSpec .SQL_INTEGER ,"true","false","false","true","true","true","ScenarioIndex","",""}
                                                    };
        protected string[,] fullDemandTabJoins = { };

        //protected string fullDemandTabName = "fullDemTab";
        //protected string[,] fullDemandTabSpec = { {"Product", "text","true","false","false"},
        //                                          {"Period", "text","true","false","false"},
        //                                           {"Customer", "text","true","false","false"},
        //                                           {"Location", "text","true","false","false"},
        //                                          { "DemandQuantity", "double","true","false","false"},
        //                                          { "PeriodIndex", "int","true","false","false"}
        //                                          //{ "ScenarioIndex", "int","true","false","false"}
        //                                          };
        //protected string fullDemandTabSQLCMD = "Select Product,Period,Customer,Country || ' '|| PostalCode as Location,DemandQuantity, demTab.PeriodIndex,demTab.ScenarioIndex " +
        //                                     "from IN_DEMAND demTab " +
        //                                      "join IN_PRODUCT prdTab on demTab.ProductIndex=prdTab.ProductIndex " +
        //                                      "join IN_CUSTOMER cTab on cTab.CustomerIndex=demTab.CustomerIndex " +
        //                                      "join IN_PERIOD perTab on perTab.PeriodIndex=demTab.PeriodIndex";

        protected string[] custDemandTabGlobalSpec={
                                                   };
        protected string[,] custDemandTabColumnSpec={
                                                    };
        protected string[,] custDemandTabJoins = { };

        //protected string fullDemandTabSQLCMD = "select Product,'M1' as Period,Customer,'DE' || ' '|| '97080' as Location,111 as DemandQuantity, 1 as PeriodIndex, 0 as ScenarioIndex "
        //                                        +"from IN_DEMAND demTab "
        //                                        +"join IN_PRODUCT prdTab on demTab.ProductIndex=prdTab.ProductIndex "
        //                                        +"join IN_CUSTOMER cTab on cTab.CustomerIndex=demTab.CustomerIndex "
        //                                        ;

        //protected string custDemandTabName = "custDemTab";

        //protected string[,] custDemandTabSpec = { {"Product", "text","true","true","false"},
        //                                          {"Period", "text","true","true","false"},
        //                                           {"Customer", "text","true","true","false"},
        //                                           {"Location", "text","true","false","false"},
        //                                          { "DemandQuantity", "double","true","false","true"},
        //                                          { "PeriodIndex", "int","false","true","false"},
        //                                          { "ScenarioIndex", "int","true","false","false"}
        //                                          };
        //protected string[,] demandTabSpec = { {"CustomerIndex", "int","true","false","false"},
        //                                        {"ProductIndex", "int","true","false","false"},
        //                                          { "PeriodIndex", "int","true","false","false"},
        // { "DemandQuantity", "double","true","false","false"},
        //                                          { "ScenarioIndex", "int","true","false","false"}
        //                                          };
        
        //protected string productTabName = "IN_PRODUCT";
        //protected string[,] productTabSpec = { { "Product", "text","true","false","false"},
        //                                          { "ProductIndex", "int","true","false","false"}
        //                                          };
        //protected string customerTabName = "IN_CUSTOMER";
        //protected string[,] customerTabSpec = { {"Customer", "text","true","false","false"},
        //                                          { "CustomerIndex", "int","true","false","false"},
        //                                          {"Country","text","true","false","false"},
        //                                          {"PostalCode","text","true","false","false"}
        //                                          };
        //protected string periodTabName = "IN_PERIOD";
        //protected string[,] periodTabSpec = { { "Period", "text","true","false","false"},
        //                                          { "PeriodIndex", "int","true","false","false"}
        //                                          };

        //protected string demandTabName = "IN_DEMAND";
        //protected string[,] demandTabSpec = { {"CustomerIndex", "int","true","false","false"},
        //                                        {"ProductIndex", "int","true","false","false"},
        //                                          { "PeriodIndex", "int","true","false","false"},
        //                                          { "ScenarioIndex", "int","true","false","false"}
        //                                          };

        //protected string demandTabName = "IN_DEMAND";
        //protected string[,] demandTabSpec = { {"CustomerIndex", "int","true","false","false"},
        //                                        {"ProductIndex", "int","true","false","false"},
        //                                          { "PeriodIndex", "int","true","false","false"},
        //                                          { "ScenarioIndex", "int","true","false","false"}
        //                                          };

        //protected string[,] rawTabSQLSpec = { { "Product", "text","true","false","false"},
        //                                     { "WGruppe", "text","true","false","false"},
        //                                     {"MonthName","text","false","false","false"},
        //                                     { "Date", "datetime","true","true","false"},
        //                                     { "Quantity", "double","true","false","true"},
        //                                     {"Covariate","double","true","false","true"}
        //                                 };

        //FilterTableCollection.TableSpec getTabSpechFromString(string[,] sTabSpec, string sTabSQL, bool isUseDim)
        //{
        //    FilterTableCollection.TableSpec tabSpec;
        //    FilterTableCollection.ColumnSpec[] sTabSQLColSpecs;
        //    int i;

        //    tabSpec = new FilterTableCollection.TableSpec();

        //    sTabSQLColSpecs = new FilterTableCollection.ColumnSpec[sTabSpec.GetLength(0)];

        //    for (i = 0; i < sTabSpec.GetLength(0); i++)
        //    {
        //        sTabSQLColSpecs[i] = new FilterTableCollection.ColumnSpec();
        //        sTabSQLColSpecs[i].ColumnName = sTabSpec[i, 0];
        //        sTabSQLColSpecs[i].SQL_Type = sTabSpec[i, 1];

        //        if (sTabSpec[i, 2] == "true")
        //            sTabSQLColSpecs[i].isVisibleColumn = true;
        //        else
        //            sTabSQLColSpecs[i].isVisibleColumn = false;

        //        if (sTabSpec[i, 3] == "true")
        //            sTabSQLColSpecs[i].isDimensionColumn = true;
        //        else
        //            sTabSQLColSpecs[i].isDimensionColumn = false;

        //        if (sTabSpec[i, 4] == "true")
        //            sTabSQLColSpecs[i].isNonDimensionColumn = true;
        //        else
        //            sTabSQLColSpecs[i].isNonDimensionColumn = false;
        //    }

        //    tabSpec.ColumnSpecs = sTabSQLColSpecs;//rawTabSQLSpec;
        //    tabSpec.BaseSQLCmd = sTabSQL;
        //    tabSpec.isUseDimensions = isUseDim;
        //    return tabSpec;
        //}
       
        //ScenGen usedScenGenerator;
        private void Form1_Load(object sender, EventArgs e)
        {
            int i;
            string currentBasePath;
            //FilterTableCollection.ListViewAdapter currLVA;
            //FilterTableCollection.TableSpec fDTabSpec,cDTabSpec;
            Logger currLogger;
            //pbLogo.ImageLocation = "logo.png";
            //pbLogo.SizeMode = PictureBoxSizeMode.AutoSize;
            //this.WindowState = FormWindowState.Maximized;
            
            try
            {
                //usedInMemDB = new SQLLiteDataBase(SQLLiteDataBase.IN_MEMORY_DB);
                //usedInMemDB.openConn();
                //createAllTables();
                selectedProduct = new List<string>();
                selectedCustomer = new List<string>();
                selectedLocation = new List<string>();
                //demandDataFTC = new FilterTableCollection(usedInMemDB, true);
                //currLVA = new FilterTableCollection.ListViewAdapter(lVScenCustomers);
                //demandDataFTC.addSelectionAdapter(usedSelectionGroup, "Customer", currLVA);//("Customer", currLVA);
                //currLVA = new FilterTableCollection.ListViewAdapter(lVScenLocations);
                //demandDataFTC.addSelectionAdapter(usedSelectionGroup, "Location", currLVA);
                //currLVA = new FilterTableCollection.ListViewAdapter(lVScenProducts);
                //demandDataFTC.addSelectionAdapter(usedSelectionGroup, "Product", currLVA);

                //fDTabSpec = new FilterTableCollection.TableSpec();//getTabSpechFromString(fullDemandTabSpec, fullDemandTabSQLCMD, false);
                //fDTabSpec.fillTableSpecFromString(fullDemandTabGlobalSpec, fullDemandTabColumnSpec, fullDemandTabJoins);

                //cDTabSpec = new FilterTableCollection.TableSpec();//getTabSpechFromString(custDemandTabSpec, fullDemandTabSQLCMD, true);
                //cDTabSpec.fillTableSpecFromString(custDemandTabGlobalSpec, custDemandTabColumnSpec, custDemandTabJoins);

                //demandDataFTC.addTable(fDTabSpec);
                //demandDataFTC.addTable(cDTabSpec);

                btnDataLoad.Text = dataLoadTaskName;
                btnRunOptimization.Text = runOptimizationTaskName;
                //usedDB = new ODBCDataBase(DSNStr);
                //usedDB.openConn();
                dataLoadStopSignalMut = new Mutex();
                clMut = new Mutex();
                loadGUI_CFG();
                //usedLDCP = new LinearDistanceCostProvider(0, 1.8);
                //setCurrentPath(Environment.CurrentDirectory);
                currC = CultureInfo.CurrentCulture;
                //currentPath = Environment.CurrentDirectory;
                ///currC.EnglishName
                currentUserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                //browseFolder.SelectedPath = baseDataDefaultPath;
                //tbUsedPath.Text = currentPath;
                //optResults = new TableWithSelection();

                dGVOptResults.DataSource = null;// optResults.getFilteredData();
                dGVOptResults.AutoGenerateColumns = true;
                usedLoggers = new LoggerCollection();
                currLogger = this;
                usedLoggers.addLogger(currLogger);


                currentBasePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + networkplanningBaseDirectory;
                loadedDataPath = currentBasePath + Path.DirectorySeparatorChar + loadedDataSubDirectory + Path.DirectorySeparatorChar;
                resultsBasePath = currentBasePath + Path.DirectorySeparatorChar + ResultsSubDir + Path.DirectorySeparatorChar;

                distanceSource = new GoogleDistanceMatrixSource();
                distanceSource.FilePath = distanceDataPath;
                distanceSource.loadExistingDistances();
                distanceSource.Distance_Logger = usedLoggers;
                isActionRunning = false;
                isQuietMessages = false;

                lbInvalidSaleplan.Text = "";
                tbBeginn.Text = "";
                tbEnde.Text = "";
                lbUsedBaseDataFolder.Text = "";
                lbCulture.Text = currC.DisplayName;
                baseDataFolderOk = false;
                isOptimizationGiven = false;
                isBaseDataElementSelected = false;
                //lbReplication.Text = "0";
                //updateOptimizationRuns();

                lblUserName.Text = currentUserName;

                lbOptimizationRunning.Text = "";
                //lbOptizationWithFixationsRunning.Text = "";


                currentOptimizationRuns = new List<OptimizationRun>();
                currentRunConfigs = new Dictionary<string, OptimizationRunConfig>();

                //optRunConfigTab = new DataTable();
                optRunTab = new DataTable();
                displayResultsTab = new DataTable();


                loadOptFolderList(true);
                loadBaseDataFolderList();
                checkBtnBaseDataLoadAndEdit();
                allowOptimizationActions(false);
                allowOpenInputs(false);
                allowOpenResults(false);
                allowVisualizeResults(false);
                for (i = 0; i < optRunIdentifierNames.Length; i++)
                {
                    optRunTab.Columns.Add(optRunIdentifierNames[i]);
                    //optRunConfigTab.Columns.Add(optRunIdentifierNames[i]);
                }

                //for (i = 0; i < optimizationSettingsColumns.Length; i++)
                //  optRunConfigTab.Columns.Add(optimizationSettingsColumns[i]);

                for (i = 0; i < resultsColumnNames.Length; i++)
                    displayResultsTab.Columns.Add(resultsColumnNames[i]);

                //dGVOptimizationRunConfigs.DataSource = optRunConfigTab;
                dGVOptimizationRuns.DataSource = optRunTab;
                dGVOptResults.DataSource = displayResultsTab;
                setDefaultInputValues();
                isReadyToClose = true;
                IsCloseRequested = false;

                cLBCheckResult.Items.Clear();
                for (i = 0; i < displayAndFileNameBases.GetLength(0); i++)
                {
                    cLBCheckResult.Items.Add(displayAndFileNameBases[i, 0]);
                }
                currOptResult = null;

                //usedScenGenerator =new ScenGen (usedInMemDB);
                //  SizeF t = new SizeF(2, 2);
                // this.AutoScaleDimensions = t;

                //cmBScenType.Items.Clear();
                //foreach (string sT in scenarioTypes)
                //{
                //    cmBScenType.Items.Add(sT);
                //}
                //cmBScenType.SelectedIndex = (Int32)ScenGen.Scenario.ScenType.TOTAL;
                //cmBDistributionType.Items.Clear();
                //foreach (string dT in distributionTypes)
                //{
                //    cmBDistributionType.Items.Add(dT);
                //}
                //cmBDistributionType.SelectedIndex = (Int32)ScenGen.DistributionType.UNIFORM;

                foreach (string currBuildTitle in buildTitles )
                {
                    cmBBuild .Items .Add(currBuildTitle );
                }

                cmBBuild .SelectedIndex =(Int32) BuildType .BUILD_1;

                selectedTabName = tCInput.SelectedTab.Name;
            }
            catch (OptimizerException exc)
            {
                GUI_ExceptionHandler(exc);
                Close();
            }
            catch (Exception exc)
            {
                GUI_ExceptionHandler(new OptimizerException(OptimizerException.OptimizerExceptionType.UnknownError, exc.Message));
                //Application.Exit();
                Close();
            }
        }

        public void setOptimizationRunState(OptimizationRunState oRST, Int32 currentIteration)
        {
            if (lbOptimizationRunning.InvokeRequired)
            {
                lbOptimizationRunning.Invoke(new feedBackOptimizerProc (setOptimizationRunState), oRST, currentIteration);
            }
            else
            {
                switch (oRST)
                {
                    case OptimizationRunState.STOPPED:
                        lbOptimizationRunning.Text = "Abgebrochen";
                        //lbReplication.Text = Convert.ToString(0, currC);
                        break;
                    case OptimizationRunState.RUNNING:
                        lbOptimizationRunning.Text = "In Ausführung";
                        //lbReplication.Text = Convert.ToString(currentIteration, currC);
                        break;
                    case OptimizationRunState.COMPLETED:
                        lbOptimizationRunning.Text = "Abgeschlossen";
                        break;
                }
            }
        }

        //private void setLbOptimizationRunningText(string txt)
        //{
        //    if (lbOptimizationRunning.InvokeRequired)
        //    {
        //        lbOptimizationRunning.Invoke(new textSetProc(setLbOptimizationRunningText), txt);
        //    }
        //    else
        //    {
        //        lbOptimizationRunning.Text = txt;
        //    }
        //}



        //private void setLbOptimizationWithFixationsRunningText(string txt)
        //{
        //    if (lbOptizationWithFixationsRunning .InvokeRequired )
        //    {
        //        lbOptizationWithFixationsRunning.Invoke(new textSetProc(setLbOptimizationWithFixationsRunningText), txt);
        //    }
        //    else
        //    {
        //        lbOptizationWithFixationsRunning.Text = txt;
        //    }
        //}

        private void checkBtnBaseDataLoadAndEdit()
        {
            btnDataLoad.Enabled = baseDataFolderOk && isOptimizationGiven ;
            //lbBaseDataToEdit .Enabled  = baseDataFolderOk && isOptimizationGiven;
            cLBCheckResult .Enabled  = baseDataFolderOk & isOptimizationGiven;
            btnEditBaseData.Enabled = baseDataFolderOk && isOptimizationGiven && isBaseDataElementSelected;
            btnInputOpen.Enabled = baseDataFolderOk && isOptimizationGiven && isBaseDataElementSelected;
        }

        private string getFinalFilePath(string fileName)
        {
            if (System.IO.File.Exists(optimizationBaseDataFolderPath + baseDataFolder + Path.DirectorySeparatorChar + fileName))
                return optimizationBaseDataFolderPath+baseDataFolder+Path.DirectorySeparatorChar + fileName;
            else
                return baseDataMainPath +baseDataFolder + Path.DirectorySeparatorChar +fileName;
        }

        private void showDLProperties(string optimizationFolder)
        {
            string loadPath = optimizationBasePath + tbOptimizationName.Text + Path.DirectorySeparatorChar + loadedDataSubDirectory + Path.DirectorySeparatorChar;
            CSV_File dlPropertiesFile;
            
            DataTable dlProperties;

            dlPropertiesFile = new CSV_File(optimizationBasePath + tbOptimizationName.Text + Path.DirectorySeparatorChar + loadedDataSubDirectory + Path.DirectorySeparatorChar + dataLoadPropertiesFileName);
            dlPropertiesFile.CSVDelimiter = outputSeparator;
            dlPropertiesFile.openForReading();
            dlPropertiesFile .IsHeadLine =true;

            dlProperties = dlPropertiesFile.getData();

            lbUsedBaseDataFolder.Text = dlProperties.Rows[0].ItemArray[0].ToString();
            dlPropertiesFile.closeDoc();
        }

        Mutex dataLoadStopSignalMut;
        bool isDataLoadStop;

        private bool DataLoadStopSignal 
        {
            get
            {
                bool s;
                dataLoadStopSignalMut.WaitOne();
                s = isDataLoadStop;
                dataLoadStopSignalMut.ReleaseMutex();

                return s;
            }
            set
            {
                dataLoadStopSignalMut.WaitOne();
                isDataLoadStop = value;
                dataLoadStopSignalMut.ReleaseMutex();
            }

        }

        private void stopDataLoad()
        {
            DataLoadStopSignal = true;

            if (dXLSWepa != null)
                dXLSWepa.stopExecution();
        }

        void checkDataLoadStop()
        {
            if (DataLoadStopSignal)
                throw new Optimizer_Exceptions.OptimizerException(OptimizerException.OptimizerExceptionType.ExecutionStoppedOnUserRequest, "");
        }

        private OptimizerDataSource getDataSource(int itemIndex)
        {
            string fullFilename;
            string sqlStr;
            fullFilename=optimizationBaseDataFolderPath + baseDataFolder+Path.DirectorySeparatorChar+ dataTypeDBTable[itemIndex, 3] ;
            if (File.Exists(fullFilename))
            {
                return new Excel_Table(fullFilename, dataTypeDBTable[itemIndex, 2]);
            }
            else
            {
                sqlStr = "Select " + dataTypeDBTable[itemIndex, 1] + " from " + dataTypeDBTable[itemIndex, 2] + " where BaseData_FolderNumber=" + usedFL;
                return new SQLTable(usedDB, sqlStr);
            }
        }

       // Mutex dXLSWepaMut;
        protected SNP_DataProvider dXLSWepa;
        protected ODBCDataBase usedDB;
        //protected const string sql_ROUTINGS = "select * from IN_ROUTING where BaseData_FolderNumber={0}";
        //protected const string sql_ICTRANSPORTS = "select * from IN_INTERCOMPANY_TRANSPORTS where BaseData_FolderNumber={0}";
        //protected const string sql_HTNE = "select * from IN_HISTORICAL_TRANSPORTS_NE where BaseData_FolderNumber={0}";
        //protected const string sql_HTSW = "select * from IN_HISTORICAL_TRANSPORTS_SW where BaseData_FolderNumber={0}";
        //protected const string sql_PL = "select * from IN_PLANTLOCATIONS where BaseData_FolderNumber={0}";
        //protected const string sql_MACHINES = "select * from IN_MACHINES where BaseData_FolderNumber={0}";
        //protected const string sql_HP = "select * from IN_HISTORICAL_PRODUCTION where BaseData_FolderNumber={0}";
        //protected const string sql_DEMAND = "select * from IN_DEMAND where BaseData_FolderNumber={0}";
        //protected const string sql_PI = "select * from IN_PRODUCT_INFO where BaseData_FolderNumber={0}";
        //protected const string sql_SHIFTS = "select * from IN_SHIFTS where BaseData_FolderNumber={0}";
        //protected const string sql_IS = "select * from IN_INITIAL_STOCK where BaseData_FolderNumber={0}";
        //protected const string sql_WCSW = "select * from IN_WAREHOUSE_CODE_SW where BaseData_FolderNumber={0}";
        //protected const string sql_SLPL = "select * from IN_SOURCELOCATION_PRODUCTIONLOCATION where BaseData_FolderNumber={0}";
        protected const string sql_ROOT = "select BaseData_FolderName,StartPeriod,EndPeriod from IN_BASEDATA_ROOT where BaseData_FolderNumber={0}";
        protected string usedFL = "0";
        static string DSNStr = "DSN=SNP_Database_Demo";

        private void dataLoad()
        {
            //Data_Excel_Wepa dXLSWepa = new Data_Excel_Wepa();
            //SNP_WEPA_Instance pI;
            //SNP_CPLEX_Optimizer usedOptimizer;
            ////if (args.Length > 0)
            ////    dXLSWepa.SrcPath = args[0];
            ////else

            ////    dXLSWepa.SrcPath = Environment.CurrentDirectory;// "C:\\Max-Con\\WEPA";//"C:\\Max-Con\\WEPA";

            //dXLSWepa.setLoggers(usedLoggers);
            //dXLSWepa.SrcPath = currentPath;

            ////pI = dXLSWepa.getProblemInstance();
            //pI = dXLSWepa.getProblemInstance(whseSouthWestEuropeFilename, locationTranslationFilename, tonnageFileName, baseDataAndWorkingPlansFileName, additionalBaseDataAndWorkingPlansFileName,
            //        demandFileName, shiftsFileName, transpFileName, transpSouthWestEuropeFileName, interPlantTranspFileName, costPerShiftFileName, costPerTonFileName,
            //        customerChainTransportCustomerRelationsFileName, tourChainFilename
            //        );


            //// pI = new SNP_WEPA_Instance();
            //usedOptimizer = new SNP_CPLEX_Optimizer();
            //usedOptimizer.setProblemInstance(ref pI);
            //usedOptimizer.optimize();
            //actionCompleted(0);
            //isActionRunning = false;

            CSV_File dataLoadPropertiesFile;

            LoggerCollection lC = new LoggerCollection();
            //Logger cLog = new ScreenLogger();

            
            SNP_WEPA_Instance pI;
            //SNP_CPLEX_Optimizer usedOptimizer;
            OptimizerException.OptimizerExceptionType et;
            //string userDirectory;
          
            //userDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            ///userDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string[] dlProperties;
            Optimizer_Data.CSV_File plantfile, customerfile, resourcefile, productfile, periodfile, processfile,
                                    capacityfile, transportplantfile, transportcustomerfile, demandfile, palletspertonfile,
                                    basketsfile, histProdFile, iniStockFile, warehouseCapacitiesFile, articleNamesFile,customerFixationsFile,customerProductRelatedTransportsFile;

            //dXLSWepaMut.WaitOne();
            dXLSWepa = new SNP_DataProvider();
            dXLSWepa.setLoggers(lC);
            dXLSWepa.ProgressLogger = this;
            //dXLSWepaMut.ReleaseMutex();

            lC.addLogger(this);
           
            //if (args.Length > 0)
            //    dXLSWepa.SrcPath = args[0];
            //else
            //dXLSWepa.SrcPath = baseDataDefaultPath;//currentPath;//Environment .CurrentDirectory ;// "C:\\Max-Con\\WEPA";//"C:\\Max-Con\\WEPA";
           
            try
            {
                if (!System.IO.Directory.Exists(loadedDataPath))
                {
                    System.IO.Directory.CreateDirectory(loadedDataPath);
                }

                checkDataLoadStop();
              
                //Generic_DataLoader gDL=new Generic_DataLoader ();
                //gDL.Root = new SQLTable(usedDB, String.Format(sql_ROOT, usedFL));
                //gDL.Demand = new SQLTable(usedDB, String.Format (sql_DEMAND,usedFL));
                //gDL.HistoricalProduction = new SQLTable(usedDB, String.Format(sql_HP  , usedFL));
                //gDL.HistoricalTransportsNE = new SQLTable(usedDB, String.Format(sql_HTNE , usedFL));
                //gDL.HistoricalTransportsSW = new SQLTable(usedDB, String.Format(sql_HTSW , usedFL));
                //gDL.InitialStock = new SQLTable(usedDB, String.Format(sql_IS , usedFL));
                //gDL.IntercompanyTransports = new SQLTable(usedDB, String.Format(sql_ICTRANSPORTS , usedFL));
                //gDL.Machines = new SQLTable(usedDB, String.Format(sql_MACHINES , usedFL));
                //gDL.PlantLocations = new SQLTable(usedDB, String.Format(sql_PL , usedFL));
                //gDL.ProductInfo = new SQLTable(usedDB, String.Format(sql_PI, usedFL));
                //gDL.Routings = new SQLTable(usedDB, String.Format(sql_ROUTINGS , usedFL));
                //gDL.Shift = new SQLTable(usedDB, String.Format(sql_SHIFTS , usedFL));
                //gDL.SourceLocationProductionLocation = new SQLTable(usedDB, String.Format(sql_SLPL, usedFL));
                //gDL.WarehouseCodeSW = new SQLTable(usedDB, String.Format(sql_WCSW, usedFL));
                //string[,] dataTypeDBTable ={ 
                // 0   {"Produktionsstandorte","LocationName,Country,PostalCode,WarehouseCapacity","IN_PLANTLOCATIONS","plantlocations.xlsx"},
                // 1   {"Lagercodes","WarehouseCode,Country,PostalCode,LocationName","IN_WAREHOUSE_CODE_SW","warehousecodessw.xlsx"},
                // 2   {"Historische Produktiontonnagen","MAC,Product_Id","IN_HISTORICAL_PRODUCTION","historicalProduction.xlsx"},
                //  3  {"Arbeitspläne","Product_Id,MAC,TonPerShift","IN_ROUTING","routings.xlsx"},
                //  4  {"Schichten","Period,MAC,NumberOfShifts","IN_SHIFTS","shifts.xlsx"},
                //  5  {"Maschinen","MAC,Location,CostPerHour","IN_MACHINES","machines.xlsx"},
                //  6  {"Produkte","Product_Id,Product_Description,KgPerPallet","IN_PRODUCT_INFO","products.xlsx"},
                //  7  {"Historische Transporte Nord-Ost-Europa","Transport_Type,Tour_Id,Product_Id,SourceCountry,SourcePostalCode,SourceLocationName,DestCountry,DestPostalCode,DestLocationName,Customer,NumberOfPallets,Price","IN_HISTORICAL_TRANSPORTS_NE","transportsNE.xlsx"},
                //  8  {"Historische Transporte Süd-West-Europa","WarehouseCode,DateOfExpedition,DestCountry,DestPostalCode,Customer,Product_Id,NumberOfPallets,Price","IN_HISTORICAL_TRANSPORTS_SW","transportsSW.xlsx"},
                //  9  {"Frachtkosten","Plant_Start,Plant_End,Cost","IN_INTERCOMPANY_TRANSPORTS","ictransports.xlsx"},
                //  10  {"Anfangslagerbestand","Product_Id,LocationName,Quantity","IN_INITIAL_STOCK","initialstocks.xlsx"},
                //  11  {"Zuordnung Verladeort zu Produktionsort","SourceLocationName,ProductionLocationName","IN_SOURCELOCATION_PRODUCTIONLOCATION","slpl.xlsx"},
                //   12     {"Verkaufsplan","Product_Id,Customer,Period,Country,MAC,Quantity","IN_DEMAND","demands.xlsx"},
                //                   };
                //gDL.Demand = getDataSource(12);
                //gDL.HistoricalProduction = getDataSource(2);
                //gDL.HistoricalTransportsNE = getDataSource(7);//new SQLTable(usedDB, String.Format(sql_HTNE , usedFL));
                //gDL.HistoricalTransportsSW = getDataSource(8);//new SQLTable(usedDB, String.Format(sql_HTSW , usedFL));
                //gDL.InitialStock = getDataSource(10);//new SQLTable(usedDB, String.Format(sql_IS , usedFL));
                //gDL.IntercompanyTransports = getDataSource(9);//new SQLTable(usedDB, String.Format(sql_ICTRANSPORTS , usedFL));
                //gDL.Machines = getDataSource(5);//new SQLTable(usedDB, String.Format(sql_MACHINES , usedFL));
                //gDL.PlantLocations = getDataSource(0);//new SQLTable(usedDB, String.Format(sql_PL , usedFL));
                //gDL.ProductInfo = getDataSource(6);//new SQLTable(usedDB, String.Format(sql_PI, usedFL));
                //gDL.Routings = getDataSource(3);//new SQLTable(usedDB, String.Format(sql_ROUTINGS , usedFL));
                //gDL.Shift = getDataSource(4);//new SQLTable(usedDB, String.Format(sql_SHIFTS , usedFL));
                //gDL.SourceLocationProductionLocation = getDataSource(11);//new SQLTable(usedDB, String.Format(sql_SLPL, usedFL));
                //gDL.WarehouseCodeSW = getDataSource(1);//new SQLTable(usedDB, String.Format(sql_WCSW, usedFL));

                DataLoader_XLS xlsDL;
               // DataLoader[] usedDLs;
                xlsDL = new DataLoader_XLS();
                //usedDLs = new DataLoader[1];

             //   usedDLs[0] = xlsDL;

                xlsDL.setFileNames(getFinalFilePath(whseSouthWestEuropeFilename),
                                                 getFinalFilePath(locationTranslationFilename),
                                                 getFinalFilePath(tonnageFileName),
                                                 getFinalFilePath(baseDataAndWorkingPlansFileName),
                                                 getFinalFilePath(additionalBaseDataAndWorkingPlansFileName),
                                                 getFinalFilePath(salesPlanFileName),
                                                 getFinalFilePath(shiftsFileName),
                                                 getFinalFilePath(transpFileName),
                                                 getFinalFilePath(transpSouthWestEuropeFileName),
                                                 getFinalFilePath(interPlantTranspFileName),
                                                 getFinalFilePath(costPerShiftFileName),
                                                 getFinalFilePath(iniStocksFilename),
                                                 getFinalFilePath(minimumStocksFilename),
                                                  getFinalFilePath(transportStartSubstituteFileName),
                                                  getFinalFilePath(whseCapFileName),
                                                  getFinalFilePath(fixationFileName),
                                                  getFinalFilePath(exclusionFileName)
                                                 );
                
                pI = dXLSWepa.getProblemInstance(xlsDL,
                                                    loadedDataPath + productsNotConsideredFileName,
                                                  loadedDataPath + processesNotConsideredFileName,
                                                 loadedDataPath + resourcesNotConsieredFileName, 
                                                 loadedDataPath + missingTransportsFileName,
                                                 loadedDataPath + customerProductsInfeasibleBasketsFileName,
                                                 loadedDataPath + fixationExclusionProductionProductsFileName,
                                                 loadedDataPath + fixationExclusionTransportationProductsFileName,
                                                 maxNumberOfThreads,
                                                 cBCheckCountryMissingTransportConnectionsCustomer .Checked,
                                                 distanceSource,
                                                 cBExternalDistanceData.Checked,
                                                 usedLDCP
                                                 );

                //getFinalFilePath(customerChainTransportCustomerRelationsFileName), 
                //getFinalFilePath(tourChainFilename), 
                //pI = dXLSWepa.getProblemInstance(getFinalFilePath(whseSouthWestEuropeFilename), 
                //                                 getFinalFilePath(locationTranslationFilename), 
                //                                 getFinalFilePath(tonnageFileName), 
                //                                 getFinalFilePath(baseDataAndWorkingPlansFileName), 
                //                                 getFinalFilePath(additionalBaseDataAndWorkingPlansFileName),
                //                                 getFinalFilePath(salesPlanFileName), 
                //                                 getFinalFilePath(shiftsFileName), 
                //                                 getFinalFilePath(transpFileName), 
                //                                 getFinalFilePath(transpSouthWestEuropeFileName), 
                //                                 getFinalFilePath(interPlantTranspFileName), 
                //                                 getFinalFilePath(costPerShiftFileName),
                //                                 getFinalFilePath(iniStocksFilename), 
                //                                 getFinalFilePath(minimumStocksFilename),
                //                                  getFinalFilePath(transportStartSubstituteFileName),
                //                                  getFinalFilePath(whseCapFileName ),
                //                                  loadedDataPath+productsNotConsideredFileName,
                //                                  loadedDataPath + processesNotConsideredFileName,
                //                                 loadedDataPath + resourcesNotConsieredFileName
                  //  );

                plantfile = new Optimizer_Data.CSV_File(loadedDataPath+plantfileName);
                plantfile.CSVDelimiter = outputSeparator;
                plantfile.openForWriting(false);

                customerfile = new Optimizer_Data.CSV_File(loadedDataPath+customerfileName);
                customerfile.CSVDelimiter = outputSeparator;
                customerfile.openForWriting(false);

                resourcefile = new Optimizer_Data.CSV_File(loadedDataPath+resourcefileName);
                resourcefile.CSVDelimiter = outputSeparator;
                resourcefile.openForWriting(false);

                productfile = new Optimizer_Data.CSV_File(loadedDataPath+productfileName);
                productfile.CSVDelimiter = outputSeparator;
                productfile.openForWriting(false);

                periodfile = new Optimizer_Data.CSV_File(loadedDataPath+periodfileName);
                periodfile.CSVDelimiter = outputSeparator;
                periodfile.openForWriting(false);

                processfile = new Optimizer_Data.CSV_File(loadedDataPath+processfileName);
                processfile.CSVDelimiter = outputSeparator;
                processfile.openForWriting(false);

                capacityfile = new Optimizer_Data.CSV_File(loadedDataPath+capacityfileName);
                capacityfile.CSVDelimiter = outputSeparator;
                capacityfile.openForWriting(false);

                transportplantfile = new Optimizer_Data.CSV_File(loadedDataPath+transportplantfileName);
                transportplantfile.CSVDelimiter = outputSeparator;
                transportplantfile.openForWriting(false);

                transportcustomerfile = new Optimizer_Data.CSV_File(loadedDataPath+transportcustomerfileName);
                transportcustomerfile.CSVDelimiter = outputSeparator;
                transportcustomerfile.openForWriting(false);

                demandfile = new Optimizer_Data.CSV_File(loadedDataPath+demandfileName);
                demandfile.CSVDelimiter = outputSeparator;
                demandfile.openForWriting(false);

                palletspertonfile = new Optimizer_Data.CSV_File(loadedDataPath+palletspertonfileName);
                palletspertonfile.CSVDelimiter = outputSeparator;
                palletspertonfile.openForWriting(false);

                basketsfile = new Optimizer_Data.CSV_File(loadedDataPath+basketsfileName);
                basketsfile.CSVDelimiter = outputSeparator;
                basketsfile.openForWriting(false);

                histProdFile = new Optimizer_Data.CSV_File(loadedDataPath+histProdFileName);
                histProdFile.CSVDelimiter = outputSeparator;
                histProdFile.openForWriting(false);

                iniStockFile = new Optimizer_Data.CSV_File(loadedDataPath+iniStFileName);
                iniStockFile.CSVDelimiter = outputSeparator;
                iniStockFile.openForWriting(false);

                warehouseCapacitiesFile = new Optimizer_Data.CSV_File(loadedDataPath + warehouseCapacitiesFileName);
                warehouseCapacitiesFile.CSVDelimiter = outputSeparator;
                warehouseCapacitiesFile.openForWriting(false);

                articleNamesFile = new Optimizer_Data.CSV_File(loadedDataPath + articleNamesFileName);
                articleNamesFile.CSVDelimiter = outputSeparator;
                articleNamesFile.openForWriting(false);

                customerFixationsFile = new Optimizer_Data.CSV_File(loadedDataPath + customerFixationsFileName);
                customerFixationsFile.CSVDelimiter = outputSeparator;
                customerFixationsFile.openForWriting(false);

                customerProductRelatedTransportsFile = new Optimizer_Data.CSV_File(loadedDataPath + customerProductRelatedTransportationFixationsFileName);
                customerProductRelatedTransportsFile.CSVDelimiter = outputSeparator;
                customerProductRelatedTransportsFile.openForWriting(false);

                ProblemInstanceWriter.writeProblemInstance (pI,plantfile,customerfile ,resourcefile,productfile,periodfile,
                                                                processfile,capacityfile,transportplantfile ,transportcustomerfile,
                                                                demandfile, histProdFile, basketsfile, palletspertonfile, iniStockFile,
                                                                warehouseCapacitiesFile, articleNamesFile, customerFixationsFile, customerProductRelatedTransportsFile);

                plantfile.closeDoc();
                customerfile.closeDoc();
                resourcefile.closeDoc();
                productfile.closeDoc();
                periodfile.closeDoc();
                processfile.closeDoc();
                capacityfile.closeDoc();
                transportplantfile.closeDoc();
                transportcustomerfile.closeDoc();
                demandfile.closeDoc();
                histProdFile.closeDoc();
                basketsfile.closeDoc();
                palletspertonfile.closeDoc();
                iniStockFile.closeDoc();
                warehouseCapacitiesFile.closeDoc();
                articleNamesFile.closeDoc();
                customerFixationsFile.closeDoc();
                customerProductRelatedTransportsFile.closeDoc();
                dataLoadPropertiesFile = new CSV_File(loadedDataPath + dataLoadPropertiesFileName);
                dataLoadPropertiesFile.CSVDelimiter = outputSeparator;
                dataLoadPropertiesFile.openForWriting(false);

                DataTable dataLoadProperties;

                dataLoadProperties = new DataTable();
                int i;
                for (i = 0; i < dataLoadPropertiesColumns.Length; i++)
                    dataLoadProperties.Columns.Add(dataLoadPropertiesColumns[i]);

                dlProperties = new string[dataLoadPropertiesColumns.Length];
                dlProperties[0] = baseDataFolder;
                dataLoadProperties.Rows.Add(dlProperties);

                dataLoadPropertiesFile.writeData(dataLoadProperties);
                dataLoadPropertiesFile.closeDoc();

                loadOptFolderList(true);
                isDataLoaded = checkDataLoad(loadedDataPath);
                setDataLoadChecked(isDataLoaded);
                setUsedBaseDataFolder(baseDataFolder);
                dXLSWepa = null;
                allowOptimizationActions(true && !isOptFoldersExceedCriticalSize );
                actionCompleted(0,0);
            }
            catch (OptimizerException e)
            {
                loadOptFolderList(true);
                isDataLoaded = checkDataLoad(loadedDataPath);
                setDataLoadChecked(isDataLoaded);
                setUsedBaseDataFolder(baseDataFolder);
                dXLSWepa = null;
                GUI_ExceptionHandler(e);
                allowOptimizationActions(false);
                actionCompleted(0, 1);
             }
            
           // dXLSWepa.outputResults(ref pI);
           
            isActionRunning = false;

            dXLSWepa = null;
            GC.Collect();
            if (IsCloseRequested)
                closeWindow ();
        }
        protected void GUI_ExceptionHandler(OptimizerException e)
        {   
            string excText = "";

            if (!isQuietMessages)
            {   
                switch (e.GeneralReason)
                {
                    case OptimizerException.OptimizerExceptionType.InputFileNotFound:
                        excText = "Eingabedatei nicht gefunden";
                        break;
                    case OptimizerException.OptimizerExceptionType.ColumnNotFound:
                        excText = "Spalte(n) in Eingabedatei nicht gefunden";
                        break;
                    case OptimizerException.OptimizerExceptionType.ExecutionStoppedOnUserRequest:
                        excText = "Ausführung auf Benutzerwunsch abgebrochen";
                        break;
                    case OptimizerException.OptimizerExceptionType .ConfigurationError :
                        excText = "Problem mit der Konfiguration";
                        break;
                    case OptimizerException.OptimizerExceptionType.UnknownError:
                        excText = "Allgemeiner Fehler";
                        break;
                };

                excText += ": " + e.Details;

                MessageBox.Show(excText, excTitle,
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.Error);
            }
        }
        protected class Optimizer_Executer 
        {   
            //public double dummyCost;
            public string inputPath;
            public string outputPath;
            public string DemandFileName;

            public Form1 parent;
          
            public OptimizationRunConfig optRunC;
           
            public bool IsFixedProductionQties;
            //public double minProdQty;
            //public double splitQty;
            //public bool isAllowSplittingIfInFixations;
            //public double capacityFactorForMachines;
            //public double minInventoryFactor;
            //public bool isOnlyProductionOptimization;
            //public bool isUseBaskets;
            //public string optimizationName;
            Mutex mut;
            bool stopSignal;

            public Optimizer_Executer()
            {
                mut = new Mutex();
            }

            public bool StopSignal
            {
                get
                {
                    bool stopS;
                    mut.WaitOne();
                    stopS = stopSignal;
                    mut.ReleaseMutex();

                    return stopS;
                }
                set
                {
                    mut.WaitOne();
                    stopSignal = value;
                    mut.ReleaseMutex();
                }
            }

            protected int execProcess(string arguments,string fileName)
            {
                string stdOut;
                string prcsName;
                bool isRunning;
                Process[] pname;
                prcsName = fileName.Substring(0, fileName.IndexOf("."));


                using (Process procss = new Process())
                {
                    int exitCode;
                    int i;
                    procss.StartInfo.FileName = prcsName;

                    procss.StartInfo.WorkingDirectory = Environment.CurrentDirectory;
                    procss.StartInfo.Arguments = arguments;

                    procss.StartInfo.CreateNoWindow = true;
                    procss.StartInfo.UseShellExecute = false;
                    procss.StartInfo.RedirectStandardOutput = true;
                    procss.Start();
                    isRunning = false;
                    //Console.WriteLine(procss.Id);
                    do
                    {
                        pname = Process.GetProcessesByName(prcsName);
                        
                        stdOut = procss.StandardOutput.ReadLine();
                        if (stdOut != null)
                            parent.usedLoggers.outputMessage(LoggerMsgType.INFO_MSG, stdOut, 0);
                        isRunning = false;
                        for (i = 0; i < pname.Length; i++)
                        {
                            if (pname[i].SessionId == procss.SessionId)
                            {
                                isRunning = true;
                            }
                        }
                    }
                    while ((isRunning//pname.Length > 0 
                        || stdOut!=null) && !StopSignal);

                    if (StopSignal)
                    {
                        if (!procss .HasExited)
                        procss.Kill();
                        StopSignal = false;
                    }

                    procss.WaitForExit();
                    exitCode = procss.ExitCode;
                    procss.Refresh();
                    procss.Close();
                    return exitCode;
                }
            }
            protected ScenGen.Scenario usedScen;
            public void setScenario(ScenGen.Scenario scen)
            {
                usedScen = scen;
            }

            public void runMerger()
            {
                string germanEnglishCulture;
                string processArguments;
                int exitStatus;
                if (optRunC.isUseGermanCulture )
                    germanEnglishCulture = "1";
                else
                    germanEnglishCulture = "0";

                processArguments = "\"" + outputPath + "\"" + " " + "\"" + outputPath + "\"" + " " + germanEnglishCulture + " " + "\"" + outputPath + "\"";

                exitStatus=execProcess(processArguments, mergerFileName);
              //  parent.actionCompleted(3, exitStatus);
                // parent.isActionRunning = false;
            }

            
            public void runOptimizer()
            {
                string processArguments;
                string stdOut;
                string prcsName;
                Process[] pname;
                string optimizationMode;
                string germanEnglishCulture;
                string allowSplittingIfInFixations;
                string onlyProductionOptimization;
                string expSt;
                string fixPlants;
                string basketUsage;
                int exitStatusO, exitStatusF, exitStatusM;
                Int32 i;
                String pABaseO,pABaseF,pFinish;
                String usedScenDemandFileName;
                String useFixationsExclusions;
                try
                {
                    CultureInfo paramC = CultureInfo.CreateSpecificCulture("en-US");

                    if (optRunC.isUseBaskets)
                        basketUsage = "1";
                    else
                        basketUsage = "0";

                    //if (isFixedProductionQties)
                    //{
                    //    //parent.setLbOptimizationWithFixationsRunningText("läuft");
                    //    optimizationMode = "1";
                    //}
                    //else
                    //{
                        //parent.setLbOptimizationRunningText ("läuft");
                    
                        optimizationMode = "0";
                    //}
                    if (optRunC.isUseGermanCulture)
                        germanEnglishCulture = "1";
                    else
                        germanEnglishCulture = "0";

                    if (optRunC.isAllowSplittingIfInFixations)
                        allowSplittingIfInFixations = "1";
                    else
                        allowSplittingIfInFixations = "0";

                    if (optRunC.isOnlyProductionOptimization)
                        onlyProductionOptimization = "1";
                    else
                        onlyProductionOptimization = "0";

                    if (optRunC.isFixPlants)
                        fixPlants = "1";
                    else
                        fixPlants = "0";

                    if (optRunC.expansionStage == BuildType.BUILD_1)
                        expSt = "1";
                    else
                        expSt = "2a";

                    if (optRunC.isUseFixationsExlusions)
                        useFixationsExclusions = "1";
                    else
                        useFixationsExclusions = "0";

                    pABaseO = optimizationMode + " " + Convert.ToString(optRunC.minProdQty, paramC) + " " + Convert.ToString(optRunC.splitQty, paramC) + " " + Convert.ToString(optRunC.minInventoryFactor, paramC) + " " + Convert.ToString(optRunC.capacityFactorForMachines, paramC) + " " + allowSplittingIfInFixations + " " + onlyProductionOptimization + " " + fixPlants + " " + "\"" + inputPath + "\"" + " " + "\"" + outputPath + "\"" + " " + germanEnglishCulture + " " + basketUsage + " " + expSt + " ";
                    //processArguments = optimizationMode + " " + Convert.ToString(optRunC.minProdQty, paramC) + " " + Convert.ToString(optRunC.splitQty, paramC) + " " + Convert.ToString(optRunC.minInventoryFactor, paramC) + " " + Convert.ToString(optRunC.capacityFactorForMachines, paramC) + " " + allowSplittingIfInFixations + " " + onlyProductionOptimization + " " +fixPlants+" "+ "\"" + inputPath + "\"" + " " + "\"" + outputPath + "\"" + " " + germanEnglishCulture + " " + basketUsage+" "+expSt+" "+DemandFileName;// +" " + "\"" + optimizationName + "\"";
                    optimizationMode = "1";
                    pABaseF = processArguments = optimizationMode + " " + Convert.ToString(optRunC.minProdQty, paramC) + " " + Convert.ToString(optRunC.splitQty, paramC) + " " + Convert.ToString(optRunC.minInventoryFactor, paramC) + " " + Convert.ToString(optRunC.capacityFactorForMachines, paramC) + " " + allowSplittingIfInFixations + " " + onlyProductionOptimization + " " + fixPlants + " " + "\"" + inputPath + "\"" + " " + "\"" + outputPath + "\"" + " " + germanEnglishCulture + " " + basketUsage + " " + expSt + " ";// +DemandFileName;
                    pFinish = " " + Convert.ToString(optRunC.fixationThreshold, paramC) + " " + useFixationsExclusions + " " + Convert.ToString(optRunC.maxGap, paramC) ;
                    //if (usedScen == null)
                    //{
                        parent.setOptimizationRunState(OptimizationRunState.RUNNING, 1);
                        processArguments = pABaseO + demandfileName+pFinish;
                        exitStatusO = execProcess(processArguments, optimizerFileName);

                        if (IsFixedProductionQties && exitStatusO == 0)
                        {
                            //optimizationMode = "1";
                            //processArguments = optimizationMode + " " + Convert.ToString(optRunC.minProdQty, paramC) + " " + Convert.ToString(optRunC.splitQty, paramC) + " " + Convert.ToString(optRunC.minInventoryFactor, paramC) + " " + Convert.ToString(optRunC.capacityFactorForMachines, paramC) + " " + allowSplittingIfInFixations + " " + onlyProductionOptimization + " " + "\"" + inputPath + "\"" + " " + "\"" + outputPath + "\"" + " " + germanEnglishCulture + " " + basketUsage;// +" " + "\"" + optimizationName + "\"";
                            //processArguments = optimizationMode + " " + Convert.ToString(optRunC.minProdQty, paramC) + " " + Convert.ToString(optRunC.splitQty, paramC) + " " + Convert.ToString(optRunC.minInventoryFactor, paramC) + " " + Convert.ToString(optRunC.capacityFactorForMachines, paramC) + " " + allowSplittingIfInFixations + " " + onlyProductionOptimization + " " + fixPlants + " " + "\"" + inputPath + "\"" + " " + "\"" + outputPath + "\"" + " " + germanEnglishCulture + " " + basketUsage + " " + expSt + " " + DemandFileName;// +" " + "\"" + optimizationName + "\"";
                            //pFinish = " " + Convert.ToString(optRunC.fixationThreshold, paramC) + " " + "0";
                            processArguments = pABaseF + demandfileName+pFinish;

                            exitStatusF = execProcess(processArguments, optimizerFileName);
                            if (exitStatusF == 0)
                            {
                                processArguments = "\"" + outputPath + "\"" + " " + "\"" + outputPath + "\"" + " " + germanEnglishCulture + " " + "\"" + outputPath + "\"";

                                exitStatusM = execProcess(processArguments, mergerFileName);
                            }
                            else exitStatusM = 0;
                        }
                        else
                            exitStatusM = exitStatusF = 0;

                        if (exitStatusO == 0)
                        {
                            parent.updateOptimizationRuns();
                            parent.usedOptimizationFolder = outputPath;
                            //parent.setOutputSelectionOnCurrentRun();
                            parent.setOptimizationRunAsSelected();
                            parent.fillResults(false, optRunC.isUseGermanCulture, false, false);
                            if (exitStatusF == 0 && IsFixedProductionQties)
                                parent.fillResults(true, optRunC.isUseGermanCulture, false, false);

                            parent.switchToResultVisualization();
                            parent.allowOpenResults(true);
                            parent.allowVisualizeResults(true);
                        }
                        else
                        {
                            //parent.currOptResult = null;
                            Directory.Delete(parent.usedOptimizationFolder, true);
                            parent.visualizeResults();
                            parent.allowOpenResults(false);
                            parent.allowVisualizeResults(false);
                        }

                        if (exitStatusF != 0)
                            parent.actionCompleted(2, exitStatusF);
                        else if (exitStatusM != 0)
                            parent.actionCompleted(3, exitStatusM);
                        else
                            parent.actionCompleted(1, exitStatusO);
                    //}
                    //else
                    //{
                    //    exitStatusF = exitStatusM = exitStatusO = 0;

                    //    for (i = 0; i < usedScen.NumberOfReplications; i++)
                    //    {   
                    //        parent.setOptimizationRunState(OptimizationRunState.RUNNING, 1+i);
                    //        usedScenDemandFileName = parent.getScenDemandFileName(usedScen.Name, i);
                    //        processArguments = pABaseO + demandfileName+pFinish;
                    //        exitStatusO = execProcess(processArguments, optimizerFileName);

                    //        if (IsFixedProductionQties && exitStatusO == 0)
                    //        {
                    //            //optimizationMode = "1";
                    //            //processArguments = optimizationMode + " " + Convert.ToString(optRunC.minProdQty, paramC) + " " + Convert.ToString(optRunC.splitQty, paramC) + " " + Convert.ToString(optRunC.minInventoryFactor, paramC) + " " + Convert.ToString(optRunC.capacityFactorForMachines, paramC) + " " + allowSplittingIfInFixations + " " + onlyProductionOptimization + " " + "\"" + inputPath + "\"" + " " + "\"" + outputPath + "\"" + " " + germanEnglishCulture + " " + basketUsage;// +" " + "\"" + optimizationName + "\"";
                    //            //pABaseF = optimizationMode + " " + Convert.ToString(optRunC.minProdQty, paramC) + " " + Convert.ToString(optRunC.splitQty, paramC) + " " + Convert.ToString(optRunC.minInventoryFactor, paramC) + " " + Convert.ToString(optRunC.capacityFactorForMachines, paramC) + " " + allowSplittingIfInFixations + " " + onlyProductionOptimization + " " + fixPlants + " " + "\"" + inputPath + "\"" + " " + "\"" + outputPath + "\"" + " " + germanEnglishCulture + " " + basketUsage + " " + expSt + " " + DemandFileName;// +" " + "\"" + optimizationName + "\"";
                    //            processArguments = pABaseF + usedScenDemandFileName;
                    //            exitStatusF = execProcess(processArguments, optimizerFileName);
                    //            if (exitStatusF == 0)
                    //            {
                    //                processArguments = "\"" + outputPath + "\"" + " " + "\"" + outputPath + "\"" + " " + germanEnglishCulture + " " + "\"" + outputPath + "\"";

                    //                exitStatusM = execProcess(processArguments, mergerFileName);
                    //            }
                    //            else exitStatusM = 0;
                    //        }
                    //        else
                    //            exitStatusM = exitStatusF = 0;

                    //        if (exitStatusF != 0 || exitStatusM != 0 || exitStatusO != 0)
                    //            break;

                    //        parent.collectCurrentResultsForScenario(i);
                    //    }
                    //    usedScen.IsImported = false;
                    //    parent.importScenResults(usedScen);

                    //    if (exitStatusF != 0)
                    //        parent.actionCompleted(2, exitStatusF);
                    //    else if (exitStatusM != 0)
                    //        parent.actionCompleted(3, exitStatusM);
                    //    else
                    //        parent.actionCompleted(1, exitStatusO);
                    //}

                    //prcsName = optimizerFileName.Substring(0, optimizerFileName.IndexOf("."));
                    //using (Process procss = new Process())
                    //{
                    //    procss.StartInfo.FileName = optimizerFileName;

                    //    procss.StartInfo.WorkingDirectory = Environment.CurrentDirectory;
                    //    procss.StartInfo.Arguments = optimizationMode + " " + Convert.ToString(minProdQty, paramC) + " " + Convert.ToString(splitQty, paramC) + " " + Convert.ToString(dummyCost, paramC) + " " + Convert.ToString(capacityFactorForMachines, paramC) + " " + allowSplittingIfInFixations + " " + onlyProductionOptimization +" "+ inputPath + " " + outputPath + " " + germanEnglishCulture;

                    //    procss.StartInfo.CreateNoWindow = true;
                    //    procss.StartInfo.UseShellExecute = false;
                    //    procss.StartInfo.RedirectStandardOutput = true;
                    //    procss.Start();

                    //    //Console.WriteLine(procss.Id);
                    //    do
                    //    {
                    //        pname = Process.GetProcessesByName(prcsName);
                    //        stdOut = procss.StandardOutput.ReadLine();
                    //        if (stdOut!=null)
                    //            parent.usedLoggers.outputMessage(LoggerMsgType.INFO_MSG, ref stdOut, 0);
                    //    }
                    //    while (pname.Length > 0);
                    //    procss.WaitForExit();

                    //    procss.Refresh();
                    //    procss.Close();

                    //}
                   

                    //if (isFixedProductionQties)
                    //    parent.actionCompleted(2, exitStatus);
                    //else
                     
                    
                    //

                    
                }
                catch (Exception e)
                {
                    parent.GUI_ExceptionHandler(new  Optimizer_Exceptions.OptimizerException(OptimizerException.OptimizerExceptionType.UnknownError, e.Message));
                }
                //if (isFixedProductionQties )
                //    parent.setLbOptimizationWithFixationsRunningText("fertig");
                //else
                    //parent.setLbOptimizationRunningText ("fertig");
                parent.setOptimizationRunState(OptimizationRunState.COMPLETED, 0);

                parent.isActionRunning = false;
                if (parent.IsCloseRequested )
                    parent.closeWindow ();
                
            }
        }

        //public void setOutputSelectionOnCurrentRun()
        //{
        //    int i;
        //    string optFolderName;
        //    string currFolderName;

        //    if (dGVOptimizationRuns.InvokeRequired)
        //    {
        //       dGVOptimizationRuns.Invoke(new returnVoidWithoutParams(setOutputSelectionOnCurrentRun));
        //    }
        //    else
        //    {
        //        optFolderName = trimLongPath(usedOptimizationFolder);
        //        for (i = 0; i < optRunTab.Rows.Count; i++)
        //        {
        //            currFolderName = optRunTab.Rows[i].ItemArray[0] + "@" + optRunTab.Rows[i].ItemArray[1] + "By" + optRunTab.Rows[i].ItemArray[2];
        //            if (currFolderName == optFolderName)
        //            {
        //                dGVOptimizationRuns.CurrentCell = dGVOptimizationRuns.Rows[i].Cells[0];
        //            }
        //        }
        //    }
        //}

        public bool IsCloseRequested {

            get {
                bool iCR;
                clMut.WaitOne();
                iCR = isCloseRequested;

                clMut.ReleaseMutex();
                return iCR;
            }

            set
            {
                clMut.WaitOne();
                isCloseRequested = value;
                clMut.ReleaseMutex();
            }
        }

        public void switchToResultVisualization()
        {
            if (tCInput.InvokeRequired)
            {
                tCInput.Invoke(new returnVoidWithoutParams(switchToResultVisualization));
            }
            else
            {
                foreach (TabPage t in tCInput.TabPages)
                {
                    if (t.Text == "Ergebnis")
                        tCInput.SelectedTab = t;
                }
            }
        }

        public void actionCompleted(int actNum,int exitStatus)
        {
            string actionName;

            isReadyToClose = true;

            if (!isQuietMessages)
            {
                if (actNum == 0)
                    actionName = dataLoadTaskName;//btnDataLoad.Text;
                else if (actNum == 1)
                    actionName = runOptimizationTaskName;//btnRunOptimization.Text;
                else if (actNum == 2)
                    actionName = runOptimizationWithFixationTaskName;
                else if (actNum == 3)
                    actionName = mergeTaskName;
                //else if (actNum == 2)
                //    actionName = btnRunOptimizationFixedProdQties.Text;
                //else if (actNum == 3)
                //    actionName = btnMergeResults.Text;
                else
                    actionName = "";

                if (exitStatus == 0)
                {
                    MessageBox.Show("\"" + actionName + "\"" + " erfolgreich beendet.", "",
                                       MessageBoxButtons.OK,
                                       MessageBoxIcon.None);
                }
                else
                {
                    MessageBox.Show("\"" + actionName + "\"" + " nicht erfolgreich beendet. Bitte beachten Sie das Protokoll oder in der Ausführung aufgetretene Fehlermeldungen.", "",
                                       MessageBoxButtons.OK,
                                       MessageBoxIcon.None);
                }
            }
            isActionRunning = false;
        }

        bool checkOptimizationFolder(string folderName,bool isAskCreate)
        {
            DialogResult isCreateOptFolder;
            OptimizationFolderInfo currOFI;

            currOFI = isOptimizationFolder(folderName);
            if (!currOFI.IsOptimizationFolder)
            {
                if (!isAskCreate)
                    return false;

                isCreateOptFolder = MessageBox.Show("Optimierungsakte " + "\"" + folderName + "\"" + " existiert nicht. Soll sie angelegt werden ?", "",
                              MessageBoxButtons.YesNo,
                              MessageBoxIcon.None);

                if (isCreateOptFolder == DialogResult.Yes)
                {
                    if (!makeOptimizationFolder(folderName))
                        return false;
                }
                else
                    return false;
            }

            return true;
        }

        private string trimLongPath(string path)
        {
            int lI;
            lI=path.LastIndexOf(Path.DirectorySeparatorChar) ;
            if (lI!= -1)
                return path.Substring(lI + 1);
            else
                return path;
        }

        private string getPathFromLongFileName(string fileName)
        {
            int lI;
            lI = fileName.LastIndexOf(Path.DirectorySeparatorChar);
            if (lI != -1)
                return fileName.Substring(0, lI);//(lI + 1);
            else
                return "";
        }

        private void saveOptimizerSettings(string fileName, 
                                           bool isFixedProductionQties,
                                            double minProdQty,
                                            double splitQty,
                                            bool isAllowSplittingIfInFixations,
                                            bool isUseBaskets,
                                            double capacityFactorForMachines,
                                            bool isOnlyProductionOptimization,
                                            double minInventoryFactor
            )
        {
            CSV_File optimizationRunSettingsFile;
            DataTable confData;
            string [] paramValues;

            confData = new DataTable();

            optimizationRunSettingsFile = new CSV_File(fileName);
            //optimizationRunSettingsFile.HeadLine = optimizationSettingsHeader;
            optimizationRunSettingsFile.IsHeadLine = true;
            optimizationRunSettingsFile.openForWriting(false);

            paramValues = new string[8];

            if (isFixedProductionQties)
                paramValues[0] = "1";
            else
                paramValues[0] = "0";

            paramValues[1] = Convert.ToString(minProdQty, currC);

            paramValues[2] = Convert.ToString(splitQty, currC);

            if (isAllowSplittingIfInFixations)
                paramValues[3] = "1";
            else
                paramValues[3] = "0";

            if (isUseBaskets)
                paramValues[4] = "1";
            else
                paramValues[4] = "0";

            paramValues[5] = Convert.ToString(capacityFactorForMachines, currC);

            if (isOnlyProductionOptimization)
                paramValues[6] = "1";
            else
                paramValues[6] = "0";

            paramValues[7] = Convert.ToString(minInventoryFactor, currC);
            //optimizationRunSettingsFile.writeData (

            confData.Rows.Add(paramValues);
        }

         private string settingsFileName="settings.csv";

        private string [] optRunIdentifierNames={
                                                    "Optimierungsakte",
                                                    "Zeit",
                                                    "Benutzer",
                                                    "Szenario"
                                                };

        public void closeWindow()
        {
            if (InvokeRequired)
            {
                Invoke(new returnVoidWithoutParams(Close));
            }
            else
                Close();
        }
       

        private void loadOptimizationRunConfigs(Dictionary<string, OptimizationRunConfig> runConfigs,List<OptimizationRun > optRuns)
        {
            OptimizationRunConfig currConfig;
            string optimizationFullFilename;
            //Dictionary<string, OptimizationRunConfig> oRunConfigs;

            //oRunConfigs = new Dictionary<string, OptimizationRunConfig>();
            runConfigs.Clear();
            foreach (OptimizationRun oRun in optRuns)
            {
                currConfig = new OptimizationRunConfig();
                optimizationFullFilename = optimizationBasePath + oRun.OptFolderName  + Path.DirectorySeparatorChar + ResultsSubDir+Path.DirectorySeparatorChar +oRun.RunFolderName + Path.DirectorySeparatorChar + settingsFileName;
                currConfig.loadConfig(optimizationFullFilename);
                runConfigs.Add(oRun.RunFolderName, currConfig);
            }

            //return oRunConfigs;
        }
       

        //public void clearEntriesFromScenario(ScenGen .Scenario currScen)
        //{
        //    String sqlCMDBase = "delete from IN_PRODUCTION_DETAIL where ScenarioName='{0}'";

        //    usedInMemDB.execSqlCommand(String.Format(sqlCMDBase, currScen.Name));
        //}

        protected class StatVar
        {
            protected double mean;
            protected UInt32 numObs;
            protected double squaredMean;

            protected double normQuantile975Percent = 1.96;
            protected double[] tDistrQuantiles975Percent =
            {
                12.706, //1
                4.303,
                3.182,
                2.776,
                2.571,
                2.447,
                2.365,
                2.306,
                2.262,
                2.228,
                2.201,
                2.179,
                2.160,
                2.145,
                2.131,
                2.120,
                2.110,
                2.101,
                2.093,
                2.086,
                2.080,
                2.074,
                2.069,
                2.064,
                2.060,
                2.042   //30
            };
            public StatVar()
            {
            }

            public void reset()
            {
                numObs = 0;
                mean = squaredMean = 0;
            }

            public double getMean()
            {
                return mean / numObs;
            }

            public double getVar()
            {
                return (squaredMean - numObs * mean) / (numObs - 1);
            }

            public double getHalfWidth()
            {
                double usedQuantil;

                if (numObs == 0)
                    return 0;

                if (numObs <= 30)
                    usedQuantil = tDistrQuantiles975Percent[numObs - 1];
                else
                    usedQuantil = normQuantile975Percent;

                return Math.Sqrt(getVar()) * usedQuantil;
            }

            public void addObservation (Double obsVal)
            {
                mean+=obsVal ;
                squaredMean =obsVal *obsVal ;
                numObs ++;
            }
        }

        /*
         protected String[,] productionDetailTabSpec ={
                                                         {"OptimizationFolder","text"},
                                                         {"ScenarioName","text"},
                                                         {"ReplicationNumber","int"},
                                                         {"Artikelnummer","text"},
                                                         {"Kunden","text"},
                                                         {"Monat","text"},
                                                         {"Werk","text"},
                                                         {"Anlage","text"},
                                                         {"Menge","double"},
                                                         {"Anzahl Schichten","double"},
                                                         {"Produktionskosten","double"}
                                                    };
         * */

        protected class AggrScenEntry
        {
            public String OptimizationFolder;
            public String ScenarioName;
            public String ArticleNumber;
            public String Customer;
            public String Month;
            public String Plant;
            public String Machine;

            public StatVar NumberOfShifts;
            public StatVar ProductionCost;
            public StatVar Quantity;

            public AggrScenEntry()
            {
                NumberOfShifts = new StatVar();
                ProductionCost = new StatVar();
                Quantity = new StatVar();
            }

            public String Key
            {
                get
                {
                    return OptimizationFolder + ScenarioName + ArticleNumber + Customer + Month + Plant + Machine;
                }
            }
        }

        //public void importScenResults(ScenGen.Scenario currScen)
        //{
        //    Int32 i;
        //    String fullDetailedProdFileNameCpy;
        //    CSV_File detailedProductionFileCopy;
        //    DataTable productionDetailsTab;
        //    DataTable dbTab;
        //    DataRow currRow;
        //    String currKey;
        //    AggrScenEntry currAScen;
        //    Dictionary<String, AggrScenEntry> aggrScens;

        //    if (!currScen.IsImported)
        //    {
        //        clearEntriesFromScenario(currScen);
        //        aggrScens = new Dictionary<string, AggrScenEntry>();

        //        dbTab = new DataTable();
        //        dbTab.Columns.Add(productionDetailTabSpec[0, 0], typeof(String));
        //        dbTab.Columns.Add(productionDetailTabSpec[1, 0], typeof(String));
        //        dbTab.Columns.Add(productionDetailTabSpec[2, 0], typeof(Int32));
        //        dbTab.Columns.Add(productionDetailTabSpec[3, 0], typeof(String));
        //        dbTab.Columns.Add(productionDetailTabSpec[4, 0], typeof(String));
        //        dbTab.Columns.Add(productionDetailTabSpec[5, 0], typeof(String));
        //        dbTab.Columns.Add(productionDetailTabSpec[6, 0], typeof(String));
        //        dbTab.Columns.Add(productionDetailTabSpec[7, 0], typeof(String));
        //        dbTab.Columns.Add(productionDetailTabSpec[8, 0], typeof(Double));
        //        dbTab.Columns.Add(productionDetailTabSpec[9, 0], typeof(Double));
        //        dbTab.Columns.Add(productionDetailTabSpec[10, 0], typeof(Double));
        //        currRow = dbTab.NewRow();
        //        dbTab.Rows.Add(currRow);

        //        /*
        //    protected String[,] productionDetailTabSpec ={
        //                                                    {"OptimizationFolder","text"},
        //                                                    {"ScenarioName","text"},
        //                                                    {"ReplicationNumber","int"},
        //                                                    {"Artikelnummer","text"},
        //                                                    {"Kunden","text"},
        //                                                    {"Monat","text"},
        //                                                    {"Werk","text"},
        //                                                    {"Anlage","text"},
        //                                                    {"Menge","double"},
        //                                                    {"Anzahl Schichten","double"},
        //                                                    {"Produktionskosten","double"}
        //                                               };
        //    * */
        //        /*
        //         *  protected string[,] detailedProductionResultsCSVSpec ={
        //                                                              {"Artikelnummer",CSV_File.STRING_TYPE,""},
        //                                                              {"Kunden",CSV_File.STRING_TYPE,""},
        //                                                              {"Monat",CSV_File.STRING_TYPE,""},
        //                                                              {"Werk",CSV_File.STRING_TYPE,""},
        //                                                              {"Anlage",CSV_File.STRING_TYPE,""},
        //                                                              {"Menge",CSV_File.STRING_TYPE,""},
        //                                                              {"Anzahl Schichten",CSV_File.STRING_TYPE,""},
        //                                                              {"Produktionskosten (Summe)",CSV_File.STRING_TYPE,""}
        //                                                          };
        //         * */
        //        for (i = 0; i < currScen.NumberOfReplications; i++)
        //        {
        //            fullDetailedProdFileNameCpy = usedOptimizationFolder + Path.DirectorySeparatorChar + String.Format(productionDetailsResultFileNameReplication, Convert.ToString(i, CultureInfo.InvariantCulture));

        //            detailedProductionFileCopy = new CSV_File(fullDetailedProdFileNameCpy);
        //            detailedProductionFileCopy.setOutputColSpecsByStrings(detailedProductionResultsCSVSpec);
        //            detailedProductionFileCopy.CSVDelimiter = CSV_SEMCOL_DELIMITER;
        //            detailedProductionFileCopy.openForReading();
        //            productionDetailsTab = detailedProductionFileCopy.getData();
        //            detailedProductionFileCopy.closeDoc();

        //            currRow.BeginEdit();
        //            currRow[productionDetailTabSpec[0, 0]] = currOptimizationRun.RunFolderName;
        //            currRow[productionDetailTabSpec[1, 0]] = currScen.Name;
        //            currRow[productionDetailTabSpec[2, 0]] = i;
        //            currRow.EndEdit();

        //            foreach (DataRow demRow in productionDetailsTab.Rows)
        //            {
        //                currRow.BeginEdit();
        //                currRow[productionDetailTabSpec[3, 0]] = demRow[detailedProductionResultsCSVSpec[0, 0]];
        //                currRow[productionDetailTabSpec[4, 0]] = demRow[detailedProductionResultsCSVSpec[1, 0]];
        //                currRow[productionDetailTabSpec[5, 0]] = demRow[detailedProductionResultsCSVSpec[2, 0]];
        //                currRow[productionDetailTabSpec[6, 0]] = demRow[detailedProductionResultsCSVSpec[3, 0]];
        //                currRow[productionDetailTabSpec[7, 0]] = demRow[detailedProductionResultsCSVSpec[4, 0]];
        //                currRow[productionDetailTabSpec[8, 0]] = Convert.ToDouble(demRow[detailedProductionResultsCSVSpec[5, 0]], currC);
        //                currRow[productionDetailTabSpec[9, 0]] = Convert.ToDouble ( demRow[detailedProductionResultsCSVSpec[6, 0]],currC);
        //                currRow[productionDetailTabSpec[10, 0]] = Convert.ToDouble (demRow[detailedProductionResultsCSVSpec[7, 0]].ToString ().Replace ("€",""),currC);
        //                currRow.EndEdit();

        //                 //OptimizationFolder + ScenarioName + ArticleNumber + Customer + Month + Plan + Machine
        //                currKey =currOptimizationRun.RunFolderName+currScen.Name+(String) demRow[detailedProductionResultsCSVSpec[0, 0]] + (String)demRow[detailedProductionResultsCSVSpec[1, 0]] +
        //                        (String) demRow[detailedProductionResultsCSVSpec[2, 0]] + (String)demRow[detailedProductionResultsCSVSpec[3, 0]]+
        //                        (String)demRow[detailedProductionResultsCSVSpec[4, 0]];

        //                if (!aggrScens.ContainsKey(currKey))
        //                {
        //                    currAScen = new AggrScenEntry();

        //                    currAScen.OptimizationFolder = currOptimizationRun.RunFolderName;
        //                    currAScen.ScenarioName = currScen.Name;
        //                    currAScen.ArticleNumber =  (String)demRow[detailedProductionResultsCSVSpec[0, 0]];
        //                    currAScen.Customer = (String)demRow[detailedProductionResultsCSVSpec[1, 0]];
        //                    currAScen.Month =(String)demRow[detailedProductionResultsCSVSpec[2, 0]];
        //                    currAScen.Plant = (String)demRow[detailedProductionResultsCSVSpec[3, 0]];
        //                    currAScen.Machine = (String)demRow[detailedProductionResultsCSVSpec[4, 0]];
        //                    aggrScens.Add(currKey, currAScen);
        //                }
        //                else
        //                {
        //                    currAScen = aggrScens[currKey];
        //                }

        //                currAScen.Quantity.addObservation((Double)currRow[productionDetailTabSpec[8, 0]]);
        //                currAScen.NumberOfShifts.addObservation((Double)currRow[productionDetailTabSpec[9, 0]]);
        //                currAScen.ProductionCost.addObservation((Double)currRow[productionDetailTabSpec[10, 0]]);
        //                usedInMemDB.writeData(dbTab, productionDetailsTabName, true);
        //            }
        //        }

        //        currScen.IsImported = true;
        //    }
        //}

        protected const String detailedProdFileName = "";
        public void collectCurrentResultsForScenario(Int32 currentReplication)
        {
            String fullDetailedProdFileName,fullDetailedProdFileNameCpy;
            
            CSV_File detailedProductionFile;
            CSV_File detailedProductionFileCopy;
            DataTable productionDetailsTab;

            fullDetailedProdFileName=usedOptimizationFolder+Path.DirectorySeparatorChar +productionDetailsResultFileName;
            fullDetailedProdFileNameCpy=usedOptimizationFolder +Path.DirectorySeparatorChar +String.Format (productionDetailsResultFileNameReplication ,Convert.ToString ( currentReplication,CultureInfo .InvariantCulture ));
            detailedProductionFile = new CSV_File(fullDetailedProdFileName);
            detailedProductionFile.setInputColSpecsByStrings(detailedProductionResultsCSVSpec);
            detailedProductionFile.CSVDelimiter = CSV_SEMCOL_DELIMITER;
            detailedProductionFile.openForReading();
            productionDetailsTab = detailedProductionFile.getData();
            detailedProductionFile.closeDoc();

            detailedProductionFileCopy = new CSV_File(fullDetailedProdFileNameCpy);
            detailedProductionFileCopy.setOutputColSpecsByStrings(detailedProductionResultsCSVSpec);
            detailedProductionFileCopy.CSVDelimiter = CSV_SEMCOL_DELIMITER;
            detailedProductionFileCopy.openForWriting(false);
            detailedProductionFileCopy.writeData(productionDetailsTab);
            detailedProductionFileCopy.closeDoc();
        }

        private void visualizeOptimizationRuns()
        {
            string [] currDataRow;
           // optRunConfigTab.Rows.Clear();
            OptimizationRunConfig currRunConfig;

            optRunTab.Rows.Clear();
            //optRunConfigTab.Rows . Clear();
            foreach (OptimizationRun run in currentOptimizationRuns)
            {
                //currDataRow = new string[9];
                //currDataRow[0] = run.OptFolderName;//reducedOptRunDirCandidate.Substring(0, atPos);
                //currDataRow[1] = run.TimeStamp.ToString(ISO_8601_FORMAT_STRING);//reducedOptRunDirCandidate.Substring(atPos + 1, byPos - atPos - 1);
                //currDataRow[2] = run.UserName;//reducedOptRunDirCandidate.Substring(byPos + 2, reducedOptRunDirCandidate.Length - byPos - 2);

                //currRunConfig = currentRunConfigs[run.RunFolderName];

                //currDataRow[3] = Convert.ToString(currRunConfig.minProdQty, currC);

                //currDataRow[4] = Convert.ToString(currRunConfig.splitQty, currC);

                ////currDataRow[5] = Convert.ToString(currRunConfig.isAllowSplittingIfInFixations, currC); ;

                //currDataRow[5] = Convert.ToString(currRunConfig.isUseBaskets, currC);

                //currDataRow[6] = Convert.ToString(currRunConfig.capacityFactorForMachines, currC);

                //currDataRow[7] = Convert.ToString(currRunConfig.isOnlyProductionOptimization, currC);

                //currDataRow[8] = Convert.ToString(currRunConfig.minInventoryFactor, currC);

                //optRunConfigTab.Rows.Add(currDataRow);

                currDataRow = new string[4];
                currDataRow[0] = run.OptFolderName;//reducedOptRunDirCandidate.Substring(0, atPos);
                currDataRow[1] = run.TimeStamp.ToString(ISO_8601_FORMAT_STRING);//reducedOptRunDirCandidate.Substring(atPos + 1, byPos - atPos - 1);
                currDataRow[2] = run.UserName;//reducedOptRunDirCandidate.Substring(byPos + 2, reducedOptRunDirCandidate.Length - byPos - 2);
                currDataRow[3] = run.ScenName;
                optRunTab.Rows.Add(currDataRow);
            }


            //optRunConfigs = new DataTable();

            //for (i = 0; i < optRunIdentifierNames.Length; i++)
            //    optRunConfigs.Columns.Add(optRunIdentifierNames[i]);

            //for (i = 0; i < OptimizationRunConfig.optimizationSettingsHeader.Length; i++)
            //    optRunConfigs.Columns.Add(OptimizationRunConfig.optimizationSettingsHeader[i]);

                // currDataRow[0] = reducedOptRunDirCandidate.Substring(0, atPos);
                //        currDataRow[1] = reducedOptRunDirCandidate.Substring(atPos + 1, byPos - atPos - 1);
                //        currDataRow[2] = reducedOptRunDirCandidate.Substring(byPos + 2, reducedOptRunDirCandidate.Length - byPos - 2);

                //        settingsFileFullPath = optRunDirCandidate + Path.DirectorySeparatorChar + settingsFileName;

                //        if (File.Exists(settingsFileFullPath))
                //        {
                //            try
                //            {
                //                OptimizationRunConfig currOptRunConf;

                //                currOptRunConf = new OptimizationRunConfig();

                //                currOptRunConf.loadConfig(settingsFileFullPath, currC);


                //                currDataRow[3] = Convert.ToString(currOptRunConf.minProdQty, currC);

                //                currDataRow[4] = Convert.ToString(currOptRunConf.splitQty, currC);


                //                currDataRow[5] = Convert.ToString(currOptRunConf.isAllowSplittingIfInFixations, currC); ;

                //                currDataRow[6] = Convert.ToString(currOptRunConf.isUseBaskets, currC);


                //                currDataRow[7] = Convert.ToString(currOptRunConf.capacityFactorForMachines, currC);


                //                currDataRow[8] = Convert.ToString(currOptRunConf.isOnlyProductionOptimization, currC);

                //                currDataRow[9] = Convert.ToString(currOptRunConf.minInventoryFactor, currC);

                //                //MinimumProductionQuantity",
                //                //                        "SplitQuantity",
                //                //                        "AllowSplittingIfInFixations",
                //                //                        "UseBaskets",
                //                //                        "CapacityFactorForMachines",
                //                //                        "OnlyProductionOptimization",
                //                //                        "MinumInventoryFactor"
                //                /*
                //                if (btnSender == btnRunOptimizationFixedProdQties)
                //                    usedOptExec.isFixedProductionQties = true;
                //                else
                //                    usedOptExec.isFixedProductionQties = false;

                //                if (tbMinProdQty.Text.Trim() == "")
                //                    usedOptExec.minProdQty = 0;
                //                else
                //                    usedOptExec.minProdQty = Convert.ToDouble(tbMinProdQty.Text, currC);

                //                if (tbSplittingQty.Text.Trim() == "")
                //                    usedOptExec.splitQty = 0;
                //                else
                //                    usedOptExec.splitQty = Convert.ToDouble(tbSplittingQty.Text, currC);

                //                usedOptExec.isAllowSplittingIfInFixations = cbRelaxSplitConstraint.Checked;

                //                usedOptExec.isUseBaskets = cbBasketUsage.Checked;

                //                if (tbCapFactor.Text.Trim() == "")
                //                    usedOptExec.capacityFactorForMachines = 1;
                //                else
                //                    usedOptExec.capacityFactorForMachines = Convert.ToDouble(tbCapFactor.Text, currC);

                //                usedOptExec.isOnlyProductionOptimization = cbIsOnlyProductionOptimization.Checked;
                //                */
            //                optRunConfigs.Rows.Add(currDataRow);
                //            }
                //            catch (Exception e)
                //            {
                //            }
                //        }
                //    }
                //}
        }

        private void updateOptimizationRuns()
        {   
            if (dGVOptimizationRuns.InvokeRequired)
            {
                dGVOptimizationRuns.Invoke (new returnVoidWithoutParams(updateOptimizationRuns ));
            }
            else
            {
                loadOptimizationRunInfos(currentOptimizationRuns, tbOptimizationName.Text);
                //loadOptimizationRunConfigs(currentRunConfigs,currentOptimizationRuns);
                visualizeOptimizationRuns();
                //dGVOptimizationRuns.DataSource = loadOptimizationRuns(tbOptimizationName.Text);
                setSelectedOptimizationRun();
                if (currentOptimizationRuns.Count > 0 && dGVOptimizationRuns.CurrentRow.Index < currentOptimizationRuns.Count)
                {
                    allowOpenResults(true);
                    allowVisualizeResults(true);
                }
                else
                {
                   allowOpenResults(false);
                   allowVisualizeResults (false);
                }
            }


        }

        public class OptimizationRun
        {
            public string OptFolderName;
            public string RunFolderName;
            public string UserName;
            public string ScenName;
            public DateTime TimeStamp;
        }

        protected List<OptimizationRun > currentOptimizationRuns;

       
        //private OptimizationRunConfig loadOptimizationRunConfig(OptimizationRun run)
        //{
        //    string optimizationFullFilename;

        //    optimizationFullFilename = optimizationBasePath + run.OptFolderName  + Path.DirectorySeparatorChar + ResultsSubDir+Path.DirectorySeparatorChar +run.RunFolderName + Path.DirectorySeparatorChar + settingsFileName;

           

        //    if (File.Exists(optimizationFullFilename))
        //    {
        //        try
        //        {
        //            OptimizationRunConfig currOptRunConf;

        //            currOptRunConf = new OptimizationRunConfig();

        //            currOptRunConf.loadConfig(optimizationFullFilename, currC);


        //            currDataRow[3] = Convert.ToString(currOptRunConf.minProdQty, currC);

        //            currDataRow[4] = Convert.ToString(currOptRunConf.splitQty, currC);


        //            currDataRow[5] = Convert.ToString(currOptRunConf.isAllowSplittingIfInFixations, currC); ;

        //            currDataRow[6] = Convert.ToString(currOptRunConf.isUseBaskets, currC);


        //            currDataRow[7] = Convert.ToString(currOptRunConf.capacityFactorForMachines, currC);


        //            currDataRow[8] = Convert.ToString(currOptRunConf.isOnlyProductionOptimization, currC);

        //            currDataRow[9] = Convert.ToString(currOptRunConf.minInventoryFactor, currC);
        //        catch (Exception e)
        //                {
        //               }
        //}

        private void loadOptimizationRunInfos( List<OptimizationRun > optimizationRuns,string optimizationFolderName)
        {
            //List<string> optimizationFolders;
            string optimizationFullFolder;
            string reducedOptRunDirCandidate;
            int atPos;
            int byPos;
            //List<OptimizationRun> resultList;
            OptimizationRun currRun;
            string[] folderNameComponents;
            optimizationRuns.Clear();
           // resultList = new List<OptimizationRun>();

            //optimizationFolders = existingOptimizationFolders();

            //optRunConfigs = new DataTable();

            //for (i = 0; i < optRunIdentifierNames.Length; i++)
            //    optRunConfigs.Columns.Add(optRunIdentifierNames[i]);

            //for (i = 0; i < OptimizationRunConfig.optimizationSettingsHeader.Length; i++)
            //    optRunConfigs.Columns.Add(OptimizationRunConfig.optimizationSettingsHeader[i]);

            // optimizationFolders = new List<string>();

            //foreach (string folder in optimizationFolders)
            //{
            optimizationFullFolder = optimizationBasePath + optimizationFolderName + Path.DirectorySeparatorChar + ResultsSubDir;

            foreach (string optRunDirCandidate in Directory.GetDirectories(optimizationFullFolder))
            {
                reducedOptRunDirCandidate = trimLongPath(optRunDirCandidate);
                
                atPos = reducedOptRunDirCandidate.IndexOf("@");
                byPos = reducedOptRunDirCandidate.IndexOf("By");
                folderNameComponents = reducedOptRunDirCandidate.Split('@');
                if (folderNameComponents .Length ==4)//(atPos != -1 && byPos != -1 && atPos < byPos)
                {
                    //currDataRow = new string[10];
                    currRun = new OptimizationRun();

                    currRun.RunFolderName = reducedOptRunDirCandidate;
                    currRun.OptFolderName = folderNameComponents[0];//reducedOptRunDirCandidate.Substring(0, atPos);
                    currRun.UserName = folderNameComponents[2];//reducedOptRunDirCandidate.Substring(byPos + 2, reducedOptRunDirCandidate.Length - byPos - 2);
                    currRun.TimeStamp = DateTime.ParseExact(folderNameComponents [1], ISO_8601_FORMAT_STRING, CultureInfo.InvariantCulture);//DateTime.ParseExact(reducedOptRunDirCandidate.Substring(atPos + 1, byPos - atPos - 1), ISO_8601_FORMAT_STRING, CultureInfo.InvariantCulture);
                    currRun.ScenName = folderNameComponents[3];
                    optimizationRuns.Add(currRun);
                    //currDataRow[0] = reducedOptRunDirCandidate.Substring(0, atPos);
                    //currDataRow[1] = reducedOptRunDirCandidate.Substring(atPos + 1, byPos - atPos - 1);
                    //currDataRow[2] = reducedOptRunDirCandidate.Substring(byPos + 2, reducedOptRunDirCandidate.Length - byPos - 2);

                    //settingsFileFullPath = optRunDirCandidate + Path.DirectorySeparatorChar + settingsFileName;

                    //if (File.Exists(settingsFileFullPath))
                    //{
                    //    try
                    //    {
                    //        OptimizationRunConfig currOptRunConf;

                    //        currOptRunConf = new OptimizationRunConfig();

                    //        currOptRunConf.loadConfig(settingsFileFullPath, currC);


                    //        currDataRow[3] = Convert.ToString(currOptRunConf.minProdQty, currC);

                    //        currDataRow[4] = Convert.ToString(currOptRunConf.splitQty, currC);


                    //        currDataRow[5] = Convert.ToString(currOptRunConf.isAllowSplittingIfInFixations, currC); ;

                    //        currDataRow[6] = Convert.ToString(currOptRunConf.isUseBaskets, currC);


                    //        currDataRow[7] = Convert.ToString(currOptRunConf.capacityFactorForMachines, currC);


                    //        currDataRow[8] = Convert.ToString(currOptRunConf.isOnlyProductionOptimization, currC);

                    //        currDataRow[9] = Convert.ToString(currOptRunConf.minInventoryFactor, currC);

                    //        //MinimumProductionQuantity",
                    //        //                        "SplitQuantity",
                    //        //                        "AllowSplittingIfInFixations",
                    //        //                        "UseBaskets",
                    //        //                        "CapacityFactorForMachines",
                    //        //                        "OnlyProductionOptimization",
                    //        //                        "MinumInventoryFactor"
                    //        /*
                    //        if (btnSender == btnRunOptimizationFixedProdQties)
                    //            usedOptExec.isFixedProductionQties = true;
                    //        else
                    //            usedOptExec.isFixedProductionQties = false;

                    //        if (tbMinProdQty.Text.Trim() == "")
                    //            usedOptExec.minProdQty = 0;
                    //        else
                    //            usedOptExec.minProdQty = Convert.ToDouble(tbMinProdQty.Text, currC);

                    //        if (tbSplittingQty.Text.Trim() == "")
                    //            usedOptExec.splitQty = 0;
                    //        else
                    //            usedOptExec.splitQty = Convert.ToDouble(tbSplittingQty.Text, currC);

                    //        usedOptExec.isAllowSplittingIfInFixations = cbRelaxSplitConstraint.Checked;

                    //        usedOptExec.isUseBaskets = cbBasketUsage.Checked;

                    //        if (tbCapFactor.Text.Trim() == "")
                    //            usedOptExec.capacityFactorForMachines = 1;
                    //        else
                    //            usedOptExec.capacityFactorForMachines = Convert.ToDouble(tbCapFactor.Text, currC);

                    //        usedOptExec.isOnlyProductionOptimization = cbIsOnlyProductionOptimization.Checked;
                    //        */
                    //        optRunConfigs.Rows.Add(currDataRow);
                    //    }
                    //    catch (Exception e)
                    //    {
                    //    }
                    //}
                }
            }
            //}

            //return optRunConfigs;

            //return resultList;//null;

        }

        //private DataTable loadOptimizationRuns()
        //{
        //    List<string> optimizationFolders;
        //    CSV_File currOptrunConfigFile;
        //    DataTable configData, optRunConfigs;
        //    int i;
        //    string[] currDataRow;
        //    string settingsFileFullPath;
        //    string optimizationFullFolder;
        //    string reducedOptRunDirCandidate;
        //    int atPos;
        //    int byPos;
        //    string timeStamp;
        //    string simName;
        //    string userName;

        //    optimizationFolders = existingOptimizationFolders();



        //    optRunConfigs = new DataTable();

        //    for (i = 0; i < optRunIdentifierNames.Length; i++)
        //        optRunConfigs.Columns.Add(optRunIdentifierNames[i]);

        //    for (i = 0; i < OptimizationRunConfig.optimizationSettingsHeader.Length; i++)
        //        optRunConfigs.Columns.Add(OptimizationRunConfig.optimizationSettingsHeader[i]);

        //    // optimizationFolders = new List<string>();

        //    foreach (string folder in optimizationFolders)
        //    {
        //        optimizationFullFolder = optimizationBasePath + folder + Path.DirectorySeparatorChar + ResultsSubDir;

        //        foreach (string optRunDirCandidate in Directory.GetDirectories(optimizationFullFolder))
        //        {
        //            reducedOptRunDirCandidate = trimLongPath(optRunDirCandidate);

        //            atPos = reducedOptRunDirCandidate.IndexOf("@");
        //            byPos = reducedOptRunDirCandidate.IndexOf("By");

        //            if (atPos != -1 && byPos != -1 && atPos < byPos)
        //            {
        //                currDataRow = new string[10];

        //                currDataRow[0] = reducedOptRunDirCandidate.Substring(0, atPos);
        //                currDataRow[1] = reducedOptRunDirCandidate.Substring(atPos + 1, byPos - atPos - 1);
        //                currDataRow[2] = reducedOptRunDirCandidate.Substring(byPos + 2, reducedOptRunDirCandidate.Length - byPos - 2);

        //                settingsFileFullPath = optRunDirCandidate + Path.DirectorySeparatorChar + settingsFileName;

        //                if (File.Exists(settingsFileFullPath))
        //                {
        //                    try
        //                    {
        //                        OptimizationRunConfig currOptRunConf;

        //                        currOptRunConf = new OptimizationRunConfig();

        //                        currOptRunConf.loadConfig(settingsFileFullPath, currC);


        //                        currDataRow[3] = Convert.ToString(currOptRunConf.minProdQty, currC);

        //                        currDataRow[4] = Convert.ToString(currOptRunConf.splitQty, currC);


        //                        currDataRow[5] = Convert.ToString(currOptRunConf.isAllowSplittingIfInFixations, currC); ;

        //                        currDataRow[6] = Convert.ToString(currOptRunConf.isUseBaskets, currC);


        //                        currDataRow[7] = Convert.ToString(currOptRunConf.capacityFactorForMachines, currC);


        //                        currDataRow[8] = Convert.ToString(currOptRunConf.isOnlyProductionOptimization, currC);

        //                        currDataRow[9] = Convert.ToString(currOptRunConf.minInventoryFactor, currC);

        //                        //MinimumProductionQuantity",
        //                        //                        "SplitQuantity",
        //                        //                        "AllowSplittingIfInFixations",
        //                        //                        "UseBaskets",
        //                        //                        "CapacityFactorForMachines",
        //                        //                        "OnlyProductionOptimization",
        //                        //                        "MinumInventoryFactor"
        //                        /*
        //                        if (btnSender == btnRunOptimizationFixedProdQties)
        //                            usedOptExec.isFixedProductionQties = true;
        //                        else
        //                            usedOptExec.isFixedProductionQties = false;

        //                        if (tbMinProdQty.Text.Trim() == "")
        //                            usedOptExec.minProdQty = 0;
        //                        else
        //                            usedOptExec.minProdQty = Convert.ToDouble(tbMinProdQty.Text, currC);

        //                        if (tbSplittingQty.Text.Trim() == "")
        //                            usedOptExec.splitQty = 0;
        //                        else
        //                            usedOptExec.splitQty = Convert.ToDouble(tbSplittingQty.Text, currC);

        //                        usedOptExec.isAllowSplittingIfInFixations = cbRelaxSplitConstraint.Checked;

        //                        usedOptExec.isUseBaskets = cbBasketUsage.Checked;

        //                        if (tbCapFactor.Text.Trim() == "")
        //                            usedOptExec.capacityFactorForMachines = 1;
        //                        else
        //                            usedOptExec.capacityFactorForMachines = Convert.ToDouble(tbCapFactor.Text, currC);

        //                        usedOptExec.isOnlyProductionOptimization = cbIsOnlyProductionOptimization.Checked;
        //                        */
        //                        optRunConfigs.Rows.Add(currDataRow);
        //                    }
        //                    catch (Exception e)
        //                    {
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return optRunConfigs;
        //}

        //private DataTable loadOptimizationRuns(string optimizationFolderName)
        //{
        //    List<string> optimizationFolders;
        //    CSV_File currOptrunConfigFile;
        //    DataTable configData,optRunConfigs;
        //    int i;
        //    string [] currDataRow;
        //    string settingsFileFullPath;
        //    string optimizationFullFolder;
        //    string reducedOptRunDirCandidate;
        //    int atPos;
        //           int byPos;
        //    string timeStamp;
        //            string simName;
        //            string userName;

        //    optimizationFolders=existingOptimizationFolders ();


            
        //    optRunConfigs=new DataTable ();

        //    for (i=0;i<optRunIdentifierNames .Length;i++)
        //        optRunConfigs .Columns.Add (optRunIdentifierNames [i]);

        //    for (i = 0; i < OptimizationRunConfig.optimizationSettingsHeader.Length; i++)
        //        optRunConfigs.Columns.Add(OptimizationRunConfig.optimizationSettingsHeader[i]);

        //   // optimizationFolders = new List<string>();

        //    //foreach (string folder in optimizationFolders)
        //    //{
        //    optimizationFullFolder = optimizationBasePath + optimizationFolderName + Path.DirectorySeparatorChar + ResultsSubDir;

        //        foreach (string optRunDirCandidate in Directory.GetDirectories(optimizationFullFolder))
        //        {
        //            reducedOptRunDirCandidate = trimLongPath(optRunDirCandidate);

        //            atPos = reducedOptRunDirCandidate.IndexOf("@");
        //            byPos = reducedOptRunDirCandidate.IndexOf("By");
                    
        //            if (atPos != -1 && byPos != -1 && atPos < byPos)
        //            {
        //                currDataRow=new string [10];

        //                currDataRow[0] = reducedOptRunDirCandidate.Substring(0, atPos);
        //                currDataRow[1] = reducedOptRunDirCandidate.Substring(atPos + 1, byPos - atPos - 1);
        //                currDataRow[2] = reducedOptRunDirCandidate.Substring(byPos + 2, reducedOptRunDirCandidate.Length - byPos-2);

        //                settingsFileFullPath=optRunDirCandidate + Path.DirectorySeparatorChar + settingsFileName;

        //                if (File.Exists (settingsFileFullPath))
        //                {
        //                    try
        //                    {
        //                        OptimizationRunConfig currOptRunConf;

        //                        currOptRunConf = new OptimizationRunConfig();

        //                        currOptRunConf.loadConfig(settingsFileFullPath, currC);

                                
        //                        currDataRow[3] = Convert.ToString(currOptRunConf.minProdQty, currC);

        //                        currDataRow[4] = Convert.ToString(currOptRunConf.splitQty, currC);


        //                        currDataRow[5] = Convert.ToString(currOptRunConf.isAllowSplittingIfInFixations, currC); ;

        //                        currDataRow[6] = Convert.ToString (currOptRunConf.isUseBaskets,currC);


        //                        currDataRow[7] = Convert.ToString(currOptRunConf.capacityFactorForMachines, currC);

                                
        //                        currDataRow[8] = Convert.ToString(currOptRunConf.isOnlyProductionOptimization, currC);

        //                        currDataRow[9] = Convert.ToString(currOptRunConf.minInventoryFactor, currC);

        //                        //MinimumProductionQuantity",
        //                        //                        "SplitQuantity",
        //                        //                        "AllowSplittingIfInFixations",
        //                        //                        "UseBaskets",
        //                        //                        "CapacityFactorForMachines",
        //                        //                        "OnlyProductionOptimization",
        //                        //                        "MinumInventoryFactor"
        //                        /*
        //                        if (btnSender == btnRunOptimizationFixedProdQties)
        //                            usedOptExec.isFixedProductionQties = true;
        //                        else
        //                            usedOptExec.isFixedProductionQties = false;

        //                        if (tbMinProdQty.Text.Trim() == "")
        //                            usedOptExec.minProdQty = 0;
        //                        else
        //                            usedOptExec.minProdQty = Convert.ToDouble(tbMinProdQty.Text, currC);

        //                        if (tbSplittingQty.Text.Trim() == "")
        //                            usedOptExec.splitQty = 0;
        //                        else
        //                            usedOptExec.splitQty = Convert.ToDouble(tbSplittingQty.Text, currC);

        //                        usedOptExec.isAllowSplittingIfInFixations = cbRelaxSplitConstraint.Checked;

        //                        usedOptExec.isUseBaskets = cbBasketUsage.Checked;

        //                        if (tbCapFactor.Text.Trim() == "")
        //                            usedOptExec.capacityFactorForMachines = 1;
        //                        else
        //                            usedOptExec.capacityFactorForMachines = Convert.ToDouble(tbCapFactor.Text, currC);

        //                        usedOptExec.isOnlyProductionOptimization = cbIsOnlyProductionOptimization.Checked;
        //                        */
        //                        optRunConfigs.Rows.Add(currDataRow);
        //                    }
        //                    catch (Exception e)
        //                    {
        //                    }
        //                }
        //            }
        //        }
        //    //}

        //    return  optRunConfigs;
        //}

        Optimizer_Executer usedOptExec;
    //    Return values
    //0	normal
    //1	problem with reading or writing file
    //2	error concerning first argument in command line
    //3	error concerning second argument in command line
    //4	error concerning third argument in command line
    //5	error concerning fourth argument in command line
    //6	error concerning fifth argument in command line
    //7	error concerning sixth argument in command line
    //8	error concerning seventh argument in command line
    //9	error concerning tenth argument in command line
    //10	error concerning eleventh argument in command line
    //14	wrong index for plant in start inventory file
    //15	wrong index for product in start inventory file
    //16	negative amount in start inventory file
    //21	negative capacities in file
    //22	more capacities in file than periods
    //23	more capacities in file than machines
    //24	less capacities in file than periods
    //25	less capacities in file than machines
    //26	wrong start index for ic-transport
    //27	wrong end index for ic-transport
    //28	negative costs for ic-transport
    //29	wrong start index for customer transport
    //30	wrong end index for customer transport
    //31	negative costs for customer transport
    //32	wrong customer index in demands
    //33	wrong product index in demands
    //34	wrong period index in demands
    //35	wrong amount in demands
    //36	non positive value for pallets per ton
    //37	not enough values for pallets per ton
    //38	too much values for pallets per ton
    //39	wrong customer index in baskets
    //41	wrong process index in fixations
    //42	wrong period index in fixations
    //43	wrong amount in fixations
    //44	wrong product index in processes
    //45	wrong machine index in processes
    //46	wrong value for tons per shift in processes
    //47	wrong value vor costs per shift in processes
    //48	wrong plant index in processes
    //51	inconsistence of data concerning production
    //52	inconsistence of data concerning transportation
    //61	negative plant index in plant capacity file
    //62	negative capacity in plant capacity file
    //99	cplex variable for which bound should be changed does not exist
    //100	wrong number of command line parameters
    //200	plant without capacity information
    //300	cplex solution process: problem is infeasible or unbounded 
        

        private void setDataLoadChecked(bool isOk)
        {
            if (cbDataLoaded.InvokeRequired)
            {
                cbDataLoaded.Invoke(new enableSetProc(setDataLoadChecked), isOk);
            }
            else
            {
                cbDataLoaded.Checked = isOk;
            }

        }

        private void setUsedBaseDataFolder(string folder)
        {
            if (lbUsedBaseDataFolder .InvokeRequired )
            {
                lbUsedBaseDataFolder.Invoke(new textSetProc(setUsedBaseDataFolder), folder);
            }
            else
            {
                lbUsedBaseDataFolder.Text = folder;
            }

        }

        //private void setScenAsSelected(string scenName)
        //{
        //    //DataRowView dRV;
        //    int i;
           
        //    for (i=0;i<dGVScens .Rows.Count ;i++)
        //    {
        //        if (dGVScens .Rows[i].Cells [0].Value .ToString ()== scenName)
        //        {
        //            dGVScens.CurrentCell = dGVScens .Rows[i].Cells [0];
        //        }
        //    }
        //}

        private void guiLaunchOptimization()
        {
        }

        //public String getScenDemandFileName(String usedScenName,Int32 currReplication)
        //{
        //    return usedScenGenerator.getScenDemandFileName(usedScenName, currReplication);
        //}
        LinearDistanceCostProvider usedLDCP;
        protected const string defaultScenName = "Default";
        private void btnDataLoad_Click(object sender, EventArgs e)
        {
            Button btnSender;
            int selectedMonth;
            int finalMonth;
            string reducedUserName;
            string usedScenName;
            int lIndex;
            string usedDemandFileName;
            Int32 currReplication;
            ScenGen.Scenario currScen;

            btnSender = (Button)sender;

            isReadyToClose = false;

            if (tbOptimizationName.Text.Trim() == "")
                return;

            if (!checkOptimizationFolder(tbOptimizationName.Text,true))
                return;

            optimizationFolderPath = optimizationBasePath + tbOptimizationName.Text+Path.DirectorySeparatorChar ;
            optimizationBaseDataFolderPath = optimizationFolderPath +  optimizationBaseDataFolderName+Path.DirectorySeparatorChar ;
            resultsBasePath = optimizationFolderPath + ResultsSubDir+Path.DirectorySeparatorChar;
            loadedDataPath = optimizationFolderPath + loadedDataSubDirectory + Path.DirectorySeparatorChar;//currentBasePath + Path.DirectorySeparatorChar + loadedDataSubDirectory + Path.DirectorySeparatorChar;

            reducedUserName = trimLongPath(currentUserName);

            if (!isActionRunning)
            {
                if (btnSender == btnDataLoad)
                {
                    DataLoadStopSignal = false;
                    /// selectedMonth = mcStartPlanning.SelectionRange.Start.Month;
                    //finalMonth=selectedMonth +2;
                    //if (finalMonth >12)
                    //  finalMonth -=12;
                    //demandFileName = string.Format(demandFileNameBase, selectedMonth.ToString ("D2"), finalMonth.ToString ("D2"));
                    usedLDCP = new LinearDistanceCostProvider(Convert.ToDouble(tbTranspCostFix.Text, currC), Convert.ToDouble(tbTranspCostVar.Text, currC));

                    currActionThread = new Thread(dataLoad);
                    currentActionType = ActionType.DATA_LOAD;
                    setOptimizationFolder();
                }
                //else if ((btnSender == btnRunOptimizationFixedProdQties || btnSender == btnMergeResults) && usedOptimizationFolder == "")
                //{
                //    MessageBox.Show("Bitte erst vollständige Optimierung ausführen.", "",
                //              MessageBoxButtons.OK,
                //              MessageBoxIcon.None);
                //    return;
                //}
                else if (btnSender == btnRunOptimization)// ||
                //btnSender == btnRunOptimizationFixedProdQties)
                {
                    usedScenName = "";
                    if (btnSender == btnRunOptimization)
                    {
                        usedScenName = defaultScenName;
                        usedDemandFileName = demandfileName;
                    }
                    else
                        return;

                    if (usedOptExec == null)
                    {
                        usedOptExec = new Optimizer_Executer();
                        usedOptExec.optRunC = new OptimizationRunConfig();

                    }
                    //if (btnSender == btnRunOptimizationFixedProdQties)
                    //    usedOptExec.isFixedProductionQties = true;
                    //else
                    usedOptExec.IsFixedProductionQties = cbAdditionalRunWithFixations.Checked;

                    if (tbMinProdQty.Text.Trim() == "")
                        usedOptExec.optRunC.minProdQty = 0;
                    else
                        usedOptExec.optRunC.minProdQty = Convert.ToDouble(tbMinProdQty.Text, currC);

                    if (tbSplittingQty.Text.Trim() == "")
                        usedOptExec.optRunC.splitQty = 0;
                    else
                        usedOptExec.optRunC.splitQty = Convert.ToDouble(tbSplittingQty.Text, currC);

                    usedOptExec.optRunC.isAllowSplittingIfInFixations = false;//cbRelaxSplitConstraint.Checked;

                    usedOptExec.optRunC.isUseBaskets = cbBasketUsage.Checked;

                    usedOptExec.optRunC.isFixPlants = cbFixPlants.Checked;

                    usedOptExec.optRunC.isUseFixationsExlusions = cbUseFixationsExclusions.Checked;

                    if (tbCapFactor.Text.Trim() == "")
                        usedOptExec.optRunC.capacityFactorForMachines = 1;
                    else
                        usedOptExec.optRunC.capacityFactorForMachines = Convert.ToDouble(tbCapFactor.Text, currC);

                    usedOptExec.optRunC.isOnlyProductionOptimization = cbIsOnlyProductionOptimization.Checked;
                    // cbRelaxSplitConstraint .Checked

                    usedOptExec.inputPath = optimizationFolderPath + loadedDataSubDirectory;

                    usedOptExec.parent = this;
                    //usedOptExec.optimizationName = tbOptimizationName.Text;
                    if (currC.NumberFormat.NumberDecimalSeparator == "," && currC.NumberFormat.NumberGroupSeparator == ".")
                        usedOptExec.optRunC.isUseGermanCulture = true;
                    else
                        usedOptExec.optRunC.isUseGermanCulture = false;

                    if (((BuildType)cmBBuild.SelectedIndex) == BuildType.BUILD_1)
                    {
                        usedOptExec.optRunC.minInventoryFactor = 0;
                        usedOptExec.optRunC.expansionStage = BuildType.BUILD_1;
                    }
                    else
                    {
                        if (tbMinInvFactor.Text == "")
                            usedOptExec.optRunC.minInventoryFactor = defaultFactorMinInventory;
                        else
                            usedOptExec.optRunC.minInventoryFactor = Convert.ToDouble(tbMinInvFactor.Text, currC);

                        usedOptExec.optRunC.expansionStage = (BuildType)cmBBuild.SelectedIndex;
                    }

                    if (tbFixationThreshold.Text == "")
                        usedOptExec.optRunC.fixationThreshold = 0;
                    else
                        usedOptExec.optRunC.fixationThreshold = Convert.ToDouble(tbFixationThreshold.Text, currC);

                    if (tbMaxGap.Text == "")
                        usedOptExec.optRunC.maxGap = 0;
                    else
                        usedOptExec.optRunC.maxGap = Convert.ToDouble(tbMaxGap.Text, currC);

                    currOptimizationRun = new OptimizationRun();

                    currOptimizationRun.UserName = reducedUserName;
                    currOptimizationRun.TimeStamp = DateTime.Now;
                    currOptimizationRun.OptFolderName = tbOptimizationName.Text;
                    currOptimizationRun.RunFolderName = currOptimizationRun.OptFolderName + "@" + currOptimizationRun.TimeStamp.ToString(ISO_8601_FORMAT_STRING) + "@" + reducedUserName + "@" + usedScenName;
                    currOptimizationRun.ScenName = usedScenName;
                    
                    //if (btnSender == btnRunOptimization)
                    //{
                    usedOptExec.DemandFileName = usedDemandFileName;

                    usedOptimizationFolder = resultsBasePath + currOptimizationRun.RunFolderName;//usedOptimizationFolderBase + Path.DirectorySeparatorChar + currOptimizationRun.OptFolderName + Path.DirectorySeparatorChar + currOptimizationRun.RunFolderName;
                   
                    usedOptExec.outputPath = usedOptimizationFolder;

                    if (!System.IO.Directory.Exists(usedOptExec.outputPath))
                    {
                        System.IO.Directory.CreateDirectory(usedOptExec.outputPath);
                    }

                    usedOptExec.optRunC.saveConfig(usedOptimizationFolder + Path.DirectorySeparatorChar + settingsFileName);

                    
                        usedOptExec.setScenario(null);

                    currentActionType = ActionType.OPTIMIZATION;
                    currActionThread = new Thread(usedOptExec.runOptimizer);
                    //}
                    //else if (btnSender == btnRunScenOptimization)
                    //{   
                    //    currScen=usedScenGenerator.getScenario (usedScenName);

                    //    for (currReplication = 0; currReplication < currScen.NumberOfReplications;currReplication++)
                    //    {
                    //        usedOptExec.DemandFileName = usedDemandFileName;

                    //        usedDemandFileName = usedScenGenerator.getScenDemandFileName(usedScenName,currReplication);

                    //        usedOptimizationFolder = resultsBasePath + currOptimizationRun.RunFolderName;//usedOptimizationFolderBase + Path.DirectorySeparatorChar + currOptimizationRun.OptFolderName + Path.DirectorySeparatorChar + currOptimizationRun.RunFolderName;

                    //        usedOptExec.outputPath = usedOptimizationFolder;

                    //        if (!System.IO.Directory.Exists(usedOptExec.outputPath))
                    //        {
                    //            System.IO.Directory.CreateDirectory(usedOptExec.outputPath);
                    //        }

                    //        currentActionType = ActionType.OPTIMIZATION;
                    //        currActionThread = new Thread(usedOptExec.runOptimizer);
                    //        currActionThread.Start();
                    //    }
                    //}
                }
                //else if (btnSender == btnMergeResults)
                //{
                //    if (usedOptimizationFolder == "")
                //    {

                //    }
                //    else
                //    {
                //        usedOptExec = new Optimizer_Executer();

                //        if (currC.NumberFormat.NumberDecimalSeparator == "," && currC.NumberFormat.NumberGroupSeparator == ".")
                //            usedOptExec.isUseGermanCulture = true;
                //        else
                //            usedOptExec.isUseGermanCulture = false;

                //        //usedOptExec.outputPath = Environment.CurrentDirectory + Path.DirectorySeparatorChar + ResultsSubDir;

                //        usedOptExec.outputPath = usedOptimizationFolder;

                //        usedOptExec.parent = this;
                //        currActionThread = new Thread(usedOptExec.runMerger);
                //    }
                //}
                currActionThread.Start();
                isActionRunning = true;

            }
            else
            {
                MessageBox.Show("Nur eine Aktion gleichzeitig erlaubt.", "",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.None);
            }

           
        }

        private void btnRunOptimization_Click(object sender, EventArgs e)
        {

        }

        private void gpExecution_Enter(object sender, EventArgs e)
        {

        }

        protected void stopOptimization()
        {
            usedOptExec.StopSignal = true;
        }

        protected void killCurrentActionThread()
        {
            if (currActionThread != null && currActionThread.IsAlive)
            {
                if (currentActionType == ActionType.DATA_LOAD)
                {
                    stopDataLoad();
                   // currActionThread.Join();
                }
                else
                {
                    //currActionThread.Abort();
                    stopOptimization();
                    
                }
                //isActionRunning = false;
            }
        }

        private void btnStopAction_Click(object sender, EventArgs e)
        {
            killCurrentActionThread();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            isQuietMessages = true;
            IsCloseRequested = true;
            killCurrentActionThread();

            if (isActionRunning)
            {
                e.Cancel = true;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private static void OpenExplorer(string path)
        {
            if (Directory.Exists(path))
                Process.Start("explorer.exe", path);
        }

        private void folderOpen(object sender, EventArgs e)
        {
            Button btnSender;
            String folderPath;
            btnSender = (Button)sender;

            if (btnSender == btnInputOpen || 
                btnSender == btnDataFolderOpen)
            {
                if (tbOptimizationName.Text.Trim()!="" && !checkOptimizationFolder(tbOptimizationName.Text,false))
                    return;

                if (btnSender == btnInputOpen)
                    folderPath = optimizationBaseDataFolderPath + baseDataFolder;
                else
                    folderPath = loadedDataPath;//loadedDataSubDirectory;

                checkBaseDataFolderOptimization(folderPath);//(optimizationBaseDataFolderPath + folderName);//+ baseDataFolder);

                OpenExplorer(folderPath);//(optimizationBaseDataFolderPath + baseDataFolder);
            }
            else if (btnSender == btnResultsOpen)
            {
                if (selectedOptimizationFolder == "")//(usedOptimizationFolder == "")
                    MessageBox.Show("Bitte einen Optimierungslauf auswählen.", "",//("Bitte erst eine Optimierung durchführen.", "",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.None);
                else
                    OpenExplorer(selectedOptimizationFolder);//(usedOptimizationFolder);
            }
        }

        private void mcStartPlanning_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void tbOptimizationName_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbExistingFolders_SelectedIndexChanged(object sender, EventArgs e)
        {   
            if  ( lbExistingFolders.SelectedItem!=null)
                tbOptimizationName.Text = lbExistingFolders.SelectedItem.ToString ();

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
        //private bool isAllowScenGen = false;
        //private bool isAllowDistrDef = false;
        //private void setAllowScenGen(bool isAllow)
        //{
        //    isAllowScenGen = isAllow;
        //    setAllowScenDef(isAllow);
        //    setAllowDistrDef(isAllow&isAllowDistrDef);
        //}

        //private void setAllowScenDef(bool isAllow)
        //{
        //    if (cmBScenType .InvokeRequired  )
        //    {
        //        cmBScenType.Invoke(new enableSetProc(setAllowScenDef), isAllow);
        //    }
        //    else
        //    {
        //        cmBScenType.Enabled = isAllow;
        //        tbScenName.Enabled = isAllow;
        //    }
        //}

        //private void setAllowDistrDef(bool isAllow)
        //{
        //    if (cmBDistributionType.InvokeRequired )
        //    {
        //        cmBDistributionType.Invoke(new enableSetProc(setAllowDistrDef), isAllow);
        //    }
        //    else
        //    {
        //        cmBDistributionType.Enabled = isAllow;
        //        tbStdDev.Enabled = isAllow;
        //    }
        //}

        private void setAllowBtnScenGen (bool isAllow)
        {
            //if (btnScenGen .InvokeRequired)
            //{
            //   btnScenGen .Invoke(new enableSetProc(setAllowBtnScenGen), isAllow);
            //}
            //else
            //{
            //   btnScenGen .Enabled = isAllow;
            //}
        }

        private void setAllowBtnRunOptimization(bool isAllow)
        {
            if (btnRunOptimization.InvokeRequired)
            {
                btnRunOptimization.Invoke(new enableSetProc(setAllowBtnRunOptimization), isAllow);
            }
            else
            {
                btnRunOptimization.Enabled = isAllow;
            }
        }

        private void setAllowBtnSelectSettingsFromPastSimulationRun(bool isAllow)
        {
            if (btnSelectSettingsFromPastSimulationRun.InvokeRequired )
            {
                btnSelectSettingsFromPastSimulationRun.Invoke(new enableSetProc(setAllowBtnSelectSettingsFromPastSimulationRun), isAllow);
            }
            else
            {
                btnSelectSettingsFromPastSimulationRun.Enabled = isAllow;
            }
        }

        //private void setAllowBtnRunOptimizationFixedProdQties(bool isAllow)
        //{
        //    if (btnRunOptimizationFixedProdQties.InvokeRequired)
        //    {
        //        btnRunOptimizationFixedProdQties.Invoke(new enableSetProc(setAllowBtnRunOptimizationFixedProdQties), isAllow);
        //    }
        //    else
        //    {
        //        btnRunOptimizationFixedProdQties.Enabled = isAllow;
        //    }
        //}

        //private void setAllowBtnMergeResults(bool isAllow)
        //{
        //    if (btnMergeResults.InvokeRequired)
        //    {
        //        btnMergeResults.Invoke(new enableSetProc(setAllowBtnMergeResults), isAllow);
        //    }
        //    else
        //    {
        //        btnMergeResults.Enabled = isAllow;
        //    }
        //}
        protected bool isAllowOptimization = false;
        private void allowOptimizationActions(bool isAllow)
        {
            isAllowOptimization = isAllow;
            setAllowBtnRunOptimization(isAllow);
            setAllowBtnSelectSettingsFromPastSimulationRun(isAllow);
            //initializeScenGen();
            //setAllowScenGen(isAllow);
            //setAllowBtnRunOptimizationFixedProdQties(isAllow);
            //setAllowBtnMergeResults(isAllow);
            //btnRunOptimization.Enabled = isAllow;
            //btnRunOptimizationFixedProdQties.Enabled = isAllow;
            //btnMergeResults.Enabled = isAllow;
        }
        private void allowOpenInputs(bool isAllow)
        {
            if (btnInputOpen.InvokeRequired)
            {
                btnInputOpen.Invoke(new enableSetProc(allowOpenInputs), isAllow);
            }
            else
            {
                btnInputOpen.Enabled = isAllow;
            }
        }

        private void allowOpenResults(bool isAllow)
        {
            if (btnResultsOpen.InvokeRequired)
            {
                btnResultsOpen.Invoke(new enableSetProc(allowOpenResults), isAllow);
            }
            else
            {
                btnResultsOpen.Enabled = isAllow;
            }
            
        }

        private void allowVisualizeResults(bool isAllow)
        {
            if (btnResultVisualization.InvokeRequired )
            {
                btnResultVisualization.Invoke(new enableSetProc(allowVisualizeResults), isAllow);
            }
            else
            {
                btnResultVisualization.Enabled = isAllow;
            }

        }


        protected string fixationFileName;// = "fixierung.xlsx";
        protected string exclusionFileName;// = "ausschluss.xlsx";

        private void lbBaseData_SelectedIndexChanged(object sender, EventArgs e)
        {   
            int i;
            //protected static string[] fileNameBases = { "WareHouseCodes", 
            //                                       "Plantlocations", 
            //                                       "Produktionstonnage",
            //                                       "Arbeitspläne und Stammdaten",
            //                                       "Fehlende_AP_für_SalesArtikel",
            //                                       "sales plan",
            //                                       "Schichten",
            //                                       "HistoricalTransports_NorthEastern_Europe",
            //                                       "HistoricalTransports_SouthWestern_Europe",
            //                                       "Freight_cost_matrix",
            //                                       "Produktionskosten",
            //                                       "SSI Datei_Lagerbestand",
            //                                       "Mindestbestand",
            //                                       "Zuordnung_Ladeort_zu_Produktionsort"
            //            
            if (lbBaseData.SelectedItem == null)
                return;
            int f;
            baseDataFolder = lbBaseData.SelectedItem.ToString();
            cR = checkBaseDataFolder(baseDataFolder);
            baseDataFolderOk = true;
            //f=baseDataFoldersDb[baseDataFolder];
           // usedFL = Convert.ToString(baseDataFoldersDb[baseDataFolder], currC);
            for (i = 0; i < cR.fCR.Length; i++)
            {
                if (!(cR.fCR[i].fS == File_State.EXISTS))
                {
                    baseDataFolderOk = false;

                    cLBCheckResult.SetItemChecked(i, false);
                }
                else
                {
                    cLBCheckResult.SetItemChecked(i, true);
                }
            }
           // for (i=0;i<cLBCheckResult .Items .Count;i++)
             //   cLBCheckResult.SetItemChecked(i, true);


            if (cR.salesPlanNameStatus == BaseData_CheckResult.SalesPlanCheckResult.OK)
            {  
                tbBeginn.Text = MONTHS[cR.startMonth];
                tbEnde.Text = MONTHS[cR.endMonth];
                lbInvalidSaleplan.Text = "";

                locationTranslationFilename = cR.fCR[0].fileName;
                whseSouthWestEuropeFilename = cR.fCR[1].fileName;
                tonnageFileName = cR.fCR[2].fileName;
                baseDataAndWorkingPlansFileName = cR.fCR[3].fileName;
                additionalBaseDataAndWorkingPlansFileName = cR.fCR[4].fileName;
                shiftsFileName = cR.fCR[5].fileName;
                transpFileName = cR.fCR[6].fileName;
                transpSouthWestEuropeFileName = cR.fCR[7].fileName;
                interPlantTranspFileName = cR.fCR[8].fileName;
                costPerShiftFileName = cR.fCR[9].fileName;
                iniStocksFilename = cR.fCR[10].fileName;
                minimumStocksFilename = cR.fCR[11].fileName;
                transportStartSubstituteFileName = cR.fCR[12].fileName;
                salesPlanFileName = cR.fCR[13].fileName;
                //demandfileName = cR.fCR[13].fileName;
                whseCapFileName = cR.fCR[14].fileName;
                fixationFileName = cR.fCR[15].fileName;
                exclusionFileName = cR.fCR[16].fileName;
            }
            else
            {
                if (cR.salesPlanNameStatus == BaseData_CheckResult.SalesPlanCheckResult.WRONG_FORMAT)
                    lbInvalidSaleplan.Text = "Ungültiges Format des Dateinames. Format muss sein \"sales plan mm-mm\"";
                tbBeginn.Text = "";
                tbEnde.Text = "";
                baseDataFolderOk = false;
            }
            
            btnDataLoad.Enabled = baseDataFolderOk && isOptimizationGiven ;
            checkBtnBaseDataLoadAndEdit();
        }

        private void cLBCheckResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            isBaseDataElementSelected = true;
            checkBtnBaseDataLoadAndEdit();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnLoadSettingsFromOptimizationRun_Click(object sender, EventArgs e)
        {  
            //DataRowView dRV;

            
                
            //        dRV = dGVOptimizationRunConfigs .CurrentRow .DataBoundItem  as DataRowView;

            //        if (dRV != null)
            //        {
            //            tbMinProdQty.Text = dRV.Row.ItemArray[3].ToString();
            //            tbSplittingQty.Text = dRV.Row.ItemArray[4].ToString();

            //            //cbRelaxSplitConstraint.Checked = Convert.ToBoolean(dRV.Row.ItemArray[5].ToString(), currC);
            //            cbBasketUsage.Checked = Convert.ToBoolean(dRV.Row.ItemArray[5].ToString(), currC);

            //            tbCapFactor.Text = dRV.Row.ItemArray[6].ToString();
            //            cbIsOnlyProductionOptimization.Checked = Convert.ToBoolean(dRV.Row.ItemArray[7].ToString(), currC);

            //            tbMinInvFactor.Text = dRV.Row.ItemArray[8].ToString();
            //            //currDataRow[3] = Convert.ToString(currOptRunConf.minProdQty, currC);

            //            //currDataRow[4] = Convert.ToString(currOptRunConf.splitQty, currC);


            //            //currDataRow[5] = Convert.ToString(currOptRunConf.isAllowSplittingIfInFixations, currC); ;

            //            //currDataRow[6] = Convert.ToString(currOptRunConf.isUseBaskets, currC);


            //            //currDataRow[7] = Convert.ToString(currOptRunConf.capacityFactorForMachines, currC);


            //            //currDataRow[8] = Convert.ToString(currOptRunConf.isOnlyProductionOptimization, currC);

            //            //currDataRow[9] = Convert.ToString(currOptRunConf.minInventoryFactor, currC);
            //        }
                
            
        }

        private bool checkDataLoad(string loadPath)
        {
            bool isLoad;

            try
            {
                if (!File.Exists(loadPath + plantfileName))
                    return false;

                if (!File.Exists(loadPath + customerfileName))
                    return false;

                if (!File.Exists(loadPath + resourcefileName))
                    return false;

                if (!File.Exists(loadPath + productfileName))
                    return false;
                if (!File.Exists(loadPath + periodfileName))
                    return false;
                if (!File.Exists(loadPath + processfileName))
                    return false;
                if (!File.Exists(loadPath + capacityfileName))
                    return false;
                if (!File.Exists(loadPath + transportplantfileName))
                    return false;
                if (!File.Exists(loadPath + transportcustomerfileName))
                    return false;
                if (!File.Exists(loadPath + demandfileName))
                    return false;

                if (!File.Exists(loadPath + palletspertonfileName))
                    return false;

                if (!File.Exists(loadPath + basketsfileName))
                    return false;

                if (!File.Exists(loadPath + histProdFileName))
                    return false;
                if (!File.Exists(loadPath + iniStFileName))
                    return false;
                if (!File.Exists(loadPath + warehouseCapacitiesFileName))
                    return false;

                if (!File.Exists(loadPath + articleNamesFileName)) 
                    return false;

                if (!File.Exists(loadPath + dataLoadPropertiesFileName ))
                    return false;
            }
            catch (Exception e)
            {
                GUI_ExceptionHandler(new OptimizerException(OptimizerException.OptimizerExceptionType.UnknownError, e.Message));
                return false;
            }

            return true;
        }

        protected void setOptimizationFolder()
        {
            string potentialDataLoadPath;

            optimizationFolderPath = optimizationBasePath + tbOptimizationName.Text + Path.DirectorySeparatorChar;
            optimizationBaseDataFolderPath = optimizationFolderPath + optimizationBaseDataFolderName + Path.DirectorySeparatorChar;
            resultsBasePath = optimizationFolderPath + ResultsSubDir + Path.DirectorySeparatorChar;
            loadedDataPath = optimizationFolderPath + loadedDataSubDirectory + Path.DirectorySeparatorChar;//currentBasePath + Path.DirectorySeparatorChar + loadedDataSubDirectory + Path.DirectorySeparatorChar;

            potentialDataLoadPath = optimizationBasePath + tbOptimizationName.Text + Path.DirectorySeparatorChar + loadedDataSubDirectory + Path.DirectorySeparatorChar;

            allowOpenInputs(true);
            isDataLoaded = checkDataLoad(potentialDataLoadPath);
            //currOptResult = null;
            //if (isDataLoaded)
            updateOptimizationRuns();

            cbDataLoaded.Checked = isDataLoaded;
            isReloadRequiredScenGen =isDataLoaded;
            if (isDataLoaded)
            {
                showDLProperties(tbOptimizationName.Text);
                allowOptimizationActions(true && !isOptFoldersExceedCriticalSize);
            }
        }

        
        //private void importLoadedDemandData()
        //{
        //    CSV_File productFile,customersFile,demandFile,periodsFile;
        //    DataTable productsTable, customersTable, demandTable,periodsTable, xCustomersTable;
        //    DataRow currDataRow;
        //    int i,scenColumnI;
        //    int countryStartPos, postalCodeStartPos;
        //    //string currCustomerName;
        //    string [] customerNameComponents;

        //    clearAllDemandTables();

        //    productFile = new CSV_File(loadedDataPath + productfileName);
        //    productFile.openForReading();
        //    productFile.IsHeadLine = false;
        //    productFile.CSVDelimiter = CSV_SEMCOL_DELIMITER;
        //    productFile.setInputColSpecsByStrings(productTabCSVSpec);
        //    productsTable = productFile.getData();
        //    productFile.closeDoc();

        //    customersFile = new CSV_File(loadedDataPath + customerfileName );
        //    customersFile.openForReading();
        //    customersFile.IsHeadLine = false;
        //    customersFile.CSVDelimiter = CSV_SEMCOL_DELIMITER;
        //    customersFile.setInputColSpecsByStrings(customersTabCSVSpec);
        //    customersTable = customersFile.getData();
        //    customersFile.closeDoc();

        //    periodsFile = new CSV_File(loadedDataPath + periodfileName);
        //    periodsFile.openForReading();
        //    periodsFile.IsHeadLine = false;
        //    periodsFile.CSVDelimiter = CSV_SEMCOL_DELIMITER;
        //    periodsFile.setInputColSpecsByStrings(periodsTabCSVSpec);
        //    periodsTable = periodsFile.getData();
        //    periodsFile.closeDoc();

        //    for (i = 0; i < customersTable.Rows.Count; i++)
        //    {
        //        currDataRow = customersTable.Rows[i];
        //        currDataRow.BeginEdit();
        //        customerNameComponents =((string) currDataRow [0]).Split (SNP_DataProvider .KEY_COMPONENT_SEPARATOR[0] );
        //        currDataRow[0] = customerNameComponents[0];
        //        if (customerNameComponents.Length > 1)
        //        {
        //            currDataRow[2] = customerNameComponents[1];
        //            currDataRow[3] = customerNameComponents[2];
        //        }
        //        else
        //        {//For compatibility to old names without separators
        //            currDataRow[2] = "";
        //            currDataRow[3] = "";
        //        }
        //        currDataRow.EndEdit();
        //    }

        //    demandFile = new CSV_File(loadedDataPath + demandfileName);
        //    demandFile.openForReading();
        //    demandFile.IsHeadLine = false;
        //    demandFile.CSVDelimiter = CSV_SPACE_DELIMITER;
        //    demandFile.setInputColSpecsByStrings(demandTabCSVSpec);
        //    demandTable = demandFile.getData();
        //    demandFile.closeDoc();

        //    for (i = 0; i < productsTable.Rows.Count; i++)
        //    {
        //        currDataRow = productsTable.Rows[i];
        //        currDataRow.BeginEdit();
        //        currDataRow[1] = i;
        //        currDataRow.EndEdit();
        //    }

        //    for (i = 0; i < customersTable.Rows.Count; i++)
        //    {
        //        currDataRow = customersTable.Rows[i];
        //        currDataRow.BeginEdit();
        //        currDataRow[1] = i;
        //        currDataRow.EndEdit();
        //    }

        //    for (i = 0; i < periodsTable.Rows.Count; i++)
        //    {
        //        currDataRow = periodsTable.Rows[i];
        //        currDataRow.BeginEdit();
        //        currDataRow[1] = i;
        //        currDataRow.EndEdit();
        //    }

        //    for (i = 0; i < demandTable.Rows.Count; i++)
        //    {
        //        currDataRow = demandTable.Rows[i];
        //        currDataRow.BeginEdit();
        //        currDataRow[4] = 0;
        //        currDataRow.EndEdit();
        //    }
        //    usedInMemDB.writeData(productsTable, productTabName, true);
        //    usedInMemDB.writeData(customersTable, customerTabName, true);
        //    usedInMemDB.writeData(periodsTable, periodTabName, true);
        //    usedInMemDB.writeData(demandTable, demandTabName, true);
        //    usedInMemDB.execSqlCommand(aggrCustChainSqlCmd);
        //    usedInMemDB.execSqlCommand(fractionsSqlCmd);
        //  // setScenSelection(null);
        //}
        protected bool isReloadRequiredScenGen=false;
        private void tbOptimizationName_TextChanged_1(object sender, EventArgs e)
        {
            if (tbOptimizationName.Text.Trim() == "")
            {
                btnDataLoad.Enabled = false;
                isOptimizationGiven = false;
                //btnScenGen .Enabled =false;
                checkBtnBaseDataLoadAndEdit();
                return;
            }
            isOptimizationGiven = true;

            btnDataLoad.Enabled = baseDataFolderOk && isOptimizationGiven;

            checkBtnBaseDataLoadAndEdit();

            if (!checkOptimizationFolder(tbOptimizationName.Text, false))
            {
                isDataLoaded = false;
                allowOpenInputs(false);
                cbDataLoaded.Checked = isDataLoaded;
                //btnScenGen.Enabled =isDataLoaded ;
            }
            else
            {
                setOptimizationFolder();
            }

            if (!isDataLoaded)
            {
                allowOptimizationActions(false);
            }
            else
            {
               // importLoadedDemandData();
            }
           
            //Check Dataload
        //     protected static string plantfileName = "plants.csv";
        //protected static string customerfileName = "customers.csv";
        //protected static string resourcefileName = "resources.csv";
        //protected static string productfileName = "products.csv";
        //protected static string periodfileName = "periods.csv";
        //protected static string processfileName = "processes.csv";
        //protected static string capacityfileName = "capacities.csv";
        //protected static string transportplantfileName = "transportsplant.csv";
        //protected static string transportcustomerfileName = "transportscustomer.csv";
        //protected static string demandfileName = "demands.csv";
        //protected static string palletspertonfileName = "pallets_per_ton.csv";
        //protected static string basketsfileName = "baskets.csv";
        //protected static string histProdFileName = "histProduction.csv";
        //protected static string iniStFileName = "startBestand.csv";
        //protected static string warehouseCapacitiesFileName = "lagerkapazitaeten.csv";

        }

        protected void copyFile(string fileName,string sourcePath, string destPath)
        {
            FileStream destFileStream,sFileStream;
            int bytesRead,chunkNr;
            byte [] buff;

            buff = new byte[1000];

            destFileStream = new FileStream(destPath+fileName, FileMode.Create);

            sFileStream=new FileStream (sourcePath +fileName,FileMode.Open  );

            chunkNr = 0;


            bytesRead = sFileStream.Read(buff, 0, buff.Length);

            while (bytesRead > 0)
            {
                destFileStream.Write(buff, 0, bytesRead);
                bytesRead = sFileStream.Read(buff, 0, buff.Length);
                chunkNr = chunkNr + 1;
            }

            destFileStream.Close();
            destFileStream.Dispose();
            sFileStream.Close();
            sFileStream.Dispose();
            //Dim destFileStream As FileStream
            //Dim bytesRead As Integer
            //Dim buff(10000) As Byte
            //Dim chunkNr As Integer

            //destFileStream = New FileStream(destFileName, FileMode.Create)

            //chunkNr = 0

            //bytesRead = sFileStr.Read(buff, 0, buff.Length)

            //While (bytesRead > 0)
            //    destFileStream.Write(buff, 0, bytesRead)
            //    bytesRead = sFileStr.Read(buff, 0, buff.Length)
            //    chunkNr = chunkNr + 1
            //End While

            //destFileStream.Close()
            //destFileStream.Dispose()



        }

        private void checkBaseDataFolderOptimization(string baseDataFolderOptimization)
        {
            
                            if (!Directory.Exists(baseDataFolderOptimization))
                            {
                                Directory.CreateDirectory(baseDataFolderOptimization);
                            }
            
        }


        

        private void exportDbToXlsx(string sqlStr, string xlsxFileName,string sheetName)
        {
            DataTable dbTab;
            Excel_Document nDoc;
            dbTab = usedDB.getData(sqlStr);
            dbTab.TableName = sheetName;
            nDoc = new Excel_Document(xlsxFileName);
            nDoc.IsCreateIfNotExist = true;
            nDoc.IsHeadLine = true;
            nDoc.OpenDoc();
            nDoc.writeData(dbTab);
            nDoc.CloseDoc();
        }
        
        private void btnEditBaseData_Click(object sender, EventArgs e)
        {
            string selectedItem;
            string selectedFile;
            DialogResult isCreateCopy;
            string baseDataFolderOptimization;
            string sqlStr;
            int i;

            if (baseDataFolderOk &&  cLBCheckResult.SelectedItem!=null)//lbBaseDataToEdit.SelectedItem!=null)
            {
                selectedItem = cLBCheckResult.SelectedItem.ToString();// lbBaseDataToEdit.SelectedItem .ToString ();
                i=0;
                foreach (object o in cLBCheckResult .Items )
                {
                    if (selectedItem ==o.ToString ())
                        break;

                    i++;
                }
                selectedFile = cR.fCR [i].fileName;
               //dataTypeDBTable[i, 3];//cR.fCR [i].fileName ;
               

               

                try
                {
                    optimizationFolderPath = optimizationBasePath + tbOptimizationName.Text + Path.DirectorySeparatorChar;
                    optimizationBaseDataFolderPath = optimizationFolderPath + optimizationBaseDataFolderName + Path.DirectorySeparatorChar;
                    baseDataFolderOptimization = optimizationBaseDataFolderPath + baseDataFolder;
                    if (tbOptimizationName.Text.Trim()!="" && checkOptimizationFolder(tbOptimizationName.Text, true))
                    {   
                        if (!File.Exists(baseDataFolderOptimization + Path.DirectorySeparatorChar + selectedFile))
                        {
                            isCreateCopy = MessageBox.Show("Kopie der ausgewählten Daten existiert noch nicht in Optimierungsakte. Soll eine Kopie erstellt werden?", "",
                                      MessageBoxButtons.YesNo,
                                      MessageBoxIcon.None);

                            if (isCreateCopy == DialogResult.Yes)
                            {
                                //if (!Directory.Exists(baseDataFolderOptimization))
                                //{
                                //    Directory.CreateDirectory(baseDataFolderOptimization);
                                //}

                                checkBaseDataFolderOptimization(baseDataFolderOptimization);
                            }
                            else
                                return;

                            copyFile(selectedFile, baseDataMainPath + baseDataFolder + Path.DirectorySeparatorChar, baseDataFolderOptimization+Path.DirectorySeparatorChar );
                            //sqlStr = "Select " + dataTypeDBTable[i, 1] + " from " + dataTypeDBTable[i, 2] + " where BaseData_FolderNumber=" + usedFL;

                            //exportDbToXlsx(sqlStr, baseDataFolderOptimization + Path.DirectorySeparatorChar + selectedFile, dataTypeDBTable[i, 2]);
                        }
                        Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
                        excel.Visible = true;
                        excel.DisplayAlerts = true;
                        excel.AskToUpdateLinks = true;
                        Microsoft.Office.Interop.Excel.Workbook wb = excel.Workbooks.Open(baseDataFolderOptimization + Path.DirectorySeparatorChar + selectedFile);
                        do
                        {
                            Thread.Sleep(EXCEL_POLLING_INTERVAL);
                        }
                        while (excel.Visible);
                        
                      //  wb.Close();
                        Marshal.ReleaseComObject(wb);

                        excel.Quit();
                        Marshal.ReleaseComObject(excel);
                        wb = null;
                        excel = null;

                        isCreateCopy = MessageBox.Show("Damit Änderungen der Stammdaten wirksam werden, müssen diese erneut geladen werden.", "",
                                     MessageBoxButtons.OK,
                                     MessageBoxIcon.None);
                    }
                }
                catch (Exception exc)
                {
                    GUI_ExceptionHandler(new OptimizerException(OptimizerException.OptimizerExceptionType.UnknownError, exc.Message));
                    return;
                }
            }
        }

        private void lbBaseDataToEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            isBaseDataElementSelected = true;
            checkBtnBaseDataLoadAndEdit();
        }

        private void lbUsedBaseDataFolder_Click(object sender, EventArgs e)
        {

        }

        string selectedOptimizationFolder;
        bool isUseGermanCulture;
        /*
            if (currC.NumberFormat.NumberDecimalSeparator == "," && currC.NumberFormat.NumberGroupSeparator == ".")
                        isUseGermanCulture = true;
                    else
                        isUseGermanCulture = false;

            selectedOptimizationRun  = currentOptimizationRuns[dGVOptimizationRuns.CurrentRow.Index];

             selectedOptimizationFolder = resultsBasePath +  selectedOptimizationRun.RunFolderName;
             fillResults(false, isUseGermanCulture, true);
        */
        private void dGVOptimizationRuns_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dGVOptResults_CellContentClick(object sender, DataGridViewRowStateChangedEventArgs e)
        {

        }

        private void dGVOptimizationRuns_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {

        }
        /*
         if (dGVOptimizationRuns.CurrentRow != null && dGVOptimizationRuns.CurrentRow.Index < currentOptimizationRuns.Count)
            {
                if (currC.NumberFormat.NumberDecimalSeparator == "," && currC.NumberFormat.NumberGroupSeparator == ".")
                    isUseGermanCulture = true;
                else
                    isUseGermanCulture = false;

                selectedOptimizationRun = currentOptimizationRuns[dGVOptimizationRuns.CurrentRow.Index];

                selectedOptimizationFolder = resultsBasePath + selectedOptimizationRun.RunFolderName;
                fillResults(false, isUseGermanCulture, true);
            }
          */

        public void setOptimizationRunAsSelected()
        {
            string currentOptimizationRunFolderName;
            int i = 0;

            if (dGVOptimizationRuns.InvokeRequired)
            {
                dGVOptimizationRuns.Invoke(new returnVoidWithoutParams(setOptimizationRunAsSelected));
            }
            else
            {
                currentOptimizationRunFolderName = trimLongPath(usedOptimizationFolder);

                foreach (OptimizationRun run in currentOptimizationRuns)
                {
                    if (run.RunFolderName == currentOptimizationRunFolderName)
                        break;

                    i++;
                }

                dGVOptimizationRuns.CurrentCell = dGVOptimizationRuns.Rows[i].Cells[0];
            }
            selectedOptimizationFolder = usedOptimizationFolder;
        }

        protected void setSelectedOptimizationRun()
        {
            OptimizationRunConfig currConfig;

            if (dGVOptimizationRuns.CurrentRow != null && dGVOptimizationRuns.CurrentRow.Index < currentOptimizationRuns.Count)
            {
                //if (currC.NumberFormat.NumberDecimalSeparator == "," && currC.NumberFormat.NumberGroupSeparator == ".")
                //    isUseGermanCulture = true;
                //else
                //    isUseGermanCulture = false;

                selectedOptimizationRun = currentOptimizationRuns[dGVOptimizationRuns.CurrentRow.Index];

                selectedOptimizationFolder = resultsBasePath + selectedOptimizationRun.RunFolderName;

                currConfig = new OptimizationRunConfig();
                currConfig.loadConfig(selectedOptimizationFolder+Path.DirectorySeparatorChar +settingsFileName);
                
                //if (currOptResult != null)
                    fillResults(false, currConfig .isUseGermanCulture ,true,true);
                    visualizeResults();
            }
        }

        private void dGVOptimizationRuns_SelectionChanged(object sender, EventArgs e)
        {
           setSelectedOptimizationRun();
        }

        private void groupBox6_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void btnSelectSettingsFromPastSimulationRun_Click(object sender, EventArgs e)
        {
            RunParameterSelection rPS;
            OptimizationRunConfig selectedRunConfig;
            rPS = new RunParameterSelection();
            //public void setParams(string oBPath,CultureInfo c,string rSubDir,string settingsFName, List<Form1.OptimizationRun> optRuns,string [] oRunIdentifierNames)
            rPS.setParams(optimizationBasePath, currC, ResultsSubDir, settingsFileName, currentOptimizationRuns, optRunIdentifierNames);
            selectedRunConfig = rPS.selectRunConfig();
            if (selectedRunConfig != null)
            {
                tbMinProdQty.Text = Convert.ToString(selectedRunConfig.minProdQty, currC);  //dRV.Row.ItemArray[3].ToString();
                tbSplittingQty.Text = Convert.ToString(selectedRunConfig.splitQty, currC);//dRV.Row.ItemArray[4].ToString();

                //cbRelaxSplitConstraint.Checked = Convert.ToBoolean(dRV.Row.ItemArray[5].ToString(), currC);
                cbBasketUsage.Checked = selectedRunConfig.isUseBaskets;//Convert.ToBoolean(dRV.Row.ItemArray[5].ToString(), currC);

                tbCapFactor.Text = Convert.ToString(selectedRunConfig.capacityFactorForMachines, currC);//dRV.Row.ItemArray[6].ToString();
                cbIsOnlyProductionOptimization.Checked = selectedRunConfig.isOnlyProductionOptimization;//Convert.ToBoolean(dRV.Row.ItemArray[7].ToString(), currC);

                tbMinInvFactor.Text = Convert.ToString(selectedRunConfig.minInventoryFactor, currC);//dRV.Row.ItemArray[8].ToString();

                cbFixPlants.Checked = selectedRunConfig.isFixPlants;
                cbUseFixationsExclusions.Checked = selectedRunConfig.isUseFixationsExlusions;
                cmBBuild.SelectedIndex = (Int32)selectedRunConfig.expansionStage;
                tbFixationThreshold.Text = Convert.ToString(selectedRunConfig.fixationThreshold, currC);
                tbMaxGap.Text = Convert.ToString(selectedRunConfig.maxGap, currC);
            }
        }

        protected bool tbDoubleInputCheck (string input)
        {
            if (input.Contains(currC.NumberFormat.NumberGroupSeparator))
            {
                MessageBox.Show("Keine Tausendertrennzeichen erlaubt.", "Eingabefehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            try
            {
                if (input.Trim()!="")
                 Convert.ToDouble(input, currC);

            }
            catch (Exception e)
            {
                MessageBox.Show("Keine gültige reelle Zahl.", "Eingabefehler",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void checkTbInput(TextBox sender)
        {
            
            if (!tbDoubleInputCheck(sender.Text))
            {
               sender.Text = "";
            }
            
        }
        private void tbMinProdQty_TextChanged(object sender, EventArgs e)
        {
            checkTbInput((TextBox)sender);
           // currC.NumberFormat .
            
            //if (!tbDoubleInputCheck(tbMinProdQty.Text))
            //{
            //    tbMinProdQty.Text = "";
            //}
            //else
            //{
            //    tbMinProdQty.Text=Convert.ToString(Convert.ToDouble(tbMinProdQty.Text, currC), currC);
            //}
        }

        private void tbSplittingQty_TextChanged(object sender, EventArgs e)
        {
            checkTbInput((TextBox)sender);
            //if (!tbDoubleInputCheck(tbSplittingQty.Text))
            //{
            //    tbSplittingQty.Text = "";
            //}
            //else
            //{
            //    tbSplittingQty.Text = Convert.ToString(Convert.ToDouble(tbSplittingQty.Text, currC), currC);
            //}
        }

        private void tbMinInvFactor_TextChanged(object sender, EventArgs e)
        {
            checkTbInput((TextBox)sender);
            //if (!tbDoubleInputCheck(tbMinInvFactor.Text))
            //{
            //    tbMinInvFactor.Text = "";
            //}
            //else
            //{
            //    tbMinInvFactor.Text = Convert.ToString(Convert.ToDouble(tbMinInvFactor.Text, currC), currC);
            //}
        }

        private void tbCapFactor_TextChanged(object sender, EventArgs e)
        {
            checkTbInput((TextBox)sender);
            //if (!tbDoubleInputCheck(tbCapFactor.Text))
            //{
            //    tbCapFactor.Text = "";
            //}
            //else
            //{
            //    tbCapFactor.Text = Convert.ToString(Convert.ToDouble(tbCapFactor.Text, currC), currC);
            //}
        }

        private void dGVOptimizationRuns_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }
/*
        protected static string plantfileName = "plants.csv";
        protected static string customerfileName = "customers.csv";
        protected static string resourcefileName = "resources.csv";
        protected static string productfileName = "products.csv";
        protected static string periodfileName = "periods.csv";
        protected static string processfileName = "processes.csv";
        protected static string capacityfileName = "capacities.csv";
        protected static string transportplantfileName = "transportsplant.csv";
        protected static string transportcustomerfileName = "transportscustomer.csv";
        protected static string demandfileName = "demands.csv";
        protected static string palletspertonfileName = "pallets_per_ton.csv";
        protected static string basketsfileName = "baskets.csv";
        protected static string histProdFileName = "histProduction.csv";
        protected static string iniStFileName = "startBestand.csv";
        protected static string warehouseCapacitiesFileName = "lagerkapazitaeten.csv";
        protected static string articleNamesFileName = "artikelnamen.csv";
        protected static string productsNotConsideredFileName = "ignorierteProdukte.csv";
        protected static string processesNotConsideredFileName = "ignorierteProzesse.csv";
        protected static string resourcesNotConsieredFileName = "ignorierteMaschinen.csv";
        */
        protected static string plantLocationFileName = "Plantlocations.csv";
        protected static string dataFileName = "optimierung_output_graphics.csv";
        protected static string transportDataFileName = "optimierung_output_graphics_ic.csv";
        protected static string imageFileName = "deutschland.png";
        private void btnResultVisualization_Click(object sender, EventArgs e)
        {
            ProductionMap currMap;
            Exception resultExc;
             //public ProductionMap(String planlocationsFile, String dataFile, String transportDataFile, String imageFile, String version)
            try
            {
                //currMap = new ProductionMap(plantLocationFileName, dataFileName, transportDataFileName, imageFileName, "0", selectedOptimizationFolder);  //(selectedOptimizationFolder);
                CultureInfo c = currC;
                if (currC.Name == "de-DE")
                {
                    currMap = new ProductionMap("1", "0", selectedOptimizationFolder);
                }
                else
                {
                    currMap = new ProductionMap("1", "1", selectedOptimizationFolder);
                }
                currMap.ShowDialog();

                resultExc = currMap.ResultException;
                if (resultExc != null)
                    throw resultExc;
            }
            catch (Exception exc)
            {
                GUI_ExceptionHandler(new OptimizerException(OptimizerException.OptimizerExceptionType.UnknownError, exc.Message));
                return;
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        List<string> selectedProduct;
        List<string> selectedCustomer;
        List<string> selectedLocation;

        private bool isInSetSelection=false;
        private string usedSelectionGroup = "default";
        private void setScenSelection(Object sender)
        {
            //List<FilterTableCollection.FilterEntries> fEL;
            //FilterTableCollection.FilterEntries pEL, lEL, cEL;
            //if (isInSetSelection)
            //    return;
            //isInSetSelection = true;

            //selectedProduct.Clear();
            //selectedCustomer.Clear();
            //selectedLocation.Clear();

            //foreach (ListViewItem lI in lVScenProducts .SelectedItems )
            //{
            //    selectedProduct.Add(lI.Text);
            //}

            //foreach (ListViewItem lI in lVScenCustomers .SelectedItems )
            //{
            //    selectedCustomer .Add(lI.Text);
            //}

            //foreach (ListViewItem lI in lVScenLocations.SelectedItems )
            //{
            //    selectedLocation .Add(lI.Text);
            //}


            //fEL = new List<FilterTableCollection.FilterEntries>();
            //pEL = new FilterTableCollection.FilterEntries();
            //lEL = new FilterTableCollection.FilterEntries();
            //cEL = new FilterTableCollection.FilterEntries();

            //pEL.colName = fullDemandTabColumnSpec [0, 0];
            //pEL.filterEntries = selectedProduct;

            //lEL.colName = fullDemandTabColumnSpec  [3, 0];
            //lEL.filterEntries = selectedLocation;

            //cEL.colName = fullDemandTabColumnSpec [2, 0];
            //cEL.filterEntries = selectedCustomer;
            //fEL.Add(pEL);
            //fEL.Add(lEL);
            //fEL.Add(cEL);

           // demandDataFTC.setFilter(usedSelectionGroup);
          //  isInSetSelection = false;
        }
        private void SelectScenarioContent(object sender, EventArgs e)
        {
            setScenSelection(sender);
        }

        private void cBCheckCountryMissingTransportConnectionsCustomer_CheckedChanged(object sender, EventArgs e)
        {

        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        dGVQ.DataSource = usedInMemDB.getData(tbQ.Text);
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("SQL-Fehler:" + ex.Message, "",
        //                          MessageBoxButtons.OK,
        //                          MessageBoxIcon.None);
        //    }
        //}
        //protected bool isReloadLoadedData=false;
        //private void btnScenGen_Click(object sender, EventArgs e)
        //{
        //    usedScenGenerator .generateScenarios (loadedDataPath,isReloadRequiredScenGen);
        //  //  isReloadLoadedData =false;
        //    isReloadRequiredScenGen =false;
        //}

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        protected string selectedTabName;

        //private void initializeScenGen()
        //{
        //    if (selectedTabName == "tbPGScenarios" && isAllowOptimization)//(tCInput.SelectedTab.Name == "tbPGScenarios" && isAllowOptimization  )
        //    {
        //        usedScenGenerator.generateScenarios(loadedDataPath, isReloadRequiredScenGen);
        //        visualizeScens();
        //        isReloadRequiredScenGen = false;
        //    }
        //}
        private void tCInput_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Laden der Daten für die Szenarien
           // int i = 0;
            selectedTabName = tCInput.SelectedTab.Name;
           //initializeScenGen();
        }
        protected string[] scenarioTypes = { "Standorte Kundenkette", "Gesamtnachfrage" };
        protected string[] distributionTypes = { "Normalverteilung", "Gleichverteilung" };

        //public void visualizeScens()
        //{
        //    DataTable vScenTab;
        //    int i;
        //    string[] currLine;
        //    List<ScenGen.Scenario> scenList;

        //    vScenTab = new DataTable();
        //    currLine = new string[2];

        //    vScenTab.Columns.Add("Name");
        //    vScenTab.Columns.Add("Typ");
        //    scenList=usedScenGenerator.getAllScenarios();
        //    foreach (ScenGen .Scenario currScen in scenList)
        //    {
        //        currLine[0] = currScen.Name;
        //        currLine[1] = scenarioTypes[(Int32) currScen.Type];
        //        vScenTab.Rows.Add(currLine);
        //    }

        //    dGVScens.DataSource = vScenTab;
        //}

        ScenGen.DistributionType usedDistrType;
        double usedStdDev;
        ScenGen.Scenario.ScenType usedScenType;

        //private void btnCreateScen_Click(object sender, EventArgs e)
        //{
        //    string usedScenName;

        //    if (usedNumberOfReplications > 0)
        //    {
        //        usedScenName = tbScenName.Text;
        //        if (isMakeNewDistr)
        //        {
        //            usedScenGenerator.setDistribution(usedDistrType, usedStdDev,usedMeanChange);
        //            isMakeNewDistr = false;
        //        }
        //        setAllowScenGen(false);
        //        usedScenGenerator.makeScenario(usedScenName, usedScenType, usedNumberOfReplications);

        //        MessageBox.Show("Szenario erfolgreich erstellt.", "",//("Bitte erst eine Optimierung durchführen.", "",
        //                          MessageBoxButtons.OK,
        //                          MessageBoxIcon.None);

        //        visualizeScens();
        //        setScenAsSelected(usedScenName);
        //        setAllowScenGen(true);
        //    }
        //    else
        //        MessageBox.Show("Es muss mindestens 1 Replikation erzeugt werden.", "",//("Bitte erst eine Optimierung durchführen.", "",
        //                          MessageBoxButtons.OK,
        //                          MessageBoxIcon.None);
        //}
        protected bool isMakeNewDistr = true;
        //private void tbStdDev_TextChanged(object sender, EventArgs e)
        //{
        //    int stdDevPct;

        //    try
        //    {
        //        stdDevPct = Convert.ToInt32(tbStdDev.Text, CultureInfo.InvariantCulture);
        //    }
        //    catch (Exception exc)
        //    {
        //        tbStdDev.Text = "";
        //        return;
        //    }

        //    //if (usedDistrType == DistributionType.UNIFORM && stdDevPct > MAX_UNIF_PCT)
        //    //{
        //    //    tbStdDev .Text ="";
        //    //    return;
        //    //}

        //    usedStdDev = ((double)stdDevPct) / 100;
        //    isMakeNewDistr = true;
        //}

        //private void cmBDistributionType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    int i;
        //    string typeStr;

        //    typeStr = cmBDistributionType.SelectedItem.ToString();
        //    for (i = 0; i < distributionTypes.Length; i++) //di .Length; i++)
        //    {
        //        if (distributionTypes[i] == typeStr)
        //            break;
        //    }

        //    usedDistrType = (ScenGen.DistributionType)i;
        //    isMakeNewDistr = true;
        //}

        //private void cmBScenType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    int i;
        //    string typeStr;

        //    typeStr = cmBScenType.SelectedItem.ToString();
        //    for (i = 0; i < scenarioTypes.Length; i++)
        //    {
        //        if (scenarioTypes[i] == typeStr)
        //            break;
        //    }

        //    usedScenType = (ScenGen.Scenario.ScenType)i;

        //    if (usedScenType == ScenGen.Scenario.ScenType.LOCATIONS)
        //    {   
        //        isAllowDistrDef = false;
        //        setAllowScenGen(isAllowScenGen);
        //    }
        //    else if (usedScenType == ScenGen.Scenario.ScenType.TOTAL)
        //    {
        //        isAllowDistrDef = true;
        //        setAllowScenGen(isAllowScenGen);
        //    }
        //}

        private void dGVScens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        //private void tbScenName_TextChanged(object sender, EventArgs e)
        //{
        //    setScenAsSelected(tbScenName.Text);
        //}

        private void cmBBuild_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuildType currBuildType;
            currBuildType = (BuildType) cmBBuild.SelectedIndex;

            switch (currBuildType)
            {
                case BuildType.BUILD_1 :
                    tbMinInvFactor.Enabled = false;
                    break;
                default:
                    tbMinInvFactor.Enabled = true;
                    break;
            }
        }

        private void pgbLoading_Click(object sender, EventArgs e)
        {

        }

        //private Int32 usedNumberOfReplications=0;
        //private void tbReplications_TextChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (tbReplications .Text.Trim() !="")
        //            usedNumberOfReplications=Convert.ToInt32 (tbReplications .Text ,CultureInfo .InvariantCulture );
        //    }
        //    catch (Exception exc)
        //    {
        //        tbReplications .Text ="";
        //        usedNumberOfReplications = 0;
        //        MessageBox.Show("Keine Zahl.", "",//("Bitte erst eine Optimierung durchführen.", "",
        //                      MessageBoxButtons.OK,
        //                      MessageBoxIcon.None);
        //    }
        //}
        //private Double usedMeanChange;
        //private void tbMean_TextChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (tbReplications.Text.Trim() != "")
        //            usedMeanChange = Convert.ToDouble (tbMean .Text, currC);
        //    }
        //    catch (Exception exc)
        //    {
        //        tbReplications.Text = "";
        //        usedNumberOfReplications = 0;
        //        MessageBox.Show("Keine Zahl.", "",//("Bitte erst eine Optimierung durchführen.", "",
        //                      MessageBoxButtons.OK,
        //                      MessageBoxIcon.None);
        //    }
        //}

        private void cbFixPlants_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void tbTranspCostFix_TextChanged(object sender, EventArgs e)
        {
            checkTbInput((TextBox)sender);
        }

        private void tbTranspCostVar_TextChanged(object sender, EventArgs e)
        {
            checkTbInput((TextBox)sender);
        }

        void allowExternalDistanceDataInput(bool isAllow)
        {
            tbTranspCostFix.Enabled = isAllow;
            tbTranspCostVar.Enabled = isAllow;
            cbOfflineMode.Enabled = isAllow;
            cBCheckCountryMissingTransportConnectionsCustomer.Enabled = isAllow;
        }

        private void cBExternalDistanceData_CheckedChanged(object sender, EventArgs e)
        {
            allowExternalDistanceDataInput(cBExternalDistanceData.Checked);
        }

        private void cbOfflineMode_CheckedChanged(object sender, EventArgs e)
        {
            distanceSource.IsOfflineMode = cbOfflineMode.Checked;
        }
    }
}
/*
  protected string fullDemandTabName = "fullDemTab";
        protected string[,] fullDemandTabSpec = { {"Product", "string","true","false","false"},
                                                  {"Period", "string","true","false","false"},
                                                   {"Customer", "string","true","false","false"},
                                                   {"Location", "string","true","false","false"},
                                                  { "DemandQuantity", "double","true","false","false"},
                                                  { "PeriodIndex", "int","true","false","false"}
                                                  //{ "ScenarioIndex", "int","true","false","false"}
                                                  };
        protected string fullDemandTabSQLCMD = "Select Product,Period,Customer,Country || ' '|| PostalCode as Location,DemandQuantity, PeriodIndex,ScenarioIndex" +
                                              "from IN_DEMAND demTab " +
                                              "join IN_PRODUCT prdTab on demTab.ProductIndex=prdTab.ProductIndex" +
                                              "join IN_CUSTOMER cTab on cTab.CustomerIndex=demTab.CustomerIndex" +
                                              "join IN_PERIOD perTab on perTab.PeriodIndex=demTab.PeriodIndex";
        protected string custDemandTabName = "custDemTab";
        protected string[,] custDemandTabSpec = { {"Product", "string","true","true","false"},
                                                  {"Period", "string","true","true","false"},
                                                   {"Customer", "string","true","true","false"},
                                                   {"Location", "string","true","false","false"},
                                                  { "DemandQuantity", "double","true","false","true"},
                                                  { "PeriodIndex", "int","false","true","false"},
                                                  { "ScenarioIndex", "int","true","false","false"}
                                                  };
*/