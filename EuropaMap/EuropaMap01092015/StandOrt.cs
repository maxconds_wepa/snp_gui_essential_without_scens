﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EuropaMap

{

    /**
     * 
     * Die Klasse Standort enthält Informationen zum eigentlichen Werk und enthält eine Liste von benachbarten Standorten
     * ausserdem kann ein Standort Zeichen:  sich als Kreis, seinen Namen, die Verbindungen zu den benachbarten Werten
     * 
     * der Radius gibt die größe des gezeichneten Kreis an er ist abhängig von der Prozentzahl, die ein Standortvalue besitzt
     * dieser verändert sich wenn von aussen calculatePercentage(Value) aufgerufen wird
     */
    class StandOrt
    {

        List<StandOrt> neighbours;
       
        public string name;
        public string plz;
        private Point point;
        SolidBrush colorBrush;//jeweils aktuell gesetzte Farbe


        /*
         * Alle Farben die es gibt können hier gesetzt werden 
         */
        //Farben der Kreise
        SolidBrush sBrushAusgewaehlt = new SolidBrush(Color.FromArgb(240, 75, 0, 130));
        SolidBrush sBrushStandard = new SolidBrush(Color.FromArgb(200, 1, 1, 155));
        SolidBrush sBrushNeighbour = new SolidBrush(Color.FromArgb(230, 95, 158, 160));//CadetBlue;
        //Farben für detailansicht Hintergrund
        SolidBrush sBrushBackgroundNeighbour = new SolidBrush(Color.FromArgb(230, 170, 200, 200));
        SolidBrush sBrushBackgroundStandard = new SolidBrush(Color.FromArgb(200, 151, 151, 255));
        SolidBrush sBrushBackgroundAusgewaehlt = new SolidBrush(Color.FromArgb(200, 175, 120, 230));
        //Hintergrund farbe (weis) für die Städte Namen
        SolidBrush sBrushBackgroundName = new SolidBrush(Color.FromArgb(230, 255, 255, 255));
       
        
        private double value;
        public static double valueAll;
        private double percent = 0;
        private double radius;

       
        //Flag die angibt ob ein Standort der vom user ausgewählte Ort ist
        private bool selected = false;

        //Flag die abgefragt wird, um die Detail informationen anzuzeigen
        private bool selectedDetail = false;
        
        
        public bool isNeighbour;
       

        public StandOrt(String name, Point point)
        {
            this.point = point;
            this.name = name;
            colorBrush = sBrushStandard;
            radius = 4;
            
            this.neighbours = new List<StandOrt>();
        }

        public StandOrt(String name,String plz, Point point)
        {
            this.point = point;
            this.name = name;
            this.plz = plz;
            colorBrush = sBrushStandard;
            radius = 4;

            this.neighbours = new List<StandOrt>();
        }


        public void setStandardColor()
        {
            this.colorBrush = sBrushStandard;
        }

        public void addNeighbours(StandOrt ort)
        {
            neighbours.Add(ort);
        }
        public void clearNeighbours()
        {
            foreach (StandOrt ort in neighbours)
            {
                ort.setStandardColor();
            }
            neighbours.Clear();
        }
        public List<StandOrt> getNeighbours()
        {
            return neighbours;
        }

        

        public String getPlz()
        {
            return plz;
        }

        //koordinaten des Standorts bezogen auf die Europakarte
        public Point getPoint()
        {
            return point;
        }





        private Point getPercentagePoint()
        {
            Point point;
            if (Math.Round(percent * 100) < 10)
            {
                point = new Point(this.point.X - 4, this.point.Y - 8);
            }
            else
            {
                point = new Point(this.point.X - 8, this.point.Y - 8);
            }
            return point;
        }



        public void setSelected(bool selected)
        {
            this.selected = selected;
            if (selected)
            {
                colorBrush = sBrushAusgewaehlt;
            }
            else
            {
                colorBrush = sBrushStandard;
            }

        }
        public bool getSelected()
        {
            return selected;
        }


        public void setDetailSelected(bool selectedDetail)
        {
            this.selectedDetail = selectedDetail;
        }
        public bool getSelectedDetail()
        {
            return selectedDetail;
        }


   
        //Skaliert den point des Standorts, der Eingelesene Wert bezieht sich auf das Bild in absoluter Größe da dieses skaliert wird muss auch jeder Standort skaliert werden 
        public void scale(double scale)
        {
            point = new Point((int)(point.X * scale), (int)(point.Y * scale));
        }

        public void setValue(double value)
        {
            this.value = value;
        }

        public void addValue(double value)
        {
            this.value = this.value + value;
        }

        public double getValue()
        {
            return value;
        }

        public void calculatePercentage(double sum)
        {
            if (sum != 0)
            {
                percent = value / sum;
            }
            else
            {
                percent = 0;
            }

            if (percent != 0)
            {

                radius = 250 * percent / 2;

                if (radius > 40)
                {
                    radius = 40;

                }


            }

            if (percent * 100 < 10)
            {

                radius = 11;
            }

            if (isNeighbour == true && this.value == 0)
            {
                radius = 0;
            }



        }


        public Boolean isMouseOver(int x, int y)
        {
            if (x > point.X - (int)radius && x < point.X + (int)radius)
            {
                if (y > point.Y - (int)radius && y < point.Y + (int)radius)
                {

                    return true;
                }
            }

            return false;
        }


        //PAINTING METHODEN
        public void paintEllipse(PaintEventArgs e)
        {
            // Create font and brush.
            Font drawFont = new Font("Arial", 10);
            SolidBrush drawBrushFont = new SolidBrush(Color.White);

            if (isNeighbour)
            {
                //this.colorBrush = new SolidBrush(Color.CadetBlue);
                this.colorBrush = sBrushNeighbour;
            }
            Rectangle rect = new System.Drawing.Rectangle(point.X - (int)radius, point.Y - (int)radius, (int)(2 * radius), (int)(2 * radius));

            e.Graphics.FillEllipse(this.colorBrush, rect);
            if (selected == false)
            {
                e.Graphics.DrawString("" + Math.Round(percent * 100), drawFont, drawBrushFont, this.getPercentagePoint());
            }
        }

        public void paintName(PaintEventArgs e)
        {


            Font drawFont = new Font("Arial", 10);
            SolidBrush drawBrush = new SolidBrush(Color.Black);
            SolidBrush drawBrush1 = new SolidBrush(Color.Blue);
            SolidBrush drawBrushBackgroundColor = sBrushBackgroundName;
            double abstand = radius * Math.Cos(Math.PI / 4);
            double offset = 3;
            abstand = abstand + offset;
            Rectangle rect = new System.Drawing.Rectangle(point.X + (int)abstand, point.Y + (int)abstand, name.Length * 8 + 10, 18);
            


            e.Graphics.FillRectangle(drawBrushBackgroundColor, rect);
            e.Graphics.DrawString(this.name, drawFont, drawBrush, rect);

        }

        public void paintVerbindungen(PaintEventArgs e)
        {
            foreach (StandOrt neighbour in neighbours)
            {
                if (neighbour.getValue() > 0)
                {
                    Verbindung verb = new Verbindung(this, neighbour);
                    verb.paintVerbindung(e);
                }
            }
        }

        public void paintDetailInformation(PaintEventArgs e, String productionSelectedItem)
        {
            int abstand = (int)(radius * Math.Cos(Math.PI / 4.0));
            int width = 100;

            Font drawFont = new Font("Arial", 10);
            int height = (int)drawFont.GetHeight() * 5;



            Rectangle rect = new Rectangle(point.X + abstand, point.Y - abstand - height, width, height);
            SolidBrush drawBrushBackgroundColor = sBrushBackgroundStandard;
            if (this.isNeighbour)
            {
                drawBrushBackgroundColor = sBrushBackgroundNeighbour;
               
            }

            if (selected)
            {
                drawBrushBackgroundColor = sBrushBackgroundAusgewaehlt;
            }

            SolidBrush drawBrush = new SolidBrush(Color.Black);
            e.Graphics.FillRectangle(drawBrushBackgroundColor, rect);

            String str = "";
            string pat = @"(kosten)"; ;
            Regex r = new Regex(pat);
            Match m = r.Match(productionSelectedItem);
            //str+=(name);

            str += Convert.ToString(this.plz);
            if (m.Success)//€
            {
                str += (Environment.NewLine + string.Format("{0:c}", this.getValue()));

                str += (Environment.NewLine + "gesamt:\n" + string.Format("{0:c}", StandOrt.valueAll));

            }
            else
            {
                if (this.getValue() > 99999)
                {

                    str += (Environment.NewLine + string.Format("{0:#.00e+00}", this.getValue()));
                }
                else
                {
                    str += (Environment.NewLine + string.Format("{0:f}", this.getValue()));
                }
                if (StandOrt.valueAll > 99999)
                {
                    str += (Environment.NewLine + "gesamt: " + string.Format("{0:#.00e+00}", StandOrt.valueAll));
                }
                else
                {
                    str += (Environment.NewLine + "gesamt: " + string.Format("{0:f}", StandOrt.valueAll));
                }

            }

            str += Environment.NewLine + "Anteil :" + string.Format("{0:0.0%}", this.percent);




            e.Graphics.DrawString(str, drawFont, drawBrush, rect);



        }


       
    }




}
