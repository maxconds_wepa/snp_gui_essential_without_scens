﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EuropaMap
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {

            if (args.Length != 5)
            {
               
                //TODO Error
                MessageBox.Show("ERROR Anzahl der ÜbergabeParameter ist falsch");
            }
            else
            {

            }



            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ProductionMap(args[0],args[1],args[2],args[3],args[4]));
        }
    }
}
