﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EuropaMap
{
    class Data
    {
      

        public static void read_header(ref string filename, ref char separator, ref List<string> attributes)
        {

            string line;

            if (!File.Exists(filename))
            {
                Console.WriteLine("Datei " + filename + " nicht vorhanden!");
                //Todo Exit 1
            }
            System.IO.StreamReader file = new System.IO.StreamReader(filename, System.Text.Encoding.UTF7);

            line = file.ReadLine();


            string[] attributesArray;
            attributesArray = line.Split(new Char[] { separator });




            //aus einem einem String Array eine String Liste Erstellen
            foreach (string i in attributesArray)
            {
                attributes.Add(i);
            }

            file.Close();
        }


        public static void read_data(ref string filename, ref char separator, ref bool headline, ref List<List<string>> series)
        {
            string line;
            if (!File.Exists(filename))
            {
                Console.WriteLine("Datei " + filename + " nicht vorhanden!");
                //Todo Exit 1
            }

            System.IO.StreamReader file = new System.IO.StreamReader(filename, System.Text.Encoding.Default);
            //System.IO.StreamReader file = new System.IO.StreamReader(filename);
            if (headline)
            {
                file.ReadLine();
            }

            while ((line = file.ReadLine()) != null)
            {
                // line = line.Replace(".", ",");
                string[] dummy;
                dummy = line.Split(new Char[] { separator });




                List<string> dummy2 = new List<string>();//aus einem einem String Array eine String Liste Erstellen
                foreach (string i in dummy)
                {
                    dummy2.Add(i);
                }
                series.Add(dummy2);
            }
            file.Close();
        }




    }
}
