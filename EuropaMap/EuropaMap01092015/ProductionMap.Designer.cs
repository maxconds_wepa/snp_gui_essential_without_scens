﻿namespace EuropaMap
{
    partial class ProductionMap
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.Produktion = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.CheckBoxMonat1 = new System.Windows.Forms.CheckBox();
            this.checkBoxMonat2 = new System.Windows.Forms.CheckBox();
            this.checkBoxMonat3 = new System.Windows.Forms.CheckBox();
            this.Zeitraum = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Produktion
            // 
            this.Produktion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Produktion.FormattingEnabled = true;
            this.Produktion.ItemHeight = 16;
            this.Produktion.Location = new System.Drawing.Point(41, 222);
            this.Produktion.Name = "Produktion";
            this.Produktion.Size = new System.Drawing.Size(288, 164);
            this.Produktion.TabIndex = 4;
            this.Produktion.SelectedIndexChanged += new System.EventHandler(this.Produktion_SelectedIndexChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(424, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1000, 900);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // CheckBoxMonat1
            // 
            this.CheckBoxMonat1.AutoSize = true;
            this.CheckBoxMonat1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckBoxMonat1.Location = new System.Drawing.Point(41, 141);
            this.CheckBoxMonat1.Name = "CheckBoxMonat1";
            this.CheckBoxMonat1.Size = new System.Drawing.Size(77, 24);
            this.CheckBoxMonat1.TabIndex = 6;
            this.CheckBoxMonat1.Text = "Januar";
            this.CheckBoxMonat1.UseVisualStyleBackColor = true;
            this.CheckBoxMonat1.CheckedChanged += new System.EventHandler(this.CheckBoxMonat1_CheckedChanged);
            // 
            // checkBoxMonat2
            // 
            this.checkBoxMonat2.AutoSize = true;
            this.checkBoxMonat2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMonat2.Location = new System.Drawing.Point(172, 141);
            this.checkBoxMonat2.Name = "checkBoxMonat2";
            this.checkBoxMonat2.Size = new System.Drawing.Size(84, 24);
            this.checkBoxMonat2.TabIndex = 7;
            this.checkBoxMonat2.Text = "Februar";
            this.checkBoxMonat2.UseVisualStyleBackColor = true;
            this.checkBoxMonat2.CheckedChanged += new System.EventHandler(this.checkBoxMonat2_CheckedChanged);
            // 
            // checkBoxMonat3
            // 
            this.checkBoxMonat3.AutoSize = true;
            this.checkBoxMonat3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMonat3.Location = new System.Drawing.Point(304, 141);
            this.checkBoxMonat3.Name = "checkBoxMonat3";
            this.checkBoxMonat3.Size = new System.Drawing.Size(63, 24);
            this.checkBoxMonat3.TabIndex = 8;
            this.checkBoxMonat3.Text = "März";
            this.checkBoxMonat3.UseVisualStyleBackColor = true;
            this.checkBoxMonat3.CheckedChanged += new System.EventHandler(this.checkBoxMonat3_CheckedChanged);
            // 
            // Zeitraum
            // 
            this.Zeitraum.FormattingEnabled = true;
            this.Zeitraum.Location = new System.Drawing.Point(41, 131);
            this.Zeitraum.Name = "Zeitraum";
            this.Zeitraum.Size = new System.Drawing.Size(0, 4);
            this.Zeitraum.TabIndex = 3;
            this.Zeitraum.SelectedIndexChanged += new System.EventHandler(this.Zeitraum_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(41, 457);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Zoom -";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(41, 495);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Zoom +";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ProductionMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1339, 762);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBoxMonat3);
            this.Controls.Add(this.checkBoxMonat2);
            this.Controls.Add(this.CheckBoxMonat1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Produktion);
            this.Controls.Add(this.Zeitraum);
            this.Name = "ProductionMap";
            this.ShowIcon = false;
            this.Text = "Graphische Analyse";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ProductionMap_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox Produktion;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox CheckBoxMonat1;
        private System.Windows.Forms.CheckBox checkBoxMonat2;
        private System.Windows.Forms.CheckBox checkBoxMonat3;
        private System.Windows.Forms.ListBox Zeitraum;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

