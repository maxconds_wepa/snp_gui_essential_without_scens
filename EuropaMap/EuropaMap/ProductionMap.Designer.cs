﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
namespace EuropaMap
{
    partial class ProductionMap
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Produktion = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.CheckBoxMonat1 = new System.Windows.Forms.CheckBox();
            this.checkBoxMonat2 = new System.Windows.Forms.CheckBox();
            this.checkBoxMonat3 = new System.Windows.Forms.CheckBox();
            this.Zeitraum = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.optCheckBox = new System.Windows.Forms.CheckBox();
            this.fixCheckBox = new System.Windows.Forms.CheckBox();
            this.kundenListBox = new System.Windows.Forms.ListBox();
            this.lableKunde = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.SuspendLayout();
            // 
            // Produktion
            // 
            this.Produktion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Produktion.FormattingEnabled = true;
            this.Produktion.ItemHeight = 16;
            this.Produktion.Location = new System.Drawing.Point(41, 222);
            this.Produktion.Name = "Produktion";
            this.Produktion.Size = new System.Drawing.Size(288, 164);
            this.Produktion.TabIndex = 4;
            this.Produktion.SelectedIndexChanged += new System.EventHandler(this.Produktion_SelectedIndexChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1000, 900);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // CheckBoxMonat1
            // 
            this.CheckBoxMonat1.AutoSize = true;
            this.CheckBoxMonat1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckBoxMonat1.Location = new System.Drawing.Point(41, 141);
            this.CheckBoxMonat1.Name = "CheckBoxMonat1";
            this.CheckBoxMonat1.Size = new System.Drawing.Size(77, 24);
            this.CheckBoxMonat1.TabIndex = 6;
            this.CheckBoxMonat1.Text = "Januar";
            this.CheckBoxMonat1.UseVisualStyleBackColor = true;
            this.CheckBoxMonat1.CheckedChanged += new System.EventHandler(this.CheckBoxMonat1_CheckedChanged);
            // 
            // checkBoxMonat2
            // 
            this.checkBoxMonat2.AutoSize = true;
            this.checkBoxMonat2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMonat2.Location = new System.Drawing.Point(172, 141);
            this.checkBoxMonat2.Name = "checkBoxMonat2";
            this.checkBoxMonat2.Size = new System.Drawing.Size(84, 24);
            this.checkBoxMonat2.TabIndex = 7;
            this.checkBoxMonat2.Text = "Februar";
            this.checkBoxMonat2.UseVisualStyleBackColor = true;
            this.checkBoxMonat2.CheckedChanged += new System.EventHandler(this.checkBoxMonat2_CheckedChanged);
            // 
            // checkBoxMonat3
            // 
            this.checkBoxMonat3.AutoSize = true;
            this.checkBoxMonat3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMonat3.Location = new System.Drawing.Point(304, 141);
            this.checkBoxMonat3.Name = "checkBoxMonat3";
            this.checkBoxMonat3.Size = new System.Drawing.Size(63, 24);
            this.checkBoxMonat3.TabIndex = 8;
            this.checkBoxMonat3.Text = "März";
            this.checkBoxMonat3.UseVisualStyleBackColor = true;
            this.checkBoxMonat3.CheckedChanged += new System.EventHandler(this.checkBoxMonat3_CheckedChanged);
            // 
            // Zeitraum
            // 
            this.Zeitraum.FormattingEnabled = true;
            this.Zeitraum.Location = new System.Drawing.Point(41, 131);
            this.Zeitraum.Name = "Zeitraum";
            this.Zeitraum.Size = new System.Drawing.Size(0, 4);
            this.Zeitraum.TabIndex = 3;
            this.Zeitraum.SelectedIndexChanged += new System.EventHandler(this.Zeitraum_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoScrollMargin = new System.Drawing.Size(5, 5);
            this.panel1.Controls.Add(this.chart3);
            this.panel1.Controls.Add(this.chart1);
            this.panel1.Controls.Add(this.chart2);
            this.panel1.Location = new System.Drawing.Point(424, 12);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(400, 10, 10, 10);
            this.panel1.Size = new System.Drawing.Size(929, 770);
            this.panel1.TabIndex = 12;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // chart3
            // 
            this.chart3.BackColor = System.Drawing.Color.Transparent;
            chartArea7.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea7.AxisX.IsLabelAutoFit = false;
            chartArea7.AxisX.LabelStyle.IsEndLabelVisible = false;
            chartArea7.AxisX.LabelStyle.IsStaggered = true;
            chartArea7.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea7.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea7);
            legend7.Name = "Legend1";
            this.chart3.Legends.Add(legend7);
            this.chart3.Location = new System.Drawing.Point(0, 524);
            this.chart3.Margin = new System.Windows.Forms.Padding(5);
            this.chart3.MaximumSize = new System.Drawing.Size(1000, 200);
            this.chart3.Name = "chart3";
            this.chart3.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chart3.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Chocolate};
            series7.ChartArea = "ChartArea1";
            series7.Legend = "Legend1";
            series7.Name = "Series1";
            this.chart3.Series.Add(series7);
            this.chart3.Size = new System.Drawing.Size(1000, 200);
            this.chart3.TabIndex = 17;
            this.chart3.Text = "chart3";
            this.chart3.Visible = false;
            this.chart3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chart3_MouseMove);
            // 
            // chart1
            // 
            this.chart1.BackColor = System.Drawing.Color.Transparent;
            chartArea8.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea8.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea8.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea8);
            legend8.Name = "Legend1";
            this.chart1.Legends.Add(legend8);
            this.chart1.Location = new System.Drawing.Point(0, 66);
            this.chart1.Margin = new System.Windows.Forms.Padding(5);
            this.chart1.MaximumSize = new System.Drawing.Size(1000, 200);
            this.chart1.Name = "chart1";
            this.chart1.Padding = new System.Windows.Forms.Padding(5);
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chart1.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Indigo};
            series8.ChartArea = "ChartArea1";
            series8.IsVisibleInLegend = false;
            series8.Legend = "Legend1";
            series8.Name = "Series1";
            this.chart1.Series.Add(series8);
            this.chart1.Size = new System.Drawing.Size(1000, 200);
            this.chart1.TabIndex = 15;
            this.chart1.Text = "chart1";
            this.chart1.Visible = false;
            this.chart1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chart1_MouseClick);
            this.chart1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chart1_MouseMove);
            // 
            // chart2
            // 
            this.chart2.BackColor = System.Drawing.Color.Transparent;
            chartArea9.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea9.AxisX.IsLabelAutoFit = false;
            chartArea9.AxisX.LabelStyle.IsEndLabelVisible = false;
            chartArea9.AxisX.LabelStyle.IsStaggered = true;
            chartArea9.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea9.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea9);
            legend9.Name = "Legend1";
            this.chart2.Legends.Add(legend9);
            this.chart2.Location = new System.Drawing.Point(0, 295);
            this.chart2.Margin = new System.Windows.Forms.Padding(5);
            this.chart2.MaximumSize = new System.Drawing.Size(1000, 200);
            this.chart2.Name = "chart2";
            this.chart2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chart2.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.CadetBlue};
            series9.ChartArea = "ChartArea1";
            series9.Legend = "Legend1";
            series9.Name = "Series1";
            this.chart2.Series.Add(series9);
            this.chart2.Size = new System.Drawing.Size(1000, 200);
            this.chart2.TabIndex = 16;
            this.chart2.Text = "chart2";
            this.chart2.Visible = false;
            this.chart2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chart2_MouseClick);
            this.chart2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chart2_MouseMove);
            // 
            // optCheckBox
            // 
            this.optCheckBox.AutoSize = true;
            this.optCheckBox.Checked = true;
            this.optCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.optCheckBox.Location = new System.Drawing.Point(41, 78);
            this.optCheckBox.Name = "optCheckBox";
            this.optCheckBox.Size = new System.Drawing.Size(67, 17);
            this.optCheckBox.TabIndex = 13;
            this.optCheckBox.Text = "Optimiert";
            this.optCheckBox.UseVisualStyleBackColor = true;
            this.optCheckBox.Visible = false;
            this.optCheckBox.CheckedChanged += new System.EventHandler(this.optCheckBox_CheckedChanged);
            // 
            // fixCheckBox
            // 
            this.fixCheckBox.AutoSize = true;
            this.fixCheckBox.Location = new System.Drawing.Point(172, 78);
            this.fixCheckBox.Name = "fixCheckBox";
            this.fixCheckBox.Size = new System.Drawing.Size(53, 17);
            this.fixCheckBox.TabIndex = 14;
            this.fixCheckBox.Text = "Fixiert";
            this.fixCheckBox.UseVisualStyleBackColor = true;
            this.fixCheckBox.Visible = false;
            this.fixCheckBox.CheckedChanged += new System.EventHandler(this.fixCheckBox_CheckedChanged);
            // 
            // kundenListBox
            // 
            this.kundenListBox.FormattingEnabled = true;
            this.kundenListBox.Location = new System.Drawing.Point(41, 442);
            this.kundenListBox.Name = "kundenListBox";
            this.kundenListBox.Size = new System.Drawing.Size(288, 225);
            this.kundenListBox.TabIndex = 15;
            this.kundenListBox.SelectedIndexChanged += new System.EventHandler(this.kundenListBox_SelectedIndexChanged);
            // 
            // lableKunde
            // 
            this.lableKunde.AutoSize = true;
            this.lableKunde.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lableKunde.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lableKunde.Location = new System.Drawing.Point(37, 415);
            this.lableKunde.Name = "lableKunde";
            this.lableKunde.Size = new System.Drawing.Size(51, 20);
            this.lableKunde.TabIndex = 16;
            this.lableKunde.Text = "label1";
            this.lableKunde.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(41, 713);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Zoom -";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(41, 684);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Zoom +";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ProductionMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1339, 762);
            this.Controls.Add(this.lableKunde);
            this.Controls.Add(this.kundenListBox);
            this.Controls.Add(this.fixCheckBox);
            this.Controls.Add(this.optCheckBox);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBoxMonat3);
            this.Controls.Add(this.checkBoxMonat2);
            this.Controls.Add(this.CheckBoxMonat1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Produktion);
            this.Controls.Add(this.Zeitraum);
            this.Name = "ProductionMap";
            this.ShowIcon = false;
            this.Text = "Graphische Analyse";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ProductionMap_FormClosed);
            this.Load += new System.EventHandler(this.ProductionMap_Load);
            this.SizeChanged += new System.EventHandler(this.ProductionMap_SizeChanged);
            this.Resize += new System.EventHandler(this.ProductionMap_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox Produktion;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox CheckBoxMonat1;
        private System.Windows.Forms.CheckBox checkBoxMonat2;
        private System.Windows.Forms.CheckBox checkBoxMonat3;
        private System.Windows.Forms.ListBox Zeitraum;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox optCheckBox;
        private System.Windows.Forms.CheckBox fixCheckBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private ListBox kundenListBox;
        private Label lableKunde;
        private Chart chart3;
        private Button button1;
        private Button button2;
    }
}

