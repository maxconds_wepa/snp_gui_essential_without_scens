﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace EuropaMap
{

    /**
     * 
     * Die Klasse Standort enthält Informationen zum eigentlichen Werk und enthält eine Liste von benachbarten Standorten
     * ausserdem kann ein Standort Zeichen:  sich als Kreis, seinen Namen, die Verbindungen zu den benachbarten Werten
     * 
     * der Radius gibt die größe des gezeichneten Kreis an er ist abhängig von der Prozentzahl, die ein Standortvalue besitzt
     * dieser verändert sich wenn von aussen calculatePercentage(Value) aufgerufen wird
     */
    class StandOrt
    {

        List<StandOrt> neighbours;
        CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
        public string name;
        public string plz;
        private Point point;
        SolidBrush colorBrush;//jeweils aktuell gesetzte Farbe

        //enthält die aufschlüsselung des Values auf Maschinene und Produkte
        public List<List<String>> mashineDataList = new List<List<String>>();

        //enthält die aufschlüsselung des Values auf Kunden und Produkte
        public List<List<String>> kundeDataList = new List<List<String>>();

        //entspricht den Indices aus mashineDataList
        public static List<String> mashineDataKopfzeile = new List<String>();

        //entspricht den Indices aus kundeDataList
        public static List<String> kundeDataKopfzeile = new List<String>();
        /*
         * Alle Farben die es gibt können hier gesetzt werden 
         */
        //Farben der Kreise
        SolidBrush sBrushAusgewaehlt = new SolidBrush(Color.FromArgb(240, 75, 0, 130));
        SolidBrush sBrushStandard = new SolidBrush(Color.FromArgb(200, 1, 1, 155));
        SolidBrush sBrushNeighbour = new SolidBrush(Color.FromArgb(230, 95, 158, 160));//CadetBlue;
        //Farben für detailansicht Hintergrund
        SolidBrush sBrushBackgroundNeighbour = new SolidBrush(Color.FromArgb(230, 170, 200, 200));
        SolidBrush sBrushBackgroundStandard = new SolidBrush(Color.FromArgb(200, 151, 151, 255));
        SolidBrush sBrushBackgroundAusgewaehlt = new SolidBrush(Color.FromArgb(200, 175, 120, 230));
        //Hintergrund farbe (weis) für die Städte Namen
        SolidBrush sBrushBackgroundName = new SolidBrush(Color.FromArgb(230, 255, 255, 255));


        private double value;
        public static double valueAll;
        private double percent = 0;
        private double radius;


        //Flag die angibt ob ein Standort der vom user ausgewählte Ort ist
        private bool selected = false;




        //Flag die abgefragt wird, um die Detail informationen anzuzeigen
        private bool selectedDetail = false;


        public bool isNeighbour;

        public void addMashineData(List<String> mashineData)
        {
            mashineDataList.Add(mashineData);
        }

        public void addKundenData(List<String> kundenData)
        {
            kundeDataList.Add(kundenData);
        }

        public List<String> getKundenNamen()
        {
            List<String> distinctKundenList = new List<String>();

            int kundenIndex = kundeDataKopfzeile.IndexOf("Kunde");
            foreach (List<String> list in kundeDataList)
            {
                for (int i = 0; i < list.Count(); i++)
                {
                    int indexErstesEd = list[kundenIndex].IndexOf("@");
                    String kundenGruppe = list[kundenIndex].Substring(0, indexErstesEd);

                    if (!distinctKundenList.Contains(kundenGruppe))
                    {

                        distinctKundenList.Add(kundenGruppe);
                    }
                }
            }
            distinctKundenList.Sort();
            return distinctKundenList;

        }

        public StandOrt(String name, Point point)
        {
            this.point = point;
            this.name = name;
            colorBrush = sBrushStandard;
            radius = 4;

            this.neighbours = new List<StandOrt>();
        }

        public StandOrt(String name, String plz, Point point)
        {
            this.point = point;
            this.name = name;
            this.plz = plz;
            colorBrush = sBrushStandard;
            radius = 4;

            this.neighbours = new List<StandOrt>();
        }


        public void setStandardColor()
        {
            this.colorBrush = sBrushStandard;
        }

        public void addNeighbours(StandOrt ort)
        {
            neighbours.Add(ort);
        }
        public void clearNeighbours()
        {
            foreach (StandOrt ort in neighbours)
            {
                ort.setStandardColor();
            }
            neighbours.Clear();
        }
        public List<StandOrt> getNeighbours()
        {
            return neighbours;
        }



        public String getPlz()
        {
            return plz;
        }

        //koordinaten des Standorts bezogen auf die Europakarte
        public Point getPoint()
        {
            return point;
        }





        private Point getPercentagePoint()
        {
            Point point;
            if (Math.Round(percent * 100) < 10)
            {
                point = new Point(this.point.X - 4, this.point.Y - 8);
            }
            else
            {
                point = new Point(this.point.X - 8, this.point.Y - 8);
            }
            return point;
        }



        public void setSelected(bool selected)
        {
            this.selected = selected;
            if (selected)
            {
                colorBrush = sBrushAusgewaehlt;
            }
            else
            {
                colorBrush = sBrushStandard;
            }

        }
        public bool getSelected()
        {
            return selected;
        }


        public void setDetailSelected(bool selectedDetail)
        {
            this.selectedDetail = selectedDetail;
        }
        public bool getSelectedDetail()
        {
            return selectedDetail;
        }



        //Skaliert den point des Standorts, der Eingelesene Wert bezieht sich auf das Bild in absoluter Größe da dieses skaliert wird muss auch jeder Standort skaliert werden 
        public void scale(double scale)
        {
            point = new Point((int)(point.X * scale), (int)(point.Y * scale));
        }

        public void setValue(double value)
        {
            this.value = value;
        }

        public void addValue(double value)
        {
            this.value = this.value + value;
        }

        public double getValue()
        {
            return value;
        }

        public void calculatePercentage(double sum)
        {
            if (sum != 0)
            {
                percent = value / sum;
            }
            else
            {
                percent = 0;
            }

            if (percent != 0)
            {

                radius = 250 * percent / 2;

                if (radius > 40)
                {
                    radius = 40;

                }


            }

            if (percent * 100 < 10)
            {

                radius = 11;
            }

            if (isNeighbour == true && this.value == 0)
            {
                radius = 0;
            }



        }


        public Boolean isMouseOver(int x, int y)
        {
            if (x > point.X - (int)radius && x < point.X + (int)radius)
            {
                if (y > point.Y - (int)radius && y < point.Y + (int)radius)
                {

                    return true;
                }
            }

            return false;
        }

        public Series getProductSeries(String mashine, String curProdItem)
        {

            List<String> distinctProductList = new List<String>();
            int mashineIndex = mashineDataKopfzeile.IndexOf("Maschine");
            int produktIndex = mashineDataKopfzeile.IndexOf("Produkt");


            //liest alle Produkte ein die auf der maschine Produziert wurden
            foreach (List<String> list in mashineDataList)
            {
                for (int i = 0; i < list.Count(); i++)
                {
                    if (mashine == list[mashineIndex])
                    {
                        if (!distinctProductList.Contains(list[produktIndex]))
                        {
                            distinctProductList.Add(list[produktIndex]);
                        }
                    }
                }
            }

            Dictionary<String, Double> valueProductDictionary = new Dictionary<String, Double>();
            foreach (string product in distinctProductList)
            {
                valueProductDictionary.Add(product, 0);
            }


            int valueIndex = mashineDataKopfzeile.IndexOf(curProdItem);
            foreach (List<String> list in mashineDataList)
            {
                if (mashine == list[mashineIndex])
                {
                    double value = Convert.ToDouble(list[valueIndex],culture);
                    valueProductDictionary[list[produktIndex]] += value;
                }
            }


            Series series = new Series();

            for (int i = 0; i < distinctProductList.Count; i++)
            {
                String product = distinctProductList[i];
                series.Points.Add(Math.Round(valueProductDictionary[product], 2));
                series.Points[i].AxisLabel = product;
                series.Font = new System.Drawing.Font("Arial", 16);
                // series.Points[i].Label = string.Format("{0:f}", valueProductDictionary[product]);



            }
            ;
            return series;
        }
        public Series getKundenGruppeProductSeries(String kundenGruppe, String curProdItem)
        {

            List<String> distinctProductList = new List<String>();
            int kundenIndex = kundeDataKopfzeile.IndexOf("Kunde");
            int produktIndex = kundeDataKopfzeile.IndexOf("Produkt");


            //liest alle Produkte eines Kundens
            foreach (List<String> list in kundeDataList)
            {
                for (int i = 0; i < list.Count(); i++)
                {

                    int indexErstesEd = list[kundenIndex].IndexOf("@");

                    if (kundenGruppe == list[kundenIndex].Substring(0, indexErstesEd))
                    {
                        if (!distinctProductList.Contains(list[produktIndex]))
                        {
                            distinctProductList.Add(list[produktIndex]);
                        }
                    }
                }
            }

            Dictionary<String, Double> valueProductDictionary = new Dictionary<String, Double>();
            foreach (string product in distinctProductList)
            {
                valueProductDictionary.Add(product, 0);
            }


            int valueIndex = kundeDataKopfzeile.IndexOf(curProdItem);
            foreach (List<String> list in kundeDataList)
            {
                int indexErstesEd = list[kundenIndex].IndexOf("@");
                if (kundenGruppe == list[kundenIndex].Substring(0, indexErstesEd))
                {
                    double value = Convert.ToDouble(list[valueIndex],culture);
                    valueProductDictionary[list[produktIndex]] += value;
                }
            }


            Series series = new Series();

            for (int i = 0; i < distinctProductList.Count; i++)
            {
                String product = distinctProductList[i];
                series.Points.Add(Math.Round(valueProductDictionary[product], 2));
                series.Points[i].AxisLabel = product;
                series.Font = new System.Drawing.Font("Arial", 16);
                // series.Points[i].Label = string.Format("{0:f}", valueProductDictionary[product]);



            }
            ;
            return series;
        }

        public Series getKundenProductSeries(String produkt, String kundenGruppe, String curProdItem)
        {

            int kundenIndex = kundeDataKopfzeile.IndexOf("Kunde");
            int produktIndex = kundeDataKopfzeile.IndexOf("Produkt");

            List<String> distinctKundenList = new List<String>();

            //liest alle Produkte eines Kundens
            foreach (List<String> list in kundeDataList)
            {


                int indexErstesEd = list[kundenIndex].IndexOf("@");
                String listKundenGruppe = list[kundenIndex].Substring(0, indexErstesEd);
                if (kundenGruppe.Equals(listKundenGruppe))
                {
                    ;
                    if (!distinctKundenList.Contains(list[kundenIndex]))
                    {
                        distinctKundenList.Add(list[kundenIndex]);
                    }
                }

            }

            Dictionary<String, Double> valueProductDictionary = new Dictionary<String, Double>();
            foreach (string kunde in distinctKundenList)
            {
                valueProductDictionary.Add(kunde, 0);
            }

            int valueIndex = kundeDataKopfzeile.IndexOf(curProdItem);
            foreach (List<String> list in kundeDataList)
            {
                foreach (String kunde in distinctKundenList)
                {
                    if (kunde == list[kundenIndex] && produkt == list[produktIndex])
                    {

                        double value = Convert.ToDouble(list[valueIndex],culture);
                        valueProductDictionary[kunde] += value;
                    }
                }

            }

            Series series = new Series();

            for (int i = 0; i < distinctKundenList.Count; i++)
            {
                String kunde = distinctKundenList[i];
                series.Points.Add(Math.Round(valueProductDictionary[kunde], 2));
                series.Points[i].AxisLabel = kunde;
                series.Font = new System.Drawing.Font("Arial", 16);
                // series.Points[i].Label = string.Format("{0:f}", valueProductDictionary[product]);



            }
            ;
            return series;



        }

        public Series getMashineSeries(String curProdItem)
        {


            List<String> distinctMashineList = new List<String>();
            int mashineIndex = mashineDataKopfzeile.IndexOf("Maschine");
            foreach (List<String> list in mashineDataList)
            {
                for (int i = 0; i < list.Count(); i++)
                {
                    if (!distinctMashineList.Contains(list[mashineIndex]))
                    {
                        distinctMashineList.Add(list[mashineIndex]);
                    }
                }
            }

            Dictionary<String, Double> valueMashineDictionary = new Dictionary<String, Double>();
            foreach (string mashine in distinctMashineList)
            {
                valueMashineDictionary.Add(mashine, 0);
            }


            int valueIndex = mashineDataKopfzeile.IndexOf(curProdItem);
            foreach (List<String> list in mashineDataList)
            {

                double value = Convert.ToDouble(list[valueIndex],culture);
                valueMashineDictionary[list[mashineIndex]] += value;
            }


            Series series = new Series();
            for (int i = 0; i < distinctMashineList.Count; i++)
            {
                String mashine = distinctMashineList[i];

                series.Points.Add(Math.Round(valueMashineDictionary[mashine], 2));
                series.Points[i].AxisLabel = mashine;
                series.Font = new System.Drawing.Font("Arial", 16);
                series.Points[i].IsValueShownAsLabel = false;
                //series.Points[i].Label = string.Format("{0:f}", valueMashineDictionary[mashine]);



            }


            return series;
        }

        public Series getKundenSeries(String curProdItem)
        {


            List<String> distinctKundenList = getKundenNamen();

            int kundenIndex = kundeDataKopfzeile.IndexOf("Kunde");
            Dictionary<String, Double> valueMashineDictionary = new Dictionary<String, Double>();
            foreach (string kunde in distinctKundenList)
            {
                valueMashineDictionary.Add(kunde, 0);
            }


            int valueIndex = kundeDataKopfzeile.IndexOf(curProdItem);
            foreach (List<String> list in kundeDataList)
            {

                int indexErstesEd = list[kundenIndex].IndexOf("@");
                String kundenGruppe = list[kundenIndex].Substring(0, indexErstesEd);
                double value = Convert.ToDouble(list[valueIndex],culture);
                valueMashineDictionary[kundenGruppe] += value;
            }


            Series series = new Series();
            for (int i = 0; i < distinctKundenList.Count; i++)
            {
                String kunde = distinctKundenList[i];

                series.Points.Add(Math.Round(valueMashineDictionary[kunde], 2));
                series.Points[i].AxisLabel = kunde;
                series.Font = new System.Drawing.Font("Arial", 16);
                series.Points[i].IsValueShownAsLabel = false;
                //series.Points[i].Label = string.Format("{0:f}", valueMashineDictionary[mashine]);



            }


            return series;
        }


        //PAINTING METHODEN
        public void paintEllipse(PaintEventArgs e)
        {
            // Create font and brush.
            Font drawFont = new Font("Arial", 10);
            SolidBrush drawBrushFont = new SolidBrush(Color.White);

            if (isNeighbour)
            {

                this.colorBrush = sBrushNeighbour;
            }
            Rectangle rect = new System.Drawing.Rectangle(point.X - (int)radius, point.Y - (int)radius, (int)(2 * radius), (int)(2 * radius));

            e.Graphics.FillEllipse(this.colorBrush, rect);
            if (selected == false)
            {
                e.Graphics.DrawString("" + Math.Round(percent * 100), drawFont, drawBrushFont, this.getPercentagePoint());
            }
        }

        public void paintName(PaintEventArgs e)
        {


            Font drawFont = new Font("Arial", 10);
            SolidBrush drawBrush = new SolidBrush(Color.Black);
            SolidBrush drawBrush1 = new SolidBrush(Color.Blue);
            SolidBrush drawBrushBackgroundColor = sBrushBackgroundName;
            double abstand = radius * Math.Cos(Math.PI / 4);
            double offset = 3;
            abstand = abstand + offset;
            Rectangle rect = new System.Drawing.Rectangle(point.X + (int)abstand, point.Y + (int)abstand, name.Length * 8 + 10, 18);



            e.Graphics.FillRectangle(drawBrushBackgroundColor, rect);
            
            e.Graphics.DrawString(this.name, drawFont, drawBrush, rect);

        }

        public void paintVerbindungen(PaintEventArgs e)
        {
            foreach (StandOrt neighbour in neighbours)
            {
                if (neighbour.getValue() > 0)
                {
                    Verbindung verb = new Verbindung(this, neighbour);
                    verb.paintVerbindung(e);
                }
            }
        }

        public void paintDetailInformation(PaintEventArgs e, String productionSelectedItem)
        {
            int abstand = (int)(radius * Math.Cos(Math.PI / 4.0));
            int width = 100;

            Font drawFont = new Font("Arial", 10);
            int height = (int)drawFont.GetHeight() * 5;



            Rectangle rect = new Rectangle(point.X + abstand, point.Y - abstand - height, width, height);
            SolidBrush drawBrushBackgroundColor = sBrushBackgroundStandard;
            if (this.isNeighbour)
            {
                drawBrushBackgroundColor = sBrushBackgroundNeighbour;

            }

            if (selected)
            {
                drawBrushBackgroundColor = sBrushBackgroundAusgewaehlt;
            }

            SolidBrush drawBrush = new SolidBrush(Color.Black);
            e.Graphics.FillRectangle(drawBrushBackgroundColor, rect);

            String str = "";
            string pat = @"(kosten)"; ;
            Regex r = new Regex(pat);
            Match m = r.Match(productionSelectedItem);
            //str+=(name);

            str += Convert.ToString(this.plz);
            if (m.Success)//€
            {

                CultureInfo ci;
                ci = new CultureInfo("de-DE");
                ci.NumberFormat.CurrencySymbol = "euro";
               // str += (Environment.NewLine + this.value.ToString("c", CultureInfo.CreateSpecificCulture("de-DE")));
                str += Environment.NewLine+string.Format("{0:c}", this.getValue());
                str += (Environment.NewLine + "gesamt:\n" + StandOrt.valueAll.ToString("c", CultureInfo.CreateSpecificCulture("de-DE")));



            }
            else
            {
                if (this.getValue() > 99999)
                {

                    str += (Environment.NewLine + string.Format("{0:#.00e+00}", this.getValue()));
                }
                else
                {
                    str += (Environment.NewLine + string.Format("{0:f}", this.getValue()));
                }
                if (StandOrt.valueAll > 99999)
                {
                    str += (Environment.NewLine + "gesamt: " + string.Format("{0:#.00e+00}", StandOrt.valueAll));
                }
                else
                {
                    str += (Environment.NewLine + "gesamt: " + string.Format("{0:f}", StandOrt.valueAll));
                }

            }

            str += Environment.NewLine + "Anteil :" + string.Format("{0:0.0%}", this.percent);




            e.Graphics.DrawString(str, drawFont, drawBrush, rect);



        }



    }




}
