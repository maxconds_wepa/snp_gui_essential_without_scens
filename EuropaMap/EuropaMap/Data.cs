﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EuropaMap
{
    class Data
    {
        public static bool german = false;

        public static void read_header(ref string filename, ref char separator, ref List<string> attributes)
        {

            string line;

            if (!File.Exists(filename))
            {
                Console.WriteLine("Datei " + filename + " nicht vorhanden!");
                //Todo Exit 1
            }
            System.IO.StreamReader file = new System.IO.StreamReader(filename, System.Text.Encoding.UTF8);
           
            line = file.ReadLine();


            string[] attributesArray;
            attributesArray = line.Split(new Char[] { separator });




            //aus einem einem String Array eine String Liste Erstellen
            foreach (string i in attributesArray)
            {
                attributes.Add(i);
            }

            file.Close();
        }


        public static void read_data(ref string filename, ref char separator, ref bool headline, ref List<List<string>> series)
        {
            string line;
            if (!File.Exists(filename))
            {
                Console.WriteLine("Datei " + filename + " nicht vorhanden!");
                //Todo Exit 1
            }

           // System.IO.StreamReader file = new System.IO.StreamReader(filename, System.Text.Encoding.Default);
            System.IO.StreamReader file = new System.IO.StreamReader(filename, System.Text.Encoding.UTF8);
            // System.IO.StreamReader file = new System.IO.StreamReader(filename, System.Text.Encoding.UTF7);
            //System.IO.StreamReader file = new System.IO.StreamReader(filename);
            if (headline)
            {
                file.ReadLine();
            }

            while ((line = file.ReadLine()) != null)
            {
              
               
                if (german)
                {
                    line = line.Replace(".", "");
                    line = line.Replace(",", ".");
                }

                string[] stringArray;
                stringArray = line.Split(new Char[] { separator });




                List<string> stringListe = new List<string>();//aus einem einem String Array eine String Liste Erstellen
                foreach (string str in stringArray)
                {
                    stringListe.Add(str);
                }
                series.Add(stringListe);
            }
            file.Close();
        }




    }
}
