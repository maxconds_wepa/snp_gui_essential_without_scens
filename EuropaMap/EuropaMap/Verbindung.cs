﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EuropaMap
{


    /**
     *Eine Verbindung besteht aus zwei Orten   
     *sie hat eine Paint methode die einen Strich von A nach B zeichnet 
     */
    class Verbindung
    {
        StandOrt ortA;
        StandOrt ortB;
        SolidBrush sBrushVerbindung = new SolidBrush(Color.Indigo);
        int penWidth = 4;

        public Verbindung(StandOrt ortA, StandOrt ortB)
        {
            this.ortA = ortA;
            this.ortB = ortB;

        }

       

  
       

        private Point getMiddlePoint()
        {
            int xPos;
            int Ypos;
            int x1 = ortA.getPoint().X;
            int x2 = ortB.getPoint().X;
            int y1 = ortA.getPoint().Y;
            int y2 = ortB.getPoint().Y;
            if (x1 > x2)
            {
                xPos = x2 + (x1 - x2) / 2;
            }
            else
            {
                xPos = x1 + (x2 - x1) / 2;
            }

            if (y1 < y2)
            {
                Ypos = y1 + (y2 - y1) / 2;
            }
            else
            {
                Ypos = y2 + (y1 - y2) / 2;
            }
            Point middlePoint = new Point(xPos, Ypos);
            return middlePoint;

        }
/*
        public void paintVerbindung(PaintEventArgs e, double gesamt)
        {
            double anteil = value / gesamt;
            if (value > 0)
            {
                Font drawFont = new Font("Arial", 10);
                SolidBrush drawBrush = new SolidBrush(Color.Black);

                

                SolidBrush solidBrush = new SolidBrush(Color.Ivory);
                Pen pen1 = new Pen(solidBrush);
                if (anteil > 0.1)
                {
                    pen1.Width = 5;

                }

                pen1.DashCap = System.Drawing.Drawing2D.DashCap.Round;
                e.Graphics.DrawLine(pen1, ortA.getPoint(), ortB.getPoint());
                Point middlePoint = getMiddlePoint();
                int width = 180;
                int height = 18;
                Rectangle rect = new Rectangle(middlePoint.X - width / 2, middlePoint.Y - height / 2, width, height);
                //e.Graphics.FillRectangle(drawBrushBackgroundColor, rect);
                e.Graphics.DrawString("" + this.value + "=: " + anteil + "%", drawFont, drawBrush, rect);
            }
        }
        */


        public void paintVerbindung(PaintEventArgs e)
        {

            
            Pen pen1 = new Pen(sBrushVerbindung);
            pen1.Width = penWidth;

            pen1.DashCap = System.Drawing.Drawing2D.DashCap.Round;
            e.Graphics.DrawLine(pen1, ortA.getPoint(), ortB.getPoint());
            /*
             Point middlePoint = getMiddlePoint();
             int width = 180;
             int height = 18;
             Rectangle rect = new Rectangle(middlePoint.X - width / 2, middlePoint.Y - height / 2, width, height);
             e.Graphics.FillRectangle(drawBrushBackgroundColor, rect);
             e.Graphics.DrawString("" + this.value+"=: "+anteil+"%", drawFont, drawBrush, rect);*/
        }

    }
}
