﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace EuropaMap
{

   


    public partial class ProductionMap : Form
    {

        List<TextBox> standortTextBoxList = new List<TextBox>();


        CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
        
        private String imagefile;
        
       
        private String planlocationsFile ="PlantLocations.csv";

        private String optDataFile = "optimierung_output_graphics.csv"; 
        private String fixDataFile = "fixierung_output_graphics.csv";
        private String dataFile = "optimierung_output_graphics.csv"; 

       
        private String optTransportDataFile= "optimierung_output_graphics_ic.csv";
        private String fixTransportDataFile = "fixierung_output_graphics_ic.csv";


        private String transportDataFile = "optimierung_output_graphics_ic.csv";



        private String mashineDataFile = "optimierung_output_produktion.csv";
        private String optMashineDataFile = "optimierung_output_produktion.csv";
        private String fixMashineDataFile = "fixierung_output_produktion.csv";

        private String kundenDataFile = "optimierung_output_kundentransport_einzel.csv";
        private String optKundenDataFile = "optimierung_output_kundentransport_einzel.csv";
        private String fixKundenDataFile = "fixierung_output_kundentransport_einzel.csv";
        
        
        String fullPath;

        private String dataPath;
        private Char separator = ';';


        List<String> selectedZeitraumItems = new List<String>();


        StandOrt lastSelectedOrt;

        string curProdItem = "";

        List<String> zeitraumList = new List<String>();//Enthält die 3 verschiedenen Monate + gesamt


        List<List<String>> dataList = new List<List<String>>();//optimierung_output_graphics.csv  //in dieser Liste werden die Standort Daten abgespeichrt, welche aus der Datei des ersten Befehlszeilenargument gelesen wird
        List<List<String>> transportDataList = new List<List<String>>();//in dieser Liste werden die Transport Daten abgespeichrt, welche aus der Datei des zweiten Befehlszeilenargument gelesen wird
        List<List<String>> mashineDataList = new List<List<String>>();
        List<List<String>> kundenDataList = new List<List<String>>();

        List<List<String>> planlocationsList = new List<List<string>>();//Rohdaten als Liste der Standorte
        List<String> kopfzeileIC = new List<String>();

        List<String> kopfzeileMashine = new List<String>();
        List<String> kopfzeileKunde = new List<String>();



        List<string> productionList = new List<string>();//Enthält alle auszuwählende Attribute


        Bitmap image;


        List<StandOrt> standOrtListe = new List<StandOrt>();//enthält alle Standorte 


        double scale;//Skalierungs faktor des Bildes
        int imageWidth;
        int imageHeight;


        /*AUFRUF DEMO:
         * PlantlocationsDemo.csv optimierung_output_graphics.csv optimierung_output_graphics_ic.csv deutschland.png 0
         * 
         */

        /*Aufruf Real
         * 
         * Plantlocations.csv optimierung_output_graphics.csv optimierung_output_graphics_ic.csv europa.png 0
         * 
         * /


        /*Konstruktor
         * o/1 für Demo /Real (setzt den initialen Skalierungsfaktor)
         * 11.0/1 Deutsch,Englische Version
         * 12.Pfad des ordners 
         * 
         */
        public ProductionMap(  String version,String language, String path)
        {

           // MessageBox.Show("start");
            if (language == "0")
            {
                Data.german = true;
            }
            dataPath = path;

         
         
         

        



            if (version.Equals("0"))
            {
                this.imagefile = "deutschland.png";
             
                scale = 0.22;
            }

            else
            {
                this.imagefile = "europa.png";
                image = new Bitmap(imagefile);
                scale = 0.4;
            }
            image = new Bitmap(imagefile);
            imageWidth = image.Width;//2384;//1081 klein
            imageHeight = image.Height;//1863;//893 klein


            InitializeComponent();

        }

        private void displayChooseVersion()
        {
            optCheckBox.Visible = true;
            fixCheckBox.Visible = true;
        }

        public void readStandortliste()
        {
            standOrtListe = new List<StandOrt>();//enthält alle Standorte 


            for (int i = 0; i < planlocationsList.Count(); i++)
            {
                Point p = new Point(Convert.ToInt32(planlocationsList[i][2]), Convert.ToInt32(planlocationsList[i][3]));
                StandOrt ort = new StandOrt(planlocationsList[i][0], planlocationsList[i][1], p);
                standOrtListe.Add(ort);
            }

        }
        protected Exception resultExc;

        public Exception ResultException
        {
            get { return resultExc; }
        }
        //Initialisierung der Komponenten ListBox und CheckBoxen(Laden der CSV Dateien)
        private void ProductionMap_Load(object sender, System.EventArgs e)
        {
            String fullDataFileName, fullTransportDataFileName;

            List<String> zeitraumListKomplett = new List<String>();

            try
            {
                resultExc = null;
                productionList = new List<string>();
                dataList = new List<List<String>>();
                transportDataList = new List<List<String>>();
                bool headline = true;

                if (dataPath != "")
                {
                    fullPath = dataPath + Path.DirectorySeparatorChar;
                }
                else
                {
                    fullPath = "";
                }

                fullDataFileName = fullPath + dataFile;
                fullTransportDataFileName = fullPath + transportDataFile;


                Data.read_header(ref fullDataFileName, ref separator, ref productionList);
                productionList.RemoveAt(0);
                productionList.RemoveAt(0);

                Data.read_header(ref fullTransportDataFileName, ref separator, ref kopfzeileIC);
                zeitraumList = new List<String>();


                //Opt_Daten werden default mäßig geladen
                loadInitialMashineData(mashineDataFile);
                loadInitialKundenData(kundenDataFile);
                loadInitialData(optDataFile, optTransportDataFile);


                headline = true;
                Data.read_data(ref planlocationsFile, ref separator, ref headline, ref planlocationsList);

                readStandortliste();

                for (int i = 0; i < dataList.Count(); i++)
                {
                    zeitraumListKomplett.Add(dataList[i][1]);
                }


                for (int i = 0; i < zeitraumListKomplett.Count(); i++)
                {
                    if (!zeitraumList.Contains(zeitraumListKomplett.ElementAt(i)))
                    {
                        zeitraumList.Add(zeitraumListKomplett.ElementAt(i));
                    }

                }


                if (File.Exists(fixDataFile))
                {
                    displayChooseVersion();
                }
                CheckBoxMonat1.Checked = true;
                Produktion.DrawMode = DrawMode.OwnerDrawFixed;
                Produktion.DrawItem += new DrawItemEventHandler(Produktion_DrawItem);
                Zeitraum.DataSource = zeitraumList;
                CheckBoxMonat1.Text = zeitraumList[0];
                checkBoxMonat2.Text = zeitraumList[1];
                checkBoxMonat3.Text = zeitraumList[2];

                Produktion.DataSource = productionList;

                image = new Bitmap(imagefile);

                //Punkte an Skalierung anpassen
                for (int i = 0; i < standOrtListe.Count(); i++)
                {
                    standOrtListe[i].scale(scale);
                }


                image = new Bitmap(image, new Size((int)(imageWidth * scale), (int)(imageHeight * scale)));
                pictureBox1.Height = image.Height;
                pictureBox1.Width = image.Width;


                pictureBox1.Image = (Image)image;


                // Connect the Paint event of the PictureBox to the event handler method.
                pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.image_Paint);
                pictureBox1.MouseClick += new MouseEventHandler(Control1_MouseClick);
                pictureBox1.MouseMove += new MouseEventHandler(Control1_MouseMove);

                // Add the PictureBox control to the Form.
                // Controls.Add(pictureBox1);
                panel1.Size = new Size(this.Width - panel1.Location.X - 20, this.Height - panel1.Location.Y - 40);
                panel1.Controls.Add(pictureBox1);

            }
            catch (Exception ex)
            {
                resultExc = ex;
                this.Close();
            }
        }

        /*LIEST aus den EXCELDATEIEN DIE OPTIMIERTE VERSION der FILES AUS UND SPEICHERT sie in Listn ab.
         die checkboxen optCheckBox/fixCheckBox laden die Initialen Daten neu ein.      
         */
        private void loadInitialMashineData(String mashineDataFile)
        {
            String fullDataFileName = fullPath + mashineDataFile;
            bool headline = true;
            mashineDataList.Clear();
            Data.read_data(ref fullDataFileName, ref separator, ref headline, ref mashineDataList);
            Data.read_header(ref fullDataFileName, ref separator, ref kopfzeileMashine);
            StandOrt.mashineDataKopfzeile.Clear();

            StandOrt.mashineDataKopfzeile.Clear();
            foreach (String spalte in kopfzeileMashine)
            {
                StandOrt.mashineDataKopfzeile.Add(spalte);
            }
        }
        private void loadInitialKundenData(String kundeDataFile)
        {
            String fullDataFileName = fullPath + kundeDataFile;
            bool headline = true;
            kundenDataList.Clear();
            Data.read_data(ref fullDataFileName, ref separator, ref headline, ref kundenDataList);
            Data.read_header(ref fullDataFileName, ref separator, ref kopfzeileKunde);
            StandOrt.kundeDataKopfzeile.Clear();

           
            foreach (String spalte in kopfzeileKunde)
            {
                StandOrt.kundeDataKopfzeile.Add(spalte);
            }
            ;
        }
        private void loadInitialData(String dataFile, String transportDataFile)
        {
            String fullDataFileName = fullPath + dataFile;
            String fullTransportDataFileName = fullPath + transportDataFile;
            bool headline = true;
            dataList.Clear();
            transportDataList.Clear();
            Data.read_data(ref fullDataFileName, ref separator, ref headline, ref dataList);
            Data.read_data(ref fullTransportDataFileName, ref separator, ref headline, ref transportDataList);
        }


        //Wird verwendet um die ListBox in alternating Colors darzustellen
        private void Produktion_DrawItem(object sender, DrawItemEventArgs e)
        {

            bool isSelected = ((e.State & DrawItemState.Selected) == DrawItemState.Selected);

            e.DrawBackground();
            Graphics g = e.Graphics;
            Font drawFont = new Font("Arial", 10);
            Color color = isSelected ? SystemColors.Highlight :
            e.Index % 2 == 0 ? Color.WhiteSmoke : Color.White;

            SolidBrush drawBrush = new SolidBrush(Color.Black);




            g.FillRectangle(new SolidBrush(color), e.Bounds);


            e.Graphics.DrawString(productionList.ElementAt(e.Index), drawFont, drawBrush, e.Bounds);
            // Print text

            e.DrawFocusRectangle();
        }


        /*
         *Zeichnet die einzelnen Elemnente der Standorte in der Reinfolge der Aufrufe 
         *ein repaint tritt ein durch den Aufruf von Invalidate();
         */
        private void image_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {



            Graphics graphics = e.Graphics;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;



            foreach (StandOrt ort in standOrtListe)
            {
                ort.paintVerbindungen(e);
            }



            for (int i = 0; i < standOrtListe.Count(); i++)
            {

                standOrtListe[i].paintEllipse(e);

            }




            for (int i = 0; i < standOrtListe.Count(); i++)
            {
                standOrtListe[i].paintName(e);
            }



            foreach (StandOrt ort in standOrtListe)
            {
                if (ort.getSelectedDetail() == true)
                {
                    ort.paintDetailInformation(e, curProdItem);
                }
            }
        }



        /**
         * Anhand der Getroffenen Auswahl von Monat und Attribut aus Listbox werden die Standorte mit den  entsprechenden werten gesetzt
         * aufruf bei jedem Selection Changed
         */
        private void loadMashineData()
        {
            int indexPeriode = kopfzeileMashine.IndexOf("Periode");
            //Liste Leeren
            foreach (StandOrt ort in standOrtListe)
            {
                ort.mashineDataList.Clear();
            }

            //Addiert weitere ausgewählte Item auf
            for (int j = 0; j < selectedZeitraumItems.Count; j++)
            {
                String curZeitraumItem = selectedZeitraumItems[j];
                int indexZeitraum = zeitraumList.IndexOf(curZeitraumItem);

                List<List<String>> selectedDateDataList = new List<List<String>>();
                if (indexZeitraum >= 0)
                {

                    //AUS Allen Daten werden die Daten mit passendem Datum gefiltert
                    for (int i = 0; i < mashineDataList.Count(); i++)
                    {

                        if (mashineDataList[i][indexPeriode] == curZeitraumItem)
                        {
                            selectedDateDataList.Add(mashineDataList[i]);
                        }

                    }

                    //JEDER STANDORT Bekommt eine Liste an mashinenDaten 
                    for (int i = 0; i < standOrtListe.Count(); i++)
                    {
                        for (int k = 0; k < selectedDateDataList.Count; k++)
                        {
                            ;
                            if (standOrtListe[i].plz == selectedDateDataList[k][0])
                            {
                                List<String> data = new List<String>();
                                for (int m = 0; m < selectedDateDataList[k].Count(); m++)
                                {
                                    data.Add(selectedDateDataList[k][m]);
                                }
                                standOrtListe[i].addMashineData(data);
                            }
                        }
                    }
                }
            }
            ;
        }
       
        private void loadKundenData()
        {
            int indexWerk = kopfzeileKunde.IndexOf("Werk");
            int indexPeriode = kopfzeileKunde.IndexOf("Periode");
            //Liste Leeren
            foreach (StandOrt ort in standOrtListe)
            {
                ort.kundeDataList.Clear();
            }

            //Addiert weitere ausgewählte Item auf
            for (int j = 0; j < selectedZeitraumItems.Count; j++)
            {
                String curZeitraumItem = selectedZeitraumItems[j];
                int indexZeitraum = zeitraumList.IndexOf(curZeitraumItem);

                List<List<String>> selectedDateDataList = new List<List<String>>();
                if (indexZeitraum >= 0)
                {

                    //AUS Allen Daten werden die Daten mit passendem Datum gefiltert
                    for (int i = 0; i < kundenDataList.Count(); i++)
                    {

                        if (kundenDataList[i][indexPeriode] == curZeitraumItem)
                        {
                            selectedDateDataList.Add(kundenDataList[i]);
                        }

                    }

                    //JEDER STANDORT Bekommt eine Liste an kundenDaten 
                    for (int i = 0; i < standOrtListe.Count(); i++)
                    {
                        for (int k = 0; k < selectedDateDataList.Count; k++)
                        {
                         
                            if (standOrtListe[i].plz == selectedDateDataList[k][indexWerk])
                            {
                                List<String> data = new List<String>();
                                for (int m = 0; m < selectedDateDataList[k].Count(); m++)
                                {
                                    data.Add(selectedDateDataList[k][m]);
                                }
                                standOrtListe[i].addKundenData(data);
                            }
                        }
                    }
                }
            }
            ;
        }
        
        private void loadTransportData()
        {

            //null setzten
            foreach (StandOrt ort in standOrtListe)
            {
                if (ort.getSelected() == true)
                {
                    StandOrt.valueAll = ort.getValue();
                    ort.calculatePercentage(ort.getValue());
                    foreach (StandOrt neighbour in ort.getNeighbours())
                    {
                        neighbour.setValue(0);
                        neighbour.calculatePercentage(ort.getValue());
                    }
                }
            }



            //einlesen
            for (int j = 0; j < selectedZeitraumItems.Count; j++)
            {

                String curZeitraumItem = selectedZeitraumItems[j];


                List<List<String>> selectedDateTransportDataList = new List<List<String>>();



                //aus der Gesamten Datei werden alle einträge mit falschem Datum nichtübernommen
                for (int i = 0; i < transportDataList.Count(); i++)
                {

                    if (transportDataList[i][2] == curZeitraumItem)
                    {

                        selectedDateTransportDataList.Add(transportDataList[i]);
                    }


                }
                int indexZeitraum = zeitraumList.IndexOf(curZeitraumItem);


                int indexProduction = kopfzeileIC.IndexOf(curProdItem);//productionList.IndexOf(curProdItem) + 3;//WerkA WerkB und Monat sind in der auswahl nicht vorhanden in den Daten aber schon  

                if (indexProduction >= 0)
                {
                    //nachbarwerte Setzten
                    for (int k = 0; k < selectedDateTransportDataList.Count; k++)
                    {
                        foreach (StandOrt ort in standOrtListe)
                        {
                            if (ort.plz == selectedDateTransportDataList[k][0])
                            {

                                if (ort.getSelected() == true)
                                {

                                    ;
                                    foreach (StandOrt neighbour in ort.getNeighbours())
                                    {
                                        if (neighbour.plz == selectedDateTransportDataList[k][1])
                                        {

                                            string valueStr =selectedDateTransportDataList[k][indexProduction];
                                            double value = Convert.ToDouble(valueStr, culture);

                                            neighbour.addValue(value);
                                            neighbour.calculatePercentage(ort.getValue());

                                        }
                                    }

                                }

                            }
                        }
                    }

                }
            }
        }


        /*
         * Lädt die Daten der Standorte  
         */
        private void loadStandortData()
        {
            //Liste Leeren
            foreach (StandOrt ort in standOrtListe)
            {
                ort.setValue(0);
            }
            StandOrt.valueAll = 0;

            //Addiert weitere ausgewählte Item auf
            for (int j = 0; j < selectedZeitraumItems.Count; j++)
            {
                String curZeitraumItem = selectedZeitraumItems[j];
                int indexZeitraum = zeitraumList.IndexOf(curZeitraumItem);
                int indexProduction = productionList.IndexOf(curProdItem) + 2;//Werk und Monat sind in der auswahl nicht vorhanden in den Daten aber schon  

                List<List<String>> selectedDateDataList = new List<List<String>>();
                if (indexZeitraum >= 0)
                {


                    for (int i = 0; i < dataList.Count(); i++)
                    {

                        if (dataList[i][1] == curZeitraumItem)
                        {
                            selectedDateDataList.Add(dataList[i]);
                        }

                    }

                    for (int i = 0; i < standOrtListe.Count(); i++)
                    {
                        for (int k = 0; k < selectedDateDataList.Count; k++)
                        {
                            if (standOrtListe[i].plz == selectedDateDataList[k][0])
                            {
                             
                                string valueStr = selectedDateDataList[k][indexProduction];
                                double value = Convert.ToDouble(valueStr,culture);
                                standOrtListe[i].addValue(value);
                            }
                        }
                    }


                    double sum = 0;

                    for (int i = 0; i < standOrtListe.Count(); i++)
                    {
                        sum += standOrtListe[i].getValue();


                    }
                    StandOrt.valueAll = sum;
                    for (int i = 0; i < standOrtListe.Count(); i++)
                    {

                        standOrtListe[i].calculatePercentage(StandOrt.valueAll);
                    }

                }
            }

        }


       

        /*falls eine neue Stadt, ein neues ProduktionsArgument oder eine anderer Zeitraum ausgewählt wurde wird selection Changed aufgerufen. 
        * hier wird die Liste StandortLIste an die neue auswahl angepasst indem die LoadFunktionen aufgerufen werden 
         * und anschliesen wird das bild neu gezeichnet
        */
        private void selectionChanged()
        {

          
            selectedZeitraumItems.Clear();

            if (CheckBoxMonat1.Checked == true)
            {
                selectedZeitraumItems.Add(zeitraumList[0]);

            }

            if (checkBoxMonat2.Checked == true)
            {

                selectedZeitraumItems.Add(zeitraumList[1]);
            }
            if (checkBoxMonat3.Checked == true)
            {
                selectedZeitraumItems.Add(zeitraumList[2]);
            }

            if (Produktion.SelectedItem != null)
            {
                curProdItem = Produktion.SelectedItem.ToString();
            }


            if (selectedZeitraumItems.Count != 0 && curProdItem != "")
            {
                loadMashineData();
                loadKundenData();
                loadStandortData();
                loadTransportData();
                // Force the form to be redrawn with the image.
                pictureBox1.Invalidate();
                ;

            }
            
            reCalculateCharts();
           
            reCalculateVerbindungen();

        }


        //wenn eine Stadt ausgewählt wird werden hier die alten Nachbarn gelöscht und die neuen erstellt
        private void reCalculateVerbindungen()
        {
            foreach (StandOrt ort in standOrtListe)
            {
                // ort.clearVerbindungen();
                ort.clearNeighbours();
                ort.isNeighbour = false;
            }


            foreach (StandOrt ort in standOrtListe)
            {
                if (ort.getSelected() == true)
                {


                    foreach (StandOrt ort2 in standOrtListe)
                    {
                        if (ort != ort2)// && ort2.getSelected() == true)
                        {
                            //ort.addVerbindung(ort2,1);
                            ort.addNeighbours(ort2);
                            ort2.isNeighbour = true;
                        }
                    }

                }
            }
        }



        private void Zeitraum_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectionChanged();
        }

        private void Produktion_SelectedIndexChanged(object sender, EventArgs e)
        {
            //bei wahl eines neuen productionItem sollen alle Orte unselected werden 
            foreach (StandOrt ort in standOrtListe)
            {
                ort.setSelected(false);
            }
            selectionChanged();
            if (curProdItem.Contains("IC"))
            {
                standardAnsichtWiederherstellen();
            }

        }


        //EVENT MOUSE Click (Chart wird erstellt oder verbindungen gezeichnet) 
        private void Control1_MouseClick(Object sender, MouseEventArgs e)
        {




            bool einOrtIstSchonAusgewaehlt = false;
            foreach (StandOrt ort in standOrtListe)
            {
                if (ort.getSelected() == true)
                {
                    einOrtIstSchonAusgewaehlt = true;
                }
            }

            if (einOrtIstSchonAusgewaehlt == false)
            {

                bool ausgewaehlt = false;
                for (int i = 0; i < standOrtListe.Count; i++)
                {
                    if (standOrtListe[i].isMouseOver(e.X, e.Y))
                    {
                        if (standOrtListe[i].getSelected() == true)//Wenn Ort Schon ausgewählt war 
                        {
                            standOrtListe[i].setSelected(false);

                        }
                        else
                        {
                            if (kopfzeileIC.IndexOf(curProdItem) >= 0)
                            {
                                standOrtListe[i].setSelected(true);
                            }
                        }

                        ausgewaehlt = true;
                    };
                }
                if (ausgewaehlt == false)
                {
                    chart1.Visible = false;
                    for (int i = 0; i < standOrtListe.Count; i++)
                    {
                        standOrtListe[i].setSelected(false);
                    }
                }
            }
            else
            {
                foreach (StandOrt ort in standOrtListe)
                {
                    ort.setSelected(false);
                }

            }
            //CHART Wird gezeichnet
            bool flagOrtAusgewaehlt = false;
            foreach (StandOrt ort in standOrtListe)
            {
                if (ort.isMouseOver(e.X, e.Y))
                {
                    lastSelectedOrt = ort;
                    selectionChanged();
                   
                    flagOrtAusgewaehlt = true;
             
                }
            }
            if (flagOrtAusgewaehlt == false)
            {
                lastSelectedOrt = null;
                selectionChanged();
            }
            
          
           
            reCalculateVerbindungen();
            pictureBox1.Invalidate();



        }
        private void reCalculateCharts()
        {

            if (lastSelectedOrt!=null&&curProdItem.Contains("Produktion"))
            {
                chart1.Series.Clear();
                chart1.ChartAreas[0].RecalculateAxesScale();              
                chart1.Series.Add(lastSelectedOrt.getMashineSeries(curProdItem));
                Title title = new Title("Maschinen-Detail-Ansicht: " + lastSelectedOrt.name);
                title.Font = new System.Drawing.Font("Arial", 14, FontStyle.Bold);
                //title.ForeColor = System.Drawing.Color.FromArgb(0, 0, 205);      
                chart1.Series[0].IsVisibleInLegend = false;
                chart1.Titles.Clear();
                chart1.ChartAreas[0].AxisX.Enabled = AxisEnabled.True;
                chart1.Titles.Add(title);
                chart1.Visible = true;
                chart2.Visible = false;
                chart3.Visible = false;
                pictureBox1.Visible = false;
                chart1.Invalidate();
            }

            kundenListBox.Visible = false;
            lableKunde.Visible = false;
            if (lastSelectedOrt != null && curProdItem.Contains("Kunde"))
            {
                chart1.Series.Clear();
                chart1.ChartAreas[0].RecalculateAxesScale();
                chart1.Series.Add(lastSelectedOrt.getKundenSeries(curProdItem));
                Title title = new Title("Kunden-Detail-Ansicht: " + lastSelectedOrt.name);
                title.Font = new System.Drawing.Font("Arial", 14, FontStyle.Bold);
                //title.ForeColor = System.Drawing.Color.FromArgb(0, 0, 205);      
                chart1.Series[0].IsVisibleInLegend = false;
                chart1.ChartAreas[0].AxisX.Enabled = AxisEnabled.False;
                chart1.Titles.Clear();
                chart1.Titles.Add(title);
                chart1.Visible = true;
                chart2.Visible = false;
                chart3.Visible = false;
                pictureBox1.Visible = false;
                chart1.Invalidate();
                kundenListBox.Visible = true;
                lableKunde.Visible = true;
                lableKunde.Text = "Kunden:";
                kundenListBox.DataSource= lastSelectedOrt.getKundenNamen();
            }

            
        }

        // EVENT MouseOver --> Städte zeigen detail Informationen
        private void Control1_MouseMove(Object sender, MouseEventArgs e)
        {
            foreach (StandOrt ort in standOrtListe)
            {
                if (ort.isMouseOver(e.X, e.Y))
                {
                    //   pictureBox1.Controls.Add(ort.getStandortTextBox(curProdItem));


                    if (ort.getSelectedDetail() == false)
                    {
                        ort.setDetailSelected(true);
                        pictureBox1.Invalidate();
                    }

                }
                else
                {

                    if (ort.getSelectedDetail() == true)
                    {
                        ort.setDetailSelected(false);
                        pictureBox1.Invalidate();
                    }
                }
            }


        }

        //verändert die Darstellung bei mouse over bar 
        private void chart1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // Call HitTest
            HitTestResult result = chart1.HitTest(e.X, e.Y);

            for (int seriesIndex = 0; seriesIndex < chart1.Series.Count(); seriesIndex++)
            {
                // Reset Data Point Attributes
                foreach (DataPoint point in chart1.Series[seriesIndex].Points)
                {
                    point.BackSecondaryColor = Color.Black;
                    point.BackHatchStyle = ChartHatchStyle.None;
                    point.BorderWidth = 1;
                }

                // If the mouse if over a data point
                if (result.ChartElementType == ChartElementType.DataPoint)
                {

                    if (result.Series.Name == chart1.Series[seriesIndex].Name)
                    {
                        // Find selected data point
                        DataPoint point = chart1.Series[seriesIndex].Points[result.PointIndex];
                        // MessageBox.Show(result.Series.Name);
                        // Change the appearance of the data point
                        point.BackSecondaryColor = Color.White;
                        point.BackHatchStyle = ChartHatchStyle.Percent25;
                        point.BorderWidth = 2;
                        point.IsValueShownAsLabel = true;



                    }
                }
                else
                {
                    // Set default cursor
                    this.Cursor = Cursors.Default;
                    foreach (DataPoint point in chart1.Series[0].Points)
                    {
                        point.IsValueShownAsLabel = false;
                    }
                }
            }
        }

        private void chart2_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // Call HitTest
            HitTestResult result = chart2.HitTest(e.X, e.Y);

            for (int seriesIndex = 0; seriesIndex < chart2.Series.Count(); seriesIndex++)
            {
                // Reset Data Point Attributes
                foreach (DataPoint point in chart2.Series[seriesIndex].Points)
                {
                    point.BackSecondaryColor = Color.Black;
                    point.BackHatchStyle = ChartHatchStyle.None;
                    point.BorderWidth = 1;
                }

                // If the mouse if over a data point
                if (result.ChartElementType == ChartElementType.DataPoint)
                {

                    if (result.Series.Name == chart2.Series[seriesIndex].Name)
                    {
                        // Find selected data point
                        DataPoint point = chart2.Series[seriesIndex].Points[result.PointIndex];
                        // MessageBox.Show(result.Series.Name);
                        // Change the appearance of the data point
                        point.BackSecondaryColor = Color.White;
                        point.BackHatchStyle = ChartHatchStyle.Percent25;
                        point.BorderWidth = 2;
                        point.IsValueShownAsLabel = true;
                    }
                }
                else
                {
                    // Set default cursor
                    this.Cursor = Cursors.Default;
                    foreach (DataPoint point in chart2.Series[0].Points)
                    {
                        point.IsValueShownAsLabel = false;
                    }
                }
            }
        }

        private void chart3_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // Call HitTest
            HitTestResult result = chart3.HitTest(e.X, e.Y);

            for (int seriesIndex = 0; seriesIndex < chart2.Series.Count(); seriesIndex++)
            {
                // Reset Data Point Attributes
                foreach (DataPoint point in chart3.Series[seriesIndex].Points)
                {
                    point.BackSecondaryColor = Color.Black;
                    point.BackHatchStyle = ChartHatchStyle.None;
                    point.BorderWidth = 1;
                }

                // If the mouse if over a data point
                if (result.ChartElementType == ChartElementType.DataPoint)
                {

                    if (result.Series.Name == chart3.Series[seriesIndex].Name)
                    {
                        // Find selected data point
                        DataPoint point = chart3.Series[seriesIndex].Points[result.PointIndex];
                        // MessageBox.Show(result.Series.Name);
                        // Change the appearance of the data point
                        point.BackSecondaryColor = Color.White;
                        point.BackHatchStyle = ChartHatchStyle.Percent25;
                        point.BorderWidth = 2;
                        point.IsValueShownAsLabel = true;
                    }
                }
                else
                {
                    // Set default cursor
                    this.Cursor = Cursors.Default;
                    foreach (DataPoint point in chart3.Series[0].Points)
                    {
                        point.IsValueShownAsLabel = false;
                    }
                }
            }
        }

        //Bei Click auf einen Balken wird Chart2 erzeugt
        private void chart1_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // Call HitTest
            HitTestResult result = chart1.HitTest(e.X, e.Y);

            for (int seriesIndex = 0; seriesIndex < chart1.Series.Count(); seriesIndex++)
            {
                // Reset Data Point Attributes
                foreach (DataPoint point in chart1.Series[seriesIndex].Points)
                {
                    point.BackSecondaryColor = Color.Black;
                    point.BackHatchStyle = ChartHatchStyle.None;
                    point.BorderWidth = 1;
                }

                // If the mouse if over a data point
                if (result.ChartElementType == ChartElementType.DataPoint)
                {

                    if (result.Series.Name == chart1.Series[seriesIndex].Name)
                    {
                        if (curProdItem.Contains("Produktion"))
                        {
                            String mashine = result.Series.Points[result.PointIndex].AxisLabel;
                            Series series = lastSelectedOrt.getProductSeries(mashine, curProdItem);

                            chart2.Series.Clear();
                            chart2.Titles.Clear();
                            chart2.ChartAreas[0].RecalculateAxesScale();

                            Title title = new Title("Produkt-Detail-Ansicht der Maschine: " + result.Series.Points[result.PointIndex].AxisLabel);
                            title.Font = new System.Drawing.Font("Arial", 14, FontStyle.Bold);
                            //title.ForeColor = System.Drawing.Color.FromArgb(0, 0, 205);      
                            chart2.Titles.Add(title);

                            chart2.Series.Add(series);
                            chart2.ChartAreas[0].Axes[0].IntervalAutoMode = IntervalAutoMode.VariableCount;
                            chart2.Series[0].IsVisibleInLegend = false;
                            chart2.Visible = true;
                        }

                        if (curProdItem.Contains("Kunde"))
                        {
                            String kunde = result.Series.Points[result.PointIndex].AxisLabel;
                            Series series = lastSelectedOrt.getKundenGruppeProductSeries(kunde, curProdItem);

                            chart2.Series.Clear();
                            chart2.Titles.Clear();
                            chart2.ChartAreas[0].RecalculateAxesScale();

                            Title title = new Title("Produkt-Detail-Ansicht des Kunden: " + result.Series.Points[result.PointIndex].AxisLabel);
                            title.Font = new System.Drawing.Font("Arial", 14, FontStyle.Bold);
                            //title.ForeColor = System.Drawing.Color.FromArgb(0, 0, 205);      
                            chart2.Titles.Add(title);

                            chart2.Series.Add(series);
                            chart2.ChartAreas[0].Axes[0].IntervalAutoMode = IntervalAutoMode.VariableCount;
                            chart2.Series[0].IsVisibleInLegend = false;
                            chart2.Visible = true;
                        }
                    }
                }
                else
                {
                    standardAnsichtWiederherstellen();
                }
            }
        }


        //Bei Click auf einen Balken wird Chart3 erzeugt
        private void chart2_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // Call HitTest
            HitTestResult result = chart2.HitTest(e.X, e.Y);

            for (int seriesIndex = 0; seriesIndex < chart2.Series.Count(); seriesIndex++)
            {
              
                // If the mouse is over a data point
                if (result.ChartElementType == ChartElementType.DataPoint)
                {

                    if (result.Series.Name == chart2.Series[seriesIndex].Name)
                    {
                    
                        if (curProdItem.Contains("Kunde"))
                        {
                            //chart2.Visible = false;
                            String product = result.Series.Points[result.PointIndex].AxisLabel;
                            //ALS Axis Lable werden am anfang und ende ein " hinzugefügt diese werden wieder entfernt
                           // product = product.Substring(1, product.Length - 2);
                             int indexDoppelpunkt = chart2.Titles[0].Text.IndexOf(":");
                            //+2 wegen : und leerzeichen danach
                             String kundenGruppe = chart2.Titles[0].Text.Substring(indexDoppelpunkt+2);
                          
                            
                           
                            Series series = lastSelectedOrt.getKundenProductSeries(product,kundenGruppe,curProdItem);
                            ;
                            chart3.Series.Clear();
                            chart3.Titles.Clear();
                            chart3.ChartAreas[0].RecalculateAxesScale();

                            Title title = new Title("Produkt: " + result.Series.Points[result.PointIndex].AxisLabel);
                            title.Font = new System.Drawing.Font("Arial", 14, FontStyle.Bold);
                            //title.ForeColor = System.Drawing.Color.FromArgb(0, 0, 205);      
                            chart3.Titles.Add(title);

                            chart3.Series.Add(series);
                            chart3.ChartAreas[0].Axes[0].IntervalAutoMode = IntervalAutoMode.VariableCount;
                            chart3.Series[0].IsVisibleInLegend = false;
                            chart3.Visible = true;
                             
                        }
                    }
                }
            }
        }







        private void standardAnsichtWiederherstellen(){
            selectionChanged();
            kundenListBox.Visible = false;
            lableKunde.Visible = false;
            lastSelectedOrt = null;
            chart2.Visible = false;
            chart1.Visible = false;
            chart3.Visible = false;
            pictureBox1.Visible = true;
    }

        private void checkBoxMonat2_CheckedChanged(object sender, EventArgs e)
        {

            selectionChanged();
        }

        private void checkBoxMonat3_CheckedChanged(object sender, EventArgs e)
        {

            selectionChanged();

        }

        private void CheckBoxMonat1_CheckedChanged(object sender, EventArgs e)
        {

            selectionChanged();
        }

        private void scaleGraphic(double scale)
        {



            readStandortliste();

            loadStandortData();
            //Punkte an Skalierung anpassen
            for (int i = 0; i < standOrtListe.Count(); i++)
            {
                standOrtListe[i].scale(scale);
            }

            image = new Bitmap(imagefile);
            image = new Bitmap(image, new Size((int)(imageWidth * scale), (int)(imageHeight * scale)));



            pictureBox1.Image = (Image)image;
            pictureBox1.Height = image.Height;
            pictureBox1.Width = image.Width;
            pictureBox1.Invalidate();
            //   MessageBox.Show("PictureBox-Height: "+pictureBox1.Height.ToString()+ "image Height: "+image.Height);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (scale - 0.05 > 0.05)
            {
                scale = scale - 0.05;
                scaleGraphic(scale);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            scale = scale + 0.05;
            scaleGraphic(scale);
        }

        private void ProductionMap_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (resultExc != null)
            //  throw resultExc;
        }

        private void optCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (optCheckBox.Checked == true)
            {
                fixCheckBox.Checked = false;
                loadInitialData(optDataFile, optTransportDataFile);
                loadInitialMashineData(optMashineDataFile);
                loadInitialKundenData(optKundenDataFile);
            }
            else
            {
                fixCheckBox.Checked = true;
                loadInitialData(fixDataFile, fixTransportDataFile);
                loadInitialMashineData(fixMashineDataFile);
                loadInitialKundenData(fixKundenDataFile);
            }
            selectionChanged();
        }

        private void fixCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (fixCheckBox.Checked == true)
            {
                optCheckBox.Checked = false;
                loadInitialData(fixDataFile, fixTransportDataFile);
                loadInitialMashineData(fixMashineDataFile);
                loadInitialKundenData(fixKundenDataFile);
            }
            else
            {
                optCheckBox.Checked = true;
                loadInitialData(optDataFile, optTransportDataFile);
                loadInitialMashineData(optMashineDataFile);
                loadInitialKundenData(optKundenDataFile);
            }
            selectionChanged();
        }

        private void ProductionMap_SizeChanged(object sender, EventArgs e)
        {
            panel1.Size = new Size(this.Width - panel1.Location.X - 20, this.Height - panel1.Location.Y - 40);
        }

        private void ProductionMap_Resize(object sender, EventArgs e)
        {

            panel1.Size = new Size(this.Width - panel1.Location.X - 20, this.Height - panel1.Location.Y - 40);
            chart1.Size = new Size(panel1.Width - 30, 300);
            chart2.Size = new Size(panel1.Width - 30, 300);
            chart3.Size = new Size(panel1.Width - 30, 300);
            chart1.Invalidate();

        }

        private void kundenListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (curProdItem.Contains("Kunde"))
            {
                foreach (DataPoint point in chart1.Series[0].Points)
                {
                    point.IsValueShownAsLabel = false;
                }
                DataPoint p = chart1.Series[0].Points[kundenListBox.SelectedIndex];
                // MessageBox.Show(result.Series.Name);
                // Change the appearance of the data point
                p.BackSecondaryColor = Color.White;
                p.BackHatchStyle = ChartHatchStyle.Percent25;
                p.BorderWidth = 2;
                p.IsValueShownAsLabel = true;

                String kunde = kundenListBox.SelectedItem.ToString();
                Series series = lastSelectedOrt.getKundenGruppeProductSeries(kunde, curProdItem);

                chart2.Series.Clear();
                chart2.Titles.Clear();
                chart2.ChartAreas[0].RecalculateAxesScale();

                Title title = new Title("Produkt-Detail-Ansicht des Kunden: " + kunde);
                title.Font = new System.Drawing.Font("Arial", 14, FontStyle.Bold);
                //title.ForeColor = System.Drawing.Color.FromArgb(0, 0, 205);      
                chart2.Titles.Add(title);
               

                chart2.Series.Add(series);
                //chart2.ChartAreas[0].Axes[0].//IntervalAutoMode = IntervalAutoMode.VariableCount;
                
               
                chart2.Series[0].IsVisibleInLegend = false;
                chart2.Visible = true;
                chart3.Visible = false;
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

      

     




















    }
}
