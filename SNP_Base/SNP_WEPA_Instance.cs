﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SNP_Utils;

namespace SNP_WEPA
{
    public interface IStorableObject
    {
        string Key{
         get;   
        }
    }

    public class IndexedItem<T>
    {
        public T item;
        public int index;
    }

    public class IndexedSet<T>
    {
        protected Dictionary<string,IndexedItem<T>> set;
        protected int currIndex;

        public IndexedSet()
        {
            set = new Dictionary<string, IndexedItem<T>>();
            currIndex = 0;
        }

        public void Add(IStorableObject item)
        {
            IndexedItem<T> nItem;

            nItem = new IndexedItem<T>();
            nItem.index = currIndex;
            currIndex++;
            nItem.item =(T) item;
            set.Add(item.Key, nItem);
        }

        public IndexedItem<T> this[string key]
        {
            get {
                if (set.ContainsKey(key))
                    return set[key];
                else
                    return null;
            }
        }

        public List<IndexedItem<T>> getAll()
        {
            List<IndexedItem<T>> all;

            all = new List<IndexedItem<T>>();

            foreach (KeyValuePair<string, IndexedItem<T>> kC in set)
                all.Add(kC.Value);

            return all;//capacities.getAllObjs();
        }
    }

    public class InitialStockValue : IStorableObject
    {
        protected string locationName;
        protected string productName;
      
        protected double val;

        public string Key
        {
            get { return productName + locationName; }
        }

        public double Val
        {
            get { return val; }
            set { val = value; }
        }

        public string LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }
       
    }

    public class CapacityValue : IStorableObject 
    {
        protected string periodName;
        protected double val;
       

        public string Key
        {
            get { return periodName; }
        }

        public double Val
        {
            get { return val; }
            set { val = value; }
        }

        public string PeriodName
        {
            get { return periodName; }
            set { periodName = value; }
        }
    }

    public class DemandValue : IStorableObject
    {
        protected string periodName;
        protected string productName;
        protected string locationName;
        protected string customerName;
        protected double val;
      

        public string Key
        {
            get { return productName + locationName + periodName + customerName; }
        }

        public double Val
        {
            get { return val; }
            set { val = value; }
        }

        public string PeriodName
        {
            get { return periodName; }
            set { periodName = value; }
        }

        public string LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }
    }

    public class Resource : IStorableObject
    {
        protected string resourceName;
        protected string locationName;
        
        protected IndexedSet<CapacityValue> capacities;//Dictionary<string, CapacityValue> capacities;//CSVector capacities;

        public Resource()
        {
            //capacities = new //CSVector();
            capacities = new IndexedSet<CapacityValue>();//new Dictionary<string, CapacityValue>();
        }

        

        public string Key
        {
            get {return resourceName;}// +locationName;
        }

        public string ResourceName
        {
            get { return resourceName; }
            set { resourceName = value; }
        }

        public string LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        public void addCapacity(CapacityValue cap)
        {
            //capacities.add( cap);
            //capacities.Add(cap.Key , cap);
            capacities.Add(cap);
        }

        public IndexedItem<CapacityValue> getCapacity(string periodKey)
        {
            return capacities[periodKey];//(CapacityValue)capacities.getObj(ref periodKey);
        }

        public List<IndexedItem<CapacityValue>> getAllCapacities()
        {
            return capacities.getAll();
            //return allCaps;//capacities.getAllObjs();
        }
    }

    public class ProductTransportationProcessConstraint  : IStorableObject 
    {
        string productName;
        string transprocessName;

        public string Key
        {
            get { return productName + transprocessName; }
        }
        public string ProductName
        {
            get { return productName ; }
            set { productName = value; }
        }

        public string TransprocessName
        {
            get { return transprocessName; }
            set { transprocessName = value; }
        }
    }


    public class TransportationProcess : IStorableObject 
    {
        string startLocation;
        string destLocation;
        double costPerTruck;
      
        public string Key
        {
            get { return startLocation + destLocation; }// +"PN" + productName;
        }

        public string StartLocation
        {
            get { return startLocation ; }
            set { startLocation = value; }
        }

        public string DestLocation
        {
            get { return destLocation; }
            set { destLocation = value; }
        }

        public Double CostPerTruck
        {
            get { return costPerTruck; }
            set { costPerTruck = value; }
        }
    }

    public class CustomerProductRelatedTransportationProcess : IStorableObject
    {
        string productName;
        string transportationProcessName;

        public string Key
        {
            get { return productName + transportationProcessName; }//customerLocationName + processName; }
        }

        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }

        public string TransportationProcessName
        {
            get { return transportationProcessName; }
            set { transportationProcessName = value; }
        }
    }

    public class CustomerRelatedProductionProcess : IStorableObject
    {
        protected List<string> customerLocationNames;
        protected List<string> processNames;
        protected string id;

        public CustomerRelatedProductionProcess()
        {
            customerLocationNames = new List<string>();
            processNames = new List<string>();
        }

        public string Key
        {
            get { return id; }//customerLocationName + processName; }
        }
        
        public void addCustomerLocationName(string name)
        {
            customerLocationNames.Add(name);
        }

        public void addProcessName(string name)
        {
            processNames.Add(name);
        }
        
        public List<string> CustomerLocationNames
        {
            get { return customerLocationNames; }
            //set { customerLocationName = value; }
        }

        public List<string> ProcessNames
        {
            get { return processNames; }
            //set { processName = value; }
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
    }

    public class ProductionProcess : IStorableObject 
    {  
        protected string productName;
        protected string resourceName;

        protected double tonPerShift;
        protected double costPerShift;

        public string Key
        {
            get { return productName + resourceName; } // locationName +
        }

        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }

        public string ResourceName
        {
            get { return resourceName; }
            set { resourceName = value; }
        }

        //public string LocationName
        //{
        //    get { return locationName; }
        //    set { locationName = value; }
        //}

        public double TonPerShift
        {
            get { return tonPerShift; }
            set { tonPerShift = value; }
        }

        public double CostPerShift
        {
            get { return costPerShift; }
            set { costPerShift = value; }
        }
    }

    public class Product : IStorableObject 
    {
        protected string productName;
        protected string historicalResource;
        protected double palletsPerTon;
        protected string productDescription;
        

        public Product () {
        }

         public string Key
        {
            get { return productName; }
        }

        

        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }

         public double PalletsPerTon
        {
            get { return palletsPerTon; }
            set { palletsPerTon = value; }
        }

         public string HistoricalResource
         {
             get { return historicalResource; }
             set { historicalResource = value; }
         }

         public string ProductDescription
         {
             get { return productDescription; }
             set { productDescription = value; }
         }
    };

    public class ProductionPrecedence :  IStorableObject
    {
        protected string precName;
        protected string succName;
        double coefficient;
       

        public ProductionPrecedence ()
        {
        }

      

        public string Key
        {
            get { return precName + succName; }
        }

        public string PrecName
        {
            get { return precName; }
            set { precName = value; }
        }

        public string SuccName
        {
            get { return succName; }
            set { succName = value; }
        }

        public double Coefficient
        {
            get { return coefficient; }
            set { coefficient = value; }
        }
    }

    public class Period : IStorableObject 
    {
        protected string periodName;
       

        public string Key
        {
            get { return periodName; }
        }

      

        public string PeriodName
        {
            get { return periodName; }
            set { periodName = value; }
        }
    }

    public class Transport : IStorableObject 
    {
        protected string transportationProcessName;
        protected string productName;
        

        public string Key
        {
            get { return transportationProcessName + productName; }
        }

     
        public string TransportationProcessName
        {
            get { return transportationProcessName; }
            set { transportationProcessName = value; }
        }

        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }
    }

    public class PlantLocation : IStorableObject
    {
        protected string locationName;
        protected int warehouseCapacity;

        public string Key
        {
            get { return locationName; }
        }

        public string LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        public int WarehouseCapacity
        {
            get { return warehouseCapacity; }
            set { warehouseCapacity = value; }
        }
    }

    public class CustomerLocation : IStorableObject
    {
        protected string locationName;
       
        public string Key
        {
            get { return locationName; }
        }

        public string LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

      
    }

    public class BasketSet : IStorableObject
    {
        protected string locationName;
      
        List<HashSet<string>> baskets;

        public BasketSet()
        {
            baskets = new List<HashSet<string>>();
        }

        public string Key
        {
            get { return locationName; }
        }

        

        public string LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        public void addBasket(HashSet<string> basket)
        {
            baskets.Add(basket);
        }

        public List<HashSet<string>> getAllBaskets()
        {
            return baskets;
        }

        public int Count
        {
            get { return baskets .Count; }
        }
    }

    public class HistProductionEntry : IStorableObject
    {
        protected string processName;
        protected string periodName;
        protected double quantity;
       

        public string Key
        {
            get { return processName + periodName; }
        }

       

        public string ProcessName
        {
            get { return processName; }
            set { processName = value; }
        }

        public string PeriodName
        {
            get { return periodName; }
            set { periodName = value; }
        }

        public double Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }
    }

    public class SNP_WEPA_Instance
    {
        protected IndexedSet< ProductionPrecedence> productionPrecedences;//CSVector productionPrecedences;
        protected IndexedSet<Product> products;//CSVector products;
        protected IndexedSet<Period> periods;//CSVector periods;
        protected IndexedSet<ProductionProcess> productionProcesses;//CSVector productionProcesses;
        protected IndexedSet<TransportationProcess> customerTransportationProcesses; //CSVector customerTransportationProcesses;
        protected IndexedSet<TransportationProcess> interplantTransportationProcesses;//CSVector interplantTransportationProcesses;
        //protected IndexedSet<Transport> customerProductTransports;//CSVector customerProductTransports;
        protected IndexedSet<Transport> interplantProductTransports;//CSVector interplantProductTransports;
        protected IndexedSet<DemandValue> demands;//CSVector demands;
        protected IndexedSet<PlantLocation> plantLocations;//CSVector plantLocations;
        protected IndexedSet<CustomerLocation> customerLocations;//CSVector customerLocations;
        protected IndexedSet<Resource> resources;//CSVector resources;
        protected IndexedSet<HistProductionEntry> histProductions;//CSVector histProductions;
        protected IndexedSet<InitialStockValue> initialStocks;
        protected IndexedSet<BasketSet> baskets;//CSVector baskets;
        protected IndexedSet<CustomerRelatedProductionProcess> cProdProcesses;
        protected IndexedSet<CustomerProductRelatedTransportationProcess> customerProductTransportationProcesses;

        protected int minRunTime;
        protected int minQtyForSplit;
        protected int minNumberPalletsPerLKW;

        public SNP_WEPA_Instance()
        {
            productionPrecedences = new IndexedSet<ProductionPrecedence>();//new CSVector();
            products = new IndexedSet<Product>();//new CSVector();
            periods = new IndexedSet<Period>();//new CSVector();
            productionProcesses = new IndexedSet<ProductionProcess>();//new CSVector();
            customerTransportationProcesses = new IndexedSet<TransportationProcess>();//new CSVector();
            interplantTransportationProcesses = new IndexedSet<TransportationProcess>();//new CSVector();
          //  productTransports=new CSVector();
            demands = new IndexedSet<DemandValue>();//new CSVector();
            plantLocations = new IndexedSet<PlantLocation>();//new CSVector();
            customerLocations = new IndexedSet<CustomerLocation>(); //new CSVector();
            resources = new IndexedSet<Resource>();//new CSVector();
            //customerProductTransports = new IndexedSet<Transport>();//new CSVector();
            interplantProductTransports = new IndexedSet<Transport>();//new CSVector();
            histProductions = new IndexedSet<HistProductionEntry>();//new CSVector();
            baskets = new IndexedSet<BasketSet>();//new CSVector();
            initialStocks = new IndexedSet<InitialStockValue>();
            cProdProcesses = new IndexedSet<CustomerRelatedProductionProcess>();
            customerProductTransportationProcesses = new IndexedSet<CustomerProductRelatedTransportationProcess>();
        }

        public void addCustomerRelatedProductionProcess(CustomerRelatedProductionProcess cProdProcess)
        {
            cProdProcesses.Add(cProdProcess);
        }

        public void addDemand(DemandValue dem)
        {
            //demands.add(dem);
            demands.Add(dem);//Add(dem.key, dem);
        }

        public IndexedItem<DemandValue> getDemand(string demandName)
        {
            return demands[demandName];//(DemandValue)demands.getObj(ref demandName);
        }
        public IndexedItem<InitialStockValue> getInitialStockValue(string key)
        {
            return initialStocks[key];
        }
        //public int getDemandIndex(string demandName)
        //{
        //    return demands[demandName].Index;//demands.getObjIndex(ref demandName);
        //}

        public void addProduct(Product prod)
        {
            //products.add(prod);
            products.Add(prod);
        }

        public IndexedItem<Product> getProduct( string productName)
        {
            return products[productName];//(Product) products.getObj(ref productName);
        }

        //public int getProductIndex(string productName)
        //{
        //    return products[productName].Index;//products.getObjIndex(ref productName);
        //}

        public IndexedItem<CustomerRelatedProductionProcess> getCustomerRelatedProductionProcess(string cProdProcessName)
        {
            return cProdProcesses[cProdProcessName];
        }

        public IndexedItem<CustomerProductRelatedTransportationProcess> getCustomerRelatedTransportationProcess(string cTranspProcessName)
        {
            return customerProductTransportationProcesses[cTranspProcessName];
        }

        public IndexedItem<CustomerLocation> getCustomerLocation(string locationName)
        {
            return customerLocations[locationName];//(Location)customerLocations.getObj(ref locationName);
        }

        //public int getCustomerLocationIndex(string locationName)
        //{
        //    return customerLocations[locationName].Index; //customerLocations.getObjIndex(ref locationName);
        //}

        public IndexedItem<PlantLocation> getPlantLocation(string locationName)
        {
            return plantLocations[locationName];//(Location)plantLocations.getObj(ref locationName);
        }

        //public int getPlantLocationIndex(string locationName)
        //{
        //    return plantLocations[locationName].Index;//plantLocations.getObjIndex(ref locationName);
        //}

        public IndexedItem<Resource> getResource(string resourceName)
        {
            return resources[resourceName];//(Resource)resources.getObj(ref resourceName);
        }

        //public int getResourceIndex(string resourceName)
        //{
        //    return resources[resourceName].Index;//resources.getObjIndex(ref resourceName);
        //}

        public IndexedItem<ProductionProcess> getProductionProcess(string processName)
        {
            return productionProcesses[processName];//(ProductionProcess)productionProcesses.getObj(ref processName);
        }

        //public int getProductionProcessIndex(string processName)
        //{
        //    return productionProcessIndex[processName].Index];//productionProcesses.getObjIndex(ref processName);
        //}

        public IndexedItem<Period> getPeriod(string periodName)
        {
            return periods[periodName];//(Period)periods.getObj(ref periodName);
        }

        //public int getPeriodIndex(string periodName)
        //{
        //    return periods.getObjIndex(ref periodName);
        //}
        
        public void addHistoricalProduction(HistProductionEntry hPE)
        {
            histProductions.Add(hPE);//histProductions.add(hPE);
        }

        public void addInitialStock(InitialStockValue iV)
        {
            initialStocks.Add(iV);
        }

        public IndexedItem<HistProductionEntry> getHistoricalProduction(string hPName)
        {
            return histProductions[hPName];//(HistProductionEntry)histProductions.getObj(ref hPName);
        }

        //public int getHistoricalProductionIndex(string hPName)
        //{
        //    return histProductions.getObjIndex(ref hPName);
        //}


        public IndexedItem<TransportationProcess>  getCustomerTransportationProcess(string transProcessName)
        {
            return customerTransportationProcesses[transProcessName];//(TransportationProcess)customerTransportationProcesses.getObj(ref transProcessName);
        }

        //public int getCustomerTransportationProcessIndex(object transProcessName)
        //{
        //    return customerTransportationProcesses.getObjIndex(ref transProcessName);
        //}



        public IndexedItem<TransportationProcess> getInterplantTransportationProcess(string transProcessName)
        {
            return interplantTransportationProcesses[transProcessName];//(TransportationProcess)interplantTransportationProcesses.getObj(ref transProcessName);
        }

        //public int getInterplantTransportationProcessIndex(object transProcessName)
        //{
        //    return interplantTransportationProcesses.getObjIndex(ref transProcessName);
        //}

      
        public void addPeriod(ref Period period)
        {
            periods.Add(period);
        }

      
        public void addProductionProcess(ProductionProcess prodProcess)
        {  
            productionProcesses.Add(prodProcess);
        }

      
        public void addCustomerTransportationProcess(TransportationProcess transProc)
        {
            customerTransportationProcesses.Add(transProc);
        }

        public void addCustomerProductTransportationProcess(CustomerProductRelatedTransportationProcess cTranspProc)
        {
            customerProductTransportationProcesses.Add(cTranspProc);
        }
       
        public void addInterplantTransportationProcess(TransportationProcess transProc)
        {
            interplantTransportationProcesses.Add(transProc);
        }


        //public void addCustomerProductTransport(Transport prodTrans)
        //{   
        //    customerProductTransports.Add(prodTrans);
        //}

        public void addInterplantProductTransport(Transport prodTrans)
        {
            interplantProductTransports.Add(prodTrans);
        }

        
        public void addPlantLocation(PlantLocation plLoc)
        {
            plantLocations.Add(plLoc);
        }

        public void addCustomerLocation(CustomerLocation clLoc)
        {
            customerLocations.Add(clLoc);
        }

        public void addResource(Resource rs)
        {
            resources.Add(rs);
        }

        public void addBasketSet(BasketSet bSet)
        {
            //baskets.add(bSet);
            baskets.Add(bSet);
        }

        public IndexedItem<BasketSet> getBasketSet(string customerLocationName)
        {
            return baskets[customerLocationName];
        }

        //public int getBasketSetIndex(string customerLocationName)
        //{
        //    return baskets.getObjIndex(ref customerLocationName);
        //}

        public List<IndexedItem <DemandValue >> getAllDemands()
        {
            return demands.getAll();
        }

        public List<IndexedItem<Product>> getAllProducts()
        {
            return products.getAll();
        }

        public List<IndexedItem<ProductionProcess >> getAllProductionProcesses()
        {
            return productionProcesses.getAll();
        }

        public List<IndexedItem<Resource>> getAllResources()
        {
            return resources.getAll();
        }

        public List<IndexedItem<Period>> getAllPeriods()
        {
            return periods.getAll();
        }

        public List<IndexedItem<PlantLocation>> getAllPlantLocations()
        {
            return plantLocations.getAll ();
        }

        public List<IndexedItem<CustomerLocation>> getAllCustomerLocations()
        {
            return customerLocations.getAll();
        }

        public List<IndexedItem<TransportationProcess >> getAllCustomerTransportationProcesses()
        {
            return customerTransportationProcesses.getAll();
        }

        public List<IndexedItem<TransportationProcess >> getAllInterplantTransportationProcesses()
        {
            return interplantTransportationProcesses.getAll();
        }

        public List<IndexedItem<InitialStockValue>> getAllInitialStocks()
        {
            return initialStocks.getAll();
        }
        //public List<IndexedItem<Transport>> getAllCustomerProductTransports()
        //{
        //    return customerProductTransports.getAll();
        //}

        public List<IndexedItem<Transport>> getAllInterplantProductTransports()
        {
            return interplantProductTransports.getAll();
        }

        public List<IndexedItem<HistProductionEntry>> getAllHistoricalProductions()
        {
            return histProductions.getAll();
        }

        public List<IndexedItem<BasketSet>> getAllBaskets()
        {
            return baskets.getAll();
        }

        public List<IndexedItem<CustomerRelatedProductionProcess>> getAllCustomerRelatedProductionProcesses()
        {
            return cProdProcesses.getAll();
        }

        public List<IndexedItem<CustomerProductRelatedTransportationProcess>> getAllCustomerRelatedTransportationProcesses()
        {
            return customerProductTransportationProcesses.getAll();
        }

        public int MinRunTime
        {
            get { return minRunTime; }
            set { minRunTime = value; }
        }

        public int MinQtyForSplit
        {
            get { return minQtyForSplit; }
            set { minQtyForSplit = value; }
        }

        //protected int minNumberPalletsPerLKW;

        public int MinNumberPalletsPerLKW
        {
            get { return minNumberPalletsPerLKW; }
            set { minNumberPalletsPerLKW = value; }
        }
    }
}

