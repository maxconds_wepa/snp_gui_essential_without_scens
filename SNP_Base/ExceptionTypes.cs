﻿
using System;

namespace Optimizer_Exceptions
{
    public class OptimizerException : Exception
    {
        public enum OptimizerExceptionType
        {
            InputFileNotFound,
            ColumnNotFound,
            UnknownError,
            ExecutionStoppedOnUserRequest,
            ConfigurationError
        }

        private string details;
        private OptimizerExceptionType code;

        public OptimizerException(OptimizerExceptionType generalReason, string detailedExplanation)
        {
            details = detailedExplanation;
            code = generalReason;
        }

        public string Details
        {
            get { return details; }
        }

        public OptimizerExceptionType GeneralReason
        {
            get { return code; }
        }

        
    }

    
}