﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace GUI_SNP_Optimization
{
    public class TableWithSelection
    {
        public struct FilterValueCollection
        {
            public int numberOfFilterValues;
            public string[] filterValues;
        };

        protected DataTable dataTab;
        protected FilterValueCollection[] filterSpec;

        public TableWithSelection(DataTable baseTable)
        {
            dataTab = baseTable;
        }

        public TableWithSelection()
        {}

        public void setTable(DataTable baseTable)
        {
            dataTab = baseTable;
            filterSpec = new FilterValueCollection[dataTab.Columns.Count];
        }
        public void setFilterValueCollection(int colNum, ref FilterValueCollection fVC)
        {
            filterSpec[colNum] = fVC;
        }

        public DataTable getFilteredData()
        {
            string filtExpression;
            int i, j;
            bool isFirst;
            isFirst = true;
            filtExpression = "";

            for (i = 0; i < dataTab.Columns.Count; i++)
            {
                if (filterSpec[i].numberOfFilterValues > 0)
                {
                    if (!isFirst)
                        filtExpression += " AND ";
                    filtExpression += "(";
                    for (j = 0; j < filterSpec[i].numberOfFilterValues; j++)
                    {
                        filtExpression += dataTab.Columns[i].ColumnName + "=" + filterSpec[i].filterValues[j];
                        if (j < filterSpec[i].numberOfFilterValues - 1)
                        {
                            filtExpression += " OR ";
                        }
                    }
                    filtExpression += ")";
                    isFirst = false;
                }
            }
            DataTable selectedData;
            try
            {
                selectedData = dataTab.Select(filtExpression).CopyToDataTable();
            }
            catch
            {
                selectedData = new DataTable();
            }
            return selectedData;
        }

        public DataTable getData()
        {
            return dataTab;
        }

        public int getNumberOfColumns()
        {
            return dataTab.Columns.Count;
        }

        public DataTable getColumnData(int colNum)
        {
            DataTable colData;
            DataView colView;

            colView = new DataView(dataTab);
            colView.Sort = dataTab.Columns[colNum].ColumnName;

            colData = colView.ToTable(dataTab.TableName, true, dataTab.Columns[colNum].ColumnName);

            return colData;
        }
    }

    
}
