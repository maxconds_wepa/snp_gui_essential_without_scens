﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SNP_WEPA;
using Logging;
using Optimizer_Exceptions;
using System.IO;

namespace SNP_Optimizer
{
    class Program
    {
        //protected static string whseSouthWestEuropeFilename = "WareHouseCodes.xlsx";
        //protected static string locationTranslationFilename = "locsFinal.xlsx";
        //protected static string tonnageFileName = "Produktionstonnage 2013+2014 Werk Automat Artikel.xlsx";
        //protected static string baseDataAndWorkingPlansFileName = "Arbeitspläne und Stammdaten 2015-03-09angepasst.xlsx";
        //protected static string additionalBaseDataAndWorkingPlansFileName = "Fehlende_AP_für_SalesArtikel_2015_01_16.xlsx";
        //protected static string demandFileName = "sales plan 01-03.xlsx";
        //protected static string shiftsFileName = "Schichten.xlsx";
        //protected static string transpFileName = "12 - Dezember kumuliert nur Detail-Daten.xlsx";
        //protected static string transpSouthWestEuropeFileName = "12 - Dezember SüdWest kumuliert nur Daten_mit Kette.xlsx";
        //                                                        //"12 - Dezember SüdWest kumuliert nur Daten.xlsx";
        //protected static string interPlantTranspFileName = "FD-GR-2100_EN_Freight_cost_matrix_2014.xlsx";
        //protected static string costPerShiftFileName = "Produktionskosten_2015-01-16.xlsx";
        //protected static string costPerTonFileName = "Produktionskosten_2015-02-09.xlsx";
        //protected static string customerChainTransportCustomerRelationsFileName = "Kunde-Kette.xlsx";
        //protected static string tourChainFilename = "Aktuelle Frachten Eurolog 2014-12_mit Kette.xlsx";

        protected static string whseSouthWestEuropeFilename = "WareHouseCodes.xlsx";
        protected static string locationTranslationFilename = "locsFinal_Troyes.xlsx";//"locsFinal.xlsx";//"locsFinal_Troyes.xlsx";//"locsFinal.xlsx";
        protected static string tonnageFileName = "Produktionstonnage 2013+2014 Werk Automat Artikel.xlsx";
        protected static string baseDataAndWorkingPlansFileName = "Arbeitspläne und Stammdaten 2015-04-20.xlsx";// "Arbeitspläne und Stammdaten 2015-03-09angepasst.xlsx";//Arbeitspläne und Stammdaten 2015-04-20.xlsx
        protected static string additionalBaseDataAndWorkingPlansFileName = "Fehlende_AP_für_SalesArtikel_2015_01_16.xlsx";
        protected static string demandFileName = "sales plan 01-03.xlsx";//"sales plan 04-06.xlsx";//"sales plan 03-05.xlsx";    
        protected static string shiftsFileName = "Schichten_angepasst_Troyes.xlsx";//"Schichten_angepasst_Troyes_1Cap206C22.xlsx";//"Schichten_angepasst_Troyes.xlsx";//"Schichten_angepasst_Troyes.xlsx";//"Schichten.xlsx";
        protected static string transpFileName = "12 - Dezember kumuliert nur Detail-DatenMitKette.xlsx";//"12 - Dezember kumuliert nur Detail-Daten.xlsx";
        protected static string transpSouthWestEuropeFileName = "12 - Dezember SüdWest kumuliert nur Daten_mit Kette.xlsx";
        //"12 - Dezember SüdWest kumuliert nur Daten.xlsx";
        protected static string interPlantTranspFileName = "FD-GR-2100_EN_Freight_cost_matrix_2014_mit_Troyes.xlsx";//"FD-GR-2100_EN_Freight_cost_matrix_2014_mit_Troyes.xlsx";//"FD-GR-2100_EN_Freight_cost_matrix_2014.xlsx";
        protected static string costPerShiftFileName = "Produktionskosten_2015-02-09.xlsx";//"Produktionskosten_2015-01-16.xlsx";
        protected static string costPerTonFileName = "Produktionskosten_2015-02-09.xlsx";
        protected static string customerChainTransportCustomerRelationsFileName = "Kunde-Kette.xlsx";
        protected static string tourChainFilename = "Aktuelle Frachten Eurolog 2014-12_mit Kette.xlsx";

        //protected static string whseSouthWestEuropeFilename = "WareHouseCodes.xlsx";
        //protected static string locationTranslationFilename = "locsFinal_Troyes.xlsx";//"locsFinal.xlsx";//"locsFinal_Troyes.xlsx";//"locsFinal.xlsx";
        //protected static string tonnageFileName = "Produktionstonnage 2013+2014 Werk Automat Artikel.xlsx";
        //protected static string baseDataAndWorkingPlansFileName = "Arbeitspläne und Stammdaten 2015-04-20.xlsx";// "Arbeitspläne und Stammdaten 2015-03-09angepasst.xlsx";//Arbeitspläne und Stammdaten 2015-04-20.xlsx
        //protected static string additionalBaseDataAndWorkingPlansFileName = "Fehlende_AP_für_SalesArtikel_2015_01_16.xlsx";
        //protected static string demandFileName = "sales plan 04-06.xlsx";//"sales plan 04-06.xlsx";//"sales plan 03-05.xlsx";    
        //protected static string shiftsFileName = "Schichten_angepasst_Troyes.xlsx";//"Schichten_angepasst_Troyes_1Cap206C22.xlsx";//"Schichten_angepasst_Troyes.xlsx";//"Schichten_angepasst_Troyes.xlsx";//"Schichten.xlsx";
        //protected static string transpFileName = "12 - Dezember kumuliert nur Detail-Daten.xlsx";
        //protected static string transpSouthWestEuropeFileName = "12 - Dezember SüdWest kumuliert nur Daten_mit Kette.xlsx";
        ////"12 - Dezember SüdWest kumuliert nur Daten.xlsx";
        //protected static string interPlantTranspFileName = "FD-GR-2100_EN_Freight_cost_matrix_2014_mit_Troyes.xlsx";//"FD-GR-2100_EN_Freight_cost_matrix_2014_mit_Troyes.xlsx";//"FD-GR-2100_EN_Freight_cost_matrix_2014.xlsx";
        //protected static string costPerShiftFileName = "Produktionskosten_2015-01-16.xlsx";
        //protected static string costPerTonFileName = "Produktionskosten_2015-02-09.xlsx";
        //protected static string customerChainTransportCustomerRelationsFileName = "Kunde-Kette.xlsx";
        //protected static string tourChainFilename = "Aktuelle Frachten Eurolog 2014-12_mit Kette.xlsx";
        protected static string iniStocksFilename = "SSI Datei_Lagerbestand_01-01-2015.xlsx";
        protected static string minimumStocksFilename = "Mindestbestand.xlsx";
        protected static string transportStartSubstituteFileName = "Zuordnung Ladeort zu ProduktionsortKategorie.xlsx";
        protected static string whseCapFileName = "LagerKapazitäten.xlsx";
        protected static string plantfileName = "plants.csv";
        protected static string customerfileName = "customers.csv";
        protected static string resourcefileName = "resources.csv";
        protected static string productfileName = "products.csv";
        protected static string periodfileName = "periods.csv";
        protected static string processfileName = "processes.csv";
        protected static string capacityfileName = "capacities.csv";
        protected static string transportplantfileName = "transportsplant.csv";
        protected static string transportcustomerfileName = "transportscustomer.csv";
        protected static string demandfileName = "demands.csv";
        protected static string palletspertonfileName = "pallets_per_ton.csv";
        protected static string basketsfileName = "baskets.csv";
        protected static string histProdFileName = "histProduction.csv";
        protected static string iniStFileName = "startBestand.csv";
        protected static string warehouseCapacitiesFileName = "lagerkapazitaeten.csv";
        protected static string articleNamesFileName = "artikelnamen.csv";
        protected static string outputSeparator = " ";
       
        static void Main(string[] args)
        {
            GoogleDistanceMatrixSource dS;

            dS = new GoogleDistanceMatrixSource();

            dS.FilePath = Environment.CurrentDirectory;
            dS.loadExistingDistances();
            dS.addStartDest("DE", "59757", "IT", "55100");
            dS.addStartDest("ES", "50600", "IT", "55100");
            dS.addStartDest("DE", "59757", "ES", "50600");
            dS.fetchDistances();
            //ES50600

         
        //    LoggerCollection lC = new LoggerCollection();
        //    Logger cLog = new ScreenLogger();
        //    SNP_DataProvider dXLSWepa = new SNP_DataProvider();
        //    SNP_WEPA_Instance pI;
        //    //SNP_CPLEX_Optimizer usedOptimizer;
        //    OptimizerException .OptimizerExceptionType et;
        //    string userDirectory;
        //    string userName;
        //    string srcPath;
        //    userDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        //    userDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        //    userName = Environment.UserName;

        //    Optimizer_Data.CSV_File plantfile, customerfile, resourcefile, productfile, periodfile, processfile,
        //                            capacityfile, transportplantfile, transportcustomerfile, demandfile, palletspertonfile,
        //                            basketsfile, histProdFile, iniStockFile, warehouseCapacitiesFile, articleNamesFile;


        //    lC.addLogger(cLog);
        //    dXLSWepa.setLoggers(lC);

        //    if (args.Length > 0)
        //    //    dXLSWepa.SrcPath = args[0];
        //        srcPath = args[0];
        //    else
        //        srcPath = Environment.CurrentDirectory;
        //      //  dXLSWepa.SrcPath = Environment .CurrentDirectory ;// "C:\\Max-Con\\WEPA";//"C:\\Max-Con\\WEPA";

        //    srcPath += Path.DirectorySeparatorChar;

        //    try
        //    {
        //        // srcPath + customerChainTransportCustomerRelationsFileName, srcPath + tourChainFilename,
        //        //pI = dXLSWepa.getProblemInstance(srcPath + whseSouthWestEuropeFilename, srcPath + locationTranslationFilename, srcPath + tonnageFileName, srcPath + baseDataAndWorkingPlansFileName, srcPath + additionalBaseDataAndWorkingPlansFileName,
        //        //    srcPath + demandFileName, srcPath + shiftsFileName, srcPath + transpFileName, srcPath + transpSouthWestEuropeFileName, srcPath + interPlantTranspFileName, srcPath + costPerShiftFileName,
        //        //    srcPath + iniStocksFilename, srcPath + minimumStocksFilename, srcPath + transportStartSubstituteFileName, srcPath + whseCapFileName
        //        //    );

        //        plantfile = new Optimizer_Data.CSV_File(plantfileName);
        //        plantfile.CSVDelimiter = outputSeparator;
        //        plantfile.openForWriting(false);

        //        customerfile = new Optimizer_Data.CSV_File(customerfileName);
        //        customerfile.CSVDelimiter = outputSeparator;
        //        customerfile.openForWriting(false);

        //        resourcefile = new Optimizer_Data.CSV_File(resourcefileName);
        //        resourcefile.CSVDelimiter = outputSeparator;
        //        resourcefile.openForWriting(false);

        //        productfile = new Optimizer_Data.CSV_File(productfileName);
        //        productfile.CSVDelimiter = outputSeparator;
        //        productfile.openForWriting(false);

        //        periodfile = new Optimizer_Data.CSV_File(periodfileName);
        //        periodfile.CSVDelimiter = outputSeparator;
        //        periodfile.openForWriting(false);

        //        processfile = new Optimizer_Data.CSV_File(processfileName);
        //        processfile.CSVDelimiter = outputSeparator;
        //        processfile.openForWriting(false);

        //        capacityfile = new Optimizer_Data.CSV_File(capacityfileName);
        //        capacityfile.CSVDelimiter = outputSeparator;
        //        capacityfile.openForWriting(false);

        //        transportplantfile = new Optimizer_Data.CSV_File(transportplantfileName);
        //        transportplantfile.CSVDelimiter = outputSeparator;
        //        transportplantfile.openForWriting(false);

        //        transportcustomerfile = new Optimizer_Data.CSV_File(transportcustomerfileName);
        //        transportcustomerfile.CSVDelimiter = outputSeparator;
        //        transportcustomerfile.openForWriting(false);

        //        demandfile = new Optimizer_Data.CSV_File(demandfileName);
        //        demandfile.CSVDelimiter = outputSeparator;
        //        demandfile.openForWriting(false);

        //        palletspertonfile = new Optimizer_Data.CSV_File(palletspertonfileName);
        //        palletspertonfile.CSVDelimiter = outputSeparator;
        //        palletspertonfile.openForWriting(false);

        //        basketsfile = new Optimizer_Data.CSV_File(basketsfileName);
        //        basketsfile.CSVDelimiter = outputSeparator;
        //        basketsfile.openForWriting(false);

        //        histProdFile = new Optimizer_Data.CSV_File(histProdFileName);
        //        histProdFile.CSVDelimiter = outputSeparator;
        //        histProdFile.openForWriting(false);

        //        iniStockFile = new Optimizer_Data.CSV_File(iniStFileName);
        //        iniStockFile.CSVDelimiter = outputSeparator;
        //        iniStockFile.openForWriting(false);

        //        warehouseCapacitiesFile = new Optimizer_Data.CSV_File(warehouseCapacitiesFileName);
        //        warehouseCapacitiesFile.CSVDelimiter = outputSeparator;
        //        warehouseCapacitiesFile.openForWriting(false);

        //        articleNamesFile = new Optimizer_Data.CSV_File(articleNamesFileName);
        //        articleNamesFile.CSVDelimiter = outputSeparator;
        //        articleNamesFile.openForWriting(false);

        //        //ProblemInstanceWriter.writeProblemInstance (pI,plantfile,customerfile ,resourcefile,productfile,periodfile,
        //        //                                                processfile,capacityfile,transportplantfile ,transportcustomerfile,
        //        //                                                demandfile, histProdFile, basketsfile, palletspertonfile, iniStockFile, warehouseCapacitiesFile,articleNamesFile);

        //        plantfile.closeDoc();
        //        customerfile.closeDoc();
        //        resourcefile.closeDoc();
        //        productfile.closeDoc();
        //        periodfile.closeDoc();
        //        processfile.closeDoc();
        //        capacityfile.closeDoc();
        //        transportcustomerfile.closeDoc();
        //        transportplantfile.closeDoc();
        //        demandfile.closeDoc();
        //        histProdFile.closeDoc();
        //        basketsfile.closeDoc();
        //        palletspertonfile.closeDoc();
        //        iniStockFile.closeDoc();
        //        warehouseCapacitiesFile.closeDoc();
        //        articleNamesFile.closeDoc();
        //    }
        //    catch (OptimizerException e)
        //    {
        //        et = e.GeneralReason;//getGeneralReason ();

        //        if (et == OptimizerException.OptimizerExceptionType.InputFileNotFound)
        //            Console.WriteLine("Datei nicht gefunden " + e.Details);
        //        else if (et == OptimizerException.OptimizerExceptionType.ColumnNotFound)
        //            Console.WriteLine("Spalte nicht gefunden " + e.Details);
        //        else
        //            Console.WriteLine("Allgemeiner Fehler " + e.Details);
        //     }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.Message);
        //    }
        //   // dXLSWepa.outputResults(ref pI);
        }
    }
}
