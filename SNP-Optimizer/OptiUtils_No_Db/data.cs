﻿using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.IO;
using System.Windows;
using System.Globalization;
using System.Threading.Tasks;
using System;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Runtime.InteropServices;
using System.Data ;
using System.Threading;

namespace Optimizer_Data
{
    public interface DataBase
    {
        void writeData(DataTable source, string tableName, bool isUseColumnTypes);
        void writeData(string [] data, string tableName);
        DataTable getData(string sqlCommandStr);
        DataTable getSchema();
        DataTable getSchema(string collectionName);
        void execSqlCommand(string sqlCommandStr);
        void beginTransaction();
        void commitTransaction();
        void openConn();
        void closeConn();
   }

    public interface OptimizerDataSource
    {
        void writeData(DataTable source);
        void writeData(string [] lineArr);
        DataTable getData();
    }


    public class CSV_File : OptimizerDataSource, IDisposable
    {
        public class ColumnSpecification
        {
            public string colName;
            public Type colType;
            public string usedDateTimeFormat;
        }
        public const string DATETIME_TYPE = "datetime";
        public const string INT_TYPE = "int";
        public const string DOUBLE_TYPE = "double";
        public const string STRING_TYPE = "string";
        private string fileName;
        private Stream strm;

        private StreamReader reader;
        private StreamWriter writer;
        private string csvDelimiter = ",";
        private bool isInputColSpecs = false;
        private bool isOutputColSpecs = false;
        private bool isHeadline = true;
        private string[] headLine;

        private ColumnSpecification[] inputColSpecs;
        private ColumnSpecification[] outputColSpecs;
        private CultureInfo inputCulture;
        private CultureInfo outputCulture;
        private bool isSuppressQuotationMarks = true;
        public CSV_File(string fN)
        {
            fileName = fN;
            strm = null;
            reader = null;
            writer = null;
        }

        public bool IsSuppressQuotationMarks
        {
            set { isSuppressQuotationMarks = value; }
            get { return isSuppressQuotationMarks; }
        }

        public CultureInfo InputCultureForConversions
        {
            set { inputCulture = value; }
            get { return inputCulture; }
        }

        public CultureInfo OutputCultureForConversions
        {
            set { outputCulture = value; }
            get { return outputCulture; }
        }

        public ColumnSpecification[] InputColumnSpecifications
        {
            set { inputColSpecs = value; isInputColSpecs = true; }
            get { return inputColSpecs; }
        }

        public ColumnSpecification[] OutputColumnSpecifications
        {
            set { outputColSpecs = value; isOutputColSpecs = true; }
            get { return outputColSpecs; }
        }

        public ColumnSpecification[] getColSpecsByStrings(string[,] cSps)
        {
            int i;
            ColumnSpecification[] usedColSpecs;

            usedColSpecs = new ColumnSpecification[cSps.GetLength(0)];
            for (i = 0; i < usedColSpecs.Length; i++)
            {
                usedColSpecs[i] = new ColumnSpecification();
                usedColSpecs[i].colName = cSps[i, 0];
                switch (cSps[i, 1])
                {
                    case STRING_TYPE:
                        usedColSpecs[i].colType = typeof(String);
                        break;
                    case INT_TYPE:
                        usedColSpecs[i].colType = typeof(Int32);
                        break;
                    case DOUBLE_TYPE:
                        usedColSpecs[i].colType = typeof(Double);
                        break;
                    case DATETIME_TYPE:
                        usedColSpecs[i].colType = typeof(DateTime);
                        break;
                }
                usedColSpecs[i].usedDateTimeFormat = cSps[i, 2];
            }

            return usedColSpecs;
        }

        public void setInputColSpecsByStrings(string[,] cSps)
        {
            inputColSpecs = getColSpecsByStrings(cSps);
            isInputColSpecs = true;
        }

        public void setOutputColSpecsByStrings(string[,] cSps)
        {
            outputColSpecs = getColSpecsByStrings(cSps);
            isOutputColSpecs = true;
        }

        public CSV_File(Stream rs)
        {
            strm = rs;
            fileName = null;
        }

        public bool IsHeadLine
        {
            get { return isHeadline; }
            set { isHeadline = value; }
        }

        public string[] HeadLine
        {
            get { return headLine; }
            set { headLine = value; }
        }

        public string CSVDelimiter
        {
            get { return csvDelimiter; }
            set { csvDelimiter = value; }
        }

        public void openForReading()
        {
            if (fileName != null)
                reader = new StreamReader(fileName,System.Text.Encoding.Default);
            else if (strm != null)
                reader = new StreamReader(strm, System.Text.Encoding.Default );
        }

        public void openForWriting(bool isAppend)
        {
            if (fileName != null)
                writer = new StreamWriter(fileName, isAppend, System.Text.Encoding.Default );
            else if (strm != null)
                writer = new StreamWriter(strm, System.Text.Encoding.Default);
        }

        public void closeDoc()
        {
            if (reader != null)
                reader.Close();
            else if (writer != null)
                writer.Close();
        }

        public void writeData(DataTable source)
        {
            int i, j;
            string currLine;
            int[] colMappingsInput = null;
            int[] colMappingsOutput = null;
            
            if (isHeadline)
            {
                currLine = "";
                i = 0;
                foreach (DataColumn col in source.Columns)
                {
                    currLine = currLine + Convert.ToString(col.ColumnName);
                    if (i < source.Columns.Count - 1)
                        currLine += csvDelimiter;
                    i++;
                }
                writer.WriteLine(currLine);
            }

            if (isInputColSpecs && isOutputColSpecs)
            {
                colMappingsInput = new int[source.Columns.Count];
                colMappingsOutput = new int[source.Columns.Count];
                j = 0;
                foreach (DataColumn col in source.Columns)
                {
                    for (i = 0; i < inputColSpecs.Length; i++)
                    {
                        if (inputColSpecs[i].colName == col.ColumnName)
                        {
                            colMappingsInput[j] = i;
                        }
                    }

                    for (i = 0; i < outputColSpecs.Length; i++)
                    {
                        if (outputColSpecs[i].colName == col.ColumnName)
                        {
                            colMappingsOutput[j] = i;
                        }
                    }
                    j++;
                }
                foreach (DataRow r in source.Rows)
                {
                    currLine = "";

                    for (i = 0; i < r.ItemArray.Length; i++)
                    {
                        if (inputColSpecs[colMappingsInput[i]].colType == typeof(DateTime))
                        { 
                            currLine += DateTime.ParseExact(r.ItemArray[i].ToString(), inputColSpecs[colMappingsInput[i]].usedDateTimeFormat, inputCulture).ToString(outputColSpecs[colMappingsOutput[i]].usedDateTimeFormat);
                        }
                        else
                        {
                            //Zwischenschritt vom Input- zum Outputdatentypen und anschließend zum String erlaubt mehr Flexibilität z.B. von double nach int->string
                            currLine += Convert.ToString(Convert.ChangeType(Convert.ChangeType(r.ItemArray[i], inputColSpecs[colMappingsInput[i]].colType, inputCulture), outputColSpecs[colMappingsOutput[i]].colType, outputCulture), outputCulture);
                        }

                        if (i < r.ItemArray.Length - 1)
                            currLine += csvDelimiter;
                    }
                    writer.WriteLine(currLine);
                }
            }
            else
            {
                foreach (DataRow r in source.Rows)
                {
                    currLine = "";

                    for (i = 0; i < r.ItemArray.Length; i++)
                    {
                        currLine = currLine + Convert.ToString(r.ItemArray[i]);
                        if (i < r.ItemArray.Length - 1)
                            currLine += csvDelimiter;
                    }
                    writer.WriteLine(currLine);
                }
            }
        }

        public void writeData(string[] lineArr)
        {
            int i;
            string currLine = "";

            for (i = 0; i < lineArr.Length; i++)
            {
                currLine = currLine + Convert.ToString(lineArr[i]);
                if (i < lineArr.Length - 1)
                    currLine += csvDelimiter;
            }
            writer.WriteLine(currLine);
        }

        public DataTable getEmptyTableFromColSpecs(ColumnSpecification [] colSpecs)
        {
            DataTable resultTab;
            int i;

            resultTab = new DataTable();
            for (i = 0; i < colSpecs.Length; i++)
            {
                resultTab.Columns.Add(new DataColumn(colSpecs[i].colName, colSpecs[i].colType));
            }

            return resultTab;
        }

        public DataTable getEmptyTableFromInputColSpecs()
        {
            return getEmptyTableFromColSpecs(inputColSpecs);
        }

        public DataTable getEmptyTableFromOutputColSpecs()
        {
            return getEmptyTableFromColSpecs(outputColSpecs);
        }

        public DataTable getData()
        {
            int i, j, numberOfCols;
            string currLine;
            DataTable resultTab;
            DataRow currDataRow;
            string[] lineArr;
            int[] colMappings = null;
            bool isReadHeadLine;

            resultTab = new DataTable();
            isReadHeadLine = false;

            numberOfCols = 0;
            while (!reader.EndOfStream)
            {
                currLine = reader.ReadLine();
                if (isSuppressQuotationMarks)
                    currLine = currLine.Replace("\"", "");
                if (isHeadline && !isReadHeadLine)
                {
                    headLine = currLine.Split(csvDelimiter[0]);

                    isReadHeadLine = true;
                    if (isInputColSpecs)
                    {
                        colMappings = new int[headLine.Length];
                        for (j = 0; j < headLine.Length; j++)
                        {
                            for (i = 0; i < inputColSpecs.Length; i++)
                            {
                                if (inputColSpecs[i].colName == headLine[j])
                                    break;
                            }
                            colMappings[j] = i;
                            if (i<inputColSpecs.Length )
                                resultTab.Columns.Add(new DataColumn(inputColSpecs[i].colName, inputColSpecs[i].colType));
                           
                            numberOfCols++;
                        }

                        for (j = numberOfCols; j < inputColSpecs.Length; j++)
                        {
                            resultTab.Columns.Add(new DataColumn(inputColSpecs[j].colName, inputColSpecs[j].colType));
                            numberOfCols++;
                        }
                    }
                    else
                    {
                        for (j = 0; j < headLine.Length; j++)
                        {
                            resultTab.Columns.Add(headLine[j]);
                            numberOfCols++;
                        }
                    }
                }
                else
                {
                    lineArr = currLine.Split(csvDelimiter[0]);

                    if (lineArr.Length > numberOfCols)
                    {
                        if (isInputColSpecs)
                        {
                            for (j = numberOfCols; j < inputColSpecs.Length; j++)
                            {
                                resultTab.Columns.Add(new DataColumn(inputColSpecs[j].colName, inputColSpecs[j].colType));
                                numberOfCols++;
                            }
                        }
                        else
                        {
                            for (j = numberOfCols; j < lineArr.Length; j++)
                            {
                                resultTab.Columns.Add();
                                numberOfCols++;
                            }
                        }
                    }

                    if (isInputColSpecs)
                    {
                        currDataRow = resultTab.NewRow();
                        currDataRow.BeginEdit();
                        if (isHeadline)
                        {
                            for (j = 0; j < lineArr.Length; j++)
                            {
                                if (colMappings[j] < inputColSpecs.Length)
                                {
                                    if (resultTab.Columns[colMappings[j]].DataType == typeof(DateTime))//(currDataRow.ItemArray[colMappings[j]].GetType() == typeof(DateTime))
                                    {
                                        currDataRow[colMappings[j]] = DateTime.ParseExact(lineArr[j], inputColSpecs[colMappings[j]].usedDateTimeFormat, inputCulture);
                                    }
                                    else
                                        currDataRow[colMappings[j]] = Convert.ChangeType(lineArr[j], resultTab.Columns[colMappings[j]].DataType, inputCulture);
                                }
                            }
                        }
                        else
                        {

                            for (j = 0; j < lineArr.Length; j++)
                            {
                                if (resultTab.Columns[j].DataType == typeof(DateTime))//(currDataRow.ItemArray[j].GetType() == typeof(DateTime))
                                {
                                    currDataRow[j] = DateTime.ParseExact(lineArr[j], inputColSpecs[j].usedDateTimeFormat, inputCulture);
                                }
                                else
                                    currDataRow[j] = Convert.ChangeType(lineArr[j], resultTab.Columns[j].DataType, inputCulture);
                            }
                        }
                        currDataRow.EndEdit();
                        resultTab.Rows.Add(currDataRow);
                    }
                    else
                        resultTab.Rows.Add(lineArr);
                }
            }

            return resultTab;
        }
       
        public DataTable getData(int startRow, int startColumn, int numberOfColumns)
        {
            int i, j, currRowNum, maxColumn;
            string currLine;
            DataTable resultTab;
            string[] lineArr;
            string[] partialLineArr;

            resultTab = new DataTable();

            currRowNum = 0;

            for (j = 0; j < numberOfColumns; j++)
                resultTab.Columns.Add();

            partialLineArr = new string[numberOfColumns];

            while (!reader.EndOfStream)
            {
                currLine = reader.ReadLine();

                if (currRowNum >= startRow)
                {
                    lineArr = currLine.Split(csvDelimiter[0]);

                    if (lineArr.Length > startColumn + numberOfColumns)
                        maxColumn = startColumn + numberOfColumns;
                    else
                        maxColumn = lineArr.Length;

                    for (j = startColumn; j < maxColumn; j++)
                        partialLineArr[j - startColumn] = lineArr[j];

                    if (maxColumn < partialLineArr.Length)
                    {
                        for (j = startColumn; j < maxColumn; j++)
                            partialLineArr[j - startColumn] = "";
                    }

                    resultTab.Rows.Add(partialLineArr);
                }

                currRowNum++;
            }

            return resultTab;
        }

        public void Dispose()
        {
            closeDoc();
        }
    }
    //public class CSV_File: OptimizerDataSource,IDisposable 
    //{
    //    public class ColumnSpecification {

    //        public string colName;
    //        public Type colType;
    //        public string usedDateTimeFormat;
    //    }
    //    public const string DATETIME_TYPE = "datetime";
    //    public const string INT_TYPE = "int";
    //    public const string DOUBLE_TYPE = "double";
    //    public const string STRING_TYPE = "string";
    //    private string fileName;
    //    private Stream strm;

    //    private StreamReader reader;
    //    private StreamWriter writer;
    //    private string csvDelimiter  = ",";
    //    private bool isColSpecs = false;
    //    private bool isHeadline = true;
    //    private string [] headLine;

    //    private ColumnSpecification [] colSpecs;
    //    private CultureInfo usedCulture;
    
    //    public CSV_File(string fN)
    //    {
    //        fileName = fN;
    //        strm = null;
    //        reader = null;
    //        writer = null;
    //    }

    //    public CultureInfo CultureForConversions
    //    {
    //        set { usedCulture = value; }
    //        get { return usedCulture; }
    //    }
        
    //    public ColumnSpecification[] ColumnSpecifications
    //    {
    //        set { colSpecs = value; isColSpecs = true; }
    //        get { return colSpecs; }
    //    }
    //    public void setColSpecsByStrings(string[,] cSps)
    //    {
    //        int i;

    //        colSpecs = new ColumnSpecification[cSps.GetLength(0)];
    //        for (i = 0; i < colSpecs.Length; i++)
    //        {
    //            colSpecs[i] = new ColumnSpecification();
    //            colSpecs[i].colName = cSps[i, 0];
    //            switch (cSps[i, 1])
    //            {
    //                case STRING_TYPE:
    //                    colSpecs[i].colType = typeof(String);
    //                    break;
    //                case INT_TYPE :
    //                    colSpecs[i].colType = typeof(Int32);
    //                    break;
    //                case DOUBLE_TYPE :
    //                    colSpecs[i].colType = typeof(Double);
    //                    break;
    //                case DATETIME_TYPE :
    //                    colSpecs[i].colType = typeof(DateTime);
    //                    break;
    //            }
    //            colSpecs[i].usedDateTimeFormat = cSps[i, 2];
    //        }
    //        isColSpecs = true;
    //    }
    //    public CSV_File(Stream rs)
    //    {
    //        strm = rs;
    //        fileName = null;
    //    }

    //    public bool IsHeadLine 
    //    {
    //        get { return isHeadline;}
    //        set {isHeadline =value;}
    //    }

    //    public string [] HeadLine
    //    {
    //        get {return headLine;}
    //        set {headLine=value;}
    //    }

    //    public string CSVDelimiter 
    //    {
    //        get{return csvDelimiter ;}
    //        set {csvDelimiter =value;}
    //    }

    //    public void openForReading ()
    //    {
    //        if (fileName!=null)
    //            reader=new StreamReader (fileName);
    //        else if (strm != null)
    //            reader=new StreamReader (strm);
    //    }

    //    public void openForWriting(bool isAppend)
    //    {
    //        if (fileName!=null)
    //            writer=new StreamWriter (fileName,isAppend );
    //        else if (strm!=null)
    //            writer=new StreamWriter (strm);
    //    }

    //    public void closeDoc ()
    //    {
    //        if (reader!=null)
    //            reader.Close ();
    //        else if (writer!=null)
    //            writer.Close ();
    //    }

    //    public void writeData(DataTable source)
    //    {
    //        int i, j; 
    //        string currLine;
    //        //DataColumn col;
    //        if (isHeadline)
    //        {
    //            currLine = "";
    //            i = 0;
    //            foreach (DataColumn col in source.Columns)
    //            {
    //                currLine = currLine + Convert.ToString(col.ColumnName );
    //                if (i < source.Columns.Count  - 1)
    //                    currLine += csvDelimiter;
    //                i++;
    //            }
    //            writer.WriteLine(currLine);
    //        }


    //        foreach (DataRow r in source.Rows)
    //        {
    //            currLine="";

    //            for (i = 0; i < r.ItemArray.Length; i++)
    //            {
    //                currLine = currLine + Convert.ToString(r.ItemArray[i]);
    //                if (i<r.ItemArray .Length -1)
    //                    currLine+=csvDelimiter;
    //            }
    //            writer.WriteLine (currLine);
    //        }
    //    }

    //    public void writeData(string[] lineArr)
    //    {
    //        int i;
    //        string currLine = "";

    //        for (i = 0; i < lineArr.Length; i++)
    //        {
    //            currLine = currLine + Convert.ToString(lineArr[i]);
    //            if (i < lineArr.Length - 1)
    //                currLine += csvDelimiter;
    //        }
    //        writer.WriteLine(currLine);
    //    }

    //    public DataTable getData()
    //    {
    //        int i, j, numberOfCols;
    //        string currLine;
    //        DataTable resultTab;
    //        DataRow currDataRow;
    //        string[] lineArr;
    //        int [] colMappings=null;
    //        bool isReadHeadLine;

    //        resultTab = new DataTable();
    //        isReadHeadLine = false;
            

    //        numberOfCols = 0;
    //        while (!reader.EndOfStream)
    //        {
    //            currLine = reader.ReadLine();

    //            if (isHeadline && !isReadHeadLine)
    //            {
    //                headLine = currLine.Split(csvDelimiter[0]);
    //                isReadHeadLine = true;
    //                if (isColSpecs)
    //                {
    //                    colMappings = new int[headLine.Length];
    //                    for (j = 0; j < headLine.Length; j++)
    //                    {
    //                        for (i = 0; i < colSpecs.Length; i++)
    //                        {
    //                            if (colSpecs[i].colName == headLine[j])
    //                                break;
    //                        }
    //                        colMappings[j] = i;
    //                        resultTab.Columns.Add(new DataColumn(colSpecs[i].colName, colSpecs[i].colType));
    //                        //resultTab.Columns.Add(headLine[j]);
    //                        numberOfCols++;
    //                    }

    //                    for (j = numberOfCols; j < colSpecs.Length; j++)
    //                    {
    //                        resultTab.Columns.Add(new DataColumn(colSpecs[j].colName, colSpecs[j].colType));
    //                        numberOfCols++;
    //                    }
    //                }
    //                else
    //                {
    //                    for (j = 0; j < headLine.Length; j++)
    //                    {
    //                        resultTab.Columns.Add(headLine[j]);
    //                        numberOfCols++;
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                lineArr = currLine.Split(csvDelimiter[0]);

    //                if (lineArr.Length > numberOfCols)
    //                {
    //                    if (isColSpecs)
    //                    {
    //                        for (j = numberOfCols; j < colSpecs .Length; j++)
    //                        {
    //                            resultTab.Columns.Add(new DataColumn (colSpecs [j].colName,colSpecs [j].colType ));
    //                            numberOfCols++;
    //                        }
    //                    }
    //                    else
    //                    {
    //                        for (j = numberOfCols; j < lineArr.Length; j++)
    //                        {
    //                            resultTab.Columns.Add();
    //                            numberOfCols++;
    //                        }
    //                    }
    //                }

    //                string test="";
    //                if (isColSpecs)
    //                {
    //                    currDataRow = resultTab.NewRow();
    //                    currDataRow.BeginEdit();
    //                    if (isHeadline)
    //                    {
    //                        for (j = 0; j < lineArr.Length; j++)
    //                        {
    //                            if (currDataRow.ItemArray[colMappings[j]].GetType() == typeof(DateTime))
    //                            {
    //                                currDataRow[colMappings[j]] = DateTime.ParseExact(lineArr[j], colSpecs[colMappings[j]].usedDateTimeFormat, CultureInfo.InvariantCulture);
    //                            }
    //                            else
    //                                currDataRow[colMappings[j]] = Convert.ChangeType(lineArr[j], resultTab.Columns[colMappings[j]].DataType, usedCulture);
    //                        }
    //                    }
    //                    else
    //                    {   
                         
    //                        for (j = 0; j < lineArr.Length; j++)
    //                        {
    //                            if (currDataRow.ItemArray[j].GetType() == typeof(DateTime))
    //                            {
    //                                currDataRow[j] = DateTime.ParseExact(lineArr[j], colSpecs[j].usedDateTimeFormat, CultureInfo.InvariantCulture);
    //                            }
    //                            else
    //                                currDataRow[j] = Convert.ChangeType(lineArr[j], resultTab.Columns[j].DataType, usedCulture);
    //                        }
    //                    }
    //                    currDataRow.EndEdit();
    //                    resultTab.Rows.Add(currDataRow);
    //                }
    //                else
    //                    resultTab.Rows.Add(lineArr);
    //                //resultTab.Rows.Add(lineArr);
    //            }
    //        }

    //        return resultTab;
    //    }

    //     //    Public Function TableToArray(ByVal startRow As Integer, ByVal startColumn As Integer, ByVal numberOfColumns As Integer)

    //    public DataTable getData(int startRow,int startColumn,int numberOfColumns)
    //    {
    //        int i, j, currRowNum, maxColumn;
    //        string currLine;
    //        DataTable resultTab;
    //        string[] lineArr;
    //        string[] partialLineArr;

    //        resultTab = new DataTable();
           
    //        currRowNum=0;

    //        for (j = 0; j < numberOfColumns; j++)
    //             resultTab.Columns.Add();
             
    //        partialLineArr=new string [numberOfColumns];

    //        while (!reader.EndOfStream)
    //        {
    //            currLine = reader.ReadLine();

    //            if (currRowNum >=startRow )
    //            {
    //                lineArr = currLine.Split(csvDelimiter[0]);

    //                if (lineArr.Length >startColumn +numberOfColumns)
    //                    maxColumn = startColumn + numberOfColumns;
    //                else
    //                    maxColumn = lineArr.Length;

    //                for (j = startColumn; j < maxColumn; j++)
    //                    partialLineArr [j-startColumn]=lineArr [j];

    //                if (maxColumn < partialLineArr.Length)
    //                {
    //                    for (j = startColumn; j < maxColumn; j++)
    //                        partialLineArr [j-startColumn]="";
    //                }

    //                resultTab.Rows.Add(partialLineArr);
    //            }

    //            currRowNum++;
    //        }

    //        return resultTab;
    //    }

    //    public void Dispose()
    //    { 
    //        closeDoc();
    //    }
    //}

    public class SQLTable : OptimizerDataSource,IDisposable 
    {
        public const string SQL_FROM = "\n from ";
        public const string SQL_SELECT = "\n select ";
        public const string SQL_SELECTDISTINCT = "\n select distinct ";
        public const string SQL_JOIN = "\n join ";
        public const string SQL_LEFTJOIN = "\n left join ";
        public const string SQL_INNERJOIN = "\n inner join ";
        public const string SQL_OUTTERJOIN = "\n outter join ";
        public const string SQL_ON = "\n on ";
        public const string SQL_BRACKET_OPEN = "\n ( ";
        public const string SQL_BRACKET_CLOSE = "\n ) ";
        public const string SQL_GROUPBY = "\n group by ";
        public const string SQL_ORDERBY = "\n order by ";
        public const string SQL_WHERE = "\n where ";
        public const string SQL_DATETIME = "datetime";
        public const string SQL_DATE = "date";
        public const string SQL_VARCHAR = "varchar";
        public const string SQL_TEXT = "text";
        public const string SQL_STR_MARKER = "'";
        public const string SQL_OR = "\n OR ";
        public const string SQL_AND = "\n AND ";
        public const string SQL_AS = " as ";
        public const string SQL_UNION = "\n union ";
        public const string SQL_COUNT = " COUNT ";
        public const string SQL_SUM = " SUM ";
        public const string SQL_AVG = " AVG ";
        public const string SQL_MIN = " MIN ";
        public const string SQL_MAX = " MAX ";
        public const string SQL_ISNULL = " IFNull ";
        public const string SQL_CROSS_JOIN = "\n cross join ";

        private string queryTxt;
        private string tableName;
        private DataBase usedDB;
        private bool isUseTypesFromDataTable;
        public bool IsUseTypesFromDataTable
        {
            get { return isUseTypesFromDataTable; }
            set { isUseTypesFromDataTable = value; }
        }
        public SQLTable(DataBase db)
        {
            usedDB = db;
        }

        public SQLTable(DataBase db,string qTxt)
        {
            usedDB = db;
            queryTxt = qTxt;
        }

        public string TableForWriting 
        {
            set { tableName = value; }
         }

        public string QueryForReading
        {
            set { queryTxt = value; }
        }
        public void writeData(string [] data)
        {
            usedDB.writeData(data, tableName);
        }
        public void writeData(DataTable source)
        {
            usedDB.writeData(source, tableName,isUseTypesFromDataTable );
        }
        public DataTable getData()
        {
            return usedDB.getData(queryTxt);
        }
        public void Dispose()
        {
           // usedDB.closeConn();
        }
    }
    //public class SQLLiteDataBase : DataBase
    //{
    //    protected const string ISO_8601_FORMAT_STRING = "yyyy-MM-ddTHH:mm:ss";
    //    public const string IN_MEMORY_DB = "Data Source=:memory:";
    //    protected const string SQL_FROM = " from ";
    //    protected const string SQL_SELECT = "select ";
    //    protected const string SQL_SELECTDISTINCT = "select distinct ";
    //    protected const string SQL_GROUPBY = " group by ";
    //    protected const string SQL_ORDERBY = " order by by ";
    //    protected const string SQL_WHERE = " where ";
    //    protected const string SQL_DATETIME = "datetime";
    //    protected const string SQL_DATE = "date";
    //    protected const string SQL_VARCHAR = "varchar";
    //    protected const string SQL_TEXT = "text";
    //    protected const string SQL_STR_MARKER = "'";
    //    protected const string SQL_OR = " OR ";
    //    protected const string SQL_AND = " AND ";
    //    private string usedConnStr;
    //    private System.Data.SQLite.SQLiteConnection usedConnection;
    //    private System.Data.SQLite.SQLiteTransaction usedTransaction;
    //    //private CultureInfo usC;
    //    public SQLLiteDataBase(string connStr)
    //    {
    //        usedConnStr = connStr;
    //        usedConnection = new System.Data.SQLite.SQLiteConnection(usedConnStr);//System.Data.Odbc.OdbcConnection(usedConnStr);
    //        usedTransaction = null;
    //        //usC = CultureInfo.CreateSpecificCulture("en-US");
    //    }

    //    public void openConn()
    //    {
    //        usedConnection.Open();
    //    }

    //    public void closeConn()
    //    {
    //        usedConnection.Close();
    //    }

    //    public void beginTransaction()
    //    {
    //        try
    //        {
    //            usedTransaction = usedConnection.BeginTransaction();
    //        }
    //        catch (Exception ex)
    //        {
    //            usedConnection = new System.Data.SQLite.SQLiteConnection(usedConnStr); //new System.Data.Odbc.OdbcConnection(usedConnStr);
    //            usedConnection.Open();
    //            usedTransaction = usedConnection.BeginTransaction();
    //        }
    //    }
    //    public void commitTransaction()
    //    {
    //        if (usedConnection.State == ConnectionState.Open)
    //            usedTransaction.Commit();

    //        usedTransaction = null;
    //    }

    //    public string DbName
    //    {
    //        get { return usedConnection.Database; }
    //    }

    //    public void writeData(string[] lineArr, string tableName)
    //    {
    //        int j;
    //        string SQLWriteQuery;
    //        SQLWriteQuery = "INSERT INTO " + tableName + " VALUES (";
    //        for (j = 0; j < lineArr.Length; j++)
    //        {
    //            SQLWriteQuery += lineArr[j];

    //            if (j < lineArr.Length - 1)
    //                SQLWriteQuery += ",";
    //        }

    //        SQLWriteQuery += ")";
    //        execSqlCommand(SQLWriteQuery);
    //    }

    //    public void writeData(DataTable source, string tableName,bool isUseColumnTypes)
    //    {
    //        int i, j;
    //        string currCellStr;
    //        string SQLWriteQuery;
    //        DateTime currDT;
    //        //for (i = 0; i < source.Rows.Count; i++)
    //        //{
    //        foreach (DataRow r in source.Rows)
    //        {
    //            SQLWriteQuery = "INSERT INTO " + tableName + " VALUES (";
    //            for (j = 0; j < r.ItemArray.Length; j++)
    //            {
    //                //currCellStr = Convert.ToString(r.ItemArray[j]);
    //                if (isUseColumnTypes)
    //                {
    //                    if (source.Columns[j].DataType == typeof(String))
    //                    {
    //                        if (Convert.ToString(r.ItemArray[j]).Contains(SQL_STR_MARKER))
    //                        {
    //                            j = j;
    //                        }

    //                        currCellStr = SQL_STR_MARKER + Convert.ToString(r.ItemArray[j]).Replace (SQL_STR_MARKER,SQL_STR_MARKER +SQL_STR_MARKER ) + SQL_STR_MARKER;
    //                    }
    //                    else if (source.Columns [j].DataType ==typeof(Double)||source.Columns [j].DataType ==typeof(Int32)||source.Columns [j].DataType ==typeof(Int64))
    //                    {
    //                        currCellStr =Convert.ToString  (r.ItemArray [j],CultureInfo.InvariantCulture);
    //                    }
    //                    else if (source.Columns [j].DataType ==typeof(DateTime))
    //                    {
    //                        currDT=(DateTime) r.ItemArray [j];
    //                        currCellStr = SQL_STR_MARKER + currDT.ToString(ISO_8601_FORMAT_STRING, CultureInfo.InvariantCulture) + SQL_STR_MARKER;
    //                    }
    //                    else
    //                        currCellStr = Convert.ToString(r.ItemArray[j], CultureInfo.InvariantCulture);
    //                }
    //                else
    //                        currCellStr = Convert.ToString(r.ItemArray[j],CultureInfo .InvariantCulture);

    //                SQLWriteQuery += currCellStr;

    //                if (j < r.ItemArray.Length - 1)
    //                    SQLWriteQuery += ",";
    //            }

    //            SQLWriteQuery += ")";
    //            execSqlCommand(SQLWriteQuery);
    //        }
    //        //}
    //    }

    //    public void writeData(DataTable source, string tableName, string[,] tabSpecs)
    //    {
    //        int i, j;
    //        bool isStr;
    //        string currCellStr;
    //        string SQLWriteQuery;
    //        //for (i = 0; i < source.Rows.Count; i++)
    //        //{
    //        foreach (DataRow r in source.Rows)
    //        {
    //            SQLWriteQuery = "INSERT INTO " + tableName + " VALUES (";
    //            for (j = 0; j < r.ItemArray.Length; j++)
    //            {
    //                currCellStr = Convert.ToString(r.ItemArray[j]);
    //                if (tabSpecs[j, 1] == SQL_DATETIME ||
    //                        tabSpecs[j, 1] == SQL_DATE ||
    //                        tabSpecs[j, 1] == SQL_TEXT ||
    //                        tabSpecs[j, 1].Contains(SQL_VARCHAR)
    //                        )
    //                {
    //                    isStr = true;
    //                }
    //                else

    //                    isStr = false;
    //                if (isStr)
    //                {
    //                    SQLWriteQuery += SQL_STR_MARKER;
    //                    currCellStr = currCellStr.Replace(SQL_STR_MARKER, SQL_STR_MARKER + SQL_STR_MARKER);
    //                }
    //                SQLWriteQuery += currCellStr;
    //                if (isStr)
    //                    SQLWriteQuery += SQL_STR_MARKER;
    //                if (j < r.ItemArray.Length - 1)
    //                    SQLWriteQuery += ",";
    //            }

    //            SQLWriteQuery += ")";
    //            execSqlCommand(SQLWriteQuery);
    //        }
    //        //}
    //    }


    //    public void execSqlCommand(string sqlCommandStr)
    //    {
    //        System.Data.SQLite.SQLiteCommand queryCommand;
    //        queryCommand = new System.Data.SQLite.SQLiteCommand(sqlCommandStr, usedConnection);//new OdbcCommand(sqlCommandStr, usedConnection);

    //        if (usedTransaction != null)
    //            queryCommand.Transaction = usedTransaction;

    //        queryCommand.CommandTimeout = 30000;

    //        try
    //        {
    //            queryCommand.ExecuteNonQuery();
    //        }
    //        catch (Exception ex)
    //        {
    //            if (usedConnection.State != ConnectionState.Open && usedTransaction == null)
    //            {
    //                usedConnection = new System.Data.SQLite.SQLiteConnection(usedConnStr);
    //                usedConnection.Open();
    //                queryCommand = new System.Data.SQLite.SQLiteCommand(sqlCommandStr, usedConnection);
    //                queryCommand.ExecuteNonQuery();
    //            }
    //            else
    //            {
    //                usedTransaction = null;
    //                usedConnection = new System.Data.SQLite.SQLiteConnection(usedConnStr); //new System.Data.Odbc.OdbcConnection(usedConnStr);
    //                usedConnection.Open();
    //                throw ex;
    //            }
    //        }
    //    }
    //    public DataTable getData(string sqlCommandStr)
    //    {
    //        System.Data.SQLite.SQLiteCommand queryCommand;
    //        DataTable myDatatable;
    //        queryCommand = new System.Data.SQLite.SQLiteCommand(sqlCommandStr, usedConnection);//new OdbcCommand(sqlCommandStr, usedConnection);
    //        queryCommand.CommandTimeout = 30000;

    //        if (usedTransaction != null)
    //            queryCommand.Transaction = usedTransaction;

    //        myDatatable = new DataTable();

    //        try
    //        {
    //            myDatatable.Load(queryCommand.ExecuteReader());

    //        }
    //        catch (Exception ex)
    //        {
    //            if (usedConnection.State != ConnectionState.Open && usedTransaction == null)
    //            {
    //                usedConnection = new System.Data.SQLite.SQLiteConnection(usedConnStr);
    //                usedConnection.Open();
    //                queryCommand = new System.Data.SQLite.SQLiteCommand(sqlCommandStr, usedConnection);
    //                myDatatable.Load(queryCommand.ExecuteReader());
    //            }
    //            else
    //            {
    //                usedTransaction = null;
    //                throw ex;
    //            }
    //        }

    //        return myDatatable;
    //    }

    //    public DataTable getSchema()
    //    {
    //        return usedConnection.GetSchema();
    //    }

    //    public DataTable getSchema(string collectionName)
    //    {
    //        return usedConnection.GetSchema(collectionName);
    //    }
    //}

    public class ODBCDataBase : DataBase
    {
        private string usedConnStr;
        private System.Data.Odbc.OdbcConnection usedConnection;
        private OdbcTransaction usedTransaction;

        public ODBCDataBase(string connStr)
        {
            usedConnStr = connStr;
            usedConnection = new System.Data.Odbc.OdbcConnection(usedConnStr);
            usedTransaction = null;
        }

        public void openConn()
        {
            usedConnection.Open();
        }

        public void closeConn()
        {
            usedConnection.Close();
        }

        public void beginTransaction()
        {
            try{
                usedTransaction = usedConnection.BeginTransaction();
            }
            catch (Exception ex)
            {
                usedConnection = new System.Data.Odbc.OdbcConnection(usedConnStr);
                usedConnection.Open();
                usedTransaction = usedConnection.BeginTransaction();
            }
        }
        public void commitTransaction()
        {
            if (usedConnection.State == ConnectionState.Open)
                usedTransaction.Commit();

            usedTransaction = null;
        }

        public string DbName
        {
            get { return usedConnection.Database; }
        }

        public void writeData(string[] lineArr, string tableName)
        {
            int j;
            string SQLWriteQuery;
            SQLWriteQuery = "INSERT INTO " + tableName + " VALUES (";
            for (j = 0; j < lineArr.Length; j++)
            {   
                SQLWriteQuery += lineArr[j] ;

                if (j < lineArr.Length - 1)
                    SQLWriteQuery += ",";
            }

            SQLWriteQuery += ")";
            execSqlCommand(SQLWriteQuery);
        }

        public void writeData(DataTable source, string tableName, bool isUseColumnTypes)
        {
            int i, j;
            string currCellStr;
            string SQLWriteQuery;
            //for (i = 0; i < source.Rows.Count; i++)
            //{
                foreach (DataRow r in source.Rows)
                {
                    SQLWriteQuery = "INSERT INTO " + tableName + " VALUES (";
                    for (j=0;j<r.ItemArray .Length;j++)
                    {
                        currCellStr = Convert.ToString(r.ItemArray[j]);
                        SQLWriteQuery += currCellStr;

                        if (j < r.ItemArray.Length - 1)
                            SQLWriteQuery += ",";
                    }

                     SQLWriteQuery += ")";
                     execSqlCommand(SQLWriteQuery);
                }
            //}
        }
        public void execSqlCommand(string sqlCommandStr)
        { 
            OdbcCommand queryCommand;
            queryCommand = new OdbcCommand(sqlCommandStr, usedConnection);

            if (usedTransaction != null)
                queryCommand.Transaction = usedTransaction;

            queryCommand.CommandTimeout = 30000;

            try
            {
                queryCommand.ExecuteNonQuery();
            }
            catch (Exception ex )
            {
                if (usedConnection.State != ConnectionState.Open && usedTransaction == null)
                {
                    usedConnection = new System.Data.Odbc.OdbcConnection(usedConnStr);
                    usedConnection.Open();
                    queryCommand = new OdbcCommand(sqlCommandStr, usedConnection);
                    queryCommand.ExecuteNonQuery();
                }
                else
                {
                    usedTransaction = null;
                    usedConnection = new System.Data.Odbc.OdbcConnection(usedConnStr);
                    usedConnection.Open();
                    throw ex;
                }
            }
        }
        public DataTable getData(string sqlCommandStr)
        {
            OdbcCommand queryCommand;
            DataTable myDatatable;
            queryCommand = new OdbcCommand(sqlCommandStr, usedConnection);
            queryCommand.CommandTimeout = 30000;

            if (usedTransaction != null)
                queryCommand.Transaction = usedTransaction;
            
            myDatatable = new DataTable();

            try
            {
                myDatatable.Load(queryCommand.ExecuteReader());

            }
            catch (Exception ex)
            {
                if (usedConnection.State != ConnectionState.Open && usedTransaction == null)
                {
                    usedConnection = new System.Data.Odbc.OdbcConnection(usedConnStr);
                    usedConnection.Open();
                    queryCommand = new OdbcCommand(sqlCommandStr, usedConnection);
                    myDatatable.Load(queryCommand.ExecuteReader());
                }
                else
                {
                    usedTransaction = null;
                    throw ex;
                }
            }

            return myDatatable;
        }

        public DataTable getSchema()
        {
            return usedConnection.GetSchema();
        }

        public DataTable getSchema(string collectionName)
        {
            return usedConnection.GetSchema(collectionName);
        }
    }

    
    //    Private usedConnection As System.Data.OleDb.OleDbConnection
    //    Private usedTransaction As OleDbTransaction
    public class OleDataBase : DataBase
    {
        private string usedConnStr;
        private System.Data.OleDb.OleDbConnection usedConnection;
        private OleDbTransaction usedTransaction;

        public OleDataBase(string connStr)
        {
            usedConnStr = connStr;
            usedConnection = new System.Data.OleDb.OleDbConnection(usedConnStr);
            usedTransaction = null;
        }

        public void openConn()
        {
            usedConnection.Open();
        }

        public void closeConn()
        {
            usedConnection.Close();
        }

        public void beginTransaction()
        {
            try
            {
                usedTransaction = usedConnection.BeginTransaction();
            }
            catch (Exception ex)
            {
                usedConnection = new System.Data.OleDb.OleDbConnection(usedConnStr);
                usedConnection.Open();
                usedTransaction = usedConnection.BeginTransaction();
            }
        }
        public void commitTransaction()
        {
            if (usedConnection.State == ConnectionState.Open)
                usedTransaction.Commit();

            usedTransaction = null;
        }

        public string DbName
        {
            get { return usedConnection.Database; }
        }

        public void writeData(string[] lineArr, string tableName)
        {
            int j;
            string SQLWriteQuery;
            SQLWriteQuery = "INSERT INTO " + tableName + " VALUES (";
            for (j = 0; j < lineArr.Length; j++)
            {
                SQLWriteQuery += lineArr[j];

                if (j < lineArr.Length - 1)
                    SQLWriteQuery += ",";
            }

            SQLWriteQuery += ")";
            execSqlCommand(SQLWriteQuery);
        }

        public void writeData(DataTable source, string tableName, bool isUseColumnTypes)
        {
            int i, j;
            string currCellStr;
            string SQLWriteQuery;
            for (i = 0; i < source.Rows.Count; i++)
            {
                foreach (DataRow r in source.Rows)
                {
                    SQLWriteQuery = "INSERT INTO " + tableName + " VALUES (";
                    for (j = 0; j < r.ItemArray.Length; j++)
                    {
                        currCellStr = Convert.ToString(r.ItemArray[j]);
                        SQLWriteQuery += currCellStr;

                        if (j < r.ItemArray.Length - 1)
                            SQLWriteQuery += ",";
                    }

                    SQLWriteQuery += ")";
                    execSqlCommand(SQLWriteQuery);
                }
            }
        }
        public void execSqlCommand(string sqlCommandStr)
        {
            OleDbCommand queryCommand;
            queryCommand = new OleDbCommand(sqlCommandStr, usedConnection);

            if (usedTransaction != null)
                queryCommand.Transaction = usedTransaction;

            queryCommand.CommandTimeout = 30000;

            try
            {
                queryCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (usedConnection.State != ConnectionState.Open && usedTransaction == null)
                {
                    usedConnection = new System.Data.OleDb.OleDbConnection(usedConnStr);
                    usedConnection.Open();
                    queryCommand = new OleDbCommand(sqlCommandStr, usedConnection);
                    queryCommand.ExecuteNonQuery();
                }
                else
                {
                    usedTransaction = null;
                    usedConnection = new System.Data.OleDb.OleDbConnection(usedConnStr);
                    usedConnection.Open();
                    throw ex;
                }
            }
        }
        public DataTable getData(string sqlCommandStr)
        {
            OleDbCommand queryCommand;
            DataTable myDatatable;
            queryCommand = new OleDbCommand(sqlCommandStr, usedConnection);
            queryCommand.CommandTimeout = 30000;

            if (usedTransaction != null)
                queryCommand.Transaction = usedTransaction;

            myDatatable = new DataTable();

            try
            {
                myDatatable.Load(queryCommand.ExecuteReader());

            }
            catch (Exception ex)
            {
                if (usedConnection.State != ConnectionState.Open && usedTransaction == null)
                {
                    usedConnection = new System.Data.OleDb.OleDbConnection(usedConnStr);
                    usedConnection.Open();
                    queryCommand = new OleDbCommand(sqlCommandStr, usedConnection);
                    myDatatable.Load(queryCommand.ExecuteReader());
                }
                else
                {
                    usedTransaction = null;
                    throw ex;
                }
            }

            return myDatatable;
        }
        public DataTable getSchema()
        {
            return usedConnection.GetSchema();
        }

        public DataTable getSchema(string collectionName)
        {
            return usedConnection.GetSchema(collectionName);
        }
    }
    
    public class Excel_Document: OptimizerDataSource, IDisposable
    {
        protected string fileName;
        protected Microsoft.Office.Interop.Excel.Application xlApp;

        protected  Microsoft.Office.Interop.Excel.Workbook currDoc;
        protected string usedSheetName;
        protected int usedStartRow;
        protected int usedStartColumn;
        protected int usedNumberOfRows;
        protected int usedNumberOfColumns;
        protected string ulCorner;
        protected string lrCorner;
        protected bool isAreaSet;
        protected bool isOpen;
        protected bool isCreateIfNotExist;
        protected bool isHeadLine;

        //Mutex docMut;

        public Excel_Document(string fN)
        {
            fileName = fN;
            isOpen = false;
           // docMut = new Mutex();
        }

        public bool IsHeadLine
        {
            set { isHeadLine = true; }
            get { return isHeadLine; }
        }

        public bool IsCreateIfNotExist
        {
            set { isCreateIfNotExist = value; }
            get {return isCreateIfNotExist;}
        }

        public void OpenDoc()
        {
            //docMut.WaitOne();
            xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlApp.DisplayAlerts = false;
            xlApp.AskToUpdateLinks = false;
            //docMut.ReleaseMutex();
            //docMut.WaitOne();
            if (File.Exists(fileName))
            {
                currDoc = xlApp.Workbooks.Open(fileName, Local: true);
            }
            else
            {
                if (isCreateIfNotExist)
                {
                    currDoc = xlApp.Workbooks.Add();
                    currDoc.SaveAs(fileName);
                }
                else
                    throw new FileNotFoundException();
            }
            
            isOpen = true;
            //docMut.ReleaseMutex();
        }

        public void CloseDoc()
        {
            if (!isOpen && xlApp != null)
                isOpen = isOpen;
                //xlApp.DisplayAlerts = false;
            if (isOpen)
            {
                //docMut.WaitOne();
                currDoc.Close();
                Marshal.ReleaseComObject(currDoc);
                currDoc = null;
                isOpen = false;
                //docMut.ReleaseMutex();
            }
            if (xlApp != null)
            {
               
                //docMut.WaitOne();
                //if (xlApp .
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                //docMut.ReleaseMutex();
            }
            xlApp = null;
        }

        public string SheetName
        {
            set { usedSheetName = value; }
            get { return usedSheetName; }
        }

        public void setSheetData(string sheetName, int startRow, int startColumn, int numberOfRows, int numberOfColumns)
        {
            usedSheetName = sheetName;
            usedStartRow = startRow;
            usedStartColumn = startColumn;
            usedNumberOfRows = numberOfRows;
            usedNumberOfColumns = numberOfColumns;
        }

        public void setSheetData(string sheetName, string ulC, string lrC)
        {
            usedSheetName = sheetName;
            ulCorner = ulC;
            lrCorner = lrC;
            isAreaSet = true;
        }

        public void writeData(string[] lineArr)
        {
        }

        protected const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public void writeData(DataTable source)
        {
            int i, i2, j;
            string lastCellName;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            Microsoft.Office.Interop.Excel.Range currRange;
            string[,] tableContent;

           

            tableContent = new string[source.Rows.Count+1, source.Columns.Count];
            i2 = 0;
            if (isHeadLine)
            {
                j = 0;
                foreach (DataColumn col in source.Columns)
                {
                    tableContent[0, j] = col.ColumnName;
                    j++;
                }
                i2++;
            }
            for (i=0; i < source.Rows.Count; i++)
            {
                for (j = 0; j < source.Columns.Count; j++)
                {
                    tableContent[i+i2, j] = source.Rows[i].ItemArray[j].ToString ();
                }
            }

            if (!isAreaSet)
            {
                ulCorner = "A1";
                foreach (Microsoft.Office.Interop.Excel.Worksheet wS in currDoc.Worksheets)
                {
                    if (wS.Name == source.TableName)
                        wS.Delete();
                }

                xlWorkSheet = currDoc.Worksheets.Add();

                xlWorkSheet.Name = source.TableName;
            }
            else
            {
                xlWorkSheet = currDoc .Worksheets[usedSheetName];
            }

             int startCol1,startCol2,startCol,startRow;
             startCol1 = startCol2 = -1;
             for (i = 0; i < alphabet.Length; i++)
             {
                 if (alphabet[i] == ulCorner[0])
                     startCol1 = i;
                 if (alphabet[i] == ulCorner[1])
                     startCol2 = i;
             }

             if (startCol2 == -1)
             {
                 startCol2 = startCol1;
                 startCol1 = -1;
                 startRow=Convert.ToInt32 (ulCorner .Substring (1))-1;
             }
             else
                 startRow=Convert.ToInt32 (ulCorner .Substring (2))-1;

             startCol1++;

             startCol = startCol1 * alphabet.Length + startCol2;

            if (source.Columns.Count + startCol> alphabet.Length)
                lastCellName = alphabet[(source.Columns.Count - 1 + startCol) / alphabet.Length - 1].ToString();
            else
                lastCellName = "";

            lastCellName = lastCellName + alphabet[(source.Columns.Count - 1 + startCol) % alphabet.Length].ToString() + Convert.ToString(source.Rows.Count + 1 + startRow);

            currRange = xlWorkSheet.Range[ulCorner, lastCellName];//xlWorkSheet .Range [ulCorner ,lastCellName ];//xlWorkSheet.get_Range(ulCorner, lastCellName);
            currRange.Value  = tableContent;
            
            //xlWorkSheet.Range[ulCorner, lastCellName] = tableContent;

            currDoc.Save();
            //currDoc.Worksheets.Add(xlWorkSheet);


        }

       

        public DataTable getData()
        {  
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            Microsoft.Office.Interop.Excel.Range xCells;
            Microsoft.Office.Interop.Excel.Range lastCell;
            DataTable resultTab;
            int maxRow, maxColumn, startRow, startColumn,i,i2,j;
            object[,] vals;
            string lastCellName;
            string [] lineArr;
            object currVal;

           // docMut.WaitOne();
            xlWorkSheet = currDoc.Worksheets[usedSheetName];

            xCells = xlWorkSheet.Cells;
            lastCell = xCells.SpecialCells(Microsoft.Office.Interop.Excel.XlCellType.xlCellTypeLastCell);

            lastCellName = lastCell.AddressLocal[ReferenceStyle: Microsoft.Office.Interop.Excel.XlReferenceStyle.xlA1];//lastCell.AddressLocal[ReferenceStyle:=Microsoft.Office.Interop.Excel.XlReferenceStyle.xlA1];

            vals = xlWorkSheet.Range[ulCorner, lastCellName].Value; //'("A" + Convert.ToString(startRow + 1), lastCell).Value) '//get_Range("A" + Convert.ToString(startRow + 1) + ":CB" + Convert.ToString(maxRow + 1))
            //docMut.ReleaseMutex();
            startRow = vals.GetLowerBound(0);
            startColumn = vals.GetLowerBound(1);

            maxRow = vals.GetUpperBound(0);
            maxColumn = vals.GetUpperBound(1);

            resultTab = new DataTable();
            i2 = 0;
            if (isHeadLine)
            {
                for (j = startColumn; j <= maxColumn; j++)
                {   
                    resultTab.Columns.Add(vals[startRow , j].ToString ());
                }
                i2++;
            }
            else
            {
                for (j = startColumn; j <= maxColumn; j++)
                    resultTab.Columns.Add();
            }
            
            lineArr = new string[maxColumn - startColumn + 1];

            for (i = startRow+i2; i <= maxRow; i++)
            {
                for (j=startColumn;j<=maxColumn ;j++)
                {
                    currVal = vals[i,j];
                    lineArr[j-startColumn] = Convert.ToString(currVal);
                }
                
                resultTab.Rows.Add(lineArr);
            }


            return resultTab;
        }

        public bool IsOpen
        {
            get {
                bool isO;
               // docMut.WaitOne();
                isO=isOpen;
                //docMut.ReleaseMutex();
                return isO;
            }
        }

        public void Dispose()
        {
            if (isOpen)
                CloseDoc();
        }
    }

    public class Optimizer_Excel_Document2007 : OptimizerDataSource,IDisposable
    {
        public enum XLS_Type
        {
            XLS,
            XLSX
        }
       
        protected string [] colTypeStrs= {"varchar(255)", "integer", "float"};

        public enum COL_Type :int 
        {
            STR_COL = 0,
            INT_COL = 1,
            DOUBLE_COL = 2
        }

        public struct ColumnSpec
        {
            public string ColumnName;
            public COL_Type ColumnType;
        }

        
        protected string fileName ;
        protected Microsoft.Office.Interop.Excel.Application xlApp;
        protected Microsoft.Office.Interop.Excel.Workbook currDoc;

        protected OleDataBase xlsDb;
        protected string xlsxTypeString = "Excel 12.0 Xml";
        protected string xlsTypeString = "Excel 8.0";

        protected const string connStrBaseWrite  = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=\"{0}\";Extended Properties='{2};HDR={1}'";
        protected const string connStrBaseRead= "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=\"{0}\";Extended Properties='{2};HDR={1};IMEX=1;MAXSCANROWS=1;READONLY=TRUE' ''MAXSCANROWS=0";

        protected const string readEntireSheetBase = "select * from [{0}]";
        
        protected const string sheetNameWithRangeTemplate= "{0}${1}:{2}";
        protected const string createSheetTemplate = "create table [{0}] ({1})";

        protected string usedSheetName;

        protected bool isAreaSet;
        protected string ulCorner;
        protected string lrCorner;
        protected bool isWrte;
        protected bool isRCSheet;

        protected bool isSQLSet;
        protected string usedSQLStatement;
        protected bool isOpen;

        protected ColumnSpec[] usedColSpecs;

        public Optimizer_Excel_Document2007 (string fN, bool isHeader, bool isWrite, XLS_Type xlst)
        {
            string hdr;
            string xlsType;

            if (xlst == XLS_Type.XLS)
                xlsType = xlsTypeString;
            else if (xlst == XLS_Type.XLSX)
                xlsType = xlsxTypeString;
            else
                xlsType = xlsTypeString;

            if (isHeader)
                hdr = "yes";
            else
                hdr = "no";
           
            isWrte = isWrite;
            
            if (isWrte) 
                xlsDb = new OleDataBase(string.Format(connStrBaseWrite, fN, hdr, xlsType));
            else
                xlsDb = new OleDataBase(string.Format(connStrBaseRead, fN, hdr, xlsType));
            
            isAreaSet = false;
            isOpen = false;
        }

        public void setSheetDataForCreate(string sheetName, ColumnSpec[] colSpecs)
        {
            usedColSpecs = colSpecs;
            usedSheetName = sheetName;
            isAreaSet = false;
            isSQLSet = false;
        }

        public void setSheetData(string sheetName, string ulC, string lrC)
        {
            usedSheetName = sheetName;
            ulCorner = ulC;
            lrCorner = lrC;
            isAreaSet = true;
            isSQLSet = false;
        }

        public void setSheetDataSQL(string sqlStatement)
        {
            usedSQLStatement = sqlStatement;
            isSQLSet = true;
            isAreaSet = false;
        }

        public void setSheetData(string sheetName)
        {
            usedSheetName = sheetName;
            isAreaSet = false;
            isSQLSet = false;
        }

        public bool IsRecreateSheet
        {
            get {return isRCSheet;}
            
            set {isRCSheet = value;}
        }

        public void OpenDoc()
        {
            xlsDb.openConn();
            isOpen = true;
        }

        public void CloseDoc()
        {
            xlsDb.closeConn();
            isOpen = false;
        }

        public string [] getWorkSheets()
        {
            int i;
            int  numberOfTabNames;
            DataTable schema;
            string [] tabNames;
            tabNames = null;
            schema = xlsDb.getSchema("Tables");

            numberOfTabNames=0;
            for (i = 0;i<schema.Rows.Count;i++)
            {
                if (schema.Rows[i].ItemArray[schema.Columns.IndexOf("TABLE_TYPE")].ToString() == "TABLE")
                    numberOfTabNames++;
            }

            tabNames =new string[numberOfTabNames];
            
            for (i = 0;i<schema.Rows.Count;i++)
            {
                if (schema.Rows[i].ItemArray[schema.Columns.IndexOf("TABLE_TYPE")].ToString() == "TABLE")
                    tabNames[i] = schema.Rows[i].ItemArray[schema.Columns.IndexOf("TABLE_NAME")].ToString();
            }

            return tabNames;
        }

        public void writeData(string[] lineArr)
        {   
            xlsDb.writeData(lineArr, usedSheetName);
        }

        public void writeData(DataTable source) 
        {
            int i, j;
            int maxColSpecI;
            DataTable schema;
            string colSpecStr;
            string createCommandStr;
            
            if (isRCSheet)
            {
                schema = xlsDb.getSchema("Tables");

                for (i = 0 ;i<schema.Rows.Count;i++)
                {
                    if (schema.Rows[i].ItemArray[schema.Columns.IndexOf("TABLE_TYPE")].ToString() == "TABLE" && 
                        schema.Rows[i].ItemArray[schema.Columns.IndexOf("TABLE_NAME")].ToString() == usedSheetName)
                        xlsDb.execSqlCommand("drop table " + usedSheetName);
                   
                }

             
                colSpecStr = "";
                maxColSpecI = usedColSpecs.GetUpperBound (0);
                for(i = 0;i<=maxColSpecI;i++)
                {
                    colSpecStr = colSpecStr + usedColSpecs[i].ColumnName + " " + colTypeStrs[(int) usedColSpecs[i].ColumnType];
                    if (i < maxColSpecI)
                        colSpecStr = colSpecStr + ",";
                }

                createCommandStr = String.Format(createSheetTemplate, usedSheetName, colSpecStr);

                xlsDb.execSqlCommand(createCommandStr);

            }

            xlsDb.writeData(source, usedSheetName,false);
        }

        public DataTable getData() 
        {
            string usedSqlCmd;
            string finalSheetStr;

            if (isSQLSet)
                return xlsDb .getData(usedSQLStatement);
            else
            {
                if (isAreaSet)
                    finalSheetStr = string.Format(sheetNameWithRangeTemplate, usedSheetName, ulCorner, lrCorner);
                else
                    finalSheetStr = usedSheetName;
                
                usedSqlCmd = string.Format(readEntireSheetBase, finalSheetStr);
                return xlsDb.getData(usedSqlCmd);
            }
        }

        public bool IsOpen
        {
            get { return isOpen; }
        }

        public void Dispose()
        {
            if (isOpen)
                CloseDoc();
        }
    }
}



