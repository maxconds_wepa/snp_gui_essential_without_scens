﻿using System.Net;
using System.Globalization;
using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using Optimizer_Data;
using Logging;
using System.Data;
using System.Threading;

public interface DistanceSource {
    void addStartDest(string startLocationCountry, string startLocationPostalCode, string destLocationCountry, string destLocationPostalCode);
    void fetchDistances();
    double distance(string startLocationCountry, string startLocationPostalCode, string destLocationCountry, string destLocationPostalCode);
}

public class GoogleDistanceMatrixSource : DistanceSource
{
    protected string googleMapsDistanceMatrixURI = "http://maps.googleapis.com/maps/api/distancematrix";
    protected string outputType = "xml";
    protected string originsBase = "origins=";
    protected string destinationsBase = "destinations=";
    protected string modeBase = "mode=";
    protected string usedMode = "driving";
    protected string sensorBase = "sensor=";
    protected string usedSensor = "false";
    protected string languageBase = "language=";
    protected string usedLanguage = "en-EN";
    protected string unitsBase = "units=";
    protected string usedUnits = "metric";
    protected string paramSeparator = "&";
    protected string itemSeparator = "|";
    protected string currentDistanceFNFileName = "currentDistances.csv";
    protected string distanceFileNameBase = "googleDistances{0}.csv";
    protected string ISO_8601_FORMAT_STRING = "yyyy-MM-ddTHH_mm_ss";
    protected int MAX_NUMBER_ELEMENTS_PER_REQUEST = 100;
    protected int MAX_NUMBER_ELEMENTS_PER_10Seconds = 100;
    protected int NUMBER_OF_SECONDS_FOR_CONSTRAINT = 15;
    protected int POLLING_INTERVAL_MILISECONDS = 10;
    protected string[,] currentDistanceFNCSVSpec ={
                                             {"CurrentDistanceFileName",CSV_File.STRING_TYPE,""}
                                         };
    protected const string CSV_SEP = ";";
    protected string[,] distanceCSVSpec ={
                                            {"StartCountry",CSV_File.STRING_TYPE,""},
                                            {"StartPostalCode",CSV_File.STRING_TYPE,""},
                                            {"StartLocatonName",CSV_File.STRING_TYPE,""},
                                            {"DestCountry",CSV_File.STRING_TYPE,""},
                                            {"DestPostalCode",CSV_File.STRING_TYPE,""},
                                            {"DestLocationName",CSV_File.STRING_TYPE,""},
                                            {"DistanceValue",CSV_File .DOUBLE_TYPE ,""},
                                            {"DistanceText",CSV_File.STRING_TYPE ,""},
                                            {"DurationValue",CSV_File.DOUBLE_TYPE ,""},
                                            {"DurationText",CSV_File.STRING_TYPE ,""},
                                        };

    System.Timers.Timer usedDownloadTimer;

    Logger usedLogger;

    protected bool isTimeUp=true;
    protected void startTimer()
    {
        isTimeUp = false;
        usedDownloadTimer.Start();
    }
    protected void timerHandler(Object source, System.Timers.ElapsedEventArgs e)
    {
        isTimeUp = true;
    }

    protected class DistanceTuple
    {
        public LocationEntry startLocation;
        public LocationEntry destLocation;
        public bool isAssigned = false;
        public double distanceVal;
        public string distanceText;
        public double durationVal;
        public string durationText;
        public string Key
        {
            get { return startLocation.Key + destLocation.Key; }
        }
    }

    protected class LocationEntry
    {
        public string locationCountry="";
        public string locationPostalCode="";
        public string locationName="";
        public string Key
        {
            get {return locationCountry +locationPostalCode;}
        }
    }

    protected class DestinationGroup
    {
        public bool isConsidered;
        public Dictionary<string,LocationEntry> destinations;

        public DestinationGroup()
        {
            destinations = new Dictionary<string, LocationEntry>();
        }

        public bool isEqual(DestinationGroup c)
        {
            if (destinations.Count != c.destinations.Count)
                return false;
            foreach (KeyValuePair <string,LocationEntry >lKV in destinations)
            {
                if (!c.destinations.ContainsKey(lKV.Key))
                    return false;
            }

            return true;
        }
    }

    protected Dictionary<string, LocationEntry> locations;
    protected Dictionary<string, DistanceTuple> dTuples;
    protected bool isBackupOldDistances = true;
    

    public GoogleDistanceMatrixSource()
    {
        dTuples = new Dictionary<string, DistanceTuple>();
        locations = new Dictionary<string, LocationEntry>();
        usedDownloadTimer = new System.Timers.Timer() ;
        usedDownloadTimer.Interval = 1000 * NUMBER_OF_SECONDS_FOR_CONSTRAINT;
        usedDownloadTimer.Elapsed  += timerHandler;
    }

    protected bool isOfflineMode=false;
    protected string usedPath;
    
    public string FilePath
    {
        set { usedPath = value+Path.DirectorySeparatorChar; }
        get { return usedPath; }
    }

    public bool IsOfflineMode
    {
        get { return isOfflineMode; }
        set { isOfflineMode = value; }
    }

    public bool IsBackupOldDistances
    {
        get { return isBackupOldDistances; }
        set { isBackupOldDistances = value; }
    }

    public Logger Distance_Logger
    {
        get { return usedLogger; }
        set { usedLogger = value; }
    }

    public void saveDistances()
    {
        CSV_File distFile, dFNFile;
        string xFN;
        string oldDistanceFileName;
        DataTable cFNTab;
        DataTable distTab;
        DataRow currDRow;
        string newDistanceFileName;

        if (!isOfflineMode)
        {
            xFN = usedPath + currentDistanceFNFileName;

            if (File.Exists(xFN))
            {
                dFNFile = new CSV_File(xFN);
                dFNFile.openForReading();
                dFNFile.CSVDelimiter = CSV_SEP;

                cFNTab = dFNFile.getData();
                dFNFile.closeDoc();
                oldDistanceFileName = (string)cFNTab.Rows[0][0];
            }
            else
                oldDistanceFileName = "";

            dFNFile = new CSV_File(xFN);
            dFNFile.openForWriting(false);
            dFNFile.CSVDelimiter = CSV_SEP;
            dFNFile.setInputColSpecsByStrings(currentDistanceFNCSVSpec);
            dFNFile.setOutputColSpecsByStrings(currentDistanceFNCSVSpec);
            newDistanceFileName = String.Format(distanceFileNameBase, DateTime.Now.ToString(ISO_8601_FORMAT_STRING));
            cFNTab = dFNFile.getEmptyTableFromOutputColSpecs();
            cFNTab.Rows.Add(cFNTab.NewRow());
            cFNTab.Rows[0][0] = newDistanceFileName;
            dFNFile.writeData(cFNTab);
            dFNFile.closeDoc();

            if (!isBackupOldDistances && oldDistanceFileName != "")
            {
                File.Delete(usedPath + oldDistanceFileName);
            }

            xFN = usedPath + newDistanceFileName;

            distFile = new CSV_File(xFN);

            distFile.CSVDelimiter = CSV_SEP;
            distFile.setInputColSpecsByStrings(distanceCSVSpec);
            distFile.setOutputColSpecsByStrings(distanceCSVSpec);

            distTab = distFile.getEmptyTableFromOutputColSpecs();

            foreach (KeyValuePair<string, DistanceTuple> distKV in dTuples)
            {
                currDRow = distTab.NewRow();
                //protected string[,] distanceCSVSpec ={
                //                           {"StartCountry",CSV_File.STRING_TYPE,""},
                //                           {"StartPostalCode",CSV_File.STRING_TYPE,""},
                //                           {"DestCountry",CSV_File.STRING_TYPE,""},
                //                           {"DestPostalCode",CSV_File.STRING_TYPE,""},
                //                           {"DistanceValue",CSV_File .DOUBLE_TYPE ,""},
                //                           {"DistanceText",CSV_File.STRING_TYPE ,""},
                //                           {"DurationValue",CSV_File.DOUBLE_TYPE ,""},
                //                           {"DurationText",CSV_File.STRING_TYPE ,""},
                //     
                currDRow.BeginEdit();

                currDRow[0] = distKV.Value.startLocation.locationCountry;
                currDRow[1] = distKV.Value.startLocation.locationPostalCode;
                currDRow[2] = distKV.Value.startLocation.locationName;
                currDRow[3] = distKV.Value.destLocation.locationCountry;
                currDRow[4] = distKV.Value.destLocation.locationPostalCode;
                currDRow[5] = distKV.Value.destLocation.locationName;
                currDRow[6] = distKV.Value.distanceVal;
                currDRow[7] = distKV.Value.distanceText;
                currDRow[8] = distKV.Value.durationVal;
                currDRow[9] = distKV.Value.durationText;
                currDRow.EndEdit();
                distTab.Rows.Add(currDRow);
            }
            distFile.openForWriting(false);
            distFile.writeData(distTab);
            distFile.closeDoc();
        }
    }

    protected LocationEntry addLocation (string locationCountry,string locationPostalCode,string locationName)
    {
        string currLocId;
        LocationEntry currLoc;
       
        currLocId = locationCountry + locationPostalCode;

        if (!locations.ContainsKey(currLocId))
        {
            currLoc = new LocationEntry();
            currLoc.locationCountry = locationCountry;
            currLoc.locationPostalCode = locationPostalCode;
            currLoc.locationName = locationName;
            locations.Add(currLocId, currLoc);

            return currLoc;
        }
        else
            return locations[currLocId];
    }

    public void loadExistingDistances()
    {
        CSV_File distFile,dFNFile;
        string xFN;
        string currLocId;
        DataTable cFNTab;
        DataTable distTab;
        DistanceTuple currDT;
        LocationEntry currLoc;
        xFN=usedPath+ currentDistanceFNFileName;

        if (File.Exists(xFN))
        {
            dFNFile = new CSV_File(xFN);
            dFNFile.openForReading();
            dFNFile.CSVDelimiter = CSV_SEP;

            cFNTab = dFNFile.getData();
            dFNFile.closeDoc();

            xFN = usedPath + (string) cFNTab.Rows[0][0];

            if (File.Exists(xFN))
            {
                distFile = new CSV_File(xFN);
                distFile.setInputColSpecsByStrings(distanceCSVSpec);
                distFile.CSVDelimiter = CSV_SEP;
                distFile.openForReading();
                distTab = distFile.getData();
                distFile.closeDoc();
                dTuples.Clear();
                 //protected string[,] distanceCSVSpec ={
                 //                           {"StartCountry",CSV_File.STRING_TYPE,""},
                 //                           {"StartPostalCode",CSV_File.STRING_TYPE,""},
                 //                           {"DestCountry",CSV_File.STRING_TYPE,""},
                 //                           {"DestPostalCode",CSV_File.STRING_TYPE,""},
                 //                           {"DistanceValue",CSV_File .DOUBLE_TYPE ,""},
                 //                           {"DistanceText",CSV_File.STRING_TYPE ,""},
                 //                           {"DurationValue",CSV_File.DOUBLE_TYPE ,""},
                 //                           {"DurationText",CSV_File.STRING_TYPE ,""},
                 //                       };
                foreach (DataRow r in distTab.Rows)
                {
                    currDT = new DistanceTuple();
                    currDT.isAssigned = true;
                    currDT.startLocation = addLocation((string)r[0], (string)r[1],(string)r[2]);

                    currDT.destLocation = addLocation((string)r[3], (string)r[4],(string)r[5]);
                    
                    currDT.distanceVal = (double)r[6];
                    currDT.distanceText = (string)r[7];
                    currDT.durationVal = (double)r[8];
                    currDT.durationText = (string)r[9];

                    dTuples.Add(currDT.Key, currDT);
                }
            }
        }
    }

    public void addStartDest(string startLocationCountry, string startLocationPostalCode, string destLocationCountry, string destLocationPostalCode)
    {
        DistanceTuple currDT;

        if (!isOfflineMode)
        {
            if (!(dTuples.ContainsKey(startLocationCountry + startLocationPostalCode + destLocationCountry + destLocationPostalCode)
                || dTuples.ContainsKey(destLocationCountry + destLocationPostalCode + startLocationCountry + startLocationPostalCode)))
            {
                currDT = new DistanceTuple();
                currDT.startLocation = addLocation(startLocationCountry, startLocationPostalCode, "");
                currDT.destLocation = addLocation(destLocationCountry, destLocationPostalCode, "");

                dTuples.Add(currDT.Key, currDT);
            }
        }
    }
    /*
    protected string googleMapsDistanceMatrixURI = "http://maps.googleapis.com/maps/api/distancematrix";
    protected string outputType = "xml";
    protected string originsBase = "origins=";
    protected string destinationsBase = "destinations=";
    protected string modeBase = "mode=";
    protected string usedMode = "driving";
    protected string sensorBase = "sensor=";
    protected string usedSensor = "false";
    protected string languageBase = "language=";
    protected string usedLanguage = "en-EN";
    protected string unitsBase = "units=";
    protected string usedUnits = "metric";
    protected string paramSeparator = "&";
    protected string itemSeparator = "|";
     * */
    protected int numberOfElementsSinceLastTimerStart = 0;
    protected int totalNumberOfElements = 0;
    protected int totalNumberOfSuccessfulElements = 0;
    protected int totalNumberOfRequests = 0;
    protected int totalNumberOfSuccessfulRequests = 0;

    protected string preparePostalCodeForRequest (string country,string pC)
    {
        string fPC;
        string sPC;
        sPC = pC.Replace("-", "");
        switch (country)
        {
            case "PL":

                if (sPC.Length > 2)

                    fPC =  "\"" + sPC.Substring(0, 2) + "-" + sPC.Substring(2) + "\""+"+"+country ;
                else
                    fPC = sPC + "+" + country;
                break;
            default:
                fPC = country + "+" + sPC;
                break;
        }

        return fPC;
    }
    protected List<DistanceTuple> getDistances(List<LocationEntry> sourceLocations, List<LocationEntry> destLocations)
    {
        string requestStr;
        bool isFirst;
        DistanceTuple currDistance;
        LocationEntry[] sLs;
        LocationEntry[] dLs;
        List<DistanceTuple> distanceList;
        int i;
        XmlNodeList rootNode;
        XmlNodeList originAdresses;
        XmlNodeList destinationAdresses;
        XmlNodeList rows;
        int rowI, colI;
        int numberOfElementsOfRequest;
        sLs = new LocationEntry[sourceLocations.Count];
        i = 0;
        foreach (LocationEntry sL in sourceLocations)
        {
            sLs[i] = sL;
            i++;
        }

        dLs = new LocationEntry[destLocations.Count];
        i = 0;
        foreach (LocationEntry dL in destLocations)
        {
            dLs[i] = dL;
            i++;
        }
//        XmlDocument 
        
        requestStr = googleMapsDistanceMatrixURI + "/" + outputType + "?" + originsBase;

        isFirst=true;
        foreach (LocationEntry lE in sourceLocations)
        {
            if (isFirst)
                isFirst = false;
            else
                requestStr += itemSeparator;

            requestStr += preparePostalCodeForRequest(lE.locationCountry, lE.locationPostalCode);//lE.locationCountry + "+" + lE.locationPostalCode;
        }

        requestStr += paramSeparator + destinationsBase;
        isFirst = true;
        foreach (LocationEntry lE in destLocations )
        {
            if (isFirst)
                isFirst = false;
            else
                requestStr += itemSeparator;

            requestStr += preparePostalCodeForRequest(lE.locationCountry, lE.locationPostalCode);//lE.locationCountry + "+" + lE.locationPostalCode;
        }


        requestStr += paramSeparator + modeBase + usedMode + paramSeparator + sensorBase + usedSensor + paramSeparator + languageBase + usedLanguage + paramSeparator + unitsBase + usedUnits;
        numberOfElementsOfRequest=sourceLocations.Count * destLocations.Count;
        //numberOfElementsSinceLastTimerStart += ;

        if (!isTimeUp && numberOfElementsSinceLastTimerStart + numberOfElementsOfRequest > MAX_NUMBER_ELEMENTS_PER_10Seconds)
        {
            if (usedLogger != null)
                usedLogger.outputMessage(LoggerMsgType.INFO_MSG, " Anzahl Distanzen in den letzten " + Convert.ToString(NUMBER_OF_SECONDS_FOR_CONSTRAINT, CultureInfo.InvariantCulture) + " Sekunden würde mit " + Convert.ToString(numberOfElementsSinceLastTimerStart + numberOfElementsOfRequest, CultureInfo.InvariantCulture) + " Distanzen das Limit von " + MAX_NUMBER_ELEMENTS_PER_10Seconds + " Distanzen in " + Convert.ToString(NUMBER_OF_SECONDS_FOR_CONSTRAINT, CultureInfo.InvariantCulture) + " Sekunden übersteigen. Warte das Ende des " + Convert.ToString(NUMBER_OF_SECONDS_FOR_CONSTRAINT, CultureInfo.InvariantCulture) + " Sekunden-Zeitfensters ab.", 0);
            do
            {
                Thread.Sleep(POLLING_INTERVAL_MILISECONDS);
            }
            while (!isTimeUp);
        }
        
        if (usedLogger != null)
            usedLogger.outputMessage(LoggerMsgType.INFO_MSG, "Hole " + Convert.ToString (sourceLocations.Count * destLocations.Count,CultureInfo .InvariantCulture ) + " Distanz(en).", 0);
        
        if (isTimeUp)
        {
            startTimer();
            numberOfElementsSinceLastTimerStart = 0;
        }

        distanceList = new List<DistanceTuple>();
        numberOfElementsSinceLastTimerStart += numberOfElementsOfRequest;
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestStr);
        WebResponse response = request.GetResponse();
        Stream dataStream = response.GetResponseStream();
        StreamReader sreader = new StreamReader(dataStream);
        string responsereader = sreader.ReadToEnd();
        response.Close();

        XmlDocument xmldoc = new XmlDocument();
        xmldoc.LoadXml(responsereader);

        totalNumberOfRequests++;
        rootNode = xmldoc.GetElementsByTagName("DistanceMatrixResponse");


        if (rootNode[0].ChildNodes[0].InnerText.Trim() == "OK")
        {
            totalNumberOfSuccessfulRequests++;
            if (usedLogger != null)
                usedLogger.outputMessage(LoggerMsgType.INFO_MSG, "Distanzen erfolgreich geholt.", 0);
            originAdresses = xmldoc.GetElementsByTagName("origin_address");
            destinationAdresses = xmldoc.GetElementsByTagName("destination_address");

            //int i;

            for (i = 0; i < sLs.Length; i++)
            {
                if (sLs[i].locationName == null || sLs[i].locationName == "")
                    sLs[i].locationName = originAdresses[i].InnerText.Trim();
            }

            for (i = 0; i < dLs.Length; i++)
            {
                if (dLs[i].locationName == null || dLs[i].locationName == "")
                    dLs[i].locationName = destinationAdresses[i].InnerText.Trim();
            }

            rows = xmldoc.GetElementsByTagName("row");
            rowI = 0;
            foreach (XmlNode row in rows)
            {
                colI = 0;
                foreach (XmlNode element in row.ChildNodes)
                {
                    currDistance = new DistanceTuple();
                    currDistance.startLocation = sLs[rowI];
                    currDistance.destLocation = dLs[colI];
                    if (element.ChildNodes[0].InnerText.Trim() == "OK")
                    {
                        currDistance.isAssigned = true;
                        currDistance.durationVal = Convert.ToDouble(element.ChildNodes[1].ChildNodes[0].InnerText, CultureInfo.InvariantCulture);
                        currDistance.durationText = element.ChildNodes[1].ChildNodes[1].InnerText;
                        currDistance.distanceVal = Convert.ToDouble(element.ChildNodes[2].ChildNodes[0].InnerText, CultureInfo.InvariantCulture);
                        currDistance.distanceText = element.ChildNodes[2].ChildNodes[1].InnerText;
                        totalNumberOfSuccessfulElements++;
                    }
                    else
                    {

                      if (usedLogger != null)
                        usedLogger.outputMessage(LoggerMsgType.INFO_MSG, "Fehler beim Holen der Distanz "+sLs[rowI].Key+"-"+dLs[colI].Key+": " + element.ChildNodes[0].InnerText.Trim(), 0);
                        currDistance.isAssigned = false;
                        currDistance.startLocation = sLs[rowI];
                        currDistance.destLocation = dLs[colI];
                        currDistance.durationVal = -1;
                        currDistance.durationText = "";
                        currDistance.distanceVal = -1;
                        currDistance.distanceText = "";
                    }

                    distanceList.Add(currDistance);
                    colI++;
                }
                rowI++;
            }
        }
        else
        {
            if (usedLogger != null)
                usedLogger.outputMessage(LoggerMsgType.INFO_MSG, "Fehler beim Holen der Distanzen:" + rootNode[0].ChildNodes[0].InnerText.Trim(), 0);

       
            for (rowI = 0; rowI < sLs.Length;rowI++)
            {
                for (colI = 0; colI < dLs.Length; colI++)
                {
                    currDistance = new DistanceTuple();
                    currDistance.isAssigned = false;
                     
                    currDistance.startLocation = sLs[rowI];
                    currDistance.destLocation = dLs[colI];
                    currDistance.durationVal = -1;
                    currDistance.durationText = "";
                    currDistance.distanceVal = -1;
                    currDistance.distanceText = "";
                    distanceList.Add(currDistance);
                }
            }
        }
            totalNumberOfElements += sourceLocations.Count * destLocations.Count;
            if (usedLogger != null)
                usedLogger.outputMessage(LoggerMsgType.INFO_MSG, " Anzahl Distanzen in den letzten " + Convert.ToString(NUMBER_OF_SECONDS_FOR_CONSTRAINT, CultureInfo.InvariantCulture) + " Sekunden:" + numberOfElementsSinceLastTimerStart, 0);
        /*
        XmlDocument xmldoc = new XmlDocument();
        xmldoc.LoadXml(responsereader);


        if (xmldoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "OK")
        {
            XmlNodeList distance = xmldoc.GetElementsByTagName("distance");
            return Convert.ToDouble(distance[0].ChildNodes[1].InnerText.Replace(" mi", ""));
        }
        */


        return distanceList;
    }

    public void fetchDistances()
    {
        Dictionary<string,DestinationGroup > startDestCombis;
        DestinationGroup  currDestinations;
        string currLocKey;
        List<LocationEntry> sourceLocations,destLocations;
        List<DistanceTuple> newDistances;

        if (!isOfflineMode)
        {
            totalNumberOfRequests = totalNumberOfSuccessfulRequests = totalNumberOfSuccessfulElements = totalNumberOfElements = 0;
            startDestCombis = new Dictionary<string, DestinationGroup>();

            foreach (KeyValuePair<string, DistanceTuple> currDTKV in dTuples)
            {
                if (!currDTKV.Value.isAssigned
                    )
                {
                    currLocKey = currDTKV.Value.startLocation.Key;
                    if (!startDestCombis.ContainsKey(currLocKey))
                    {
                        currDestinations = new DestinationGroup();
                        startDestCombis.Add(currLocKey, currDestinations);
                    }
                    else
                        currDestinations = startDestCombis[currLocKey];

                    currDestinations.destinations.Add(currDTKV.Value.destLocation.Key, currDTKV.Value.destLocation);
                }
            }

            sourceLocations = new List<LocationEntry>();
            destLocations = new List<LocationEntry>();
            DistanceTuple destTuple;
            foreach (KeyValuePair<string, DestinationGroup> destGKV in startDestCombis)
            {
                if (!destGKV.Value.isConsidered)
                {
                    sourceLocations.Clear();
                    destLocations.Clear();
                    sourceLocations.Add(locations[destGKV.Key]);
                    foreach (KeyValuePair<string, LocationEntry> dLocKV in destGKV.Value.destinations)
                    {
                        destLocations.Add(dLocKV.Value);
                    }
                    foreach (KeyValuePair<string, DestinationGroup> destGKV2 in startDestCombis)
                    {
                        if (destGKV2.Value.isEqual(destGKV.Value) && destGKV.Key != destGKV2.Key)
                        {
                            sourceLocations.Add(locations[destGKV2.Key]);
                        }
                    }

                    newDistances = getDistances(sourceLocations, destLocations);

                    foreach (DistanceTuple nDist in newDistances)
                    {
                        destTuple = dTuples[nDist.Key];
                        destTuple.isAssigned = nDist.isAssigned;
                        destTuple.durationVal = nDist.durationVal;
                        destTuple.durationText = nDist.durationText;
                        destTuple.distanceVal = nDist.distanceVal;
                        destTuple.distanceText = nDist.distanceText;
                    }
                }
            }

            if (usedLogger != null)
            {
                usedLogger.outputMessage(LoggerMsgType.INFO_MSG, Convert.ToString(totalNumberOfElements, CultureInfo.InvariantCulture) + " neue Distanzen geholt. Davon erfolgreich:" + Convert.ToString(totalNumberOfSuccessfulElements, CultureInfo.InvariantCulture), 0);
                usedLogger.outputMessage(LoggerMsgType.INFO_MSG, "Anzahl Abfragen:" + Convert.ToString(totalNumberOfRequests, CultureInfo.InvariantCulture) + " Davon erfolgreich:" + Convert.ToString(totalNumberOfSuccessfulRequests, CultureInfo.InvariantCulture), 0);
            }
            saveDistances();
        }
    }
    public double distance(string startLocationCountry, string startLocationPostalCode, string destLocationCountry, string destLocationPostalCode)
    {
        if (!dTuples.ContainsKey(startLocationCountry+startLocationPostalCode +  destLocationCountry+destLocationPostalCode))
            return -1;

        return dTuples[startLocationCountry+startLocationPostalCode +  destLocationCountry+destLocationPostalCode].distanceVal/1000; //In Km umrechnen, da Google die Distanzen in m angibt.
    }
}