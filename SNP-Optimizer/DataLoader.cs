﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimizer_Data;
using System.IO;
using System.Globalization;
using System.Collections;
using System.Threading;
using System.Data;

using SNP_WEPA;
using Logging;
using Optimizer_Exceptions;
using System.Diagnostics;

namespace SNP_Optimizer
{
    public interface DataLoader
    {
        void getSpecialFixations(EmptyTaskProcEnvironment tPE, ref List<ProcessFilterEntry> fixList, Logger logger,
                                            CultureInfo currC);
        //void getSpecialFixations(EmptyTaskProcEnvironment tPE,ref List<ProcessFilterEntry> fixList,Logger logger,
        //                                 CultureInfo currC);

        //void getSpecialExclusions(EmptyTaskProcEnvironment tPE,ref List<ProcessFilterEntry> exList,Logger logger,
        //                                 CultureInfo currC);
        void getSpecialExclusions(EmptyTaskProcEnvironment tPE, ref List<ProcessFilterEntry> exList, Logger logger,
                                           CultureInfo currC);

        void getLocationsAndLocsMachines(EmptyTaskProcEnvironment tPE,
                                         ref Dictionary<string, WEPA_PlantInfo> wPlantInfos,
                                         ref  Dictionary<string, WEPA_Location> wPlantLocations,
                                        ref Dictionary<string, WEPA_Resource> wResources,
                                         Dictionary<string, ResourceInformationState> resourcesNotConsidered,
                                        Logger logger,
                                           CultureInfo currC
                                        );

        void getInitialStocks(EmptyTaskProcEnvironment tPE,
                              Dictionary<string, WEPA_Location> wPlantLocations,
                              Dictionary<string, Product> preliminaryProducts,
                              ref  Dictionary<string, InitialStockValue> preliminaryInitialStocks,
                              Logger logger,
                              CultureInfo currC
                              );

        void getFirstLastMonth(ref int firstMonth, ref int lastMonth, Logger logger,
                                    CultureInfo currC);

        void getTransportStartSubstitutions(EmptyTaskProcEnvironment tPE, ref Dictionary<string, string> transportStartSubstitions, Logger logger);

        void readTransportsNE(EmptyTaskProcEnvironment tPE, Logger logger);

        void readTransportsSW(EmptyTaskProcEnvironment tPE, Logger logger);

        void readWHSESWEurope(EmptyTaskProcEnvironment tPE, Logger logger);

        void getInterplantTransports(EmptyTaskProcEnvironment tPE, Dictionary<string, WEPA_Location> wPlantLocations, SNP_WEPA_Instance pI, CultureInfo currC, Logger logger);

        void getTransportData(EmptyTaskProcEnvironment tPE,
                              Dictionary<string, HashSet<string>> transportCustomerCustomerChainMatchings,
            ref Dictionary<string, WEPA_Location> wCustomerLocations,
            ref Dictionary<string, HashSet<string>> productsForCustomerLocation,
            ref Dictionary<string, HashSet<string>> toursForCustomer,
            ref Dictionary<string, TourData> tours,
             ref Dictionary<string, ArticleLocationDistribution> locationDistributionPerCustomerProduct,
              ref Dictionary<string, ArticleLocationDistribution> locationDistributionPerCustomer,
            ref Dictionary<string, WHSESWEuropeLocation> warehouseLocsSWEurope,
            ref Dictionary<string, TourData> icTours,
            Dictionary<string, WEPA_Location> wPlantLocations,
            Dictionary<string, WEPA_PlantInfo> wPlantInfos,
            Dictionary<string, string> transportStartSubstitions,
            SNP_WEPA_Instance pI,
            CultureInfo currC,
            Logger logger,
            bool isUseGivenCustomerSalesToCustomerTransportRelations
            );

        void getDemandData(EmptyTaskProcEnvironment tPE,
                  ref Dictionary<string, RawDemand> rawDemands,
                  ref Dictionary<string, Product> preliminaryProducts,
                  ref HashSet<string> historicalProductionProcesses,
                  ref Dictionary<String,HashSet<String>> historicalProductionProcessForCustomerProduct,
                  ref Dictionary<string, HistProductionEntry> preliminaryHistoricalProductions,
            //ref Dictionary<string, HashSet<string>> articlesWithDemandsOfCustomer, 
                  ref HashSet<string> customersWithDemands,
                  SNP_WEPA_Instance pI,
                  CultureInfo currC,
                  int firstMonth,
                  double DEFAULT_KG_PER_PALLET,
                  Logger logger,
                  bool isConsiderCountryInfoOfDemands
              );

        void getTonnageData(EmptyTaskProcEnvironment tPE,
                            ref Dictionary<string, ProductionProcess> tonnageProductionProcesses,
                            Dictionary<string, Resource> preliminaryResources,
            //  HashSet<string> articlesWithTonnages,
                            Logger logger
                          );

        void getBaseDataAndWorkingPlans(EmptyTaskProcEnvironment tPE,
                                        ref Dictionary<string, ProductionProcess> preliminaryProductionProcesses,
                                        ref HashSet<string> productsWithProcesses,
                                        Logger logger,
                                        Dictionary<string, WEPA_Resource> wResources,
                                        Dictionary<string, Resource> preliminaryResources,
                                        Dictionary<string, ProductionProcess> tonnageProductionProcesses,
                                        Dictionary<string, ResourceInformationState> resourcesNotConsidered,
                                        Dictionary<string, ProcessInformationState> processesNotConsidered,
                                        Dictionary<string, Product> preliminaryProducts,
                                        HashSet<string> usedPreliminaryResources,
                                        HashSet<string> historicalProductionProcesses,
            // HashSet<string> articlesWithTonnages,
                                        CultureInfo currC,
                                        bool isUseHistoricalProcessIfNoTonnages,
                                        bool isUseCostPerShift
                                       );

        void getShiftData(EmptyTaskProcEnvironment tPE,
                          ref Dictionary<string, Resource> preliminaryResources,
                          Logger logger,
                          Dictionary<string, WEPA_Location> wPlantLocations,
                          Dictionary<string, WEPA_Resource> wResources,
                          Dictionary<string, ResourceInformationState> resourcesNotConsidered,
                          SNP_WEPA_Instance pI,
                          CultureInfo currC,
                          ref HashSet<string> usedPreliminaryResources
                        );
    }

    public class Excel_Table : OptimizerDataSource, IDisposable
    {
        Excel_Document xlsDoc;
        string usedSheetName;
        public Excel_Table(string fileName, string sheetName)
        {
            xlsDoc = new Excel_Document(fileName);
            xlsDoc.IsHeadLine = true;
            usedSheetName = sheetName;
        }

        public void writeData(DataTable source)
        {
            xlsDoc.OpenDoc();
            xlsDoc.writeData(source);
            xlsDoc.CloseDoc();
        }
        public void writeData(string[] lineArr)
        {
            xlsDoc.writeData(lineArr);
        }
        public DataTable getData()
        {
            DataTable result;
            xlsDoc.OpenDoc();
            xlsDoc.setSheetData(usedSheetName, "A1", "ZZ10000");
            result = xlsDoc.getData();
            xlsDoc.CloseDoc();

            return result;
        }

        public void Dispose()
        {
            xlsDoc.Dispose();
        }

    }

    public class Generic_DataLoader : DataLoader
    {
        public OptimizerDataSource Routings;
        public OptimizerDataSource IntercompanyTransports;
        public OptimizerDataSource HistoricalTransportsNE;
        public OptimizerDataSource HistoricalTransportsSW;
        public OptimizerDataSource PlantLocations;
        public OptimizerDataSource Machines;
        public OptimizerDataSource HistoricalProduction;
        public OptimizerDataSource Demand;
        public OptimizerDataSource ProductInfo;
        public OptimizerDataSource Shift;
        public OptimizerDataSource InitialStock;
        public OptimizerDataSource WarehouseCodeSW;
        public OptimizerDataSource SourceLocationProductionLocation;
        public OptimizerDataSource Root;
        public OptimizerDataSource SpecialFixations;
        public OptimizerDataSource SpecialExclusions;

        protected const int HOURS_PER_SHIFT = 8;
        protected const string COUNTRY_INDICATOR = "@COUNTRY@";
        protected const string POSTAL_CODE_INDICATOR = "@POSTALCODE@";


        public void getSpecialFixations(EmptyTaskProcEnvironment tPE, ref List<ProcessFilterEntry> fixList, Logger logger,
                                          CultureInfo currC)
        {
            DataTable fixationTab;
            ProcessFilterEntry currPPFE;
            String currTypeStr;

            fixList = new List<ProcessFilterEntry>();

            if (SpecialFixations != null)
            {
                tPE.addUsedResource((IDisposable)SpecialFixations);

                fixationTab = SpecialFixations.getData();

                tPE.removeUsedResource((IDisposable)SpecialFixations);

                if (tPE.StopSignal)
                    return;

                foreach (DataRow r in fixationTab.Rows)
                {
                    currPPFE = new ProcessFilterEntry();
                    currTypeStr = r[0].ToString();

                    if (currTypeStr == "Transport")
                    {
                        currPPFE.ProcessType = ProcessFilterEntry.TypeOfProcess.Transport;
                    }
                    else
                    {
                        currPPFE.ProcessType = ProcessFilterEntry.TypeOfProcess.Production;
                    }

                    currPPFE.CustomerName = r[1].ToString().Trim();
                    currPPFE.CountryName = r[2].ToString().Trim();
                    currPPFE.ProductName = r[3].ToString().Trim();
                    currPPFE.ProductionCountryName = r[4].ToString().Trim();
                    currPPFE.PlantLocationName = r[5].ToString().Trim();
                    currPPFE.MachineName = r[6].ToString().Trim();
                    //currPPFE.ValidUntilDate = r[7].ToString().Trim();
                    currPPFE.IsDate = false;
                    fixList.Add(currPPFE);
                }
            }
        }

        public void getSpecialExclusions(EmptyTaskProcEnvironment tPE, ref List<ProcessFilterEntry> exList, Logger logger,
                                          CultureInfo currC)
        {
            DataTable exclusionTab;
            ProcessFilterEntry currPPFE;
            String currTypeStr;

            exList = new List<ProcessFilterEntry>();

            if (SpecialExclusions != null)
            {
                tPE.addUsedResource((IDisposable)SpecialExclusions);

                exclusionTab = SpecialFixations.getData();

                tPE.removeUsedResource((IDisposable)SpecialExclusions);

                if (tPE.StopSignal)
                    return;

                foreach (DataRow r in exclusionTab.Rows)
                {
                    currPPFE = new ProcessFilterEntry();
                    currTypeStr = r[0].ToString();

                    if (currTypeStr == "Transport")
                    {
                        currPPFE.ProcessType = ProcessFilterEntry.TypeOfProcess.Transport;
                    }
                    else
                    {
                        currPPFE.ProcessType = ProcessFilterEntry.TypeOfProcess.Production;
                    }

                    currPPFE.CustomerName = r[1].ToString().Trim();
                    currPPFE.CountryName = r[2].ToString().Trim();
                    currPPFE.ProductName = r[3].ToString().Trim();
                    currPPFE.ProductionCountryName = r[4].ToString().Trim();
                    currPPFE.PlantLocationName = r[5].ToString().Trim();
                    currPPFE.MachineName = r[6].ToString().Trim();
                    currPPFE.IsDate = false;
                    //currPPFE.ValidUntilDate = r[7].ToString().Trim();

                    exList.Add(currPPFE);
                }
            }
        }
        //gets plant location infos and locations of machines
        public void getLocationsAndLocsMachines(EmptyTaskProcEnvironment tPE,
                                         ref Dictionary<string, WEPA_PlantInfo> wPlantInfos,
                                         ref  Dictionary<string, WEPA_Location> wPlantLocations,
                                        ref Dictionary<string, WEPA_Resource> wResources,
                                         Dictionary<string, ResourceInformationState> resourcesNotConsidered,
                                        Logger logger,
                                           CultureInfo currC
                                        )
        {
            DataTable plantLocTab;
            DataTable macTab;
            WEPA_Location currLoc;
            WEPA_PlantInfo currPlantInfo;
            WEPA_Resource currWResource;
            ResourceInformationState currRs;
            string currLocName, currLocId, currMacName;
            int i;

            tPE.addUsedResource((IDisposable)PlantLocations);

            plantLocTab = PlantLocations.getData();

            tPE.removeUsedResource((IDisposable)PlantLocations);

            if (tPE.StopSignal)
                return;

            wPlantInfos = new Dictionary<string, WEPA_PlantInfo>();
            wPlantLocations = new Dictionary<string, WEPA_Location>();

            for (i = 0; i < plantLocTab.Rows.Count; i++)
            {
                currLocName = plantLocTab.Rows[i].ItemArray[0].ToString().Trim();
                if (currLocName != "" && !wPlantLocations.ContainsKey(currLocName)) //Checks whether the location is not void and the location does not exists in the set of plant locations
                //Thus, duplicates are ignored.
                {
                    currLoc = new WEPA_Location();
                    currLoc.LocationCountry = plantLocTab.Rows[i].ItemArray[1].ToString().Trim();
                    currLoc.LocationPLZ = plantLocTab.Rows[i].ItemArray[2].ToString().Trim();

                    currLocId = currLoc.LocationCountry + currLoc.LocationPLZ;
                    if (!wPlantInfos.ContainsKey(currLocId))
                    {
                        currPlantInfo = new WEPA_PlantInfo();
                        currPlantInfo.WarehouseCapacity = Convert.ToInt32(plantLocTab.Rows[i].ItemArray[3].ToString(), currC); ;
                        currPlantInfo.LocationName = currLocName;
                        wPlantInfos.Add(currLocId, currPlantInfo);
                    }

                    wPlantLocations.Add(currLocName, currLoc);
                }
            }

            tPE.addUsedResource((IDisposable)Machines);

            macTab = Machines.getData();

            tPE.removeUsedResource((IDisposable)Machines);
            if (tPE.StopSignal)
                return;

            wResources = new Dictionary<string, WEPA_Resource>();

            for (i = 0; i < macTab.Rows.Count; i++)
            {
                currMacName = macTab.Rows[i].ItemArray[0].ToString().Trim();
                currLocName = macTab.Rows[i].ItemArray[1].ToString().Trim();
                if (currMacName != "")
                {
                    if (wPlantLocations.ContainsKey(currLocName))
                    {
                        if (!wResources.ContainsKey(currMacName))
                        {
                            currWResource = new WEPA_Resource();
                            currWResource.LocationName = currLocName;
                            currWResource.costPerShift = Convert.ToDouble(macTab.Rows[i].ItemArray[2].ToString().Trim(), currC) * HOURS_PER_SHIFT;
                            wResources.Add(currMacName, currWResource);
                        }
                    }
                    else
                    {
                        if (!wResources.ContainsKey(currMacName))
                        {
                            if (!resourcesNotConsidered.ContainsKey(currMacName))
                            {
                                currRs = new ResourceInformationState();
                                currRs.MachineName = currMacName;
                                currRs.HasNoCapacity = currRs.HasNoLocation = false;
                                resourcesNotConsidered.Add(currMacName, currRs);
                            }
                            else
                                currRs = resourcesNotConsidered[currMacName];

                            currRs.HasNoLocation = true;
                        }
                    }
                }
            }
        }

        public void getInitialStocks(EmptyTaskProcEnvironment tPE,
                              Dictionary<string, WEPA_Location> wPlantLocations,
                              Dictionary<string, Product> preliminaryProducts,
                              ref  Dictionary<string, InitialStockValue> preliminaryInitialStocks,
                              Logger logger,
                              CultureInfo currC
                              )
        {

            DataTable iniStocksTab;

            string plantName;
            string productName;
            string key;
            InitialStockValue currIniStVal;
            int i;
            double initStQty;
            string msg;
            WEPA_Location currPlantLoc;
            string currLocName;
            //Stopwatch sw = new Stopwatch();

            //sw.Start();
            preliminaryInitialStocks = new Dictionary<string, InitialStockValue>();

            msg = "Lese Anfangsbestände...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            if (tPE.StopSignal)
                return;

            tPE.addUsedResource((IDisposable)InitialStock);

            if (tPE.StopSignal)
                return;

            iniStocksTab = InitialStock.getData();

            tPE.removeUsedResource((IDisposable)InitialStock);
            if (tPE.StopSignal)
                return;

            for (i = 0; i < iniStocksTab.Rows.Count; i++)
            {
                productName = iniStocksTab.Rows[i].ItemArray[0].ToString().Trim();
                plantName = iniStocksTab.Rows[i].ItemArray[1].ToString().Trim();
                initStQty = Convert.ToDouble(iniStocksTab.Rows[i].ItemArray[2], currC);

                if (
                wPlantLocations.ContainsKey(plantName) &&
                preliminaryProducts.ContainsKey(productName) &&
                initStQty > 0
                )
                {
                    currPlantLoc = wPlantLocations[plantName];
                    currLocName = currPlantLoc.LocationCountry + currPlantLoc.LocationPLZ;

                    key = currLocName + productName;
                    if (!preliminaryInitialStocks.ContainsKey(key))
                    {
                        currIniStVal = new InitialStockValue();

                        currIniStVal.LocationName = currLocName;
                        currIniStVal.ProductName = productName;

                        currIniStVal.Val = initStQty;
                        preliminaryInitialStocks.Add(key, currIniStVal);
                    }

                }
            }

            //sw.Stop();
            msg = "Anfangsbestände fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        public void getFirstLastMonth(ref int firstMonth, ref int lastMonth, Logger logger,
                                    CultureInfo currC)
        {
            DataTable rootTab;
            string msg;

            //   Stopwatch sw = new Stopwatch();

            // sw.Start();
            msg = "Lese Basis-Informationen...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            rootTab = Root.getData();

            firstMonth = Convert.ToInt32(rootTab.Rows[0].ItemArray[1].ToString(), currC);
            lastMonth = Convert.ToInt32(rootTab.Rows[0].ItemArray[2].ToString(), currC);

            msg = "Basis-Informationen fertig.";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        public void getTransportStartSubstitutions(EmptyTaskProcEnvironment tPE, ref Dictionary<string, string> transportStartSubstitions, Logger logger)
        {
            DataTable transpStartSubsitutionsTab;
            string msg;
            int i;

            //Stopwatch sw = new Stopwatch();

            //sw.Start();
            msg = "Lese Transport-Start-Ersetzungen...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            transportStartSubstitions = new Dictionary<string, string>();

            if (tPE.StopSignal)
                return;

            tPE.addUsedResource((IDisposable)SourceLocationProductionLocation);

            if (tPE.StopSignal)
                return;
            transpStartSubsitutionsTab = SourceLocationProductionLocation.getData();

            tPE.removeUsedResource((IDisposable)SourceLocationProductionLocation);
            if (tPE.StopSignal)
                return;

            for (i = 0; i < transpStartSubsitutionsTab.Rows.Count; i++)
            {
                transportStartSubstitions.Add(transpStartSubsitutionsTab.Rows[i].ItemArray[0].ToString().Trim(), transpStartSubsitutionsTab.Rows[i].ItemArray[1].ToString().Trim());
            }

            //sw.Stop();

            msg = "Transport-Start-Ersetzungen fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        protected DataTable histTranspTabNE;
        public void readTransportsNE(EmptyTaskProcEnvironment tPE, Logger logger)
        {
            string msg;

            //Stopwatch sw = new Stopwatch();

            msg = "Lade Transporte Nord-Ost-Europa...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            if (tPE.StopSignal)
                return;

            tPE.addUsedResource((IDisposable)HistoricalTransportsNE);

            if (tPE.StopSignal)
                return;
            histTranspTabNE = HistoricalTransportsNE.getData();

            tPE.removeUsedResource((IDisposable)HistoricalTransportsNE);
            if (tPE.StopSignal)
                return;

            //sw.Stop();

            msg = "Transporte Nord-Ost-Europa fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

        }
        DataTable histTranspTabSW;
        public void readTransportsSW(EmptyTaskProcEnvironment tPE, Logger logger)
        {

            string msg;
            //Stopwatch sw = new Stopwatch();
            //sw.Start();

            msg = "Lade Transporte Süd-West-Europa...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            if (tPE.StopSignal)
                return;

            tPE.addUsedResource((IDisposable)HistoricalTransportsSW);

            if (tPE.StopSignal)
                return;
            histTranspTabSW = HistoricalTransportsSW.getData();
            if (tPE.StopSignal)
                return;

            tPE.removeUsedResource((IDisposable)HistoricalTransportsSW);
            if (tPE.StopSignal)
                return;

            //sw.Stop();

            msg = "Transporte Süd-West-Europa fertig";// +"Zeit:" + sw.Elapsed;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

        }
        protected DataTable whseSWEuropeTab;
        public void readWHSESWEurope(EmptyTaskProcEnvironment tPE, Logger logger)
        {
            string msg;
            //Stopwatch sw = new Stopwatch();
            //sw.Start();

            msg = "Lade Läger Süd-West-Europa...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            if (tPE.StopSignal)
                return;

            tPE.addUsedResource((IDisposable)WarehouseCodeSW);

            if (tPE.StopSignal)
                return;
            whseSWEuropeTab = WarehouseCodeSW.getData();

            tPE.removeUsedResource((IDisposable)WarehouseCodeSW);
            if (tPE.StopSignal)
                return;

            //sw.Stop();
            msg = "Läger Süd-West-Europa fertig";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        public void getInterplantTransports(EmptyTaskProcEnvironment tPE, Dictionary<string, WEPA_Location> wPlantLocations, SNP_WEPA_Instance pI, CultureInfo currC, Logger logger)
        {
            string msg;

            DataTable iTranspTab;
            TransportationProcess currTranspP;
            WEPA_Location currSLoc, currDLoc;
            string currLocationNameS, currLocationNameD;
            string currStartLocation, currDestLocation;
            double currCostPerTruck;
            PlantLocation currPlantLoc;
            int i, j;
            //Stopwatch sw = new Stopwatch();
            //sw.Start();

            msg = "Lese Interplant-Transporte...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            if (tPE.StopSignal)
                return;

            tPE.addUsedResource((IDisposable)IntercompanyTransports);

            if (tPE.StopSignal)
                return;
            iTranspTab = IntercompanyTransports.getData();

            tPE.removeUsedResource((IDisposable)IntercompanyTransports);
            if (tPE.StopSignal)
                return;

            for (i = 0; i < iTranspTab.Rows.Count; i++)
            {
                currLocationNameS = iTranspTab.Rows[i].ItemArray[0].ToString().Trim();
                currLocationNameD = iTranspTab.Rows[i].ItemArray[1].ToString().Trim();
                if (tPE.StopSignal)
                    return;

                if (wPlantLocations.ContainsKey(currLocationNameS) && wPlantLocations.ContainsKey(currLocationNameD))
                {
                    currSLoc = wPlantLocations[currLocationNameS];

                    currDLoc = wPlantLocations[currLocationNameD];

                    currStartLocation = currSLoc.LocationCountry + currSLoc.LocationPLZ;
                    currDestLocation = currDLoc.LocationCountry + currDLoc.LocationPLZ;

                    if (currStartLocation != currDestLocation)
                    {
                        currCostPerTruck = Convert.ToDouble(iTranspTab.Rows[i].ItemArray[2].ToString().Trim(), currC);

                        if (pI.getPlantLocation(currStartLocation) == null)
                        {
                            currPlantLoc = new PlantLocation();
                            currPlantLoc.LocationName = currStartLocation;
                            pI.addPlantLocation(currPlantLoc);
                        }

                        if (pI.getPlantLocation(currDestLocation) == null)
                        {
                            currPlantLoc = new PlantLocation();
                            currPlantLoc.LocationName = currDestLocation;
                            pI.addPlantLocation(currPlantLoc);
                        }

                        currTranspP = new TransportationProcess();
                        currTranspP.StartLocation = currStartLocation;

                        currTranspP.DestLocation = currDestLocation;
                        currTranspP.CostPerTruck = currCostPerTruck;

                        pI.addInterplantTransportationProcess(currTranspP);
                    }
                }
            }
            //sw.Stop();
            msg = "Interplant-Transporte fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        public void getTransportData(EmptyTaskProcEnvironment tPE,
                              Dictionary<string, HashSet<string>> transportCustomerCustomerChainMatchings,
            ref Dictionary<string, WEPA_Location> wCustomerLocations,
            ref Dictionary<string, HashSet<string>> productsForCustomerLocation,
            ref Dictionary<string, HashSet<string>> toursForCustomer,
            ref Dictionary<string, TourData> tours,
             ref Dictionary<string, ArticleLocationDistribution> locationDistributionPerCustomerProduct,
              ref Dictionary<string, ArticleLocationDistribution> locationDistributionPerCustomer,
            ref Dictionary<string, WHSESWEuropeLocation> warehouseLocsSWEurope,
            ref Dictionary<string, TourData> icTours,
            Dictionary<string, WEPA_Location> wPlantLocations,
            Dictionary<string, WEPA_PlantInfo> wPlantInfos,
            Dictionary<string, string> transportStartSubstitions,
            SNP_WEPA_Instance pI,
            CultureInfo currC,
            Logger logger,
            bool isUseGivenCustomerSalesToCustomerTransportRelations
            )
        {
            string msg;
            HashSet<string> customersWithTransports;
            TourData currTour; //ok: Aktuelle Tour
            ArticleLocationDistribution currCustomerProductLocationDistribution, currCustomerLocationDistribution; //ok:Für aktuelle kundenproduktspezifische bzw. kundenspezifische Verteilungen der Nachfrage eines Kunden
            string tourId, prodId;
            string tourKey, sourceLocId, destLocId, customerWithDemandId;
            int i, j;
            string customerProductKey;
            double numberPallets, price;
            string whseId, dateExp, cap, countryDest, whseLocId;//, destCorpId, corpId;
            string currCustomerLocation;
            string destLocCountry, destLocPLZ;
            HashSet<string> currCustomerTours;
            WEPA_Location newCustomerLocation;
            string currLocName;
            WEPA_Location currLocation;
            string chainStr;
            WHSESWEuropeLocation currSWWHSELoc;

            //   Stopwatch sw = new Stopwatch();

            // sw.Start();

            msg = "Transportdaten...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            transportCustomerCustomerChainMatchings = new Dictionary<string, HashSet<string>>();
            wCustomerLocations = new Dictionary<string, WEPA_Location>();
            productsForCustomerLocation = new Dictionary<string, HashSet<string>>();
            toursForCustomer = new Dictionary<string, HashSet<string>>();

            tours = new Dictionary<string, TourData>();

            locationDistributionPerCustomerProduct = new Dictionary<string, ArticleLocationDistribution>();
            locationDistributionPerCustomer = new Dictionary<string, ArticleLocationDistribution>();
            customersWithTransports = new HashSet<string>();

            warehouseLocsSWEurope = new Dictionary<string, WHSESWEuropeLocation>();
            icTours = new Dictionary<string, TourData>();
            WHSESWEuropeLocation currSWLoc;
            for (i = 0; i < whseSWEuropeTab.Rows.Count; i++)
            {
                if (tPE.StopSignal)
                    return;

                currSWLoc = new WHSESWEuropeLocation();
                currSWLoc.LocationCountry = whseSWEuropeTab.Rows[i].ItemArray[1].ToString().Trim();
                currSWLoc.LocationPLZ = whseSWEuropeTab.Rows[i].ItemArray[2].ToString().Trim().Replace("-", "");
                currSWLoc.LocationName = whseSWEuropeTab.Rows[i].ItemArray[3].ToString().Trim();

                warehouseLocsSWEurope.Add(whseSWEuropeTab.Rows[i].ItemArray[0].ToString().Trim(), currSWLoc);
            }

            for (i = 0; i < histTranspTabNE.Rows.Count; i++)
            {
                if (tPE.StopSignal)
                    return;
                prodId = histTranspTabNE.Rows[i].ItemArray[2].ToString().Trim();
                sourceLocId = histTranspTabNE.Rows[i].ItemArray[3].ToString().Trim() + histTranspTabNE.Rows[i].ItemArray[4].ToString().Trim().Replace("-", "");
                if (histTranspTabNE.Rows[i].ItemArray[0].ToString().Trim() == "FG")
                {
                    tourId = histTranspTabNE.Rows[i].ItemArray[1].ToString().Trim();

                    currLocName = histTranspTabNE.Rows[i].ItemArray[5].ToString().Trim();

                    if (transportStartSubstitions.ContainsKey(currLocName))
                    {
                        currLocName = transportStartSubstitions[currLocName];
                        currLocation = wPlantLocations[currLocName];
                        sourceLocId = currLocation.LocationCountry + currLocation.LocationPLZ;
                    }
                    //else
                    //sourceLocId = histTranspTabNE.Rows[i].ItemArray[histTranspTabNE.ColNums[4]].ToString().Trim() + histTranspTabNE.Rows[i].ItemArray[histTranspTabNE.ColNums[5]].ToString().Trim().Replace("-", "");//transpTab.cells[i, sourceCountryColumnNum].Trim() + transpTab.cells[i, sourcePlz1ColumnNum].Trim().Replace("-", "");

                    destLocCountry = histTranspTabNE.Rows[i].ItemArray[6].ToString().Trim();
                    destLocPLZ = histTranspTabNE.Rows[i].ItemArray[7].ToString().Trim().Replace("-", "");
                    destLocId = destLocCountry + destLocPLZ;

                    customerWithDemandId = histTranspTabNE.Rows[i].ItemArray[9].ToString().Trim();

                    //customerWithDemandId = "";
                    //if (histTranspTabNE.ColNums[10] != -1)
                    //{
                    //    if (
                    //        !(chainStr == "" ||
                    //        chainStr.Contains("#N/A") || chainStr.Contains("-2146826246")

                    //        )
                    //    )
                    //    {
                    //        customerWithDemandId = chainStr;
                    //    }

                    //}

                    if (customerWithDemandId != "" && histTranspTabNE.Rows[i].ItemArray[10].ToString().Trim() != "" &&
                        histTranspTabNE.Rows[i].ItemArray[11].ToString().Trim() != "")
                    {
                        if (isUseGivenCustomerSalesToCustomerTransportRelations && !customersWithTransports.Contains(customerWithDemandId))
                            customersWithTransports.Add(customerWithDemandId);

                        tourKey = tourId + sourceLocId + destLocId + customerWithDemandId;

                        numberPallets = Convert.ToDouble(histTranspTabNE.Rows[i].ItemArray[10].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                        if (numberPallets > 0)
                        {
                            price = Convert.ToDouble(histTranspTabNE.Rows[i].ItemArray[11].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                            if (!tours.ContainsKey(tourKey))
                            {
                                currTour = new TourData();
                                currTour.TourId = tourId;
                                currTour.SourceLocId = sourceLocId;
                                currTour.DestLocId = destLocId;
                                currTour.TourNumberPallets = 0;
                                currTour.TourPrice = 0;

                                currTour.Customer = customerWithDemandId;
                                tours.Add(tourKey, currTour);

                                if (!toursForCustomer.ContainsKey(customerWithDemandId))
                                {
                                    currCustomerTours = new HashSet<string>();
                                    toursForCustomer.Add(customerWithDemandId, currCustomerTours);
                                }
                                else
                                    currCustomerTours = toursForCustomer[customerWithDemandId];

                                currCustomerTours.Add(tourKey);

                                if (!wCustomerLocations.ContainsKey(destLocId))
                                {
                                    newCustomerLocation = new WEPA_Location();
                                    newCustomerLocation.LocationCountry = destLocCountry;
                                    newCustomerLocation.LocationPLZ = destLocPLZ;
                                    wCustomerLocations.Add(destLocId, newCustomerLocation);
                                }
                            }
                            else
                                currTour = tours[tourKey];

                            currTour.TourNumberPallets += numberPallets;
                            currTour.TourPrice += price;

                            if (!currTour.ProductsOnTour.Contains(prodId))
                                currTour.ProductsOnTour.Add(prodId);

                            customerProductKey = currTour.Customer + prodId;

                            currCustomerLocation = currTour.Customer + currTour.DestLocId;

                            if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                            {
                                currCustomerProductLocationDistribution = new ArticleLocationDistribution();
                                locationDistributionPerCustomerProduct.Add(customerProductKey, currCustomerProductLocationDistribution);
                            }
                            else
                                currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                            if (!currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation.ContainsKey(currTour.DestLocId))
                                currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation.Add(currTour.DestLocId, numberPallets);
                            else
                                currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] =
                                    currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] + numberPallets;

                            if (!locationDistributionPerCustomer.ContainsKey(currTour.Customer))
                            {
                                currCustomerLocationDistribution = new ArticleLocationDistribution();
                                locationDistributionPerCustomer.Add(currTour.Customer, currCustomerLocationDistribution);
                            }
                            else
                                currCustomerLocationDistribution = locationDistributionPerCustomer[currTour.Customer];

                            if (!currCustomerLocationDistribution.NumberPalletsTransportedPerLocation.ContainsKey(currTour.DestLocId))
                                currCustomerLocationDistribution.NumberPalletsTransportedPerLocation.Add(currTour.DestLocId, numberPallets);
                            else
                                currCustomerLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] =
                                    currCustomerLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] + numberPallets;
                        }
                    }
                }
                else if (histTranspTabNE.Rows[i].ItemArray[0].ToString().Trim() == "FG IC")
                {
                    tourId = histTranspTabNE.Rows[i].ItemArray[1].ToString().Trim();//transpTab.cells[i, tourColumnNum].Trim();
                    currLocName = histTranspTabNE.Rows[i].ItemArray[5].ToString().Trim();

                    if (transportStartSubstitions.ContainsKey(currLocName))
                    {
                        currLocName = transportStartSubstitions[currLocName];
                        currLocation = wPlantLocations[currLocName];
                        sourceLocId = currLocation.LocationCountry + currLocation.LocationPLZ;
                    }
                    else
                        sourceLocId = histTranspTabNE.Rows[i].ItemArray[3].ToString().Trim() + histTranspTabNE.Rows[i].ItemArray[4].ToString().Trim().Replace("-", "");//transpTab.cells[i, sourceCountryColumnNum].Trim() + transpTab.cells[i, sourcePlz1ColumnNum].Trim().Replace("-", "");

                    currLocName = histTranspTabNE.Rows[i].ItemArray[8].ToString().Trim();
                    if (transportStartSubstitions.ContainsKey(currLocName))
                    {
                        currLocName = transportStartSubstitions[currLocName];
                        currLocation = wPlantLocations[currLocName];
                        destLocId = currLocation.LocationCountry + currLocation.LocationPLZ;
                    }
                    else
                        destLocId = histTranspTabNE.Rows[i].ItemArray[6].ToString().Trim() + histTranspTabNE.Rows[i].ItemArray[7].ToString().Trim().Replace("-", "");//destLocId = destLocCountry + destLocPLZ;//histTranspTabNE.rawTable.cells[i, histTranspTabNE.colNums[4]].Trim() + histTranspTabNE.rawTable.cells[i, histTranspTabNE.colNums[5]].Trim().Replace("-", "");

                    if (sourceLocId != destLocId && pI.getInterplantTransportationProcess(sourceLocId + destLocId) == null && wPlantInfos.ContainsKey(sourceLocId) && wPlantInfos.ContainsKey(destLocId))
                    {
                        if (histTranspTabNE.Rows[i].ItemArray[10].ToString().Trim() != "" &&
                            histTranspTabNE.Rows[i].ItemArray[11].ToString().Trim() != "")//if (customerWithDemandId != "" && transpTab.cells[i, numberPalletsColumnNum].Trim() != "" && transpTab.cells[i, priceColumnNum].Trim() != "")
                        {
                            tourKey = tourId + sourceLocId + destLocId;

                            numberPallets = Convert.ToDouble(histTranspTabNE.Rows[i].ItemArray[10].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                            if (numberPallets > 0)
                            {
                                if (histTranspTabNE.Rows[i].ItemArray[11].ToString().Trim() == "")
                                    price = 0;
                                else
                                    price = Convert.ToDouble(histTranspTabNE.Rows[i].ItemArray[11].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                if (!icTours.ContainsKey(tourKey))
                                {
                                    currTour = new TourData();
                                    currTour.TourId = tourId;
                                    currTour.SourceLocId = sourceLocId;
                                    currTour.DestLocId = destLocId;
                                    currTour.TourNumberPallets = 0;
                                    currTour.TourPrice = 0;

                                    currTour.Customer = "";
                                    icTours.Add(tourKey, currTour);
                                }
                                else
                                    currTour = icTours[tourKey];

                                currTour.TourNumberPallets += numberPallets;
                                currTour.TourPrice += price;

                                if (!currTour.ProductsOnTour.Contains(prodId))
                                    currTour.ProductsOnTour.Add(prodId);
                            }
                        }
                    }
                }
            }

            for (i = 0; i < histTranspTabSW.Rows.Count; i++)
            {
                if (tPE.StopSignal)
                    return;

                prodId = histTranspTabSW.Rows[i].ItemArray[5].ToString().Trim();
                whseId = histTranspTabSW.Rows[i].ItemArray[0].ToString().Trim();
                dateExp = histTranspTabSW.Rows[i].ItemArray[1].ToString().Trim();
                cap = histTranspTabSW.Rows[i].ItemArray[3].ToString().Trim();
                countryDest = histTranspTabSW.Rows[i].ItemArray[2].ToString().Trim();

                currSWWHSELoc = warehouseLocsSWEurope[whseId];

                if (transportStartSubstitions.ContainsKey(currSWWHSELoc.LocationName))
                {
                    whseLocId = transportStartSubstitions[currSWWHSELoc.LocationName];
                    currLocation = wPlantLocations[whseLocId];
                    sourceLocId = currLocation.LocationCountry + currLocation.LocationPLZ;
                }
                else
                    sourceLocId = currSWWHSELoc.LocationCountry + currSWWHSELoc.LocationPLZ;

                tourId = whseId + dateExp + countryDest;

                destLocId = countryDest + cap;

                customerWithDemandId = "";

                customerWithDemandId = histTranspTabSW.Rows[i].ItemArray[4].ToString().Trim();

                if (customerWithDemandId != "" && histTranspTabSW.Rows[i].ItemArray[6].ToString().Trim() != "")
                {
                    if (isUseGivenCustomerSalesToCustomerTransportRelations && !customersWithTransports.Contains(customerWithDemandId))
                        customersWithTransports.Add(customerWithDemandId);

                    tourKey = tourId + sourceLocId + destLocId + customerWithDemandId;
                    numberPallets = Convert.ToDouble(histTranspTabSW.Rows[i].ItemArray[6].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                    if (numberPallets > 0)
                    {
                        price = Convert.ToDouble(histTranspTabSW.Rows[i].ItemArray[7].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                        if (!tours.ContainsKey(tourKey))
                        {
                            currTour = new TourData();
                            currTour.TourId = tourId;
                            currTour.SourceLocId = sourceLocId;
                            currTour.DestLocId = destLocId;
                            currTour.TourNumberPallets = 0;
                            currTour.TourPrice = 0;

                            currTour.Customer = customerWithDemandId;
                            tours.Add(tourKey, currTour);

                            if (!toursForCustomer.ContainsKey(customerWithDemandId))
                            {
                                currCustomerTours = new HashSet<string>();
                                toursForCustomer.Add(customerWithDemandId, currCustomerTours);
                            }
                            else
                                currCustomerTours = toursForCustomer[customerWithDemandId];

                            currCustomerTours.Add(tourKey);
                            if (!wCustomerLocations.ContainsKey(destLocId))
                            {
                                newCustomerLocation = new WEPA_Location();

                                newCustomerLocation.LocationCountry = countryDest;
                                newCustomerLocation.LocationPLZ = cap;
                                wCustomerLocations.Add(destLocId, newCustomerLocation);
                            }
                        }
                        else
                            currTour = tours[tourKey];

                        currTour.TourNumberPallets += numberPallets;
                        currTour.TourPrice += price;

                        if (!currTour.ProductsOnTour.Contains(prodId))
                            currTour.ProductsOnTour.Add(prodId);

                        customerProductKey = currTour.Customer + prodId;

                        if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                        {
                            currCustomerProductLocationDistribution = new ArticleLocationDistribution();
                            locationDistributionPerCustomerProduct.Add(customerProductKey, currCustomerProductLocationDistribution);
                        }
                        else
                            currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                        if (!currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation.ContainsKey(currTour.DestLocId))
                            currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation.Add(currTour.DestLocId, numberPallets);
                        else
                            currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] =
                                currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] + numberPallets;

                        if (!locationDistributionPerCustomer.ContainsKey(currTour.Customer))
                        {
                            currCustomerLocationDistribution = new ArticleLocationDistribution();
                            locationDistributionPerCustomer.Add(currTour.Customer, currCustomerLocationDistribution);
                        }
                        else
                            currCustomerLocationDistribution = locationDistributionPerCustomer[currTour.Customer];

                        if (!currCustomerLocationDistribution.NumberPalletsTransportedPerLocation.ContainsKey(currTour.DestLocId))
                            currCustomerLocationDistribution.NumberPalletsTransportedPerLocation.Add(currTour.DestLocId, numberPallets);
                        else
                            currCustomerLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] =
                                currCustomerLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] + numberPallets;
                    }
                }
            }
            //sw.Stop();

            msg = "Transportdaten fertig.";// +"Zeit:" + sw.Elapsed;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        public void getDemandData(EmptyTaskProcEnvironment tPE,
                  ref Dictionary<string, RawDemand> rawDemands,
                  ref Dictionary<string, Product> preliminaryProducts,
                  ref HashSet<string> historicalProductionProcesses,
                  ref Dictionary<String, HashSet<String>> historicalProductionProcessForCustomerProduct,
                  ref Dictionary<string, HistProductionEntry> preliminaryHistoricalProductions,
            //ref Dictionary<string, HashSet<string>> articlesWithDemandsOfCustomer,
                  ref HashSet<string> customersWithDemands,
                  SNP_WEPA_Instance pI,
                  CultureInfo currC,
                  int firstMonth,
                  double DEFAULT_KG_PER_PALLET,
                  Logger logger,
                  bool isConsiderCountryInfoOfDemands
              )
        {
            // Optimizer_Data.Excel_Document demDoc;
            // string completeFN;
            DataTable tab;
            int i, j;
            int currProductIndex;
            int numberPeriods;
            string currCellStr;
            string currArtName;
            string currHistoricalResource;
            int endOfFirstWord;
            Product currProduct;
            Period currPeriod;
            RawDemand currDemand;
            HistProductionEntry currHPE;
            string msg;
            string currPeriodName;
            int startPeriod;
            int monthNum;
            double currDemVal;
            HashSet<string> currProductsOfCustomers;

            string currDemandKey, currCustomerName, currHistProdKey, currCountryKey;


            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            // completeFN = demandFileName;

            // if (File.Exists(completeFN))
            //{
            msg = "Lese Bedarfe...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            if (tPE.StopSignal)
                return;
            rawDemands = new Dictionary<string, RawDemand>();
            customersWithDemands = new HashSet<string>();
            preliminaryProducts = new Dictionary<string, Product>();

            historicalProductionProcesses = new HashSet<string>();
            preliminaryHistoricalProductions = new Dictionary<string, HistProductionEntry>();
            //articlesWithDemandsOfCustomer = new Dictionary<string, HashSet<string>>();
            currProductIndex = 0;
            //demDoc = null;

            //demDoc = new Optimizer_Data.Excel_Document(comp   leteFN);
            tPE.addUsedResource((IDisposable)Demand);
            //demDoc.OpenDoc();
            if (tPE.StopSignal)
                return;
            tab = Demand.getData();//loadTableFromExcelDocument(demDoc, demandSheetName, demandColNames, demandColMatchTypes);
            //demDoc.CloseDoc();
            //tPE.removeUsedResource(demDoc);
            if (tPE.StopSignal)
                return;
            //checkColumns(tab, demandColNames, demandFileName, demandSheetName);
            //if (tPE.StopSignal)
            //    return;
            numberPeriods = 0;

            //startPeriod = Convert.ToInt32(tab.RawTable.Rows[tab.FirstRow].ItemArray[tab.ColNums[4]].ToString().Substring(1));
            //monthNum = firstMonth;
            //for (i = tab.ColNums[4]; i < tab.RawTable.Columns.Count; i++)
            //{
            //    if (tPE.StopSignal)
            //        return;
            //    currPeriodName = "M" + Convert.ToString(i - tab.ColNums[4] + startPeriod);
            //    if (tab.RawTable.Rows[tab.FirstRow].ItemArray[i].ToString().Trim() != currPeriodName)
            //        break;

            //    currPeriod = new Period();
            //    currPeriod.PeriodName = MONTHS[monthNum];
            //    pI.addPeriod(ref currPeriod);
            //    numberPeriods++;
            //    monthNum++;
            //    if (monthNum == MONTHS.Length)
            //        monthNum = 0;
            //}

            for (i = 0; i < tab.Rows.Count; i++)//(i = tab.FirstRow + 1; i < tab.RawTable.Rows.Count; i++)
            {
                if (tPE.StopSignal)
                    return;

                currArtName = tab.Rows[i].ItemArray[0].ToString().Trim();
                currPeriodName = tab.Rows[i].ItemArray[2].ToString().Trim();
                currCellStr = tab.Rows[i].ItemArray[4].ToString().Trim();

                endOfFirstWord = currCellStr.IndexOf(" ");
                if (endOfFirstWord != -1)
                    currHistoricalResource = currCellStr.Substring(0, endOfFirstWord);
                else
                    currHistoricalResource = currCellStr;

                if (currHistoricalResource != "HW")
                {
                    if (pI.getPeriod(currPeriodName) == null)//(!relevantPeriods.Contains(currPeriodName))
                    {
                        currPeriod = new Period();
                        currPeriod.PeriodName = currPeriodName;
                        pI.addPeriod(ref currPeriod);
                    }

                    if (!preliminaryProducts.ContainsKey(currArtName))
                    {
                        currProduct = new Product();
                        currProduct.ProductName = currArtName;
                        currProduct.PalletsPerTon = 1000 / DEFAULT_KG_PER_PALLET;
                        currProduct.ProductDescription = "";//tab.RawTable.Rows[i].ItemArray[tab.ColNums[5]].ToString().Trim();
                        preliminaryProducts.Add(currArtName, currProduct);

                        currProductIndex++;
                    }
                    else
                        currProduct = preliminaryProducts[currArtName];

                    currCellStr = tab.Rows[i].ItemArray[5].ToString().Trim();
                    if (currCellStr != "")
                    {
                        currDemVal = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                        if (currDemVal > 0)
                        {
                            currCustomerName = tab.Rows[i].ItemArray[1].ToString().Trim();
                            currCountryKey = tab.Rows[i].ItemArray[3].ToString().Trim();

                            currDemandKey = currProduct.ProductName + currCustomerName + currPeriodName;
                            if (isConsiderCountryInfoOfDemands)
                                currDemandKey = currDemandKey + currCountryKey;

                            if (!customersWithDemands.Contains(currCustomerName))
                                customersWithDemands.Add(currCustomerName);

                            //if (!articlesWithDemandsOfCustomer.ContainsKey(currCustomerName))
                            //{
                            //    currProductsOfCustomers = new HashSet<string>();
                            //    articlesWithDemandsOfCustomer.Add(currCustomerName, currProductsOfCustomers);
                            //}
                            //else
                            //    currProductsOfCustomers = articlesWithDemandsOfCustomer[currCustomerName];

                            //if (!currProductsOfCustomers.Contains(currProduct.ProductName))
                            //  currProductsOfCustomers.Add(currProduct.ProductName);

                            if (!rawDemands.ContainsKey(currDemandKey))
                            {
                                currDemand = new RawDemand();
                                currDemand.ProductName = currProduct.ProductName;
                                currDemand.PeriodName = currPeriodName;
                                currDemand.CustomerName = currCustomerName;
                                currDemand.Val = 0;
                                if (isConsiderCountryInfoOfDemands)
                                    currDemand.CountryName = currCountryKey;
                                rawDemands.Add(currDemandKey, currDemand);
                            }
                            else
                                currDemand = rawDemands[currDemandKey];

                            currDemand.Val += currDemVal;

                            currHistProdKey = currProduct.ProductName + currHistoricalResource + currPeriodName;
                            if (!preliminaryHistoricalProductions.ContainsKey(currHistProdKey))
                            {
                                currHPE = new HistProductionEntry();
                                currHPE.PeriodName = currPeriodName;
                                currHPE.ProcessName = currProduct.ProductName + currHistoricalResource;
                                currHPE.Quantity = 0;

                                if (!historicalProductionProcesses.Contains(currHPE.ProcessName))
                                    historicalProductionProcesses.Add(currHPE.ProcessName);

                                preliminaryHistoricalProductions.Add(currHistProdKey, currHPE);
                            }
                            else
                                currHPE = preliminaryHistoricalProductions[currHistProdKey];

                            currHPE.Quantity += currDemVal;
                        }
                    }
                }
            }
            //  sw.Stop();

            msg = "Bedarfe fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            //}
            //else
            //    throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, demandFileName);
        }

        public void getTonnageData(EmptyTaskProcEnvironment tPE,
                            ref Dictionary<string, ProductionProcess> tonnageProductionProcesses,
                            Dictionary<string, Resource> preliminaryResources,
            //  HashSet<string> articlesWithTonnages,
                            Logger logger
                          )
        {
            // Optimizer_Data.Excel_Document tnDoc;
            //string completeFN;
            DataTable tab;
            string currResourceName, lastResourceName;
            string currAutomatName, currArtName;
            ProductionProcess currProductionProcess;
            int i, j;
            int firstConversionPosition, numberOfConversionPositions;
            string currCellStr;
            object tempObj;
            string msg;
            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            //completeFN = tonnageFileName;

            //if (File.Exists(completeFN))
            //{
            msg = "Lese Historische Tonnagen...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            if (tPE.StopSignal)
                return;
            tonnageProductionProcesses = new Dictionary<string, ProductionProcess>();


            tPE.addUsedResource((IDisposable)HistoricalProduction);

            if (tPE.StopSignal)
                return;
            tab = HistoricalProduction.getData();//loadTableFromExcelDocument(tnDoc, tonnageSheetName, tonnageColNames, tonnageColMatchTypes);
            //tnDoc.CloseDoc();
            tPE.removeUsedResource((IDisposable)HistoricalProduction);
            if (tPE.StopSignal)
                return;
            // checkColumns(tab, tonnageColNames, tonnageFileName, tonnageSheetName);
            //if (tPE.StopSignal)
            //  return;
            for (i = 0; i < tab.Rows.Count; i++)
            {
                currAutomatName = tab.Rows[i].ItemArray[0].ToString().Trim();
                currArtName = tab.Rows[i].ItemArray[1].ToString().Trim();

                //if (!articlesWithTonnages.Contains(currArtName))
                //    articlesWithTonnages.Add(currArtName);

                currResourceName = currAutomatName;
                tempObj = currResourceName;

                if (preliminaryResources.ContainsKey(currResourceName))
                {
                    currProductionProcess = new ProductionProcess();

                    currProductionProcess.ProductName = currArtName;
                    currProductionProcess.ResourceName = currAutomatName;

                    tonnageProductionProcesses.Add((string)currProductionProcess.Key, currProductionProcess);
                }
            }

            //  sw.Stop();

            msg = "Tonnagen fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            //}
            //else
            //    throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, tonnageFileName);
        }

        public void getBaseDataAndWorkingPlans(EmptyTaskProcEnvironment tPE,
                                        ref Dictionary<string, ProductionProcess> preliminaryProductionProcesses,
                                        ref HashSet<string> productsWithProcesses,
                                        Logger logger,
                                        Dictionary<string, WEPA_Resource> wResources,
                                        Dictionary<string, Resource> preliminaryResources,
                                        Dictionary<string, ProductionProcess> tonnageProductionProcesses,
                                        Dictionary<string, ResourceInformationState> resourcesNotConsidered,
                                        Dictionary<string, ProcessInformationState> processesNotConsidered,
                                        Dictionary<string, Product> preliminaryProducts,
                                        HashSet<string> usedPreliminaryResources,
                                        HashSet<string> historicalProductionProcesses,
            // HashSet<string> articlesWithTonnages,
                                        CultureInfo currC,
                                        bool isUseHistoricalProcessIfNoTonnages,
                                        bool isUseCostPerShift
                                       )
        {
            DataTable tab;
            int i, j;
            string currTitleCellStr, currCellStr;
            string currProcessName;
            string currAutomatName, currArtName;
            double kgPerPallet;
            double currTonPerShift;
            object tempObj;
            ProductionProcess currProcess;
            Product currProduct;
            Resource currResource;
            string msg;
            string currPlantName;
            int endOfAutomatName;
            bool isResource, isTonnageNecessary, isTonnageAvailable;
            ProcessInformationState currPCIS;
            //Stopwatch sw = new Stopwatch();
            //sw.Start();

            msg = "Lese Arbeitspläne...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            preliminaryProductionProcesses = new Dictionary<string, ProductionProcess>();

            productsWithProcesses = new HashSet<string>();
            if (tPE.StopSignal)
                return;


            tPE.addUsedResource((IDisposable)ProductInfo);

            //bDWPDoc.OpenDoc();
            if (tPE.StopSignal)
                return;
            tab = ProductInfo.getData();//loadTableFromExcelDocument(bDWPDoc, workingPlansSheetName, workingPlansColNames, workingPlansColMatchTypes);
            //bDWPDoc.CloseDoc();
            tPE.removeUsedResource((IDisposable)ProductInfo);
            if (tPE.StopSignal)
                return;
            //checkColumns(tab, workingPlansColNames, baseDataAndWorkingPlansFileName, workingPlansSheetName);
            for (i = 0; i < tab.Rows.Count; i++)//(i = tab.FirstRow + 1; i < tab.RawTable.Rows.Count; i++)
            {
                if (tPE.StopSignal)
                    return;
                currArtName = tab.Rows[i].ItemArray[0].ToString().Trim();

                if (preliminaryProducts.ContainsKey(currArtName))
                {
                    currProduct = preliminaryProducts[currArtName];
                    currProduct.ProductDescription = tab.Rows[i].ItemArray[1].ToString().Trim();
                    currCellStr = tab.Rows[i].ItemArray[2].ToString().Trim();

                    if (currCellStr != "")
                        kgPerPallet = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                    else
                        kgPerPallet = 0;


                    if (kgPerPallet > 0)
                        currProduct.PalletsPerTon = 1000 / kgPerPallet;
                }
            }


            tPE.addUsedResource((IDisposable)ProductInfo);
            tab = Routings.getData();
            tPE.removeUsedResource((IDisposable)ProductInfo);
            if (tPE.StopSignal)
                return;



            for (i = 0; i < tab.Rows.Count; i++)//(i = tab.FirstRow + 1; i < tab.RawTable.Rows.Count; i++)
            {
                if (tPE.StopSignal)
                    return;
                currArtName = tab.Rows[i].ItemArray[0].ToString().Trim();
                if (preliminaryProducts.ContainsKey(currArtName))
                {
                    currAutomatName = tab.Rows[i].ItemArray[1].ToString().Trim();
                    currProcessName = currArtName + currAutomatName;

                    currProcessName = currArtName + currAutomatName;

                    isResource = isTonnageNecessary = isTonnageAvailable = false;

                    if (preliminaryResources.ContainsKey(currAutomatName))
                    {
                        isResource = true;
                        currResource = preliminaryResources[currAutomatName];
                        currProcessName = currArtName + currAutomatName;

                        //if (articlesWithTonnages.Contains(currArtName))
                        //    isTonnageNecessary = true;

                        if (tonnageProductionProcesses.ContainsKey(currProcessName))
                        {
                            currProcess = tonnageProductionProcesses[currProcessName];
                            isTonnageAvailable = true;
                        }
                        else
                            currProcess = null;

                        currCellStr = tab.Rows[i].ItemArray[2].ToString().Trim();

                        if (currCellStr != "")
                            currTonPerShift = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                        else
                            currTonPerShift = 0;

                        if (currTonPerShift > 0)
                        {
                            if (currProcess == null
                                   && (isUseHistoricalProcessIfNoTonnages && historicalProductionProcesses.Contains(currArtName + currAutomatName))
                                )
                            {
                                currProcess = new ProductionProcess();
                                currProcess.ResourceName = currAutomatName;
                                currProcess.ProductName = currArtName;
                            }

                            if (currProcess != null) //Process is only added to the final set of processes if 1) in case of an article with past production the process already has been applied 2) the article does not have past production
                            {
                                if (!usedPreliminaryResources.Contains(currAutomatName))
                                    usedPreliminaryResources.Add(currAutomatName);
                                currProcess.TonPerShift = currTonPerShift;
                                WEPA_Resource currWResource;
                                //currPlantName = wPlantLocsForMachines[currAutomatName];
                                currWResource = wResources[currAutomatName];
                                //if (isUseCostPerShift)
                                //{
                                //if (wCostPerShiftForMachines.ContainsKey(currAutomatName))
                                //    currProcess.CostPerShift = wCostPerShiftForMachines[currAutomatName];
                                //else
                                //    currProcess.CostPerShift = 0;
                                currProcess.CostPerShift = currWResource.costPerShift;
                                //}
                                //else
                                //{
                                //    if (wCostPerShiftForMachines.ContainsKey(currAutomatName))
                                //        currProcess.CostPerShift = wCostPerShiftForMachines[currAutomatName];
                                //    else
                                //        currProcess.CostPerShift = 0;
                                //}

                                if (!preliminaryProductionProcesses.ContainsKey(currProcess.Key))
                                {
                                    preliminaryProductionProcesses.Add(currProcess.Key, currProcess);
                                    if (!productsWithProcesses.Contains(currArtName))
                                        productsWithProcesses.Add(currArtName);
                                }
                            }
                            else
                            {

                            }
                        }
                    }

                    if (!isResource || (!isTonnageAvailable && isTonnageNecessary))
                    {
                        if (!processesNotConsidered.ContainsKey(currProcessName))
                        {
                            currPCIS = new ProcessInformationState();
                            currPCIS.IsHistoricalProductionNeeded = isTonnageNecessary;
                            currPCIS.IsNotExistHistoricalProduction = !isTonnageAvailable;
                            currPCIS.HasNoResourceWithCapacity = !isResource;
                            currPCIS.ProductName = currArtName;
                            currPCIS.ResourceName = currAutomatName;
                            processesNotConsidered.Add(currProcessName, currPCIS);

                            currPCIS = processesNotConsidered[currProcessName];
                        }

                    }
                }
            }

            //sw.Stop();

            msg = "Arbeitspläne fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        public void getShiftData(EmptyTaskProcEnvironment tPE,
                          ref Dictionary<string, Resource> preliminaryResources,
                          Logger logger,
                          Dictionary<string, WEPA_Location> wPlantLocations,
                          Dictionary<string, WEPA_Resource> wResources,
                          Dictionary<string, ResourceInformationState> resourcesNotConsidered,
                          SNP_WEPA_Instance pI,
                          CultureInfo currC,
                          ref HashSet<string> usedPreliminaryResources
                        )
        {

            string completeFN;
            string currAutomatName;
            Resource currResource;
            DataTable tab;
            CapacityValue currCapVal;
            ResourceInformationState currRS;
            int i, j, spaceCharPos;
            string currTitleCellStr, currCellStr, periodName;
            double totalCapVal;
            object tempObj;
            string msg;
            Dictionary<string, double> tCapV;
            IndexedItem<Period> currPI;
            //Stopwatch sw = new Stopwatch();

            tCapV = new Dictionary<string, double>();
            //sw.Start();

            msg = "Lese Schichtdaten...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            preliminaryResources = new Dictionary<string, Resource>();
            if (tPE.StopSignal)
                return;

            tPE.addUsedResource((IDisposable)Shift);

            if (tPE.StopSignal)
                return;
            tab = Shift.getData(); //loadTableFromExcelDocument(shiftsDoc, shiftDataSheetName, shiftDataColNames, shiftDataColMatchTypes);
            // shiftsDoc.CloseDoc();
            tPE.removeUsedResource((IDisposable)Shift);
            if (tPE.StopSignal)
                return;
            // checkColumns(tab, shiftDataColNames, shiftsFileName, shiftDataSheetName);
            //if (tPE.StopSignal)
            //  return;
            //double currCapVal;
            double currVal;
            for (j = 0; j < tab.Rows.Count; j++)
            {
                if (tPE.StopSignal)
                    return;
                currAutomatName = tab.Rows[j].ItemArray[1].ToString().Trim();
                if (wResources.ContainsKey(currAutomatName))
                {
                    currCellStr = tab.Rows[j].ItemArray[2].ToString().Trim();
                    if (currCellStr != "")
                        currVal = Convert.ToInt32(currCellStr, currC);
                    else
                        currVal = 0;
                    if (!tCapV.ContainsKey(currAutomatName))
                        tCapV.Add(currAutomatName, currVal);
                    else
                        tCapV[currAutomatName] += currVal;
                }

            }

            for (j = 0; j < tab.Rows.Count; j++)
            {
                if (tPE.StopSignal)
                    return;
                currAutomatName = tab.Rows[j].ItemArray[1].ToString().Trim();
                periodName = tab.Rows[j].ItemArray[0].ToString().Trim();



                if (wResources.ContainsKey(currAutomatName) && (tCapV.ContainsKey(currAutomatName) && tCapV[currAutomatName] > 0))
                {


                    //if (totalCapVal > 0)
                    //{
                    WEPA_Location currLoc;
                    WEPA_Resource currWResource;

                    if (!preliminaryResources.ContainsKey(currAutomatName))
                    {

                        currResource = new Resource();
                        currResource.ResourceName = currAutomatName;
                        currWResource = wResources[currAutomatName];
                        currLoc = wPlantLocations[currWResource.LocationName];//[wPlantLocsForMachines[currAutomatName]];

                        currResource.LocationName = currLoc.LocationCountry + currLoc.LocationPLZ;
                        preliminaryResources.Add(currResource.Key, currResource);
                    }
                    else
                        currResource = preliminaryResources[currAutomatName];

                    if (periodName != "")
                    {
                        currPI = pI.getPeriod(periodName);
                        if (currPI != null)
                        {
                            currCapVal = new CapacityValue();
                            currCapVal.PeriodName = periodName;

                            currCellStr = tab.Rows[j].ItemArray[2].ToString().Trim();
                            if (currCellStr != "")
                                currCapVal.Val = Convert.ToInt32(currCellStr, currC);
                            else
                                currCapVal.Val = 0;

                            currResource.addCapacity(currCapVal);
                        }
                    }
                    // }
                }
            }

            usedPreliminaryResources = new HashSet<string>();
            //sw.Stop();

            msg = "Schichtdaten fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }
    }


    public class DataLoader_XLS : DataLoader
    {
        protected XLSTable histTranspTabNE, histTranspTabSW, whseSWEuropeTab;

        protected string[] MONTHS = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

        protected string fixationFileName = "fixierung.xlsx";
        protected string exclusionFileName = "ausschluss.xlsx";

        protected string whseSouthWestEuropeFilename = "WareHouseCodes.xlsx";
        protected string locationTranslationFilename = "locsFinal.xlsx";
        protected string warehouseCapacitiesFilename = "";
        protected string tonnageFileName = "Produktionstonnage 2013+2014 Werk Automat Artikel.xlsx";
        protected string baseDataAndWorkingPlansFileName = "Arbeitspläne und Stammdaten 2015-03-09angepasst.xlsx";
        protected string additionalBaseDataAndWorkingPlansFileName = "Fehlende_AP_für_SalesArtikel_2015_01_16.xlsx";
        protected string demandFileName = "sales plan 01-03.xlsx";
        protected string shiftsFileName = "Schichten.xlsx";
        protected string transpFileName = "12 - Dezember kumuliert nur Detail-Daten.xlsx";
        protected string transpSouthWestEuropeFileName = "12 - Dezember SüdWest kumuliert nur Daten.xlsx";
        protected string interPlantTranspFileName = "FD-GR-2100_EN_Freight_cost_matrix_2014.xlsx";
        protected string costPerShiftFileName = "Produktionskosten_2015-01-16.xlsx";

        protected string transportStartSubstituteFileName = "Zuordnung Ladeort zu ProduktionsortKategorie.xlsx";

        protected string iniStocksFilename = "SSI Datei_Lagerbestand_01-01-2015.xlsx";

        protected string plantmacSheetName = "PlantMachines";
        protected string[] plantMacColNames = { "Plant", "Machine" };
        protected XLSTable.MatchType[] plantMacColMatchType = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string locSheetName = "Locations";
        protected string[] locColNames = { "Name", "Country", "PLZ_Part1" };
        protected XLSTable.MatchType[] locColMatchType = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string matchingFileName = "matchings.csv";
        protected string csvDelim = ";";
        protected string[] matchingHL = { "Transport Customer", "Customer chain" };

        protected static readonly string[,] countryTab = { {"Algeria","DZ" },
                                           {"Austria","AT"},
                                           {"Belgium","BE"},
                                           {"Bosnia And Herzegowina","BA"},
                                           {"Bulgaria","BG"},
                                           {"Cameroon","CM"},
                                           {"Croatia","HR"},
                                           {"Cyprus","CY"},
                                           {"Czech Republic","CZ"},
                                           {"Denmark","DK"},
                                           {"Estonia","EE"},
                                           {"Finland","FI"},
                                           {"France","FR"},
                                           {"Germany","DE"},
                                           {"Greece","GR"},
                                           {"Hungary","HU"},
                                           {"Ireland","IE"},
                                           {"Iceland","IS"},
                                           {"Israel","IL"},
                                           {"Italy","IT"},
                                           {"Latvia","LV"},
                                           {"Lithuania","LT"},
                                           {"Luxembourg","LU"},
                                           {"Moldova","MD"},
                                           {"Morocco","MA"},
                                           {"Netherlands","NL"},
                                           {"Norway","NO"},
                                           {"Poland","PL"},
                                           {"Portugal","PT"},
                                           {"Romania","RO"},
                                           {"Russian Federation","RU"},
                                           {"Slovakia","SK"},
                                           {"Slovenia","SI"},
                                           {"Spain","ES"},
                                           {"Sweden","SE"},
                                           {"Switzerland","CH"},
                                           {"Ukraine","UA"},
                                           {"United Kingdom (UK)","GB"}
                                         };

        protected string fixationSheetName = "Tabelle1";
        //      0               1               2           3                   4                                                           6                   7           8
        protected string[] fixationColNames = { "Einschränkung", "Kundenkette", "Kundenland", "Artikelnummer", "Produktionsland / Ausgangspunkt (Land) für Transporte", "Werk / Lagerstandort", "Maschine", "Gültig bis" };

        protected XLSTable.MatchType[] fixationColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string exclusionSheetName = "Tabelle1";
        protected string[] exclusionColNames = { "Einschränkung", "Kundenkette", "Kundenland", "Artikelnummer", "Produktionsland / Ausgangspunkt (Land) für Transporte", "Werk / Lagerstandort", "Maschine", "Gültig bis" };
        protected XLSTable.MatchType[] exclusionColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };



        protected string iniStocksSheetName = "SSI Datei";
        protected string[] iniStocksColNames = { "stock move unit PL" };
        protected XLSTable.MatchType[] iniStocksColMatchTypes = { XLSTable.MatchType.EXACT };

        protected string prodSheetName = "Products";
        protected string[] prodColNames = { "Product", "PalletsPerTon" };

        protected string prodProcSheetName = "ProductionProcesses";
        protected string[] prodProcColNames = { "Product", "Resource", "TonPerShift", "CostPerShift", "Location" };

        protected string capSheetName = "ResourcesAndCapacities";
        protected string[] capColNames = { "Resource", "Period", "Capacity" };

        protected string customerChainSheetName = "Kunde-Kette";
        protected string[] customerChainColNames = { "Kette", "Kunde" };
        protected XLSTable.MatchType[] customerChainColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string rsSheetName = "Resources";
        protected string[] rsColNames = { "Resource", "Location" };
        protected XLSTable.MatchType[] rsColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string costPerShiftSheetName = "Tagesproduktion";
        protected string[] costPerShiftColNames = { "productionline", "€/Stunde" };
        protected XLSTable.MatchType[] costPerShiftColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string transpNESheetName = "transdata";

        protected string[] transpNEColNames = { "Code", "Tour Nummer", "# Paletten 2", "Preis", "Bel. - LKZ", "Bel. - PLZ", "Entl. - LKZ", "Entl. - PLZ", "Article No", "Kette", "Beladeort", "Entladeort" };//{ "Art", "Tour Nummer", "# Paletten 2", "Preis", "Bel. - LKZ", "Bel. - PLZ", "Entl. - LKZ", "Entl. - PLZ", "Article No", "Kette", "Beladeort", "Entladeort" };

        protected XLSTable.MatchType[] transpNEColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, 
                                                                XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, 
                                                                XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT  };


        protected string transpSWSheetName = "Süd-West";
        protected string[] transpSWColNames = { "Warehouse", "Date of Expedition", "CAP", "Country Dest", "Item Code", "Qty Pallets", "Amount", "Kette" };
        protected XLSTable.MatchType[] transpSWColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT,
                                                          XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT};

        protected string whseSWSheetName = "Tabelle1";
        protected string[] whseSWColNames = { "Code", "country", "postal code", "city" };
        protected XLSTable.MatchType[] whseSWColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string interPlantTranspSheetName = "Finished goods";
        protected string[] interPlantTranspColNames = { "from / to" };
        protected XLSTable.MatchType[] interPlantTranspColMatchTypes = { XLSTable.MatchType.EXACT };

        protected string demandSheetName = "detail";
        protected string[] demandColNames = { "no. plan", "customer", "country", "prod. Line", "plan (pallets)", "description" };
        protected XLSTable.MatchType[] demandColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT
                                                        , XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT};

        protected string tonnageSheetName = "Tabelle1";
        protected string[] tonnageColNames = { "converting in ton", "Automat", "Artikel" };
        protected XLSTable.MatchType[] tonnageColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, 
                                                         XLSTable.MatchType.EXACT };

        protected string workingPlansSheetName = "detail";
        protected string[] workingPlansColNames = { "article number", "paper weight / unit (kg)", "t/shift" };
        protected XLSTable.MatchType[] workingPlansColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.SUBSTRING };

        protected string addWorkingPlansSheetName = "Tabelle1";
        protected string[] addWorkingPlansColNames = { "Artikelnummern aus Salesplan", "t/Schicht mit alten Stammdaten", "t/Schicht", "Automat" };
        protected XLSTable.MatchType[] addWorkingPlansColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string shiftDataSheetName = "settings capacity";
        protected string[] shiftDataColNames = { "January", "(" };
        protected XLSTable.MatchType[] shiftDataColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.SUBSTRING };

        protected string tourChainSheetName = "data";
        protected string[] tourChainColNames = { "Tournr./Transportnr.", "Kette" };
        protected XLSTable.MatchType[] tourChainColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string transportStartSubstituteSheetName = "NamenErsetzung";
        protected string[] transportStartSubstituteColNames = { "Zeilenbeschriftungen", "Kategorie", "Zu verwendender Name" };
        protected XLSTable.MatchType[] transportStartSubstituteColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string warehouseCapacitySheetName = "Sheet1";
        protected string[] warehouseCapacityColNames = { "Standort", "Lagerkapazität" };
        protected XLSTable.MatchType[] warehouseCapacityColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };
        protected const int HOURS_PER_SHIFT = 8;

        public DataLoader_XLS()
        {
        }

        public void setFileNames(string whseSouthWestEuropeFN,
            string locationTranslationFN,
            string tonnageFN,
            string baseDataAndWorkingPlansFN,
            string additionalBaseDataAndWorkingPlansFN,
            string demandFN,
            string shiftsFN,
            string transpFN,
            string transpSouthWestEuropeFN,
            string interPlantTranspFN,
            string costPerShiftFN,
            string iniStocksFN,
            string minimumStocksFN,
            string transportStartSubstituteFN,
            string warehouseCapacitiesFN,
            string fixationFN,
            string exclusionFN
            )
        {
            whseSouthWestEuropeFilename = whseSouthWestEuropeFN;
            locationTranslationFilename = locationTranslationFN;
            warehouseCapacitiesFilename = warehouseCapacitiesFN;
            tonnageFileName = tonnageFN;
            baseDataAndWorkingPlansFileName = baseDataAndWorkingPlansFN;
            additionalBaseDataAndWorkingPlansFileName = additionalBaseDataAndWorkingPlansFN;
            demandFileName = demandFN;
            shiftsFileName = shiftsFN;
            transpFileName = transpFN;
            transpSouthWestEuropeFileName = transpSouthWestEuropeFN;
            interPlantTranspFileName = interPlantTranspFN;
            costPerShiftFileName = costPerShiftFN;
            iniStocksFilename = iniStocksFN;
            transportStartSubstituteFileName = transportStartSubstituteFN;
            fixationFileName = fixationFN;
            exclusionFileName = exclusionFN;
        }

        protected XLSTable loadTableFromExcelDocument(Optimizer_Data.Excel_Document xlsDoc,
                                                       string sheetName,
                                                       string[] colNames,
                                                       XLSTable.MatchType[] colMatchTypes
                                                       )
        {
            string uLC, lRC;
            XLSTable resultXTab;
            int i, j, k;
            string currCellStr;

            uLC = "A1";
            lRC = "ZZ1000000";

            xlsDoc.setSheetData(sheetName, uLC, lRC);

            resultXTab = new XLSTable();
            resultXTab.ColNums = new int[colNames.Length];
            resultXTab.RowNums = new int[colNames.Length];

            resultXTab.RawTable = xlsDoc.getData();

            resultXTab.FirstRow = 0;
            i = j = 0;
            for (k = 0; k < colNames.Length; k++)
            {
                for (i = 0; i < resultXTab.RawTable.Rows.Count; i++)
                {
                    for (j = 0; j < resultXTab.RawTable.Rows[i].ItemArray.Length; j++)
                    {
                        currCellStr = resultXTab.RawTable.Rows[i].ItemArray[j].ToString().Trim();

                        if (
                            (colNames[k] == "*" && currCellStr != "") ||
                            (colMatchTypes[k] == XLSTable.MatchType.EXACT && currCellStr == colNames[k]) ||
                            (colMatchTypes[k] == XLSTable.MatchType.SUBSTRING && currCellStr.Contains(colNames[k]))
                            )
                        {
                            resultXTab.ColNums[k] = j;
                            resultXTab.RowNums[k] = i;
                            if (resultXTab.FirstRow < i)
                                resultXTab.FirstRow = i;

                            break;
                        }
                    }
                    if (j < resultXTab.RawTable.Columns.Count)
                        break;
                }
                if (i == resultXTab.RawTable.Rows.Count ||
                    j == resultXTab.RawTable.Columns.Count)
                {
                    resultXTab.ColNums[k] = -1;
                    resultXTab.RowNums[k] = -1;
                }
            }

            return resultXTab;
        }
        protected string trimLongPath(string path)
        {
            int lI;
            lI = path.LastIndexOf(Path.DirectorySeparatorChar);
            if (lI != -1)
                return path.Substring(lI + 1);
            else
                return path;
        }
        protected void checkColumns(XLSTable tab, string[] columnNames, string fileName, string sheetName)
        {
            int i;
            string columnPath;

            columnPath = "\"" + trimLongPath(fileName) + "\"" + "." + "\"" + sheetName + "\"" + ".";

            string missingColumns = "";
            for (i = 0; i < tab.ColNums.Length; i++)
            {
                if (tab.ColNums[i] == -1)
                {
                    if (missingColumns != "")
                        missingColumns += ", ";
                    missingColumns += columnPath + columnNames[i];
                }
            }

            if (missingColumns != "")
            {
                throw new Optimizer_Exceptions.OptimizerException(Optimizer_Exceptions.OptimizerException.OptimizerExceptionType.ColumnNotFound, missingColumns);
            }
        }
        public void getLocationsAndLocsMachines(EmptyTaskProcEnvironment tPE,
                                           ref Dictionary<string, WEPA_PlantInfo> wPlantInfos,
                                           ref  Dictionary<string, WEPA_Location> wPlantLocations,
                                           ref  Dictionary<string, WEPA_Resource> wResources,
                                           Dictionary<string, ResourceInformationState> resourcesNotConsidered,
                                          Logger logger,
                                             CultureInfo currC
                                          )
        {
            Optimizer_Data.Excel_Document locsDoc;
            Optimizer_Data.Excel_Document costPerShiftDoc;
            //string completeFN;
            string msg;
            WEPA_Location currLoc;
            XLSTable locsTab, locMacTab;
            int i;
            string currLocId;
            string currLocName;
            string currMacName;
            XLSTable costPerShiftTab;
            double currCostPerShift;
            string currResourceName;
            WEPA_PlantInfo currPlantInfo;
            WEPA_Resource currWResource;
            ResourceInformationState currRs;
            XLSTable wCTab;
            string currPlantName;
            WEPA_Location currWLoc;
            WEPA_PlantInfo currWPlInfo;
            string capacityStr;
            Optimizer_Data.Excel_Document wCDoc;

            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            msg = "Lese Standorte und Maschinen...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            //completeFN = locationTranslationFilename;

            locsDoc = null;
            wPlantInfos = new Dictionary<string, WEPA_PlantInfo>();
            wPlantLocations = new Dictionary<string, WEPA_Location>();
            wResources = new Dictionary<string, WEPA_Resource>();

            if (!File.Exists(locationTranslationFilename))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, locationTranslationFilename);

            if (!File.Exists(costPerShiftFileName))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, costPerShiftFileName);

            if (!File.Exists(warehouseCapacitiesFilename))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, locationTranslationFilename);

            if (tPE.StopSignal)
                return;

            locsDoc = new Optimizer_Data.Excel_Document(locationTranslationFilename);
            tPE.addUsedResource(locsDoc);

            locsDoc.OpenDoc();

            locsTab = loadTableFromExcelDocument(locsDoc, locSheetName, locColNames, locColMatchType);
            locMacTab = loadTableFromExcelDocument(locsDoc, plantmacSheetName, plantMacColNames, plantMacColMatchType);

            locsDoc.CloseDoc();

            tPE.removeUsedResource(locsDoc);
            if (tPE.StopSignal)
                return;
            checkColumns(locsTab, locColNames, locationTranslationFilename, locSheetName);
            checkColumns(locMacTab, plantMacColNames, locationTranslationFilename, plantmacSheetName);
            if (tPE.StopSignal)
                return;
            for (i = locsTab.FirstRow + 1; i < locsTab.RawTable.Rows.Count; i++)
            {
                currLocName = locsTab.RawTable.Rows[i].ItemArray[locsTab.ColNums[0]].ToString().Trim();
                if (currLocName != "" && !wPlantLocations.ContainsKey(currLocName))//Checks whether the location is not void and the location does not exists in the set of plant locations
                //Thus, duplicates are ignored.
                {
                    currLoc = new WEPA_Location();
                    currLoc.LocationCountry = locsTab.RawTable.Rows[i].ItemArray[locsTab.ColNums[1]].ToString().Trim();
                    currLoc.LocationPLZ = locsTab.RawTable.Rows[i].ItemArray[locsTab.ColNums[2]].ToString().Trim();

                    currLocId = currLoc.LocationCountry + currLoc.LocationPLZ;
                    if (!wPlantInfos.ContainsKey(currLocId))
                    {
                        currPlantInfo = new WEPA_PlantInfo();
                        currPlantInfo.WarehouseCapacity = 0;
                        currPlantInfo.LocationName = currLocName;
                        wPlantInfos.Add(currLocId, currPlantInfo);
                    }

                    wPlantLocations.Add(currLocName, currLoc);
                }
            }

            for (i = locMacTab.FirstRow + 1; i < locMacTab.RawTable.Rows.Count; i++)
            {
                currMacName = locMacTab.RawTable.Rows[i].ItemArray[locMacTab.ColNums[1]].ToString().Trim();
                currLocName = locMacTab.RawTable.Rows[i].ItemArray[locMacTab.ColNums[0]].ToString().Trim();
                if (currMacName != "")
                {
                    //Check whether the machine name is not empts
                    if (wPlantLocations.ContainsKey(currLocName)) //Check whether the location is a valid plant location
                    {
                        if (!wResources.ContainsKey(currMacName)) //Add resource if not already added (otherwise we have a duplicate which is ignored)
                        {
                            currWResource = new WEPA_Resource();
                            currWResource.LocationName = currLocName;
                            currWResource.costPerShift = 0;

                            wResources.Add(currMacName, currWResource);
                        }
                    }
                    else
                    {
                        if (!wResources.ContainsKey(currMacName)) //if the machine has no valid plant location and there is no entry yet add machine to the set of ignored resources.
                        {
                            if (!resourcesNotConsidered.ContainsKey(currMacName))
                            {
                                currRs = new ResourceInformationState();
                                currRs.MachineName = currMacName;
                                currRs.HasNoCapacity = currRs.HasNoLocation = false;
                                resourcesNotConsidered.Add(currMacName, currRs);
                            }
                            else
                                currRs = resourcesNotConsidered[currMacName];

                            currRs.HasNoLocation = true;
                        }
                    }
                }
            }

            //sw.Start();

            costPerShiftDoc = null;
            if (tPE.StopSignal)
                return;

            msg = "Lese Kosten pro Schicht...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            costPerShiftDoc = new Optimizer_Data.Excel_Document(costPerShiftFileName);
            tPE.addUsedResource(costPerShiftDoc);
            costPerShiftDoc.OpenDoc();
            costPerShiftTab = loadTableFromExcelDocument(costPerShiftDoc, costPerShiftSheetName, costPerShiftColNames, costPerShiftColMatchTypes);
            costPerShiftDoc.CloseDoc();
            tPE.removeUsedResource(costPerShiftDoc);
            if (tPE.StopSignal)
                return;

            checkColumns(costPerShiftTab, costPerShiftColNames, costPerShiftFileName, costPerShiftSheetName);
            if (tPE.StopSignal)
                return;

            for (i = costPerShiftTab.FirstRow + 1; i < costPerShiftTab.RawTable.Rows.Count; i++)
            {
                currResourceName = costPerShiftTab.RawTable.Rows[i].ItemArray[costPerShiftTab.ColNums[0]].ToString().Trim();
                if (wResources.ContainsKey(currResourceName))//Check whether the resource name belongs to a valid resource) ;In case of duplicates take the cost from the last entry
                {
                    currCostPerShift = Convert.ToDouble(costPerShiftTab.RawTable.Rows[i].ItemArray[costPerShiftTab.ColNums[1]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, "")) * HOURS_PER_SHIFT;

                    currWResource = wResources[currResourceName];
                    currWResource.costPerShift = currCostPerShift;
                }
            }

            //sw.Stop();
            msg = "Kosten pro Schicht Fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            //sw.Start();

            msg = "Lese Lagerkapazitäten...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            wCDoc = null;

            if (tPE.StopSignal)
                return;
            wCDoc = new Optimizer_Data.Excel_Document(warehouseCapacitiesFilename);
            tPE.addUsedResource(wCDoc);

            wCDoc.OpenDoc();
            wCTab = loadTableFromExcelDocument(wCDoc, warehouseCapacitySheetName, warehouseCapacityColNames, warehouseCapacityColMatchTypes);
            wCDoc.CloseDoc();

            tPE.removeUsedResource(wCDoc);
            if (tPE.StopSignal)
                return;

            checkColumns(wCTab, warehouseCapacityColNames, warehouseCapacitiesFilename, warehouseCapacitySheetName);
            if (tPE.StopSignal)
                return;

            for (i = wCTab.FirstRow + 1; i < wCTab.RawTable.Rows.Count; i++)
            {
                currPlantName = wCTab.RawTable.Rows[i].ItemArray[wCTab.ColNums[0]].ToString().Trim();
                capacityStr = wCTab.RawTable.Rows[i].ItemArray[wCTab.ColNums[1]].ToString().Trim();
                if (currPlantName != "" && capacityStr != "") //if the plant name and the string for the capacity value are not empty
                {
                    if (wPlantLocations.ContainsKey(currPlantName)) //Check whether the plant name belongs to a valid plant location add capacity value;In case of duplicates take the value of the last entry
                    {
                        currWLoc = wPlantLocations[currPlantName];

                        currLocName = currWLoc.LocationCountry + currWLoc.LocationPLZ;

                        currWPlInfo = wPlantInfos[currLocName];
                        currWPlInfo.WarehouseCapacity = Convert.ToInt32(capacityStr, currC);
                    }
                }
            }

            //sw.Stop();
            msg = "Standorte und Maschinen eingelesen.";// +"Zeit:" + sw.Elapsed;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        public void getFirstLastMonth(ref int firstMonth, ref int lastMonth, Logger logger,
                                    CultureInfo currC)
        {
            string reducedDemandFileName;
            int mSepIndex;
            if (demandFileName.LastIndexOf(Path.DirectorySeparatorChar) != -1)
                reducedDemandFileName = demandFileName.Substring(demandFileName.LastIndexOf(Path.DirectorySeparatorChar) + 1);
            else
                reducedDemandFileName = demandFileName;

            mSepIndex = reducedDemandFileName.IndexOf("-");
            firstMonth = Convert.ToInt32(reducedDemandFileName.Substring(mSepIndex - 2, 2), currC) - 1;
            lastMonth = Convert.ToInt32(reducedDemandFileName.Substring(mSepIndex + 1, 2), currC) - 1;
        }

        public void getInitialStocks(EmptyTaskProcEnvironment tPE,
                                Dictionary<string, WEPA_Location> wPlantLocations,
                                  Dictionary<string, Product> preliminaryProducts,
                                ref  Dictionary<string, InitialStockValue> preliminaryInitialStocks,
                               Logger logger,
                               CultureInfo currC
              )
        {
            Optimizer_Data.Excel_Document iniStocksDoc;
            //string completeFN;
            XLSTable iniStocksTab;
            string[] locationDescriptorEls;
            int descriptColNum, productColNum;
            string plantName;
            string productName;
            string key;
            InitialStockValue currIniStVal;
            int i;
            double initStQty;
            string msg;
            WEPA_Location currPlantLoc;
            string currLocName;
            //Stopwatch sw = new Stopwatch();

            //sw.Start();
            preliminaryInitialStocks = new Dictionary<string, InitialStockValue>();

            //completeFN = iniStocksFilename;

            msg = "Lese Anfangsbestände...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            iniStocksDoc = null;
            if (File.Exists(iniStocksFilename))
            {
                if (tPE.StopSignal)
                    return;
                iniStocksDoc = new Optimizer_Data.Excel_Document(iniStocksFilename);
                tPE.addUsedResource(iniStocksDoc);
                iniStocksDoc.OpenDoc();
                iniStocksTab = loadTableFromExcelDocument(iniStocksDoc, iniStocksSheetName, iniStocksColNames, iniStocksColMatchTypes);
                iniStocksDoc.CloseDoc();
                tPE.removeUsedResource(iniStocksDoc);
                if (tPE.StopSignal)
                    return;
                checkColumns(iniStocksTab, iniStocksColNames, iniStocksFilename, iniStocksSheetName);
                if (tPE.StopSignal)
                    return;
                descriptColNum = iniStocksTab.ColNums[0] - 1;
                productColNum = iniStocksTab.ColNums[0] - 2;

                for (i = iniStocksTab.RowNums[0] + 1; i < iniStocksTab.RawTable.Rows.Count; i++)
                {
                    locationDescriptorEls = iniStocksTab.RawTable.Rows[i].ItemArray[descriptColNum].ToString().Split(' ');

                    if (locationDescriptorEls.Length >= 2 &&
                        (locationDescriptorEls[0] == "Grp1" ||
                        locationDescriptorEls[0] == "Grp2" ||
                        locationDescriptorEls[0] == "Grp3")
                        )
                    {
                        plantName = locationDescriptorEls[1].Trim();

                        if (iniStocksTab.RawTable.Rows[i].ItemArray[iniStocksTab.ColNums[0]].ToString().Trim() != "")
                            initStQty = Convert.ToDouble(iniStocksTab.RawTable.Rows[i].ItemArray[iniStocksTab.ColNums[0]], currC);
                        else
                            initStQty = 0;

                        switch (plantName) //Spezialfälle 
                        {
                            case "Poland":
                                plantName = "Piechowice";
                                break;
                        }

                        productName = iniStocksTab.RawTable.Rows[i].ItemArray[productColNum].ToString().Trim();

                        if (
                        wPlantLocations.ContainsKey(plantName) &&
                        preliminaryProducts.ContainsKey(productName) &&
                        initStQty > 0
                        ) //Accept initial stock value if the plant name as well as the product name belongs to a valid plant location and a valid product
                        {
                            currPlantLoc = wPlantLocations[plantName];
                            currLocName = currPlantLoc.LocationCountry + currPlantLoc.LocationPLZ;

                            key = currLocName + productName;
                            if (!preliminaryInitialStocks.ContainsKey(key))
                            {
                                currIniStVal = new InitialStockValue();

                                currIniStVal.LocationName = currLocName;
                                currIniStVal.ProductName = productName;

                                currIniStVal.Val = initStQty;
                                preliminaryInitialStocks.Add(key, currIniStVal);
                            }

                        }
                    }
                }


            }
            //sw.Stop();
            msg = "Anfangsbestände fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        //Auxiliary procedure to determine the ISO 3166 Alpha2 (two letters) abbreviation for a country name
        protected string getCountryAbbrevISO3166Alpha2(string countryName)
        {
            int i;
            for (i = 0; i <= countryTab.GetUpperBound(0); i++)
            {
                if (countryTab[i, 0] == countryName)
                    return countryTab[i, 1];
            }

            return null;
        }

        public void getTransportStartSubstitutions(EmptyTaskProcEnvironment tPE, ref Dictionary<string, string> transportStartSubstitions, Logger logger)
        {
            Excel_Document transpStartDoc;
            XLSTable transpStartSubsitutionsTab;
            string msg;
            int i;

            //Stopwatch sw = new Stopwatch();

            //sw.Start();
            msg = "Lese Transport-Start-Ersetzungen...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            transportStartSubstitions = new Dictionary<string, string>();

            if (!File.Exists(transportStartSubstituteFileName))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, transportStartSubstituteFileName);

            transpStartDoc = null;

            if (tPE.StopSignal)
                return;
            transpStartDoc = new Excel_Document(transportStartSubstituteFileName);
            tPE.addUsedResource(transpStartDoc);
            transpStartDoc.OpenDoc();
            transpStartSubsitutionsTab = loadTableFromExcelDocument(transpStartDoc, transportStartSubstituteSheetName, transportStartSubstituteColNames, transportStartSubstituteColMatchTypes);
            transpStartDoc.CloseDoc();
            tPE.removeUsedResource(transpStartDoc);
            if (tPE.StopSignal)
                return;
            checkColumns(transpStartSubsitutionsTab, transportStartSubstituteColNames, transportStartSubstituteFileName, transportStartSubstituteSheetName);
            if (tPE.StopSignal)
                return;

            for (i = transpStartSubsitutionsTab.FirstRow + 1; i < transpStartSubsitutionsTab.RawTable.Rows.Count; i++)
            {
                if (transpStartSubsitutionsTab.RawTable.Rows[i].ItemArray[transpStartSubsitutionsTab.ColNums[1]].ToString().Trim() == "W")//if (transpStartSubsitutionsTab.rawTable.cells[i, transpStartSubsitutionsTab.colNums[1]].Trim() == "W")
                {
                    //If mapping is of type "W" (Werk) then the location indicated in the historical transport data is mapped to the plant location given in the column "Zu verwendender Name"
                    transportStartSubstitions.Add(transpStartSubsitutionsTab.RawTable.Rows[i].ItemArray[transpStartSubsitutionsTab.ColNums[0]].ToString().Trim(), transpStartSubsitutionsTab.RawTable.Rows[i].ItemArray[transpStartSubsitutionsTab.ColNums[2]].ToString().Trim());
                }
            }

            //sw.Stop();

            msg = "Transport-Start-Ersetzungen fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        public void readTransportsNE(EmptyTaskProcEnvironment tPE, Logger logger)
        {
            Optimizer_Data.Excel_Document transpDoc;
            //string completeFN;
            string msg;


            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            //completeFN = transpFileName;

            if (File.Exists(transpFileName))
            {
                msg = "Lade Transporte Nord-Ost-Europa...";
                logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
                if (tPE.StopSignal)
                    return;

                transpDoc = null;

                transpDoc = new Optimizer_Data.Excel_Document(transpFileName);
                tPE.addUsedResource(transpDoc);
                transpDoc.OpenDoc();

                histTranspTabNE = loadTableFromExcelDocument(transpDoc, transpNESheetName, transpNEColNames, transpNEColMatchTypes);

                transpDoc.CloseDoc();
                tPE.removeUsedResource(transpDoc);
                if (tPE.StopSignal)
                    return;
                checkColumns(histTranspTabNE, transpNEColNames, transpFileName, transpNESheetName);
                //  sw.Stop();

                msg = "Transporte Nord-Ost-Europa fertig.";// +"Zeit:" + sw.Elapsed; ;
                logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            }
            else
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, transpFileName);
        }

        public void readTransportsSW(EmptyTaskProcEnvironment tPE, Logger logger)
        {
            Optimizer_Data.Excel_Document transpDoc;
            //string completeFN;
            string msg;
            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            //completeFN = transpSouthWestEuropeFileName;

            if (File.Exists(transpSouthWestEuropeFileName))
            {
                msg = "Lade Transporte Süd-West-Europa...";
                logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
                if (tPE.StopSignal)
                    return;

                transpDoc = null;

                transpDoc = new Optimizer_Data.Excel_Document(transpSouthWestEuropeFileName);
                tPE.addUsedResource(transpDoc);
                transpDoc.OpenDoc();

                histTranspTabSW = loadTableFromExcelDocument(transpDoc, transpSWSheetName, transpSWColNames, transpSWColMatchTypes);
                if (tPE.StopSignal)
                    return;
                transpDoc.CloseDoc();
                tPE.removeUsedResource(transpDoc);
                if (tPE.StopSignal)
                    return;
                checkColumns(histTranspTabSW, transpSWColNames, transpSouthWestEuropeFileName, transpSWSheetName);

                //    sw.Stop();

                msg = "Transporte Süd-West-Europa fertig";// +"Zeit:" + sw.Elapsed;
                logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            }
            else
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, transpSouthWestEuropeFileName);
        }

        public void readWHSESWEurope(EmptyTaskProcEnvironment tPE, Logger logger)
        {
            //string completeFN;
            string msg;
            Optimizer_Data.Excel_Document whseSWEuropeDoc;
            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            //completeFN = whseSouthWestEuropeFilename;

            if (!File.Exists(whseSouthWestEuropeFilename))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, whseSouthWestEuropeFilename);
            //{
            msg = "Lade Läger Süd-West-Europa...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            if (tPE.StopSignal)
                return;
            whseSWEuropeDoc = null;

            whseSWEuropeDoc = new Optimizer_Data.Excel_Document(whseSouthWestEuropeFilename);
            tPE.addUsedResource(whseSWEuropeDoc);
            whseSWEuropeDoc.OpenDoc();

            whseSWEuropeTab = loadTableFromExcelDocument(whseSWEuropeDoc, whseSWSheetName, whseSWColNames, whseSWColMatchTypes);

            whseSWEuropeDoc.CloseDoc();
            tPE.removeUsedResource(whseSWEuropeDoc);
            if (tPE.StopSignal)
                return;
            checkColumns(whseSWEuropeTab, whseSWColNames, whseSouthWestEuropeFilename, whseSWSheetName);

            //sw.Stop();
            msg = "Läger Süd-West-Europa fertig";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            //}
            //else
            //   throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, whseSouthWestEuropeFilename);
        }

        public void getInterplantTransports(EmptyTaskProcEnvironment tPE, Dictionary<string, WEPA_Location> wPlantLocations, SNP_WEPA_Instance pI, CultureInfo currC, Logger logger)
        {
            string msg;
            Optimizer_Data.Excel_Document iTranspDoc;
            XLSTable iTranspTab;
            TransportationProcess currTranspP;
            WEPA_Location currSLoc, currDLoc;
            string currCellStr;
            string currLocationName;
            string currStartLocation, currDestLocation;
            double currCostPerTruck;
            int endOfFirstWord;
            PlantLocation currPlantLoc;
            int i, j;

            //Stopwatch sw = new Stopwatch();
            //sw.Start();

            msg = "Lese Interplant-Transporte...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            if (!File.Exists(interPlantTranspFileName))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, interPlantTranspFileName);

            if (tPE.StopSignal)
                return;
            iTranspDoc = new Optimizer_Data.Excel_Document(interPlantTranspFileName);

            tPE.addUsedResource(iTranspDoc);
            iTranspDoc.OpenDoc();

            iTranspTab = loadTableFromExcelDocument(iTranspDoc, interPlantTranspSheetName, interPlantTranspColNames, interPlantTranspColMatchTypes);

            iTranspDoc.CloseDoc();
            tPE.removeUsedResource(iTranspDoc);
            if (tPE.StopSignal)
                return;
            checkColumns(iTranspTab, interPlantTranspColNames, interPlantTranspFileName, interPlantTranspSheetName);
            if (tPE.StopSignal)
                return;
            for (i = iTranspTab.FirstRow + 1; i < iTranspTab.RawTable.Rows.Count; i++)
            {
                currLocationName = iTranspTab.RawTable.Rows[i].ItemArray[iTranspTab.ColNums[0]].ToString().Trim();
                if (tPE.StopSignal)
                    return;
                if (currLocationName == "")
                    break;

                if (wPlantLocations.ContainsKey(currLocationName)) //Check whether the source location is a valid plant location
                {
                    currSLoc = wPlantLocations[currLocationName];

                    for (j = iTranspTab.ColNums[0] + 1; j < iTranspTab.RawTable.Columns.Count; j++)
                    {
                        currLocationName = iTranspTab.RawTable.Rows[iTranspTab.FirstRow].ItemArray[j].ToString().Trim();
                        if (wPlantLocations.ContainsKey(currLocationName)) //Check whether the destination location is a valid plant location
                        {
                            currDLoc = wPlantLocations[currLocationName];

                            currStartLocation = currSLoc.LocationCountry + currSLoc.LocationPLZ;
                            currDestLocation = currDLoc.LocationCountry + currDLoc.LocationPLZ;

                            if (currStartLocation != currDestLocation)
                            {
                                currCellStr = iTranspTab.RawTable.Rows[i].ItemArray[j].ToString().Trim();
                                //currCostPerTruck = 0;
                                if (currCellStr != "")  //New(28.8.2015): Ignore interplant transports if cost fields are empty
                                //Old: Empty fields were assumed to indicate zero transport cost
                                {
                                    endOfFirstWord = currCellStr.IndexOf(" ", 0); //only the first part of the numeric string is intepreted as a number
                                    if (endOfFirstWord != -1)
                                        currCellStr = currCellStr.Substring(0, endOfFirstWord);

                                    currCostPerTruck = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                    //}

                                    if (pI.getPlantLocation(currStartLocation) == null)
                                    {
                                        currPlantLoc = new PlantLocation();
                                        currPlantLoc.LocationName = currStartLocation;
                                        pI.addPlantLocation(currPlantLoc);
                                    }

                                    if (pI.getPlantLocation(currDestLocation) == null)
                                    {
                                        currPlantLoc = new PlantLocation();
                                        currPlantLoc.LocationName = currDestLocation;
                                        pI.addPlantLocation(currPlantLoc);
                                    }

                                    currTranspP = new TransportationProcess();
                                    currTranspP.StartLocation = currStartLocation;

                                    currTranspP.DestLocation = currDestLocation;
                                    currTranspP.CostPerTruck = currCostPerTruck;

                                    pI.addInterplantTransportationProcess(currTranspP);
                                }
                            }
                        }
                    }
                }
            }
            //sw.Stop();
            msg = "Interplant-Transporte fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }



        public void getTransportData(EmptyTaskProcEnvironment tPE,
                                 Dictionary<string, HashSet<string>> transportCustomerCustomerChainMatchings,
               ref Dictionary<string, WEPA_Location> wCustomerLocations,
               ref Dictionary<string, HashSet<string>> productsForCustomerLocation,
               ref Dictionary<string, HashSet<string>> toursForCustomer,
               ref Dictionary<string, TourData> tours,
                ref Dictionary<string, ArticleLocationDistribution> locationDistributionPerCustomerProduct,
                 ref Dictionary<string, ArticleLocationDistribution> locationDistributionPerCustomer,
               ref Dictionary<string, WHSESWEuropeLocation> warehouseLocsSWEurope,
               ref Dictionary<string, TourData> icTours,
               Dictionary<string, WEPA_Location> wPlantLocations,
               Dictionary<string, WEPA_PlantInfo> wPlantInfos,
               Dictionary<string, string> transportStartSubstitions,
               SNP_WEPA_Instance pI,
               CultureInfo currC,
               Logger logger,
               bool isUseGivenCustomerSalesToCustomerTransportRelations
               )
        {
            string msg;
            HashSet<string> customersWithTransports;
            TourData currTour; //ok: Aktuelle Tour
            ArticleLocationDistribution currCustomerProductLocationDistribution, currCustomerLocationDistribution; //ok:Für aktuelle kundenproduktspezifische bzw. kundenspezifische Verteilungen der Nachfrage eines Kunden
            string tourId, prodId;
            string tourKey, sourceLocId, destLocId, customerWithDemandId;
            int i, j;
            string customerProductKey;
            double numberPallets, price;
            string whseId, dateExp, cap, countryDest, whseLocId;
            string currCustomerLocation;
            string destLocCountry, destLocPLZ;
            HashSet<string> currCustomerTours;
            WEPA_Location newCustomerLocation;
            string currLocName;
            WEPA_Location currLocation;
            string chainStr;
            WHSESWEuropeLocation currSWWHSELoc;

            //  Stopwatch sw = new Stopwatch();

            //            sw.Start();

            msg = "Transportdaten...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            transportCustomerCustomerChainMatchings = new Dictionary<string, HashSet<string>>();
            wCustomerLocations = new Dictionary<string, WEPA_Location>();
            productsForCustomerLocation = new Dictionary<string, HashSet<string>>();
            toursForCustomer = new Dictionary<string, HashSet<string>>();

            tours = new Dictionary<string, TourData>();

            locationDistributionPerCustomerProduct = new Dictionary<string, ArticleLocationDistribution>();
            locationDistributionPerCustomer = new Dictionary<string, ArticleLocationDistribution>();
            customersWithTransports = new HashSet<string>();

            warehouseLocsSWEurope = new Dictionary<string, WHSESWEuropeLocation>();
            icTours = new Dictionary<string, TourData>();
            WHSESWEuropeLocation currSWLoc;
            for (i = whseSWEuropeTab.FirstRow + 1; i < whseSWEuropeTab.RawTable.Rows.Count; i++)
            {
                if (tPE.StopSignal)
                    return;

                currSWLoc = new WHSESWEuropeLocation();

                //Note: We need the full information of the location beside the name since we use in a first step
                //the transports to determine the distribution of the demands of a customer in a country =>thus we need at least the country information. The PLZ is used here for internal identification.
                //in the second step we derive transports distances with costs per truck where we could use the information from the plant location table instead. In this step
                //we discard transports from start locations not being valid plant locations.

                currSWLoc.LocationCountry = whseSWEuropeTab.RawTable.Rows[i].ItemArray[whseSWEuropeTab.ColNums[1]].ToString().Trim();
                currSWLoc.LocationPLZ = whseSWEuropeTab.RawTable.Rows[i].ItemArray[whseSWEuropeTab.ColNums[2]].ToString().Trim().Replace("-", "");
                currSWLoc.LocationName = whseSWEuropeTab.RawTable.Rows[i].ItemArray[whseSWEuropeTab.ColNums[3]].ToString().Trim();

                warehouseLocsSWEurope.Add(whseSWEuropeTab.RawTable.Rows[i].ItemArray[whseSWEuropeTab.ColNums[0]].ToString().Trim(), currSWLoc);
            }

            for (i = histTranspTabNE.FirstRow + 1; i < histTranspTabNE.RawTable.Rows.Count; i++)
            {
                if (tPE.StopSignal)
                    return;
                prodId = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[8]].ToString().Trim();
                sourceLocId = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[4]].ToString().Trim() + histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[5]].ToString().Trim().Replace("-", "");

                if (histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[0]].ToString().Trim() == "FG") //If transport is a "FINISHED GOOD" transport
                {
                    tourId = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[1]].ToString().Trim();

                    currLocName = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[10]].ToString().Trim();

                    if (transportStartSubstitions.ContainsKey(currLocName)) //Check whether there is a substitution for the start location name e.g. Lille for Tilburg since there are locations only used for
                    //stock keepign or distribution that are related to plant locations.
                    {
                        currLocName = transportStartSubstitions[currLocName];
                        currLocation = wPlantLocations[currLocName];
                        sourceLocId = currLocation.LocationCountry + currLocation.LocationPLZ;
                    }
                    else
                        sourceLocId = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[4]].ToString().Trim() + histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[5]].ToString().Trim().Replace("-", "");//transpTab.cells[i, sourceCountryColumnNum].Trim() + transpTab.cells[i, sourcePlz1ColumnNum].Trim().Replace("-", "");

                    destLocCountry = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[6]].ToString().Trim();
                    destLocPLZ = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[7]].ToString().Trim().Replace("-", "");
                    destLocId = destLocCountry + destLocPLZ;

                    chainStr = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[9]].ToString().Trim();

                    customerWithDemandId = "";
                    if (histTranspTabNE.ColNums[10] != -1) //Ignore erroneous entries for the customer chain
                    {
                        if (
                            !(chainStr == "" ||
                            chainStr.Contains("#N/A") || chainStr.Contains("-2146826246")

                            )
                        )
                        {
                            customerWithDemandId = chainStr;
                        }
                    }

                    if (customerWithDemandId != "" && histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[2]].ToString().Trim() != "" &&
                        histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[3]].ToString().Trim() != "")
                    {
                        if (isUseGivenCustomerSalesToCustomerTransportRelations && !customersWithTransports.Contains(customerWithDemandId))
                            customersWithTransports.Add(customerWithDemandId);

                        tourKey = tourId + sourceLocId + destLocId + customerWithDemandId;

                        numberPallets = Convert.ToDouble(histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[2]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                        if (numberPallets > 0) //Only transport entries with positive numbers of pallets.
                        {
                            price = Convert.ToDouble(histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[3]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                            if (!tours.ContainsKey(tourKey))
                            {
                                currTour = new TourData();
                                currTour.TourId = tourId;
                                currTour.SourceLocId = sourceLocId;
                                currTour.DestLocId = destLocId;
                                currTour.TourNumberPallets = 0;
                                currTour.TourPrice = 0;

                                currTour.Customer = customerWithDemandId;
                                tours.Add(tourKey, currTour);

                                if (!toursForCustomer.ContainsKey(customerWithDemandId)) //Add if necessary tour to the tours of the respective customer =>
                                //Needed for the calculation of customer related baskets 
                                {
                                    currCustomerTours = new HashSet<string>();
                                    toursForCustomer.Add(customerWithDemandId, currCustomerTours);
                                }
                                else
                                    currCustomerTours = toursForCustomer[customerWithDemandId];

                                currCustomerTours.Add(tourKey);

                                if (!wCustomerLocations.ContainsKey(destLocId)) //Add destination location to the set of customer locations =>Needed for the calculation of the distribution of customer related demand
                                {
                                    newCustomerLocation = new WEPA_Location();
                                    newCustomerLocation.LocationCountry = destLocCountry;
                                    newCustomerLocation.LocationPLZ = destLocPLZ;
                                    wCustomerLocations.Add(destLocId, newCustomerLocation);
                                }
                            }
                            else
                                currTour = tours[tourKey];

                            currTour.TourNumberPallets += numberPallets;
                            currTour.TourPrice += price;

                            if (!currTour.ProductsOnTour.Contains(prodId)) //Add the product to the set of products on the respective tour =>Needed for the calculation of baskets
                                currTour.ProductsOnTour.Add(prodId);

                            customerProductKey = currTour.Customer + prodId;

                            currCustomerLocation = currTour.Customer + currTour.DestLocId;

                            if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                            {
                                currCustomerProductLocationDistribution = new ArticleLocationDistribution();
                                locationDistributionPerCustomerProduct.Add(customerProductKey, currCustomerProductLocationDistribution);
                            }
                            else
                                currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                            if (!currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation.ContainsKey(currTour.DestLocId)) // Add destination location of the current tour to the set of destination locations for
                                // the customer product (required for calculating the distribution of customer demands among
                                // customer locations for a given customer product
                                currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation.Add(currTour.DestLocId, numberPallets);
                            else //increase the number of pallets delivered to the customer location
                                currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] =
                                    currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] + numberPallets;

                            if (!locationDistributionPerCustomer.ContainsKey(currTour.Customer))
                            {
                                currCustomerLocationDistribution = new ArticleLocationDistribution();
                                locationDistributionPerCustomer.Add(currTour.Customer, currCustomerLocationDistribution);
                            }
                            else
                                currCustomerLocationDistribution = locationDistributionPerCustomer[currTour.Customer];

                            if (!currCustomerLocationDistribution.NumberPalletsTransportedPerLocation.ContainsKey(currTour.DestLocId))// Add destination location of the current tour to the set of destination locations for
                                // the customer (required for calculating the distribution of customer demands among
                                // customer locations
                                currCustomerLocationDistribution.NumberPalletsTransportedPerLocation.Add(currTour.DestLocId, numberPallets);
                            else
                                currCustomerLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] =
                                    currCustomerLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] + numberPallets;
                        }
                    }
                }
                else if (histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[0]].ToString().Trim() == "FG IC")   //Consider intercompany transports for the calculation of transport costs missing in the
                //ic cost matrix
                {
                    tourId = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[1]].ToString().Trim();
                    currLocName = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[10]].ToString().Trim();

                    if (transportStartSubstitions.ContainsKey(currLocName))//Check whether there is a substitution for the start location name e.g. Lille for Tilburg since there are locations only used for
                    //stock keepign or distribution that are related to plant locations.
                    {
                        currLocName = transportStartSubstitions[currLocName];
                        currLocation = wPlantLocations[currLocName];
                        sourceLocId = currLocation.LocationCountry + currLocation.LocationPLZ;
                    }
                    else
                        sourceLocId = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[4]].ToString().Trim() + histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[5]].ToString().Trim().Replace("-", "");//transpTab.cells[i, sourceCountryColumnNum].Trim() + transpTab.cells[i, sourcePlz1ColumnNum].Trim().Replace("-", "");

                    currLocName = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[11]].ToString().Trim();
                    if (transportStartSubstitions.ContainsKey(currLocName))//Check whether there is a substitution for the destination location name (this is possible as we consider intercompany transports) e.g. Lille for Tilburg since there are locations only used for
                    //stock keepign or distribution that are related to plant locations.
                    {
                        currLocName = transportStartSubstitions[currLocName];
                        currLocation = wPlantLocations[currLocName];
                        destLocId = currLocation.LocationCountry + currLocation.LocationPLZ;
                    }
                    else
                        destLocId = histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[6]].ToString().Trim() + histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[7]].ToString().Trim().Replace("-", "");//destLocId = destLocCountry + destLocPLZ;//histTranspTabNE.rawTable.cells[i, histTranspTabNE.colNums[4]].Trim() + histTranspTabNE.rawTable.cells[i, histTranspTabNE.colNums[5]].Trim().Replace("-", "");

                    if (sourceLocId != destLocId &&
                        pI.getInterplantTransportationProcess(sourceLocId + destLocId) == null && //Consider transport only if no interplant transport already exists for the given distance
                        wPlantInfos.ContainsKey(sourceLocId) &&                                 // and the start and destination locations are both valid plant locations
                        wPlantInfos.ContainsKey(destLocId)
                        )
                    {
                        if (histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[2]].ToString().Trim() != "" &&
                            histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[3]].ToString().Trim() != "") //Consider entry only if there are non-empty entries for the number of pallets and the price
                        {
                            tourKey = tourId + sourceLocId + destLocId;

                            numberPallets = Convert.ToDouble(histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[2]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                            if (numberPallets > 0)
                            {
                                if (histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[3]].ToString().Trim() == "")
                                    price = 0;
                                else
                                    price = Convert.ToDouble(histTranspTabNE.RawTable.Rows[i].ItemArray[histTranspTabNE.ColNums[3]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                if (!icTours.ContainsKey(tourKey))
                                {
                                    currTour = new TourData();
                                    currTour.TourId = tourId;
                                    currTour.SourceLocId = sourceLocId;
                                    currTour.DestLocId = destLocId;
                                    currTour.TourNumberPallets = 0;
                                    currTour.TourPrice = 0;

                                    currTour.Customer = "";
                                    icTours.Add(tourKey, currTour);
                                }
                                else
                                    currTour = icTours[tourKey];

                                currTour.TourNumberPallets += numberPallets;
                                currTour.TourPrice += price;

                                if (!currTour.ProductsOnTour.Contains(prodId))
                                    currTour.ProductsOnTour.Add(prodId);
                            }
                        }
                    }
                }
            }

            for (i = histTranspTabSW.FirstRow + 1; i < histTranspTabSW.RawTable.Rows.Count; i++)
            {
                if (tPE.StopSignal)
                    return;

                prodId = histTranspTabSW.RawTable.Rows[i].ItemArray[histTranspTabSW.ColNums[4]].ToString().Trim();
                whseId = histTranspTabSW.RawTable.Rows[i].ItemArray[histTranspTabSW.ColNums[0]].ToString().Trim();
                dateExp = histTranspTabSW.RawTable.Rows[i].ItemArray[histTranspTabSW.ColNums[1]].ToString().Trim();
                cap = histTranspTabSW.RawTable.Rows[i].ItemArray[histTranspTabSW.ColNums[2]].ToString().Trim();
                countryDest = histTranspTabSW.RawTable.Rows[i].ItemArray[histTranspTabSW.ColNums[3]].ToString().Trim();

                currSWWHSELoc = warehouseLocsSWEurope[whseId];

                if (transportStartSubstitions.ContainsKey(currSWWHSELoc.LocationName))//Check whether there is a substitution for the start location name e.g. Lille for Tilburg since there are locations only used for
                //stock keepign or distribution that are related to plant locations.
                {
                    whseLocId = transportStartSubstitions[currSWWHSELoc.LocationName];
                    currLocation = wPlantLocations[whseLocId];
                    sourceLocId = currLocation.LocationCountry + currLocation.LocationPLZ;
                }
                else
                    sourceLocId = currSWWHSELoc.LocationCountry + currSWWHSELoc.LocationPLZ;

                tourId = whseId + dateExp + countryDest;//Note: As we do not have an explicit id for the tour we construct one ourselves by merging the warehouse id, delivery date and destination country being likely
                // the relevant facts characterizing a tour

                destLocId = countryDest + cap;

                customerWithDemandId = "";

                customerWithDemandId = histTranspTabSW.RawTable.Rows[i].ItemArray[histTranspTabSW.ColNums[7]].ToString().Trim();

                if (customerWithDemandId != "" && histTranspTabSW.RawTable.Rows[i].ItemArray[histTranspTabSW.ColNums[5]].ToString().Trim() != "") //Only entries with non-empty fields
                //for the customer and number of pallets are considered
                {
                    if (isUseGivenCustomerSalesToCustomerTransportRelations && !customersWithTransports.Contains(customerWithDemandId))
                        customersWithTransports.Add(customerWithDemandId);

                    tourKey = tourId + sourceLocId + destLocId + customerWithDemandId;
                    numberPallets = Convert.ToDouble(histTranspTabSW.RawTable.Rows[i].ItemArray[histTranspTabSW.ColNums[5]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                    if (numberPallets > 0)//Only entries with a positive number of pallets are considered
                    {
                        price = Convert.ToDouble(histTranspTabSW.RawTable.Rows[i].ItemArray[histTranspTabSW.ColNums[6]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                        if (!tours.ContainsKey(tourKey)) //Add a new tour if necessessary
                        {
                            currTour = new TourData();
                            currTour.TourId = tourId;
                            currTour.SourceLocId = sourceLocId;
                            currTour.DestLocId = destLocId;
                            currTour.TourNumberPallets = 0;
                            currTour.TourPrice = 0;

                            currTour.Customer = customerWithDemandId;
                            tours.Add(tourKey, currTour);

                            if (!toursForCustomer.ContainsKey(customerWithDemandId))
                            {
                                currCustomerTours = new HashSet<string>();
                                toursForCustomer.Add(customerWithDemandId, currCustomerTours);
                            }
                            else
                                currCustomerTours = toursForCustomer[customerWithDemandId];

                            currCustomerTours.Add(tourKey);
                            if (!wCustomerLocations.ContainsKey(destLocId))
                            {
                                newCustomerLocation = new WEPA_Location();

                                newCustomerLocation.LocationCountry = countryDest;
                                newCustomerLocation.LocationPLZ = cap;
                                wCustomerLocations.Add(destLocId, newCustomerLocation);
                            }
                        }
                        else
                            currTour = tours[tourKey];

                        currTour.TourNumberPallets += numberPallets;
                        currTour.TourPrice += price;

                        if (!currTour.ProductsOnTour.Contains(prodId))
                            currTour.ProductsOnTour.Add(prodId);

                        customerProductKey = currTour.Customer + prodId;

                        if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                        {
                            currCustomerProductLocationDistribution = new ArticleLocationDistribution();
                            locationDistributionPerCustomerProduct.Add(customerProductKey, currCustomerProductLocationDistribution);
                        }
                        else
                            currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                        if (!currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation.ContainsKey(currTour.DestLocId))// Add destination location of the current tour to the set of destination locations for
                            // the customer product (required for calculating the distribution of customer demands among
                            // customer locations for a given customer product
                            currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation.Add(currTour.DestLocId, numberPallets);
                        else
                            currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] =
                                currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] + numberPallets;

                        if (!locationDistributionPerCustomer.ContainsKey(currTour.Customer))
                        {
                            currCustomerLocationDistribution = new ArticleLocationDistribution();
                            locationDistributionPerCustomer.Add(currTour.Customer, currCustomerLocationDistribution);
                        }
                        else
                            currCustomerLocationDistribution = locationDistributionPerCustomer[currTour.Customer];

                        if (!currCustomerLocationDistribution.NumberPalletsTransportedPerLocation.ContainsKey(currTour.DestLocId))// Add destination location of the current tour to the set of destination locations for
                            // the customer (required for calculating the distribution of customer demands among
                            // customer locations
                            currCustomerLocationDistribution.NumberPalletsTransportedPerLocation.Add(currTour.DestLocId, numberPallets);
                        else
                            currCustomerLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] =
                                currCustomerLocationDistribution.NumberPalletsTransportedPerLocation[currTour.DestLocId] + numberPallets;
                    }
                }
            }
            //          sw.Stop();

            msg = "Transportdaten fertig.";// +"Zeit:" + sw.Elapsed;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        public void getDemandData(EmptyTaskProcEnvironment tPE,
                  ref Dictionary<string, RawDemand> rawDemands,
               ref Dictionary<string, Product> preliminaryProducts,
               ref HashSet<string> historicalProductionProcesses,
               ref Dictionary<String, HashSet<String>> historicalProductionProcessForCustomerProduct,
               ref Dictionary<string, HistProductionEntry> preliminaryHistoricalProductions,
            //ref Dictionary<string, HashSet<string>> articlesWithDemandsOfCustomer, 
               ref HashSet<string> customersWithDemands,
               SNP_WEPA_Instance pI,
               CultureInfo currC,
               int firstMonth,
               double DEFAULT_KG_PER_PALLET,
               Logger logger,
               bool isConsiderCountryInfoOfDemands
               )
        {
            Optimizer_Data.Excel_Document demDoc;
            //string completeFN;
            XLSTable tab;
            int i, j;
            int currProductIndex;
            int numberPeriods;
            string currCellStr;
            string currArtName;
            string currHistoricalResource;
            int endOfFirstWord;
            Product currProduct;
            Period currPeriod;
            RawDemand currDemand;
            HistProductionEntry currHPE;
            string msg;
            string currPeriodName;
            int startPeriod;
            int monthNum;
            double currDemVal;
            //HashSet<string> currProductsOfCustomers;
            string currDemandKey, currCustomerName, currHistProdKey, currCountryKey;

            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            //completeFN = demandFileName;

            if (File.Exists(demandFileName))
            {
                msg = "Lese Bedarfe...";
                logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
                if (tPE.StopSignal)
                    return;
                rawDemands = new Dictionary<string, RawDemand>();
                customersWithDemands = new HashSet<string>();
                preliminaryProducts = new Dictionary<string, Product>();

                historicalProductionProcesses = new HashSet<string>();
                preliminaryHistoricalProductions = new Dictionary<string, HistProductionEntry>();
                historicalProductionProcessForCustomerProduct = new Dictionary<string, HashSet<string>>();
                //articlesWithDemandsOfCustomer = new Dictionary<string, HashSet<string>>();
                currProductIndex = 0;
                demDoc = null;

                demDoc = new Optimizer_Data.Excel_Document(demandFileName);
                tPE.addUsedResource(demDoc);
                demDoc.OpenDoc();

                tab = loadTableFromExcelDocument(demDoc, demandSheetName, demandColNames, demandColMatchTypes);
                demDoc.CloseDoc();
                tPE.removeUsedResource(demDoc);
                if (tPE.StopSignal)
                    return;
                checkColumns(tab, demandColNames, demandFileName, demandSheetName);
                if (tPE.StopSignal)
                    return;
                numberPeriods = 0;

                startPeriod = Convert.ToInt32(tab.RawTable.Rows[tab.FirstRow].ItemArray[tab.ColNums[4]].ToString().Substring(1));
                monthNum = firstMonth;
                for (i = tab.ColNums[4]; i < tab.RawTable.Columns.Count; i++)
                {
                    if (tPE.StopSignal)
                        return;
                    currPeriodName = "M" + Convert.ToString(i - tab.ColNums[4] + startPeriod); //Collects all relevant columns with period information:
                    //We enumerate the periods:if the period name in the file is no longer match, we abort the search
                    if (tab.RawTable.Rows[tab.FirstRow].ItemArray[i].ToString().Trim() != currPeriodName)
                        break;

                    currPeriod = new Period();
                    currPeriod.PeriodName = MONTHS[monthNum]; //Add new period to the problem instance
                    pI.addPeriod(ref currPeriod);
                    numberPeriods++;
                    monthNum++;
                    if (monthNum == MONTHS.Length) //If the month num reaches the end we have a wrap around to the start of the year
                        monthNum = 0;
                }

                for (i = tab.FirstRow + 1; i < tab.RawTable.Rows.Count; i++)
                {
                    if (tPE.StopSignal)
                        return;
                    currArtName = tab.RawTable.Rows[i].ItemArray[tab.ColNums[0]].ToString().Trim();

                    currCellStr = tab.RawTable.Rows[i].ItemArray[tab.ColNums[3]].ToString().Trim();

                    endOfFirstWord = currCellStr.IndexOf(" ");
                    if (endOfFirstWord != -1)
                        currHistoricalResource = currCellStr.Substring(0, endOfFirstWord); //Take only the first word as resource name
                    else
                        currHistoricalResource = currCellStr;

                    if (currHistoricalResource != "HW")         //Consider only lines where the product is no trade good (Handelsware) such that it has to be produced on a resource
                    {
                        if (!preliminaryProducts.ContainsKey(currArtName)) //Add product if necessary
                        {
                            currProduct = new Product();
                            currProduct.ProductName = currArtName;
                            currProduct.PalletsPerTon = 1000 / DEFAULT_KG_PER_PALLET;
                            currProduct.ProductDescription = tab.RawTable.Rows[i].ItemArray[tab.ColNums[5]].ToString().Trim();
                            preliminaryProducts.Add(currArtName, currProduct);

                            currProductIndex++;
                        }
                        else
                            currProduct = preliminaryProducts[currArtName];

                        monthNum = firstMonth;
                        for (j = 0; j < numberPeriods; j++)
                        {
                            if (tPE.StopSignal)
                                return;
                            currCellStr = tab.RawTable.Rows[i].ItemArray[j + tab.ColNums[4]].ToString().Trim();
                            if (currCellStr != "")
                            {
                                currDemVal = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                if (currDemVal > 0) // Consider only entries with positive demand
                                {
                                    currCustomerName = tab.RawTable.Rows[i].ItemArray[tab.ColNums[1]].ToString().Trim();
                                    if (currCustomerName == "OSTERHEIDER")
                                        j += 0;
                                    currPeriodName = MONTHS[monthNum];
                                    currCountryKey = getCountryAbbrevISO3166Alpha2(tab.RawTable.Rows[i].ItemArray[tab.ColNums[2]].ToString().Trim());

                                    currDemandKey = currProduct.ProductName + currCustomerName + currPeriodName;
                                    if (isConsiderCountryInfoOfDemands)
                                        currDemandKey = currDemandKey + currCountryKey;

                                    if (!customersWithDemands.Contains(currCustomerName)) //Add customer to the customers with demand =>Needed to identify all relevant customers when
                                        //constructing artifical transports for customers without historical transports
                                        customersWithDemands.Add(currCustomerName);

                                    //if (!articlesWithDemandsOfCustomer.ContainsKey(currCustomerName)) 
                                    //{
                                    //    currProductsOfCustomers = new HashSet<string>();
                                    //    articlesWithDemandsOfCustomer.Add(currCustomerName, currProductsOfCustomers);
                                    //}
                                    //else
                                    //    currProductsOfCustomers = articlesWithDemandsOfCustomer[currCustomerName];

                                    //if (!currProductsOfCustomers.Contains(currProduct.ProductName))//Add product to the set of products with demand for a customer
                                    //    currProductsOfCustomers.Add(currProduct.ProductName);

                                    if (!rawDemands.ContainsKey(currDemandKey))
                                    {
                                        currDemand = new RawDemand();
                                        currDemand.ProductName = currProduct.ProductName;
                                        currDemand.PeriodName = currPeriodName;
                                        currDemand.CustomerName = currCustomerName;
                                        currDemand.Val = 0;
                                        if (isConsiderCountryInfoOfDemands)
                                            currDemand.CountryName = currCountryKey;
                                        rawDemands.Add(currDemandKey, currDemand);
                                    }
                                    else
                                        currDemand = rawDemands[currDemandKey];

                                    currDemand.Val += currDemVal;

                                    currHistProdKey = currProduct.ProductName + currHistoricalResource + currPeriodName;//Add historical production needed for the case there no historical tonnages available
                                    if (!preliminaryHistoricalProductions.ContainsKey(currHistProdKey))
                                    {
                                        currHPE = new HistProductionEntry();
                                        currHPE.PeriodName = currPeriodName;
                                        currHPE.ProcessName = currProduct.ProductName + currHistoricalResource;
                                        currHPE.Quantity = 0;

                                        if (!historicalProductionProcesses.Contains(currHPE.ProcessName))
                                            historicalProductionProcesses.Add(currHPE.ProcessName);//Add historical production process needed for the case there no historical tonnages available

                                       // string currCustProdId;
                                       
                                        preliminaryHistoricalProductions.Add(currHistProdKey, currHPE);
                                    }
                                    else
                                        currHPE = preliminaryHistoricalProductions[currHistProdKey];

                                    HashSet<String> currCustomerRelatedHistoricalProductionProcesses;
                                    //currCustProdId = currCustomerName + currProduct.ProductName;
                                    if (!historicalProductionProcessForCustomerProduct.ContainsKey(currCustomerName))//(currCustProdId))
                                    {
                                        currCustomerRelatedHistoricalProductionProcesses = new HashSet<string>();
                                        historicalProductionProcessForCustomerProduct.Add(currCustomerName, currCustomerRelatedHistoricalProductionProcesses);
                                        //(currCustProdId, currCustomerRelatedHistoricalProductionProcesses);
                                    }
                                    else
                                        currCustomerRelatedHistoricalProductionProcesses = historicalProductionProcessForCustomerProduct[currCustomerName];//historicalProductionProcessForCustomerProduct[currCustProdId];

                                    if (!currCustomerRelatedHistoricalProductionProcesses.Contains(currHPE.ProcessName))
                                        currCustomerRelatedHistoricalProductionProcesses.Add(currHPE.ProcessName);

                                    currHPE.Quantity += currDemVal;
                                }
                            }
                            monthNum++;
                            if (monthNum == MONTHS.Length)
                                monthNum = 0;
                        }
                    }
                }
                //sw.Stop();

                msg = "Bedarfe fertig.";// +"Zeit:" + sw.Elapsed; ;
                logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            }
            else
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, demandFileName);
        }

        public void getTonnageData(EmptyTaskProcEnvironment tPE,
               ref Dictionary<string, ProductionProcess> tonnageProductionProcesses,
               Dictionary<string, Resource> preliminaryResources,
            // HashSet<string> articlesWithTonnages,
               Logger logger)
        {
            Optimizer_Data.Excel_Document tnDoc;
            //string completeFN;
            XLSTable tab;
            string currResourceName, lastResourceName;
            string currAutomatName, currArtName;
            ProductionProcess currProductionProcess;
            int i, j;
            int firstConversionPosition, numberOfConversionPositions;
            string currCellStr;
            object tempObj;
            string msg;
            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            //completeFN = tonnageFileName;

            if (File.Exists(tonnageFileName))
            {
                msg = "Lese Historische Tonnagen...";
                logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
                if (tPE.StopSignal)
                    return;
                tonnageProductionProcesses = new Dictionary<string, ProductionProcess>();

                tnDoc = null;

                tnDoc = new Optimizer_Data.Excel_Document(tonnageFileName);
                tPE.addUsedResource(tnDoc);
                tnDoc.OpenDoc();

                tab = loadTableFromExcelDocument(tnDoc, tonnageSheetName, tonnageColNames, tonnageColMatchTypes);
                tnDoc.CloseDoc();
                tPE.removeUsedResource(tnDoc);
                if (tPE.StopSignal)
                    return;
                checkColumns(tab, tonnageColNames, tonnageFileName, tonnageSheetName);
                if (tPE.StopSignal)
                    return;
                firstConversionPosition = 0;
                numberOfConversionPositions = 0;

                for (i = tab.ColNums[0]; i < tab.RawTable.Columns.Count; i++)
                {
                    if (tPE.StopSignal)
                        return;
                    currCellStr = tab.RawTable.Rows[tab.RowNums[0]].ItemArray[i].ToString().Trim();
                    if (currCellStr == tonnageColNames[0])//looks for heading "converting in ton" indicating the columns with historical production
                    {
                        if (numberOfConversionPositions == 0)
                            firstConversionPosition = i;

                        numberOfConversionPositions++;
                    }
                }

                currResourceName = "";
                for (j = tab.FirstRow + 1; j < tab.RawTable.Rows.Count; j++)
                {
                    if (tPE.StopSignal)
                        return;
                    currArtName = tab.RawTable.Rows[j].ItemArray[tab.ColNums[2]].ToString().Trim();

                    if (currArtName != "all items")
                    {
                        for (i = 0; i < numberOfConversionPositions; i++)
                        {
                            currCellStr = tab.RawTable.Rows[j].ItemArray[i + tab.ColNums[0]].ToString().Trim();
                            if (currCellStr != "") //As as the value string is not empty we have found a value
                                break;
                        }

                        lastResourceName = currResourceName;

                        if (i < numberOfConversionPositions)
                        {
                            currAutomatName = tab.RawTable.Rows[j].ItemArray[tab.ColNums[1]].ToString().Trim();

                            //if (!articlesWithTonnages.Contains(currArtName))
                            //    articlesWithTonnages.Add(currArtName);//Add product to the set of product with historical production=>Needed to identify articles where historical processes are necessary

                            currResourceName = currAutomatName;
                            tempObj = currResourceName;

                            if (preliminaryResources.ContainsKey(currResourceName))
                            {
                                currProductionProcess = new ProductionProcess();//Add process to the set of historical production process =>needed to find feasible combinations of products as resources (production processes)

                                currProductionProcess.ProductName = currArtName;
                                currProductionProcess.ResourceName = currAutomatName;

                                tonnageProductionProcesses.Add((string)currProductionProcess.Key, currProductionProcess);
                            }
                        }
                    }
                }

                //sw.Stop();

                msg = "Tonnagen fertig.";// +"Zeit:" + sw.Elapsed; ;
                logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            }
            else
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, tonnageFileName);
        }

        public void getBaseDataAndWorkingPlans(EmptyTaskProcEnvironment tPE,
                ref Dictionary<string, ProductionProcess> preliminaryProductionProcesses,
                ref HashSet<string> productsWithProcesses, Logger logger,
            //Dictionary<string, string> wPlantLocsForMachines, 
                Dictionary<string, WEPA_Resource> wResources,
            //Dictionary<string, double> wCostPerShiftForMachines,
                Dictionary<string, Resource> preliminaryResources,
                Dictionary<string, ProductionProcess> tonnageProductionProcesses,
                 Dictionary<string, ResourceInformationState> resourcesNotConsidered,
                Dictionary<string, ProcessInformationState> processesNotConsidered,
                Dictionary<string, Product> preliminaryProducts,
                HashSet<string> usedPreliminaryResources,
                HashSet<string> historicalProductionProcesses,
            // HashSet<string> articlesWithTonnages,
                CultureInfo currC,
                bool isUseHistoricalProcessIfNoTonnages,
                bool isUseCostPerShift)
        {
            Optimizer_Data.Excel_Document bDWPDoc, aBDWPDoc;
            //string completeFN1, completeFN2;
            XLSTable tab;
            int i, j;
            string currTitleCellStr, currCellStr;
            string currProcessName;
            string currAutomatName, currArtName;
            double kgPerPallet;
            double currTonPerShift;
            object tempObj;
            ProductionProcess currProcess;
            Product currProduct;
            Resource currResource;
            string msg;
            string currPlantName;
            int endOfAutomatName;
            bool isResource, isTonnageAvailable;//, isTonnageNecessary
            ProcessInformationState currPCIS;
            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            //completeFN1 = baseDataAndWorkingPlansFileName;

            if (!File.Exists(baseDataAndWorkingPlansFileName))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, baseDataAndWorkingPlansFileName);
            //completeFN2 = additionalBaseDataAndWorkingPlansFileName;
            if (!File.Exists(additionalBaseDataAndWorkingPlansFileName))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, additionalBaseDataAndWorkingPlansFileName);

            msg = "Lese Arbeitspläne...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            preliminaryProductionProcesses = new Dictionary<string, ProductionProcess>();

            productsWithProcesses = new HashSet<string>();
            if (tPE.StopSignal)
                return;
            bDWPDoc = new Optimizer_Data.Excel_Document(baseDataAndWorkingPlansFileName);
            tPE.addUsedResource(bDWPDoc);
            bDWPDoc.OpenDoc();
            if (tPE.StopSignal)
                return;
            tab = loadTableFromExcelDocument(bDWPDoc, workingPlansSheetName, workingPlansColNames, workingPlansColMatchTypes);
            bDWPDoc.CloseDoc();
            tPE.removeUsedResource(bDWPDoc);
            if (tPE.StopSignal)
                return;
            checkColumns(tab, workingPlansColNames, baseDataAndWorkingPlansFileName, workingPlansSheetName);

            for (i = tab.FirstRow + 1; i < tab.RawTable.Rows.Count; i++)
            {
                if (tPE.StopSignal)
                    return;
                currArtName = tab.RawTable.Rows[i].ItemArray[tab.ColNums[0]].ToString().Trim();

                if (preliminaryProducts.ContainsKey(currArtName))
                {
                    currProduct = preliminaryProducts[currArtName];

                    currCellStr = tab.RawTable.Rows[i].ItemArray[tab.ColNums[1]].ToString().Trim();

                    if (currCellStr != "")
                        kgPerPallet = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                    else
                        kgPerPallet = 0;

                    if (kgPerPallet > 0)
                    {
                        currProduct.PalletsPerTon = 1000 / kgPerPallet;

                        for (j = tab.ColNums[2]; j < tab.RawTable.Columns.Count; j++)
                        {
                            currTitleCellStr = tab.RawTable.Rows[tab.RowNums[2]].ItemArray[j].ToString().Trim();
                            currCellStr = tab.RawTable.Rows[i].ItemArray[j].ToString().Trim();

                            if (currTitleCellStr.Substring(0, workingPlansColNames[2].Length) == workingPlansColNames[2])
                            {
                                currAutomatName = currTitleCellStr.Substring(workingPlansColNames[2].Length + 1).Trim();
                                currProcessName = currArtName + currAutomatName;

                                isResource = isTonnageAvailable = false;//= isTonnageNecessary 

                                if (preliminaryResources.ContainsKey(currAutomatName))
                                {
                                    isResource = true;
                                    currResource = preliminaryResources[currAutomatName];
                                    currProcessName = currArtName + currAutomatName;

                                    //if (articlesWithTonnages.Contains(currArtName))
                                    //  isTonnageNecessary = true;

                                    if (tonnageProductionProcesses.ContainsKey(currProcessName))
                                    {
                                        currProcess = tonnageProductionProcesses[currProcessName];
                                        isTonnageAvailable = true;
                                    }
                                    else
                                        currProcess = null;

                                    if (currCellStr != "" && currCellStr != "#VALUE!")
                                        currTonPerShift = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                    else
                                        currTonPerShift = 0;

                                    if (currTonPerShift > 0)
                                    {
                                        if (currProcess == null
                                               && (isUseHistoricalProcessIfNoTonnages && historicalProductionProcesses.Contains(currArtName + currAutomatName))
                                            )
                                        {
                                            currProcess = new ProductionProcess();
                                            currProcess.ResourceName = currAutomatName;
                                            currProcess.ProductName = currArtName;
                                            isTonnageAvailable = true;
                                        }

                                        if (currProcess != null) //Process is only added to the final set of processes if 
                                        //1) in case of an article with past production the process already has been applied 
                                        //2) the article does not have past production
                                        {
                                            if (!usedPreliminaryResources.Contains(currAutomatName))
                                                usedPreliminaryResources.Add(currAutomatName);
                                            currProcess.TonPerShift = currTonPerShift;
                                            WEPA_Resource currWResource;
                                            //currPlantName = wPlantLocsForMachines[currAutomatName];
                                            currWResource = wResources[currAutomatName];
                                            //if (isUseCostPerShift)
                                            //{
                                            //if (wCostPerShiftForMachines.ContainsKey(currAutomatName))
                                            //    currProcess.CostPerShift = wCostPerShiftForMachines[currAutomatName];
                                            //else
                                            //    currProcess.CostPerShift = 0;
                                            currProcess.CostPerShift = currWResource.costPerShift;
                                            //}
                                            //else
                                            //{
                                            //    if (wCostPerShiftForMachines.ContainsKey(currAutomatName))
                                            //        currProcess.CostPerShift = wCostPerShiftForMachines[currAutomatName];
                                            //    else
                                            //        currProcess.CostPerShift = 0;
                                            //}

                                            if (!preliminaryProductionProcesses.ContainsKey(currProcess.Key))
                                            {
                                                preliminaryProductionProcesses.Add(currProcess.Key, currProcess);
                                                if (!productsWithProcesses.Contains(currArtName))
                                                    productsWithProcesses.Add(currArtName);
                                            }
                                        }
                                        else
                                        {

                                        }
                                    }
                                }

                                if (!isResource || (!isTonnageAvailable))//&& isTonnageNecessary
                                {
                                    if (!processesNotConsidered.ContainsKey(currProcessName))
                                    {
                                        currPCIS = new ProcessInformationState();
                                        currPCIS.IsHistoricalProductionNeeded = true; //isTonnageNecessary;
                                        currPCIS.IsNotExistHistoricalProduction = !isTonnageAvailable;
                                        currPCIS.HasNoResourceWithCapacity = !isResource;
                                        currPCIS.ProductName = currArtName;
                                        currPCIS.ResourceName = currAutomatName;
                                        processesNotConsidered.Add(currProcessName, currPCIS);

                                        currPCIS = processesNotConsidered[currProcessName];
                                    }

                                }

                            }
                        }
                    }
                }
            }

            if (tPE.StopSignal)
                return;
            aBDWPDoc = new Optimizer_Data.Excel_Document(additionalBaseDataAndWorkingPlansFileName);
            tPE.addUsedResource(aBDWPDoc);
            aBDWPDoc.OpenDoc();

            tab = loadTableFromExcelDocument(aBDWPDoc, addWorkingPlansSheetName, addWorkingPlansColNames, addWorkingPlansColMatchTypes);
            aBDWPDoc.CloseDoc();
            tPE.removeUsedResource(aBDWPDoc);
            if (tPE.StopSignal)
                return;
            checkColumns(tab, addWorkingPlansColNames, additionalBaseDataAndWorkingPlansFileName, addWorkingPlansSheetName);

            for (i = tab.FirstRow + 1; i < tab.RawTable.Rows.Count; i++)
            {
                if (tPE.StopSignal)
                    return;
                currArtName = tab.RawTable.Rows[i].ItemArray[tab.ColNums[0]].ToString().Trim();
                currAutomatName = tab.RawTable.Rows[i].ItemArray[tab.ColNums[3]].ToString().Trim();
                endOfAutomatName = currAutomatName.IndexOf(" ");

                if (endOfAutomatName != -1)
                    currAutomatName = currAutomatName.Substring(0, endOfAutomatName);

                tempObj = currAutomatName;

                if (preliminaryResources.ContainsKey(currAutomatName))
                {
                    currResource = preliminaryResources[currAutomatName];
                    currProcessName = currArtName + currAutomatName;

                    if (tonnageProductionProcesses.ContainsKey(currProcessName))
                        currProcess = tonnageProductionProcesses[currProcessName];
                    else
                        currProcess = null;

                    try
                    {
                        currCellStr = tab.RawTable.Rows[i].ItemArray[tab.ColNums[2]].ToString().Trim();
                        currTonPerShift = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                        if (currTonPerShift == 0)
                        {
                            try
                            {
                                currCellStr = tab.RawTable.Rows[i].ItemArray[tab.ColNums[1]].ToString().Trim();
                                currTonPerShift = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                            }
                            catch (Exception e2)
                            {
                                currTonPerShift = 0;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            currCellStr = tab.RawTable.Rows[i].ItemArray[tab.ColNums[1]].ToString().Trim();//tab.cells[i, oldTonPerShiftColumnNum].Trim();
                            currTonPerShift = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                        }
                        catch (Exception e2)
                        {
                            currTonPerShift = 0;
                        }
                    }

                    if (currTonPerShift > 0)
                    {
                        //if (currProcess == null && (!articlesWithTonnages.Contains(currArtName) &&
                        //                    (
                        //                        (isUseHistoricalProcessIfNoTonnages && historicalProductionProcesses.Contains(currProcessName)) ||
                        //                        !isUseHistoricalProcessIfNoTonnages
                        //                        )
                        //                    )
                        //)
                        if (currProcess == null
                                               && (isUseHistoricalProcessIfNoTonnages && historicalProductionProcesses.Contains(currArtName + currAutomatName)))
                        {
                            currProcess = new ProductionProcess();
                            currProcess.ResourceName = currAutomatName;
                            currProcess.ProductName = currArtName;
                        }

                        if (currProcess != null) //Process is only added to the final set of processes if 1) in case of an article with past production the process already has been applied
                        //2) the article does not have past production
                        {
                            if (!usedPreliminaryResources.Contains(currAutomatName))
                                usedPreliminaryResources.Add(currAutomatName);

                            currProcess.TonPerShift = currTonPerShift;
                            WEPA_Resource currWResource;

                            currWResource = wResources[currAutomatName];
                            currProcess.CostPerShift = currWResource.costPerShift;
                            //if (isUseCostPerShift)
                            //{
                            //    if (wCostPerShiftForMachines.ContainsKey(currAutomatName))
                            //        currProcess.CostPerShift = wCostPerShiftForMachines[currAutomatName];
                            //    else
                            //        currProcess.CostPerShift = 0;
                            //}
                            //else
                            //{
                            //    if (wCostPerShiftForMachines.ContainsKey(currAutomatName))
                            //        currProcess.CostPerShift = wCostPerShiftForMachines[currAutomatName];
                            //    else
                            //        currProcess.CostPerShift = 0;
                            //}

                            //currPlantName = wPlantLocsForMachines[currAutomatName];

                            if (!preliminaryProductionProcesses.ContainsKey(currProcess.Key))
                            {
                                preliminaryProductionProcesses.Add(currProcess.Key, currProcess);
                                if (!productsWithProcesses.Contains(currArtName))
                                    productsWithProcesses.Add(currArtName);
                            }
                            else
                            {
                            }
                        }
                    }
                }
            }

            //sw.Stop();

            msg = "Arbeitspläne fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        public void getShiftData(EmptyTaskProcEnvironment tPE,
              ref Dictionary<string, Resource> preliminaryResources,
              Logger logger,
              Dictionary<string, WEPA_Location> wPlantLocations,
            // Dictionary<string, string> wPlantLocsForMachines,
             Dictionary<string, WEPA_Resource> wResources,
               Dictionary<string, ResourceInformationState> resourcesNotConsidered,
              SNP_WEPA_Instance pI,
              CultureInfo currC,
              ref HashSet<string> usedPreliminaryResources)
        {
            Optimizer_Data.Excel_Document shiftsDoc;
            //string completeFN;
            string currAutomatName;
            Resource currResource;
            XLSTable tab;
            CapacityValue currCapVal;
            ResourceInformationState currRS;
            int i, j, spaceCharPos;
            string currTitleCellStr, currCellStr, periodName;
            double totalCapVal;
            object tempObj;
            string msg;
            WEPA_Location currLoc;
            WEPA_Resource currWResource;
            IndexedItem<Period> currPI;

            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            //completeFN = shiftsFileName;
            if (!File.Exists(shiftsFileName))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, shiftsFileName);

            msg = "Lese Schichtdaten...";
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            preliminaryResources = new Dictionary<string, Resource>();
            if (tPE.StopSignal)
                return;
            shiftsDoc = new Optimizer_Data.Excel_Document(shiftsFileName);
            tPE.addUsedResource(shiftsDoc);
            shiftsDoc.OpenDoc();

            tab = loadTableFromExcelDocument(shiftsDoc, shiftDataSheetName, shiftDataColNames, shiftDataColMatchTypes);
            shiftsDoc.CloseDoc();
            tPE.removeUsedResource(shiftsDoc);
            if (tPE.StopSignal)
                return;
            checkColumns(tab, shiftDataColNames, shiftsFileName, shiftDataSheetName);
            if (tPE.StopSignal)
                return;
            for (j = tab.ColNums[1]; j < tab.RawTable.Columns.Count; j++)
            {
                if (tPE.StopSignal)
                    return;
                currTitleCellStr = tab.RawTable.Rows[tab.RowNums[1]].ItemArray[j].ToString().Trim();
                if (currTitleCellStr != "")
                {
                    spaceCharPos = currTitleCellStr.IndexOf(" ");
                    if (spaceCharPos != -1)
                        currAutomatName = currTitleCellStr.Substring(0, spaceCharPos);
                    else
                        currAutomatName = currTitleCellStr;

                    tempObj = currAutomatName;

                    if (wResources.ContainsKey(currAutomatName))//Consider only valid resources
                    {
                        totalCapVal = 0;

                        for (i = tab.RowNums[0]; i < tab.RawTable.Rows.Count; i++)
                        {
                            periodName = tab.RawTable.Rows[i].ItemArray[tab.ColNums[0]].ToString().Trim();
                            if (periodName != "")
                            {
                                currPI = pI.getPeriod(periodName);
                                if (currPI != null)
                                {
                                    currCellStr = tab.RawTable.Rows[i].ItemArray[j].ToString().Trim();

                                    if (currCellStr != "")
                                        totalCapVal += Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                }
                            }
                        }

                        if (totalCapVal > 0)//Only if total capacity is non-zero capacity values are added=>Otherwise the resource is ignored
                        {
                            currResource = new Resource();
                            currResource.ResourceName = currAutomatName;
                            currWResource = wResources[currAutomatName];
                            currLoc = wPlantLocations[currWResource.LocationName];//[wPlantLocsForMachines[currAutomatName]];

                            currResource.LocationName = currLoc.LocationCountry + currLoc.LocationPLZ;

                            for (i = tab.RowNums[0]; i < tab.RawTable.Rows.Count; i++)
                            {
                                periodName = tab.RawTable.Rows[i].ItemArray[tab.ColNums[0]].ToString().Trim();

                                if (periodName != "")
                                {
                                    currPI = pI.getPeriod(periodName);
                                    if (currPI != null)
                                    {
                                        currCapVal = new CapacityValue();
                                        currCapVal.PeriodName = periodName;

                                        currCellStr = tab.RawTable.Rows[i].ItemArray[j].ToString().Trim();
                                        if (currCellStr != "")
                                            currCapVal.Val = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                        else
                                            currCapVal.Val = 0;

                                        currResource.addCapacity(currCapVal);
                                    }
                                }
                            }

                            preliminaryResources.Add((string)currResource.Key, currResource); //Add resource to the set of preliminary resources
                        }
                        else
                        {
                            if (!resourcesNotConsidered.ContainsKey(currAutomatName))
                            {
                                currRS = new ResourceInformationState();
                                currRS.MachineName = currAutomatName;
                                currRS.HasNoCapacity = currRS.HasNoLocation = false;
                                resourcesNotConsidered.Add(currAutomatName, currRS);
                            }
                            else
                                currRS = resourcesNotConsidered[currAutomatName];

                            currRS.HasNoCapacity = true;
                        }
                    }
                }
            }

            usedPreliminaryResources = new HashSet<string>();
            //            sw.Stop();

            msg = "Schichtdaten fertig.";// +"Zeit:" + sw.Elapsed; ;
            logger.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }
        protected const string DATE_FORMAT_STRING = "dd.MM.yyyy";

        public void getSpecialFixations(EmptyTaskProcEnvironment tPE, ref List<ProcessFilterEntry> fixList, Logger logger,
                                           CultureInfo currC)
        {
            Optimizer_Data.Excel_Document fixationDoc;
            XLSTable fixationTab;
            int i,j;
            String dateUntilStr;
            ProcessFilterEntry currPPFE;
            String currTypeStr;

            fixList = new List<ProcessFilterEntry>();

            if (fixationFileName != null)
            {  
                fixationDoc = new Optimizer_Data.Excel_Document(fixationFileName);
                tPE.addUsedResource(fixationDoc);
                fixationDoc.OpenDoc();
                fixationTab = loadTableFromExcelDocument(fixationDoc, fixationSheetName, fixationColNames, fixationColMatchTypes);
                fixationDoc.CloseDoc();
                tPE.removeUsedResource(fixationDoc);

                //tPE.removeUsedResource((IDisposable)SpecialFixations);

                if (tPE.StopSignal)
                    return;

                //foreach (DataRow r in fixationTab.)
                for (i=fixationTab .FirstRow+1;i<fixationTab.RawTable .Rows .Count;i++)
                {
                    currPPFE = new ProcessFilterEntry();
                    currTypeStr = fixationTab.RawTable.Rows[i].ItemArray[0].ToString().Trim();//r[0].ToString();

                    if (currTypeStr != "")
                    {
                        if (currTypeStr == "Transport")
                        {
                            currPPFE.ProcessType = ProcessFilterEntry.TypeOfProcess.Transport;
                        }
                        else
                        {
                            currPPFE.ProcessType = ProcessFilterEntry.TypeOfProcess.Production;
                        }

                        currPPFE.CustomerName = fixationTab.RawTable.Rows[i].ItemArray[1].ToString().Trim();//r[1]
                        currPPFE.CountryName = fixationTab.RawTable.Rows[i].ItemArray[2].ToString().Trim();//r[2]
                        currPPFE.ProductName = fixationTab.RawTable.Rows[i].ItemArray[3].ToString().Trim();//r[3]
                        currPPFE.ProductionCountryName = fixationTab.RawTable.Rows[i].ItemArray[4].ToString().Trim();//r[4]
                        currPPFE.PlantLocationName = fixationTab.RawTable.Rows[i].ItemArray[5].ToString().Trim();//r[5]
                        currPPFE.MachineName = fixationTab.RawTable.Rows[i].ItemArray[6].ToString().Trim();//r[6]

                        dateUntilStr=fixationTab.RawTable.Rows[i].ItemArray[7].ToString().Trim();
                        if (dateUntilStr != "")
                        {
                            currPPFE.IsDate = true;
                            currPPFE.ValidUntilDate = DateTime.ParseExact(dateUntilStr, DATE_FORMAT_STRING, currC);//r[7]
                        }
                        else
                            currPPFE.IsDate = false;
                        fixList.Add(currPPFE);
                    }
                }
            }
        }

        public void getSpecialExclusions(EmptyTaskProcEnvironment tPE, ref List<ProcessFilterEntry> exList, Logger logger,
                                          CultureInfo currC)
        {
            //DataTable exclusionTab;
            //ProcessFilterEntry currPPFE;
            //String currTypeStr;

            //exList = new List<ProcessFilterEntry>();

            //if (SpecialExclusions != null)
            //{
            //    tPE.addUsedResource((IDisposable)SpecialExclusions);

            //    exclusionTab = SpecialFixations.getData();

            //    tPE.removeUsedResource((IDisposable)SpecialExclusions);

            //    if (tPE.StopSignal)
            //        return;

            //    foreach (DataRow r in exclusionTab.Rows)
            //    {
            //        currPPFE = new ProcessFilterEntry();
            //        currTypeStr = r[0].ToString();

            //        if (currTypeStr == "Transport")
            //        {
            //            currPPFE.ProcessType = ProcessFilterEntry.TypeOfProcess.Transport;
            //        }
            //        else
            //        {
            //            currPPFE.ProcessType = ProcessFilterEntry.TypeOfProcess.Production;
            //        }

            //        currPPFE.CustomerName = r[1].ToString().Trim();
            //        currPPFE.CountryName = r[2].ToString().Trim();
            //        currPPFE.ProductName = r[3].ToString().Trim();
            //        currPPFE.ProductionCountryName = r[4].ToString().Trim();
            //        currPPFE.PlantLocationName = r[5].ToString().Trim();
            //        currPPFE.MachineName = r[6].ToString().Trim();
            //        currPPFE.ValidUntilDate = r[7].ToString().Trim();

            //        exList.Add(currPPFE);
            //    }
            //}
            Optimizer_Data.Excel_Document exclusionDoc;
            XLSTable exclusionTab;
            int i, j;
            String dateUntilStr;
            ProcessFilterEntry currPPFE;
            String currTypeStr;

            exList = new List<ProcessFilterEntry>();

            if (exclusionFileName != null)
            {
                exclusionDoc = new Optimizer_Data.Excel_Document(exclusionFileName);
                tPE.addUsedResource(exclusionDoc);
                exclusionDoc.OpenDoc();
                exclusionTab = loadTableFromExcelDocument(exclusionDoc, exclusionSheetName, exclusionColNames, exclusionColMatchTypes);
                exclusionDoc.CloseDoc();
                tPE.removeUsedResource(exclusionDoc);

                //tPE.removeUsedResource((IDisposable)SpecialFixations);

                if (tPE.StopSignal)
                    return;

                //foreach (DataRow r in fixationTab.)
                for (i = exclusionTab.FirstRow + 1; i < exclusionTab.RawTable.Rows.Count; i++)
                {
                    currPPFE = new ProcessFilterEntry();
                    currTypeStr = exclusionTab.RawTable.Rows[i].ItemArray[0].ToString().Trim();//r[0].ToString();
                    if (currTypeStr != "")
                    {
                        if (currTypeStr == "Transport")
                        {
                            currPPFE.ProcessType = ProcessFilterEntry.TypeOfProcess.Transport;
                        }
                        else
                        {
                            currPPFE.ProcessType = ProcessFilterEntry.TypeOfProcess.Production;
                        }

                        currPPFE.CustomerName = exclusionTab.RawTable.Rows[i].ItemArray[1].ToString().Trim();//r[1]
                        currPPFE.CountryName = exclusionTab.RawTable.Rows[i].ItemArray[2].ToString().Trim();//r[2]
                        currPPFE.ProductName = exclusionTab.RawTable.Rows[i].ItemArray[3].ToString().Trim();//r[3]
                        currPPFE.ProductionCountryName = exclusionTab.RawTable.Rows[i].ItemArray[4].ToString().Trim();//r[4]
                        currPPFE.PlantLocationName = exclusionTab.RawTable.Rows[i].ItemArray[5].ToString().Trim();//r[5]
                        currPPFE.MachineName = exclusionTab.RawTable.Rows[i].ItemArray[6].ToString().Trim();//r[6]
                        //currPPFE.ValidUntilDate = exclusionTab.RawTable.Rows[i].ItemArray[7].ToString().Trim();//r[7]
                        dateUntilStr = exclusionTab.RawTable.Rows[i].ItemArray[7].ToString().Trim();
                        if (dateUntilStr != "")
                        {
                            currPPFE.IsDate = true;
                            currPPFE.ValidUntilDate = DateTime.ParseExact(dateUntilStr, DATE_FORMAT_STRING, currC);//r[7]
                        }
                        else
                            currPPFE.IsDate = false;
                        exList.Add(currPPFE);
                    }
                }
            }
        }
    }


}
