﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SNP_WEPA;
using Optimizer_Data;
using System.Data;
using System.Globalization;

namespace SNP_Optimizer
{
    public class ProblemInstanceWriter
    {
        protected const string NUMBERS_CULTURE = "en-US";
        protected const int NUMBER_DIGITS = 4;

        public static void writeProblemInstance(SNP_WEPA_Instance pI, Optimizer_Data.OptimizerDataSource plants, 
                                                Optimizer_Data.OptimizerDataSource customers, 
                                                Optimizer_Data.OptimizerDataSource resources, 
                                                Optimizer_Data.OptimizerDataSource products, 
                                                Optimizer_Data.OptimizerDataSource periods,
                                                Optimizer_Data.OptimizerDataSource productionProcesses,
                                                Optimizer_Data.OptimizerDataSource capacities,
                                                Optimizer_Data.OptimizerDataSource interPlantTransportProcesses,
                                                Optimizer_Data.OptimizerDataSource customerTransportsProcesses,
                                               Optimizer_Data.OptimizerDataSource demands,
                                                  Optimizer_Data.OptimizerDataSource historicalProduction,
                                               Optimizer_Data.OptimizerDataSource basketSet,
                                                Optimizer_Data.OptimizerDataSource palletsPerTon,
                                                Optimizer_Data.OptimizerDataSource initialStocks,
                                                Optimizer_Data.OptimizerDataSource warehouseCapacities,
                                                Optimizer_Data.OptimizerDataSource articleNames,
                                                Optimizer_Data.OptimizerDataSource customerRelatedProductionProcesses,
                                                Optimizer_Data.OptimizerDataSource customerProductRelatedTransportationProcesses


            )
        {
            int i, j;
            double [,] capArr;
            string[] lineArr;
            double[] palPerTonArr;
            CultureInfo usC;
            List<IndexedItem<PlantLocation>> usedPlantsSNPB;
            List<IndexedItem<CustomerLocation>> usedCustomersSNPB;
            List<IndexedItem<Resource>> usedResourcesSNPB;
            List<IndexedItem<Product>> usedProductsSNPB;
            List<IndexedItem<Period>> usedPeriodsSNPB;
            List<IndexedItem<ProductionProcess>> usedProductionProcessesSNPB;
            List<IndexedItem<TransportationProcess>> usedPlantTransportationProcessesSNPB;
            List<IndexedItem<TransportationProcess>> usedCustomerTransportationProcessesSNPB;
            List<IndexedItem<DemandValue>> usedDemandsSNPB;
            List<IndexedItem<HistProductionEntry>> usedHistoricalProductionsSNPB;
            List<IndexedItem<BasketSet>> usedBasketSetsSNPB;
            List<IndexedItem<InitialStockValue>> initalStocksSNPB;
            List<IndexedItem<CustomerRelatedProductionProcess>> usedCustomerRelatedProductionProcessesSNPB;
            List<IndexedItem<CustomerProductRelatedTransportationProcess>> usedCustomerProductRelatedTransportationProcessesSNPB;
            List<IndexedItem<CapacityValue>> currCapacities;

            usC= CultureInfo.CreateSpecificCulture(NUMBERS_CULTURE);
            usedPlantsSNPB = pI.getAllPlantLocations();
            usedCustomersSNPB = pI.getAllCustomerLocations();
            usedResourcesSNPB = pI.getAllResources();
            usedProductsSNPB = pI.getAllProducts();
            usedPeriodsSNPB = pI.getAllPeriods();
            usedProductionProcessesSNPB = pI.getAllProductionProcesses();
            usedPlantTransportationProcessesSNPB = pI.getAllInterplantTransportationProcesses();
            usedCustomerTransportationProcessesSNPB = pI.getAllCustomerTransportationProcesses();
            usedDemandsSNPB = pI.getAllDemands();
            usedBasketSetsSNPB = pI.getAllBaskets();
            usedHistoricalProductionsSNPB = pI.getAllHistoricalProductions();
            initalStocksSNPB = pI.getAllInitialStocks ();
            usedCustomerRelatedProductionProcessesSNPB = pI.getAllCustomerRelatedProductionProcesses();
            usedCustomerProductRelatedTransportationProcessesSNPB = pI.getAllCustomerRelatedTransportationProcesses();

            lineArr = new string[1];
            
            foreach (IndexedItem<PlantLocation> o in usedPlantsSNPB)
            {
                lineArr[0] = o.item.Key;
                plants.writeData(lineArr);
            }

            foreach (IndexedItem<CustomerLocation> o in usedCustomersSNPB)
            {
                lineArr[0] = o.item.Key;
                customers.writeData(lineArr);
            }

            capArr=new double [usedResourcesSNPB.Count,usedPeriodsSNPB.Count];

            foreach (IndexedItem<Resource> o in usedResourcesSNPB)
            {
                lineArr[0] = o.item.Key;
                resources.writeData(lineArr);
                currCapacities=o.item.getAllCapacities ();

                foreach(IndexedItem <CapacityValue> currCap in currCapacities )
                {
                    j=pI.getPeriod(currCap .item.PeriodName).index;
                    capArr[o.index,j]=currCap.item.Val;
                }
            }

           
            palPerTonArr = new double [usedProductsSNPB.Count];
            foreach (IndexedItem<Product> o in usedProductsSNPB)
            {
                lineArr[0] = o.item.Key;
                products.writeData(lineArr);
                palPerTonArr[o.index] = o.item.PalletsPerTon;
            }

            foreach (IndexedItem<Period> o in usedPeriodsSNPB)
            {
                lineArr[0] = o.item.Key;
                periods.writeData(lineArr);
            }

            for (i = 0; i < usedProductsSNPB.Count; i++)
            {
                lineArr[0] = Convert.ToString (palPerTonArr[i],usC);
                palletsPerTon.writeData(lineArr);
            }

            lineArr = new string[5];
            IndexedItem < Resource> currResourceI;
            foreach (IndexedItem<ProductionProcess> o in usedProductionProcessesSNPB)
            {
                lineArr[0] = Convert.ToString (pI.getProduct(o.item.ProductName).index);
                lineArr[1] = Convert.ToString(pI.getResource(o.item.ResourceName).index);
                lineArr[2] = Convert.ToString(Math.Round(o.item.TonPerShift,NUMBER_DIGITS), usC);
                lineArr[3] = Convert.ToString(Math.Round(o.item.CostPerShift,NUMBER_DIGITS), usC);
                currResourceI = pI.getResource(o.item.ResourceName);
                lineArr[4] = Convert.ToString(pI.getPlantLocation(currResourceI.item .LocationName ).index); //Convert.ToString(pI.getPlantLocation(o.item.Location).index);
                productionProcesses.writeData(lineArr);
            }

            lineArr = new string[usedPeriodsSNPB.Count];

            for (i = 0; i < usedResourcesSNPB.Count; i++)
            {
                for (j = 0; j < usedPeriodsSNPB.Count; j++)
                    lineArr[j] = Convert.ToString(capArr[i, j], usC);

                capacities.writeData(lineArr);
            }

            lineArr = new string[3];
            foreach (IndexedItem<TransportationProcess> o in usedPlantTransportationProcessesSNPB)
            {
                lineArr[0] = Convert.ToString(pI.getPlantLocation(o.item.StartLocation).index);
                lineArr[1] = Convert.ToString(pI.getPlantLocation(o.item.DestLocation).index);
                lineArr[2] = Convert.ToString(Math.Round (o.item.CostPerTruck,NUMBER_DIGITS), usC);
                interPlantTransportProcesses.writeData(lineArr);
            }

            foreach (IndexedItem<TransportationProcess> o in usedCustomerTransportationProcessesSNPB)
            {
                lineArr[0] = Convert.ToString(pI.getPlantLocation(o.item.StartLocation).index);
                lineArr[1] = Convert.ToString(pI.getCustomerLocation(o.item.DestLocation).index);
                lineArr[2] = Convert.ToString(Math.Round (o.item.CostPerTruck,NUMBER_DIGITS ), usC);
                customerTransportsProcesses.writeData(lineArr);
            }

            foreach (IndexedItem<HistProductionEntry> o in usedHistoricalProductionsSNPB)
            {
                lineArr[0] = Convert.ToString(pI.getProductionProcess(o.item.ProcessName).index);
                lineArr[1] = Convert.ToString(pI.getPeriod(o.item.PeriodName).index);
                lineArr[2] = Convert.ToString(Math.Round (o.item.Quantity,NUMBER_DIGITS ), usC);
                historicalProduction.writeData(lineArr);
            }

            lineArr = new string[4];

            foreach (IndexedItem<DemandValue> o in usedDemandsSNPB )
            {
                lineArr[0] = Convert.ToString(pI.getCustomerLocation(o.item.LocationName ).index);
                lineArr[1] = Convert.ToString(pI.getProduct(o.item.ProductName).index);
                lineArr[2] = Convert.ToString(pI.getPeriod(o.item.PeriodName).index);
                lineArr[3] = Convert.ToString(Math.Round(o.item.Val,NUMBER_DIGITS ), usC);
                demands.writeData(lineArr);
            }
            List<HashSet<string>> currBaskets;

            foreach (IndexedItem<BasketSet> o in usedBasketSetsSNPB)
            {
                currBaskets=o.item.getAllBaskets();

                foreach(HashSet <string> currBasket in currBaskets )
                {
                    lineArr = new string[currBasket .Count+1 ];
                    lineArr [0]=Convert.ToString (pI.getCustomerLocation (o.item .LocationName ).index);
                    i=1;
                    foreach (string bProd in currBasket)
                    {
                       lineArr[i]=Convert.ToString (pI.getProduct (bProd).index);
                        i++;

                    }
                   basketSet .writeData (lineArr );
                }
            }

            lineArr = new string[3];

            foreach (IndexedItem<InitialStockValue> o in initalStocksSNPB)
            {
                lineArr[0] = Convert.ToString(pI.getPlantLocation(o.item.LocationName).index);
                lineArr[1] = Convert.ToString(pI.getProduct(o.item.ProductName).index);
                lineArr[2] = Convert.ToString(Math.Round(o.item.Val, NUMBER_DIGITS), usC);
                initialStocks.writeData(lineArr);
            }

            lineArr = new string[2];

            foreach (IndexedItem<PlantLocation> o in usedPlantsSNPB)
            {
                lineArr[0] = Convert.ToString(o.index ) ;
                lineArr[1] = Convert.ToString(o.item.WarehouseCapacity);

                warehouseCapacities.writeData(lineArr);
            }

            foreach (IndexedItem<Product> o in usedProductsSNPB )
            {
                lineArr[0] = o.item .ProductName;
                lineArr[1] = o.item.ProductDescription;

                articleNames.writeData(lineArr);
            }

            lineArr = new string[4];
            IndexedItem<ProductionProcess> currProcess;
            List<string> currProcessNames;
            List<string> currCustLocationNames;
            foreach (IndexedItem<CustomerRelatedProductionProcess> o in usedCustomerRelatedProductionProcessesSNPB)
            {
                currProcessNames = o.item.ProcessNames;
                currCustLocationNames =o.item .CustomerLocationNames;
                foreach (string currProcessName in currProcessNames)
                {
                    foreach (string currCustLocationName in currCustLocationNames)
                    {
                        currProcess = pI.getProductionProcess(currProcessName);
                        lineArr[0] = Convert.ToString(o.index);
                        lineArr[1] = Convert.ToString(pI.getProduct(currProcess.item.ProductName).index);
                        lineArr[2] = Convert.ToString(pI.getCustomerLocation(currCustLocationName).index);
                        lineArr[3] = Convert.ToString(currProcess.index);

                        customerRelatedProductionProcesses.writeData(lineArr);
                    }
                }
            }

            IndexedItem<TransportationProcess> currTransportationProcess;
            IndexedItem<Product> currProduct;
            lineArr = new string[3];
            foreach (IndexedItem<CustomerProductRelatedTransportationProcess> o in usedCustomerProductRelatedTransportationProcessesSNPB)
            {
                currTransportationProcess = pI.getCustomerTransportationProcess(o.item.TransportationProcessName);
                currProduct = pI.getProduct(o.item.ProductName);
                lineArr[0] = Convert.ToString(o.index);
                lineArr[1] = Convert.ToString(currProduct.index);
                lineArr[2] = Convert.ToString(currTransportationProcess.index);

                customerProductRelatedTransportationProcesses.writeData(lineArr);
            }
        }
    }
}
