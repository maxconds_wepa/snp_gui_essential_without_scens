﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptiFunctions.Data;
using System.IO;
using System.Globalization;
using System.Collections;
using System.Threading;
using System.Data;
using SNP_WEPA;
using OptiFunctions.Logging;
using OptiFunctions.Exceptions;
namespace SNP_Optimizer
{
   public  class Data_Excel_Wepa
    {
        protected class ProductInformationState
        {
            public string productName;
            public string customerName;
            public string countryName;
            public bool hasNoTranportationProcess;
            public bool hasNoProductionProcess;
        }

        protected class ProcessInformationState
        {
            public string productName;
            public string resourceName;
            public bool hasNoResourceWithCapacity;
            public bool isHistoricalProductionNeeded;
            public bool isNotExistHistoricalProduction;
        }

        protected class RawTransportProcess
        {
            public string sourceLocId;
            public string destLocId;
            public int numberOfHistoricalTrucks;
            public double avgCostPerTruck;
            public HashSet<string> productsTransported;
            public int maxNumberOfProductsOnTour2;
            public RawTransportProcess()
            {
                productsTransported = new HashSet<string>();
            }
        }

        protected Dictionary<string, string> warehouseLocsSWEurope;
       
        protected class ProductLackingInformationEntry
        {
            public Product product;
            public bool isLackingTonPerPallet=false;
            public bool isLackingTransport = false;
            public bool isLackingProductionProcess = false;
        }

        protected class ProductionProcessLackingInformationEntry
        {
            public Product product;
            public bool isLackingMachineWithCapacity = false;
        }

        protected class WEPA_Location
        {
            public string locationName;
            public string locationCountry;
            public string locationPLZ;
        }

        protected class RawDemand
        {
            public string ProductName;
            public string PeriodName;
            public string CustomerName;
            public string CountryName;
            public double Val;
        };

        protected class TourData
        {
            public string tourId;
            public double tourNumberPallets;
            public double tourPrice;
            public string sourceLocId;
            public string destLocId;
            public string customer;

            public HashSet<string> productsOnTour;

            public TourData()
            {
                productsOnTour = new HashSet<string>();
            }
        }


        protected class CustomerProductTour //To determine feasible combinations on a truck to a customer
        {
            public string product;
            public HashSet<string> tourIds;
            public bool isSingleProductTour;
            public CustomerProductTour()
            {
                tourIds = new HashSet<string>();
                isSingleProductTour = false;
            }
        }

        protected class ArticleLocationDistribution //To determine distribution of customer product demands to customer locations
        {
            public Dictionary<string, double> numberPalletsTransportedPerLocation;

            public ArticleLocationDistribution()
            {
                numberPalletsTransportedPerLocation = new Dictionary<string, double>();
            }
        }

        protected class TransportCustomerEntry
        {
            public string fullName;
            public string compactName;
            public HashSet<string> nameParts;

            public TransportCustomerEntry()
            {
                nameParts = new HashSet<string>();
            }
        }

        protected class TransportCustomerCustomerChainMatching
        {
            public string transportCustomer;
            public string customerChain;
        }

        public class EmptyTaskProcEnvironment : SubTask.ISubtaskProcEnvironment
        {
            public delegate void proc();
            public delegate void PIProc(SNP_WEPA_Instance pI);

            public proc callProc;

            public PIProc callProcPI;

            SubTask parent;

            public SNP_WEPA_Instance usedPI;

            public EmptyTaskProcEnvironment()
            {
                callProc = null;
                callProcPI = null;
            }

            public void setSubTask(SubTask sT)
            {
                parent = sT;
            }

            public void start()
            {
                if (callProc != null)
                    callProc();

                if (callProcPI != null)
                    callProcPI(usedPI);
            }

            public SubTask getSubTask(SubTaskScheduler stSched, params SubTask[] precList)
            {
                int i;
                SubTask newSubTask;

                newSubTask = new SubTask(stSched);
                newSubTask.Procedure = this;

                for (i = 0; i < precList.Length; i++)
                    newSubTask.addPredecessor(precList[i]);

                return newSubTask;
            }
        }

        protected class XLSTable
        {
            public DataTable rawTable;
            public int[] colNums;
            public int[] rowNums;
            public int firstRow;
            
            public enum MatchType {
                    EXACT,
                    SUBSTRING
            }
        }

        public const int NUMBER_PALLETS_PER_TRUCK = 33;
        public const double DEFAULT_KG_PER_PALLET = 10;
        public const int HOURS_PER_SHIFT = 8;
        public const string DUMMY_LOCATION_NAME = "DUMMY_LOC";
        public const double THRESHOLD_ACCEPTANCE_AVG_TRANSPORT_COST = 50;
        public const int MIN_PARTIAL_TRANSPORT_CUSTOMER_NAME_MATCHES = 2;
        public const int MAX_PARTIAL_TRANSPORT_CUSTOMER_NAME_MATCHES = 1;
        public char[] nameSeparators;
       
        protected bool isWarrantFeasiblityWithoutInterplantTransports =false;
        protected bool isUseHistoricalProcessIfNoTonnages = true;
        protected bool isUseCostPerShift = false;
        protected bool isUseGivenCustomerSalesToCustomerTransportRelations = true;
        protected bool isConsiderCountryInfoOfDemands =true;
        protected bool isConsiderTourChainAssignments = true;

        protected const double MAX_DBL = Double.MaxValue;
        protected  string [] MONTHS = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        protected string srcPath;
        protected string whseSouthWestEuropeFilename = "WareHouseCodes.xlsx";
        protected string locationTranslationFilename = "locsFinal.xlsx";
        protected string tonnageFileName = "Produktionstonnage 2013+2014 Werk Automat Artikel.xlsx";
        protected string baseDataAndWorkingPlansFileName = "Arbeitspläne und Stammdaten 2015-03-09angepasst.xlsx";
        protected string additionalBaseDataAndWorkingPlansFileName = "Fehlende_AP_für_SalesArtikel_2015_01_16.xlsx";
        protected string demandFileName = "sales plan 01-03.xlsx";
        protected string shiftsFileName = "Schichten.xlsx";
        protected string transpFileName = "12 - Dezember kumuliert nur Detail-Daten.xlsx";
        protected string transpSouthWestEuropeFileName = "12 - Dezember SüdWest kumuliert nur Daten.xlsx";
        protected string interPlantTranspFileName = "FD-GR-2100_EN_Freight_cost_matrix_2014.xlsx";
        protected string costPerShiftFileName = "Produktionskosten_2015-01-16.xlsx";
        protected string costPerTonFileName = "Produktionskosten_2015-02-09.xlsx";
        protected string customerChainTransportCustomerRelationsFileName = "Kunde-Kette.xlsx";
        protected string tourChainFilename = "Aktuelle Frachten Eurolog 2014-12_mit Kette.xlsx";
        protected CultureInfo currC;
       
        protected string plantmacSheetName = "PlantMachines";
        protected string[] plantMacColNames = { "Plant", "Machine" };
        protected XLSTable.MatchType[] plantMacColMatchType = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT};
        protected Excel_Document2007.COL_Type[] plantMacColTypes = { Excel_Document2007.COL_Type.STR_COL, Excel_Document2007.COL_Type.STR_COL };


        protected string locSheetName = "Locations";
        protected string[] locColNames = { "Name","Country", "PLZ_Part1"};
        protected XLSTable.MatchType[] locColMatchType = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };
        protected Excel_Document2007.COL_Type[] locColTypes = { Excel_Document2007.COL_Type.STR_COL, Excel_Document2007.COL_Type.STR_COL, Excel_Document2007.COL_Type.STR_COL, Excel_Document2007.COL_Type.STR_COL };

        protected LoggerCollection loggers;

        protected string matchingFileName = "matchings.csv";
        protected string csvDelim = ";";
        protected string[] matchingHL = { "Transport Customer", "Customer chain" };

        protected static readonly string[,] countryTab = { {"Algeria","DZ" },
                                           {"Austria","AT"},
                                           {"Belgium","BE"},
                                           {"Bosnia And Herzegowina","BA"},
                                           {"Bulgaria","BG"},
                                           {"Cameroon","CM"},
                                           {"Croatia","HR"},
                                           {"Cyprus","CY"},
                                           {"Czech Republic","CZ"},
                                           {"Denmark","DK"},
                                           {"Estonia","EE"},
                                           {"Finland","FI"},
                                           {"France","FR"},
                                           {"Germany","DE"},
                                           {"Greece","GR"},
                                           {"Hungary","HU"},
                                           {"Ireland","IE"},
                                           {"Iceland","IS"},
                                           {"Israel","IL"},
                                           {"Italy","IT"},
                                           {"Latvia","LV"},
                                           {"Lithuania","LT"},
                                           {"Luxembourg","LU"},
                                           {"Moldova","MD"},
                                           {"Morocco","MA"},
                                           {"Netherlands","NL"},
                                           {"Norway","NO"},
                                           {"Poland","PL"},
                                           {"Portugal","PT"},
                                           {"Romania","RO"},
                                           {"Russian Federation","RU"},
                                           {"Slovakia","SK"},
                                           {"Slovenia","SI"},
                                           {"Spain","ES"},
                                           {"Sweden","SE"},
                                           {"Switzerland","CH"},
                                           {"Ukraine","UA"},
                                           {"United Kingdom (UK)","GB"}
                                         };

        protected string prodSheetName = "Products";
        protected string[] prodColNames = { "Product", "PalletsPerTon" };
        protected Excel_Document2007.COL_Type[] prodColTypes = { Excel_Document2007.COL_Type.STR_COL, Excel_Document2007.COL_Type.DOUBLE_COL };

        protected string prodProcSheetName = "ProductionProcesses";
        protected string[] prodProcColNames = { "Product", "Resource", "TonPerShift", "CostPerShift", "Location" };
        protected Excel_Document2007.COL_Type[] prodProcColTypes = { Excel_Document2007.COL_Type.STR_COL, Excel_Document2007.COL_Type.STR_COL, Excel_Document2007.COL_Type.DOUBLE_COL, Excel_Document2007.COL_Type.DOUBLE_COL, Excel_Document2007.COL_Type.STR_COL };

        protected string capSheetName = "ResourcesAndCapacities";
        protected string[] capColNames = { "Resource", "Period", "Capacity" };
        protected Excel_Document2007.COL_Type[] capColTypes = { Excel_Document2007.COL_Type.STR_COL, Excel_Document2007.COL_Type.STR_COL, Excel_Document2007.COL_Type.DOUBLE_COL };

        protected string customerChainSheetName = "Kunde-Kette";
        protected string[] customerChainColNames = { "Kette", "Kunde" };
        protected XLSTable.MatchType[] customerChainColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string rsSheetName = "Resources";
        protected string[] rsColNames = { "Resource", "Location" };
        protected XLSTable.MatchType[] rsColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };
       
        protected string costPerShiftSheetName = "Tagesproduktion";
        protected string[] costPerShiftColNames = { "productionline", "€/Tonne neu", "€/Stunde alt" };
        protected XLSTable.MatchType[] costPerShiftColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };
       
        protected string costPerShiftNeuSheetName = "Tagesproduktion";
        protected string[] costPerShiftNeuColNames = { "productionline", "€/Tonne neu", "€/Stunde alt" };
        protected XLSTable.MatchType[] costPerShiftNeuColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string transpNESheetName = "transdata";
        protected string[] transpNEColNames = { "Art", "Tour Nummer", "# Paletten 2", "Preis", "Bel. - LKZ", "Bel. - PLZ", "Bel. - 2 PLZ", "Entl. - LKZ", "Entl. - PLZ", "Article No", "Empfänger" };
        protected XLSTable.MatchType[] transpNEColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, 
                                                                XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, 
                                                                XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

        protected string transpSWSheetName = "Süd-West";
        protected string[] transpSWColNames = { "Warehouse", "Date of Expedition", "Destination Corporate Name", "Corporate Name", "CAP", "Country Dest", "Item Code", "Qty Pallets", "Amount", "Kette" };
        protected XLSTable.MatchType[] transpSWColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT,
                                                          XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT};

        protected string whseSWSheetName = "Tabelle1";
        protected string[] whseSWColNames = { "Code","country","postal code"};
        protected XLSTable.MatchType[] whseSWColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

       protected string interPlantTranspSheetName = "Finished goods";
       protected string[] interPlantTranspColNames = { "from / to" };
       protected XLSTable.MatchType[] interPlantTranspColMatchTypes = { XLSTable.MatchType.EXACT };

       protected string demandSheetName = "detail";
       protected string[] demandColNames = { "no. plan", "customer", "country", "prod. Line", "plan (pallets)", "description" };
       protected XLSTable.MatchType[] demandColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT
                                                        , XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT};

       protected string tonnageSheetName = "Tabelle1";
       protected string[] tonnageColNames = { "converting in ton", "Automat", "Werk", "Artikel" };
       protected XLSTable.MatchType[] tonnageColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, 
                                                         XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

       protected string workingPlansSheetName = "detail";
       protected string[] workingPlansColNames = { "article number", "paper weight / unit (kg)", "t/shift"};
       protected XLSTable.MatchType[] workingPlansColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.SUBSTRING };
       
       protected string addWorkingPlansSheetName = "Tabelle1";
       protected string[] addWorkingPlansColNames = { "Artikelnummern aus Salesplan", "t/Schicht mit alten Stammdaten", "t/Schicht", "Automat" };
       protected XLSTable.MatchType[] addWorkingPlansColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };

       protected string shiftDataSheetName = "settings capacity";
       protected string[] shiftDataColNames = { "January", "("};
       protected XLSTable.MatchType[] shiftDataColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.SUBSTRING };

       protected string tourChainSheetName = "data";
       protected string[] tourChainColNames = { "Tournr./Transportnr.", "Kette" };
       protected XLSTable.MatchType[] tourChainColMatchTypes = { XLSTable.MatchType.EXACT, XLSTable.MatchType.EXACT };
       
       protected int numberOfParallelThreads=1;

        protected HashSet<string> articlesWithTonnages;
        protected Dictionary<string, ProductionProcess> preliminaryProductionProcesses;
        protected Dictionary<string, ProductionProcess> tonnageProductionProcesses;
        protected Dictionary<string, Resource> preliminaryResources;
        protected Dictionary<string, Product> preliminaryProducts;
        protected Dictionary<string, string> preliminaryProductsNames;
        protected Dictionary<string, ProductLackingInformationEntry> productsWithLackingInformation;
        protected Dictionary<string, ProductionProcessLackingInformationEntry> productionProcessesWithLackingInformation;
        protected Dictionary<string, HistProductionEntry> preliminaryHistoricalProductions;
        protected Dictionary<string, HashSet<string>> articlesWithDemandsOfCustomer;
        protected HashSet<string> usedPreliminaryResources;
        protected Dictionary<string, RawDemand> rawDemands;
        protected HashSet<string> customersWithDemands;
        protected Dictionary<string, WEPA_Location> wPlantLocations;
        protected Dictionary<string, string> wPlantLocsForMachines;
        protected Dictionary<string, double> wCostPerShiftForMachines;
        protected Dictionary<string, double> wCostPerTonForMachines;
        protected Dictionary<string, List<TransportCustomerEntry>> customerChainTransportCustomersRelations;
        protected HashSet<string> historicalProductionProcesses;
        protected HashSet<string> wPlants;
        protected HashSet<string> productsWithProcesses;
        protected Dictionary<string, HashSet<string>> transportCustomerCustomerChainMatchings;
        protected Dictionary<string, TourData> tours; //ok: Für Aufnahme der Gesamtmenge an Touren
        protected Dictionary<string, HashSet<string>> toursForCustomer; //ok: für Warenkörbe gebraucht
        protected Dictionary<string, ArticleLocationDistribution> locationDistributionPerCustomerProduct; //ok: für Verteilung der Nachfrage auf Kundenstandorte gebraucht
        protected Dictionary<string, ArticleLocationDistribution> locationDistributionPerCustomer; //ok: für Verteilung der Nachfrage auf Kundenstandorte gebraucht (wenn keine produktspezifischen Transporte vorhanden sind)
        protected XLSTable histTranspTabNE, histTranspTabSW, whseSWEuropeTab;
        Dictionary<string, WEPA_Location> wCustomerLocations;
        Dictionary<string, List<string>> transportProcessesToLocation; //ok: für Transprte zu Kundenstandort
        List<string> currTransportProcessesToDestLocation; //ok: Für aktuelle Transporte zu einem Kundenstandort
        Dictionary<string, RawTransportProcess> transportProcesses; //ok: für Transporte
        Dictionary<string, HashSet<string>> productsForCustomerLocation; //ok: Für Prüfung bei kundenstandortbezogenen Warenkörben, ob Produkt an den Standort geliefert wird.
        //SNP_WEPA_Instance pI;
        HashSet<string> productLocation; //ok: Für Prüfung, ob Fabrikstandort für Produktion überhaupt benötigt wird und damit ein Transport angelegt werden soll.
        protected Dictionary<string, string> tourChainAssignments;

        Dictionary<string, ProductInformationState> productsNotConsidered;
        Dictionary<string, ProcessInformationState> processesNotConsidered;
        Dictionary<string, HashSet<string>> relevantProductsForCustomer;

        protected int firstMonth;
        protected int lastMonth;

        

        //protected string whseSouthWestEuropeFilename = "WareHouseCodes.xlsx";
        //protected string locationTranslationFilename = "locsFinal.xlsx";
        //protected string tonnageFileName = "Produktionstonnage 2013+2014 Werk Automat Artikel.xlsx";
        //protected string baseDataAndWorkingPlansFileName = "Arbeitspläne und Stammdaten 2015-03-09angepasst.xlsx";
        //protected string additionalBaseDataAndWorkingPlansFileName = "Fehlende_AP_für_SalesArtikel_2015_01_16.xlsx";
        //protected string demandFileName = "sales plan 01-03.xlsx";
        //protected string shiftsFileName = "Schichten.xlsx";
        //protected string transpFileName = "12 - Dezember kumuliert nur Detail-Daten.xlsx";
        //protected string transpSouthWestEuropeFileName = "12 - Dezember SüdWest kumuliert nur Daten.xlsx";
        //protected string interPlantTranspFileName = "FD-GR-2100_EN_Freight_cost_matrix_2014.xlsx";
        //protected string costPerShiftFileName = "Produktionskosten_2015-01-16.xlsx";
        //protected string costPerTonFileName = "Produktionskosten_2015-02-09.xlsx";
        //protected string customerChainTransportCustomerRelationsFileName = "Kunde-Kette.xlsx";
        //protected string tourChainFilename = "Aktuelle Frachten Eurolog 2014-12_mit Kette.xlsx";

        public SNP_WEPA_Instance getProblemInstance(string whseSouthWestEuropeFN,
            string locationTranslationFN,
            string tonnageFN,
            string baseDataAndWorkingPlansFN,
            string additionalBaseDataAndWorkingPlansFN,
            string demandFN,
            string shiftsFN,
            string transpFN,
            string transpSouthWestEuropeFN,
            string interPlantTranspFN,
            string costPerShiftFN,
            string costPerTonFN,
            string customerChainTransportCustomerRelationsFN,
            string tourChainFN)
        {
            SNP_WEPA_Instance pI;
            int i;
            string objKey;
            string msg;
            Location currLoc;
            int mSepIndex;
            EmptyTaskProcEnvironment locsLocsMachinesPE, costPerShiftPE, customerChainTransportCustomersRelationsPE, demandDataPE, shiftDataPE, tonnageDataPE, baseDataAndWorkingPlansPE, tpNEEuropePE, tpSWEuropePE,whseSWEuropePE;
            SubTask locsLocsMachinesST, costPerShiftST, customerChainTransportCustomersRelationsST, demandDataST, shiftDataST, tonnageDataST, baseDataAndWorkingPlansST, tpNEEuropeST, tpSWEuropeST, whseSWEuropeST;
            SubTaskScheduler usedSTSched;
            EmptyTaskProcEnvironment transportDataPE;
            SubTask transportDataST;
            EmptyTaskProcEnvironment productionProcessesPE;
            SubTask productionProcessesST;
            EmptyTaskProcEnvironment transportProcessesPE;
            SubTask transportProcessesST;
            EmptyTaskProcEnvironment customerDemandsPE;
            SubTask customerDemandsST;
            EmptyTaskProcEnvironment basketsPE;
            SubTask basketsST;
            EmptyTaskProcEnvironment interplantTransportsPE;
            SubTask interplantTransportsST;
            EmptyTaskProcEnvironment tourChainPE;
            SubTask tourChainST;
            SubTaskScheduler.SUBTASK_SCHED_RESULT result;

            whseSouthWestEuropeFilename =  whseSouthWestEuropeFN;
            locationTranslationFilename =  locationTranslationFN;
            tonnageFileName = tonnageFN;
            baseDataAndWorkingPlansFileName = baseDataAndWorkingPlansFN;
            additionalBaseDataAndWorkingPlansFileName = additionalBaseDataAndWorkingPlansFN;
            demandFileName = demandFN;
            shiftsFileName = shiftsFN;
            transpFileName = transpFN;
            transpSouthWestEuropeFileName = transpSouthWestEuropeFN;
            interPlantTranspFileName = interPlantTranspFN;
            costPerShiftFileName = costPerShiftFN;
            costPerTonFileName= costPerTonFN;
            customerChainTransportCustomerRelationsFileName = customerChainTransportCustomerRelationsFN;
            tourChainFilename = tourChainFN;

            mSepIndex=demandFileName.IndexOf("-");
            firstMonth = Convert.ToInt32 (demandFileName.Substring (mSepIndex-2,2),currC )-1;
            lastMonth = Convert.ToInt32(demandFileName.Substring(mSepIndex+1, 2), currC)-1;


            productsNotConsidered = new Dictionary<string, ProductInformationState>();
            processesNotConsidered = new Dictionary<string, ProcessInformationState>();

            tourChainAssignments = new Dictionary<string, string>();
            articlesWithTonnages = new HashSet<string>();
            usedSTSched=new SubTaskScheduler  ();
            usedSTSched.MaxNumberRunningInParallel = numberOfParallelThreads;

            msg = "Initialisierungen...";

            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

            productsWithLackingInformation = new Dictionary<string, ProductLackingInformationEntry>();
            productionProcessesWithLackingInformation = new Dictionary<string, ProductionProcessLackingInformationEntry>();
            pI = new SNP_WEPA_Instance();
            
            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

            

            locsLocsMachinesPE = new EmptyTaskProcEnvironment();
            locsLocsMachinesPE.callProc = getLocationsAndLocsMachines;

            locsLocsMachinesST = locsLocsMachinesPE.getSubTask(usedSTSched);

            usedSTSched.addSubTask(locsLocsMachinesST);

            costPerShiftPE = new EmptyTaskProcEnvironment();
            
            if (isUseCostPerShift)
            {
                costPerShiftPE.callProc = getCostPerShiftMachines;
            }
            else
            {
                costPerShiftPE.callProc = getCostPerShiftMachinesNeu;
            }
            
            costPerShiftST = costPerShiftPE.getSubTask(usedSTSched);
            
            usedSTSched.addSubTask(costPerShiftST);

            if (isUseGivenCustomerSalesToCustomerTransportRelations)
            {
                customerChainTransportCustomersRelationsPE = new EmptyTaskProcEnvironment();
                customerChainTransportCustomersRelationsPE.callProc = getCustomerChainTransportCustomersRelations;
                customerChainTransportCustomersRelationsST = customerChainTransportCustomersRelationsPE.getSubTask(usedSTSched);

                usedSTSched.addSubTask(customerChainTransportCustomersRelationsST);
            }
            else
                customerChainTransportCustomersRelationsST = null;

            demandDataPE = new EmptyTaskProcEnvironment();
            demandDataPE.callProcPI = getDemandData;
            demandDataPE.usedPI = pI;
            demandDataST = demandDataPE.getSubTask(usedSTSched);
            usedSTSched.addSubTask(demandDataST);
           
            shiftDataPE = new EmptyTaskProcEnvironment();
            shiftDataPE.callProcPI = getShiftData;
            shiftDataPE.usedPI = pI;
            shiftDataST = shiftDataPE.getSubTask(usedSTSched, locsLocsMachinesST);
            shiftDataST.addPredecessor(demandDataST);
            usedSTSched.addSubTask(shiftDataST);

            tonnageDataPE = new EmptyTaskProcEnvironment();
            tonnageDataPE.callProcPI = getTonnageData;
            tonnageDataPE.usedPI = pI;
            tonnageDataST = tonnageDataPE.getSubTask(usedSTSched, shiftDataST);
            tonnageDataST.addPredecessor(shiftDataST);
            usedSTSched.addSubTask(tonnageDataST);

            baseDataAndWorkingPlansPE = new EmptyTaskProcEnvironment();
            baseDataAndWorkingPlansPE.callProcPI = getBaseDataAndWorkingPlans;
            baseDataAndWorkingPlansPE.usedPI = pI;
            baseDataAndWorkingPlansST = baseDataAndWorkingPlansPE.getSubTask(usedSTSched, tonnageDataST, demandDataST);
            baseDataAndWorkingPlansST.addPredecessor(tonnageDataST);
            baseDataAndWorkingPlansST.addPredecessor(costPerShiftST);
           
            usedSTSched.addSubTask(baseDataAndWorkingPlansST);

            tpNEEuropePE=new EmptyTaskProcEnvironment ();
            tpNEEuropePE.callProc = readTransportsNE;
            tpNEEuropeST = tpNEEuropePE.getSubTask(usedSTSched);

            tpSWEuropePE=new EmptyTaskProcEnvironment ();
            tpSWEuropePE.callProc = readTransportsSW;
            tpSWEuropeST = tpSWEuropePE.getSubTask(usedSTSched);

          //  tpSWEuropePE = new EmptyTaskProcEnvironment();
//            tpSWEuropePE.callProc = readTransportsSW;
  //          tpSWEuropeST = tpSWEuropePE.getSubTask(usedSTSched);

           // usedSTSched.addSubTask(tpSWEuropeST);

            whseSWEuropePE=new EmptyTaskProcEnvironment ();
            whseSWEuropePE.callProc = readWHSESWEurope;
            whseSWEuropeST = whseSWEuropePE.getSubTask(usedSTSched);

            usedSTSched.addSubTask(tpNEEuropeST);
            usedSTSched.addSubTask(tpSWEuropeST);
            usedSTSched.addSubTask(whseSWEuropeST);

           
            transportDataPE = new EmptyTaskProcEnvironment();
          
            transportDataPE.callProcPI = getTransportData;
            transportDataPE.usedPI = pI;
            transportDataST = transportDataPE.getSubTask(usedSTSched);

            transportDataST.addPredecessor (tpNEEuropeST);
            transportDataST.addPredecessor (tpSWEuropeST);
           
            transportDataST.addPredecessor (whseSWEuropeST);
            transportDataST.addPredecessor(demandDataST);
            transportDataST.addPredecessor(baseDataAndWorkingPlansST);

            if (isConsiderTourChainAssignments)
            {
                tourChainPE = new EmptyTaskProcEnvironment();
                tourChainPE.callProc = getTourChainAssignments;
                tourChainST = tourChainPE.getSubTask(usedSTSched);
                usedSTSched.addSubTask(tourChainST);
                transportDataST.addPredecessor(tourChainST);
            }

            if (isUseGivenCustomerSalesToCustomerTransportRelations)
                transportDataST.addPredecessor(customerChainTransportCustomersRelationsST);
            usedSTSched.addSubTask(transportDataST);

            productionProcessesPE = new EmptyTaskProcEnvironment();
            productionProcessesPE.callProc = prepareProductionProcesses;
            productionProcessesST = productionProcessesPE.getSubTask(usedSTSched);
            productionProcessesST.addPredecessor(transportDataST);
            usedSTSched.addSubTask(productionProcessesST);

            transportProcessesPE = new EmptyTaskProcEnvironment();
            transportProcessesPE.callProc  = prepareTransportationProcesses ;
            transportProcessesST = transportProcessesPE.getSubTask(usedSTSched);
            transportProcessesST.addPredecessor(transportDataST);
            usedSTSched.addSubTask(transportProcessesST);

            customerDemandsPE = new EmptyTaskProcEnvironment();
            customerDemandsPE.callProcPI = prepareCustomerDemands;
            customerDemandsPE.usedPI = pI;
            customerDemandsST = customerDemandsPE.getSubTask(usedSTSched);
            customerDemandsST.addPredecessor(transportProcessesST);
            usedSTSched.addSubTask(customerDemandsST);

            basketsPE = new EmptyTaskProcEnvironment();
            basketsPE.callProcPI = prepareBaskets ;
            basketsPE.usedPI = pI;
            basketsST = basketsPE.getSubTask(usedSTSched);
            basketsST.addPredecessor(customerDemandsST);
            usedSTSched.addSubTask(basketsST);

            interplantTransportsPE = new EmptyTaskProcEnvironment();
            interplantTransportsPE.callProcPI = getInterplantTransports;
            interplantTransportsPE.usedPI = pI;
            interplantTransportsST = interplantTransportsPE.getSubTask(usedSTSched);
            interplantTransportsST.addPredecessor(locsLocsMachinesST);
            usedSTSched.addSubTask(interplantTransportsST);
            result=usedSTSched.execSubTaskSet();

            if (result == SubTaskScheduler.SUBTASK_SCHED_RESULT.FINISHED_OK)
            {
                i = 0;

                foreach (KeyValuePair<string, ProductionProcess> kP in preliminaryProductionProcesses)
                {
                    ProductionProcess currProcess;

                    currProcess = kP.Value;
                    if (!isWarrantFeasiblityWithoutInterplantTransports && pI.getPlantLocation(currProcess.LocationName) == null)
                    {
                        currLoc = new Location();
                        currLoc.LocationName = currProcess.LocationName;
                        pI.addPlantLocation(currLoc);
                    }

                    if (pI.getProduct(currProcess.ProductName) != null && pI.getPlantLocation(currProcess.LocationName) != null)
                        pI.addProductionProcess(currProcess);
                }

                foreach (KeyValuePair<string, Resource> kR in preliminaryResources)
                {
                    Resource currResource;
                    currResource = kR.Value;

                    objKey = kR.Key;
                    if (usedPreliminaryResources.Contains(objKey))
                    {
                        pI.addResource(currResource);
                    }
                }

                foreach (KeyValuePair<string, HistProductionEntry> cHPET in preliminaryHistoricalProductions)
                {
                    HistProductionEntry currHPE = cHPET.Value;
                    if (pI.getProductionProcess(currHPE.ProcessName) != null)
                        pI.addHistoricalProduction(currHPE);
                }
            }
            else
            {
                if (locsLocsMachinesST== usedSTSched .FirstSubTaskWithException && locsLocsMachinesST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                    throw locsLocsMachinesST.Exc ;

                if (costPerShiftST == usedSTSched .FirstSubTaskWithException  && costPerShiftST.CurrentState ==SubTask .SubTaskState .FINISHED_ERROR )
                    throw costPerShiftST.Exc;

                if (customerChainTransportCustomersRelationsST== usedSTSched .FirstSubTaskWithException  &&customerChainTransportCustomersRelationsST.CurrentState ==SubTask .SubTaskState .FINISHED_ERROR )
                    throw  customerChainTransportCustomersRelationsST.Exc;

                if (demandDataST== usedSTSched .FirstSubTaskWithException  && demandDataST.CurrentState ==SubTask .SubTaskState .FINISHED_ERROR )
                    throw demandDataST.Exc;

                if (shiftDataST== usedSTSched .FirstSubTaskWithException  &&shiftDataST.CurrentState ==SubTask .SubTaskState .FINISHED_ERROR )
                    throw shiftDataST.Exc;

                if (tonnageDataST== usedSTSched .FirstSubTaskWithException  &&tonnageDataST.CurrentState ==SubTask .SubTaskState .FINISHED_ERROR )
                    throw tonnageDataST.Exc;

                if (baseDataAndWorkingPlansST== usedSTSched .FirstSubTaskWithException  &&baseDataAndWorkingPlansST.CurrentState ==SubTask .SubTaskState .FINISHED_ERROR )
                    throw baseDataAndWorkingPlansST.Exc;

                if (tpNEEuropeST== usedSTSched .FirstSubTaskWithException  &&tpNEEuropeST.CurrentState ==SubTask .SubTaskState .FINISHED_ERROR )
                    throw tpNEEuropeST.Exc;

                if (tpSWEuropeST== usedSTSched .FirstSubTaskWithException  &&tpSWEuropeST.CurrentState ==SubTask .SubTaskState .FINISHED_ERROR )
                    throw tpSWEuropeST.Exc;

                if (whseSWEuropeST == usedSTSched.FirstSubTaskWithException && whseSWEuropeST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                    throw  whseSWEuropeST.Exc;

            }

            //Dictionary<string, ProductInformationState> productsNotConsidered;
            //Dictionary<string, ProcessInformationState> processesNotConsidered;
            //Dictionary<string, HashSet<string>> relevantProductsForCustomer;

            DatTable productsNotConsideredTab,processesNotConsideredTab,productDescTab;

            productsNotConsideredTab=new DatTable ();
            productsNotConsideredTab.numberOfRows = productsNotConsidered.Count;
            productsNotConsideredTab.numberOfColumns = 5;

            productsNotConsideredTab.cells = new string[productsNotConsideredTab.numberOfRows, productsNotConsideredTab.numberOfColumns];
            i = 0;
            foreach (KeyValuePair<string,ProductInformationState> pE in productsNotConsidered)
            {
                productsNotConsideredTab.cells[i, 0] = pE.Value.productName;
                productsNotConsideredTab.cells[i, 1] = pE.Value.customerName;
                productsNotConsideredTab.cells[i, 2] = pE.Value.countryName;

                if (pE.Value.hasNoProductionProcess)
                    productsNotConsideredTab.cells[i, 3] = "1";
                else
                    productsNotConsideredTab.cells[i, 3] = "0";

                if (pE.Value.hasNoTranportationProcess )
                    productsNotConsideredTab.cells[i, 4] = "1";
                else
                    productsNotConsideredTab.cells[i, 4] = "0";

                i++;
            }

            CSV_File productsNotConsideredFile;
            string productsNotConsideredFilename = "productsNotConsidered.csv";
            productsNotConsideredFile = new CSV_File(ref productsNotConsideredFilename);
            productsNotConsideredFile.setIsHeadLine(false);
            productsNotConsideredFile.OpenForWriting(false);
            productsNotConsideredFile.ArrayToTable(productsNotConsideredTab);
            productsNotConsideredFile.CloseDoc();

            processesNotConsideredTab.numberOfRows = processesNotConsidered.Count;
            processesNotConsideredTab.numberOfColumns = 5;
            processesNotConsideredTab.cells = new string[processesNotConsideredTab.numberOfRows, processesNotConsideredTab.numberOfColumns];
            i = 0;
            foreach (KeyValuePair<string, ProcessInformationState> prcE in processesNotConsidered)
            {
                processesNotConsideredTab.cells[i, 0] = prcE.Value.productName;
                processesNotConsideredTab.cells[i, 1] = prcE.Value.resourceName;

                if (prcE.Value.hasNoResourceWithCapacity)
                    processesNotConsideredTab.cells[i, 2] = "1";
                else
                    processesNotConsideredTab.cells[i, 2] = "0";

                if (prcE.Value.isHistoricalProductionNeeded )
                    processesNotConsideredTab.cells[i, 3] = "1";
                else
                    processesNotConsideredTab.cells[i, 3] = "0";

                if (prcE.Value.isNotExistHistoricalProduction)
                    processesNotConsideredTab.cells[i, 4] = "1";
                else
                    processesNotConsideredTab.cells[i, 4] = "0";

                i++;
            }

            CSV_File processesNotConsideredFile;
            string processesNotConsideredFilename = "processesNotConsidered.csv";
            processesNotConsideredFile = new CSV_File(ref processesNotConsideredFilename);
            processesNotConsideredFile.setIsHeadLine(false);
            processesNotConsideredFile.OpenForWriting(false);
            processesNotConsideredFile.ArrayToTable(processesNotConsideredTab);
            processesNotConsideredFile.CloseDoc();

            List<IndexedItem<Product>> relevantProducts = pI.getAllProducts();

            productDescTab = new DatTable();
            productDescTab.numberOfRows = relevantProducts.Count;
            productDescTab.numberOfColumns = 2;
            productDescTab.cells = new string[productDescTab.numberOfRows, productDescTab.numberOfColumns];
            i = 0;
            foreach (IndexedItem<Product> pEl in relevantProducts)
            {
                productDescTab.cells[i, 0] = pEl.item.ProductName;
                productDescTab.cells[i, 1] = preliminaryProductsNames[pEl.item.ProductName];
                i++;
            }

            CSV_File productDescFile;
            string productDescFilename = "artikelnamen.csv";
            productDescFile = new CSV_File(ref productDescFilename);
            productDescFile.setIsHeadLine(false);
            productDescFile.OpenForWriting(false);

            productDescFile.setCSVDelimier(ref csvDelim);
            productDescFile.ArrayToTable(productDescTab);
            productDescFile.CloseDoc();

            return pI;
        }

       

        protected XLSTable loadTableFromExcelDocument( Optimizer_Data.Excel_Document xlsDoc, 
                                                       string sheetName, 
                                                       string[] colNames,
                                                       XLSTable .MatchType [] colMatchTypes
                                                       )
        {
            string uLC, lRC;
            XLSTable resultXTab;
            int i, j,k;
            string currCellStr;
            
            uLC = "A1";
            lRC="ZZ1000000";

            xlsDoc.setSheetData(sheetName, uLC, lRC);

            resultXTab = new XLSTable();
            resultXTab.colNums = new int[colNames.Length];
            resultXTab.rowNums = new int[colNames.Length];

            resultXTab.rawTable = xlsDoc.getData();

            resultXTab.firstRow = 0;
            i = j = 0;
            for (k = 0; k < colNames.Length; k++)
            {
                for (i = 0; i < resultXTab.rawTable.Rows.Count; i++)
                {
                    for (j = 0; j < resultXTab.rawTable.Rows[i].ItemArray.Length; j++)
                    {
                        currCellStr = resultXTab.rawTable.Rows[i].ItemArray[j].ToString().Trim();//resultXTab.rawTable.cells[i, j].Trim();

                        if (
                            (colNames[k] == "*" && currCellStr != "") || 
                            (colMatchTypes[k]==XLSTable .MatchType.EXACT && currCellStr==colNames[k])||
                            (colMatchTypes[k]==XLSTable .MatchType .SUBSTRING && currCellStr.Contains(colNames[k]))
                            )
                        {
                            resultXTab.colNums[k] = j;
                            resultXTab.rowNums[k] = i;
                            if (resultXTab.firstRow < i)
                                resultXTab.firstRow = i;

                            break;
                        }
                    }
                    if (j < resultXTab.rawTable.Columns.Count)//(j < resultXTab.rawTable.numberOfColumns)
                        break;
                }
                if (i == resultXTab.rawTable.Rows.Count || 
                    j == resultXTab.rawTable.Columns.Count)//(i == resultXTab.rawTable.numberOfRows || j == resultXTab.rawTable.numberOfColumns)
                {
                    resultXTab.colNums[k] = -1;
                    resultXTab.rowNums[k] = -1;
                }
            }

            return resultXTab;
        }

        protected void getLocationsAndLocsMachines()
        {
            Optimizer_Data.Excel_Document locsDoc;
            string completeFN;
           
            string msg;
            
            msg = "Lese Standorte und Maschinen...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
           
          
            string currLocId;
            completeFN = srcPath + Path.DirectorySeparatorChar + locationTranslationFilename;
          
            WEPA_Location currLoc;
            XLSTable locsTab,locMacTab;
            int i;

            wPlants = new HashSet<string>();
            wPlantLocations = new Dictionary<string, WEPA_Location>();
            wPlantLocsForMachines = new Dictionary<string, string>();

            if (File.Exists(completeFN))
            {
                locsDoc = new Optimizer_Data.Excel_Document(completeFN);

                locsDoc.OpenDoc();
                locsTab = loadTableFromExcelDocument(locsDoc, locSheetName, locColNames, locColMatchType);
                string currLocName;
                for (i = locsTab.firstRow + 1; i < locsTab.rawTable.Rows.Count; i++)//(i = locsTab.firstRow + 1; i < locsTab.rawTable.numberOfRows; i++)
                {
                    currLocName=locsTab.rawTable.Rows[i].ItemArray[locsTab.colNums[0]].ToString().Trim();
                    if (currLocName != "")
                    {
                        currLoc = new WEPA_Location();
                        currLoc.locationName = currLocName;//  [i, locsTab.colNums[0]];//locsTab.rawTable.cells[i, locsTab.colNums[0]];
                        currLoc.locationCountry = locsTab.rawTable.Rows[i].ItemArray[locsTab.colNums[1]].ToString().Trim(); //locsTab.rawTable.cells[i, locsTab.colNums[1]];
                        currLoc.locationPLZ = locsTab.rawTable.Rows[i].ItemArray[locsTab.colNums[2]].ToString().Trim();//locsTab.rawTable.cells[i, locsTab.colNums[2]];

                        currLocId = currLoc.locationCountry + currLoc.locationPLZ;
                        if (!wPlants.Contains(currLocId))
                            wPlants.Add(currLocId);

                        wPlantLocations.Add(currLoc.locationName, currLoc);
                    }
                }

                locMacTab = loadTableFromExcelDocument(locsDoc, plantmacSheetName, plantMacColNames, plantMacColMatchType);
                string currMacName;
                for (i = locMacTab.firstRow + 1; i < locMacTab.rawTable.Rows.Count; i++)
                {
                    currMacName=locMacTab.rawTable.Rows[i].ItemArray[locMacTab.colNums[1]].ToString().Trim();
                    if (currMacName != "")
                    {
                        if (!wPlantLocsForMachines.ContainsKey (currMacName))

                        wPlantLocsForMachines.Add(currMacName,
                        locMacTab.rawTable.Rows[i].ItemArray[locMacTab.colNums[0]].ToString().Trim());
                    }
                }

                locsDoc.CloseDoc();
            }
            else
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, locationTranslationFilename);
            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
        }

        //protected void getLocationsAndLocsMachines2007()
        //{
        //    Excel_Document2007 locsDoc;
        //    string completeFN;
        //    string uLC, lRC;
        //    uLC = "A2";
        //    lRC = "D65536";
        //    string sheetNameLocs = "Locations";
        //    string sheetNameLocsMachines = "PlantMachines";
        //    string currLocId;
        //    completeFN = srcPath + Path.DirectorySeparatorChar + locationTranslationFilename;
        //    DatTable tab;
        //    WEPA_Location currLoc;
        //    int i;
        //    wPlants = new HashSet<string>();
        //    wPlantLocations = new Dictionary<string, WEPA_Location>();
        //    wPlantLocsForMachines = new Dictionary<string, string>();
        //    locsDoc = new Excel_Document2007(ref completeFN, false, false, Excel_Document2007.XLS_Type.XLSX);
        //    locsDoc.setSheetData(ref sheetNameLocs, ref uLC, ref lRC);

        //    locsDoc.OpenDoc();
        //    tab = locsDoc.TableToArray();

        //    for (i = 0; i < tab.numberOfRows; i++)
        //    {
        //        currLoc = new WEPA_Location();
        //        currLoc.locationName = tab.cells[i, 0];
        //        currLoc.locationCountry = tab.cells[i, 1];
        //        currLoc.locationPLZ = tab.cells[i, 2];

        //        currLocId = currLoc.locationCountry + currLoc.locationPLZ;
        //        if (!wPlants.Contains(currLocId))
        //            wPlants.Add(currLocId);

        //        wPlantLocations.Add(currLoc.locationName, currLoc);
        //    }

        //    locsDoc.setSheetData(ref sheetNameLocsMachines, ref uLC, ref lRC);
        //    tab = locsDoc.TableToArray();

        //    for (i = 0; i < tab.numberOfRows; i++)
        //    {
        //        wPlantLocsForMachines.Add(tab.cells[i, 1].Trim(), tab.cells[i, 0].Trim());
        //    }

        //    locsDoc.CloseDoc();
        //}

        protected string getCountryAbbrevISO3166Alpha2(string countryName)
        {
            int i;
            for (i = 0; i < countryTab.GetUpperBound (0); i++)
            {
                if (countryTab[i, 0] == countryName)
                    return countryTab[i, 1];
            }

            return null;
        }

        public Data_Excel_Wepa()
        {
            currC = CultureInfo.CurrentCulture;
           
            nameSeparators =new char[2];
            nameSeparators [0]= ' ';
            nameSeparators [1]='\t';      
        }

        public Data_Excel_Wepa(string p)
        {
            srcPath = p;
        }

        public string SrcPath
        {
            get { return srcPath; }
            set { srcPath = value; }
        }

        public void setLoggers(LoggerCollection lC)
        {
            loggers = lC;
        }

        protected void getTourChainAssignments()
        {
            Optimizer_Data.Excel_Document tourChainAssignmentDoc;
            string completeFN;
            XLSTable tourChainTable;
            completeFN = srcPath + Path.DirectorySeparatorChar + tourChainFilename;
            int i;
          

            if (!File.Exists(completeFN))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, tourChainFilename);

            tourChainAssignmentDoc = new Optimizer_Data.Excel_Document(completeFN);
            tourChainAssignmentDoc.OpenDoc();

            tourChainTable = loadTableFromExcelDocument(tourChainAssignmentDoc, tourChainSheetName, tourChainColNames, tourChainColMatchTypes);
            tourChainAssignmentDoc.CloseDoc();

            for (i = tourChainTable.firstRow + 1; i < tourChainTable.rawTable.Rows .Count ; i++)
            {
                if (!tourChainAssignments.ContainsKey(tourChainTable.rawTable.Rows [i].ItemArray [tourChainTable.colNums[0]].ToString ().Trim ()))
                {  
                    tourChainAssignments.Add(tourChainTable.rawTable.Rows [i].ItemArray [ tourChainTable.colNums[0]].ToString ().Trim (), tourChainTable.rawTable.Rows [i].ItemArray [tourChainTable.colNums[1]].ToString ().Trim ());// tourChainTable.rawTable.cells[i, tourChainTable.colNums[1]]);
                }
            }
        }

        protected void getCustomerChainTransportCustomersRelations()
        {
            Optimizer_Data.Excel_Document relationsDoc;
            string completeFN;
           
            List<TransportCustomerEntry> currTransportCustomerNames;
            TransportCustomerEntry newEntry;
            int i,j;
            string chainName, customerName;
            string msg;
            XLSTable customerChainTable;

            completeFN = srcPath + Path.DirectorySeparatorChar + customerChainTransportCustomerRelationsFileName;
            msg = "Lese Kundenkette-Transportkundenbeziehungen...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

            if (File.Exists(completeFN))
            {
                customerChainTransportCustomersRelations = new Dictionary<string, List<TransportCustomerEntry>>();

                relationsDoc = new Optimizer_Data.Excel_Document(completeFN);

                relationsDoc.OpenDoc();
                customerChainTable = loadTableFromExcelDocument(relationsDoc, customerChainSheetName, customerChainColNames, customerChainColMatchTypes);

                relationsDoc.CloseDoc();

                for (i = customerChainTable.firstRow + 1; i < customerChainTable.rawTable.Rows .Count ; i++)
                {
                    chainName = customerChainTable.rawTable.Rows [i].ItemArray [ customerChainTable.colNums[0]].ToString ().Trim();//tab.cells [i,chainColumn ].Trim ();
                    customerName = customerChainTable.rawTable.Rows [i].ItemArray [ customerChainTable.colNums[1]].ToString ().Trim();//tab.cells[i, customerColumn].Trim();

                    if (!customerChainTransportCustomersRelations.ContainsKey(chainName))
                    {
                        currTransportCustomerNames = new List<TransportCustomerEntry>();
                        customerChainTransportCustomersRelations.Add(chainName, currTransportCustomerNames);
                    }
                    else
                        currTransportCustomerNames = customerChainTransportCustomersRelations[chainName];

                    newEntry = new TransportCustomerEntry();
                    newEntry.fullName = customerName.ToUpper();
                    newEntry.compactName = newEntry.fullName.Replace(" ", "").Replace("\t", "");
                    string[] currParts = customerName.Split(nameSeparators);
                    for (j = 0; j < currParts.Length; j++)
                    {
                        newEntry.nameParts.Add(currParts[j].ToUpper());

                    }
                    currTransportCustomerNames.Add(newEntry);

                }
            }
            else
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, customerChainTransportCustomerRelationsFileName);
            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
        }

        protected void getCostPerShiftMachines()
        {
            Optimizer_Data.Excel_Document costPerShiftDoc;
            string completeFN;
            string msg;
            XLSTable costPerShiftTab;
            double currCostPerShift;
            int i;

            completeFN = srcPath + Path.DirectorySeparatorChar + costPerShiftFileName;
            if (File.Exists (completeFN))
            {
                msg = "Lese Kosten pro Schicht...";
                loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

                wCostPerShiftForMachines = new Dictionary<string, double>();

                costPerShiftDoc = new Optimizer_Data.Excel_Document(completeFN);

                costPerShiftDoc.OpenDoc();
                costPerShiftTab = loadTableFromExcelDocument(costPerShiftDoc, costPerShiftSheetName, costPerShiftColNames,costPerShiftNeuColMatchTypes);
            
                costPerShiftDoc.CloseDoc();
    
                for (i = costPerShiftTab.firstRow+1; i < costPerShiftTab.rawTable.Rows .Count; i++)
                {
                    currCostPerShift = Convert.ToDouble(costPerShiftTab.rawTable.Rows [i].ItemArray [costPerShiftTab.colNums [1]].ToString ().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, "")) * HOURS_PER_SHIFT;
                    wCostPerShiftForMachines.Add(costPerShiftTab.rawTable.Rows [i].ItemArray [ costPerShiftTab.colNums[0]].ToString ().Trim(), currCostPerShift);
                }

                msg = "Fertig.";
                loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
            }
            else
                  throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound,  costPerShiftFileName);
        }

        protected void getCostPerShiftMachinesNeu()
        {
            Optimizer_Data.Excel_Document costPerShiftDoc;
            string completeFN;
            XLSTable costPerShiftTab;
            double currCostPerShift;
            int i;
            string msg;
           
            completeFN = srcPath + Path.DirectorySeparatorChar + costPerTonFileName;

            if (File.Exists(completeFN))
            {


                msg = "Lese Kosten pro Schicht...";
                loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

                wCostPerShiftForMachines = new Dictionary<string, double>();

                costPerShiftDoc = new Optimizer_Data.Excel_Document(completeFN);

                costPerShiftDoc.OpenDoc();

                costPerShiftTab = loadTableFromExcelDocument(costPerShiftDoc, costPerShiftNeuSheetName, costPerShiftNeuColNames, costPerShiftNeuColMatchTypes);//costPerShiftDoc.TableToArray();
                costPerShiftDoc.CloseDoc();

                for (i = costPerShiftTab.firstRow + 1; i < costPerShiftTab.rawTable.Rows .Count; i++)
                {
                    currCostPerShift = Convert.ToDouble(costPerShiftTab.rawTable.Rows [i].ItemArray [ costPerShiftTab.colNums[1]].ToString ().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, "")) * HOURS_PER_SHIFT;
                    wCostPerShiftForMachines.Add(costPerShiftTab.rawTable.Rows[i].ItemArray [ costPerShiftTab.colNums[0]].ToString ().Trim(), currCostPerShift);
                }
                msg = "Fertig.";
                loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
            }
            else
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, costPerTonFileName);
        }

        protected List<HashSet<string>> pruneBasketCandidateSet(List<HashSet<string>> rawSet)
        {
            int i, j;
            bool isAllContained;

            HashSet<string>[] tempBasketCandidateSet;
            List<HashSet<string>> prunedBasketCandidateSet;
            List<HashSet<string>> prunedBasketCandidateSet2;

            prunedBasketCandidateSet = new List<HashSet<string>>();
            prunedBasketCandidateSet2 = new List<HashSet<string>>();
            tempBasketCandidateSet = new HashSet<string>[rawSet.Count];            
            j=0;
                
            foreach (HashSet<string> currBasketCandidate in rawSet)
            {
                HashSet<string> tBasketCandidate = new HashSet<string>();
                              
                foreach (string prod in currBasketCandidate)                    
                    tBasketCandidate.Add(prod);
    
                tempBasketCandidateSet[j] = tBasketCandidate;
                    
                j++;        
            }

            //Eliminiere Warenkörbe, die vollständige durch kleinere Warenkörbe überdeckt werden
            j=0;
            foreach (HashSet<string> currBasketCandidate in rawSet)
            {
                i = 0;
                foreach (HashSet<string> currBasketCandidate2 in rawSet)
                {
                    if (i != j)
                    {
                        if (currBasketCandidate.Count < currBasketCandidate2.Count || (currBasketCandidate.Count == currBasketCandidate2.Count && j<i))
                        {
                            isAllContained = true;

                            foreach (string prod in currBasketCandidate)
                            {
                                if (!currBasketCandidate2.Contains(prod))
                                {
                                    isAllContained = false;
                                    break;
                                }
                            }
                            if (isAllContained)
                            {
                                foreach (string prod in currBasketCandidate)
                                {
                                    if (tempBasketCandidateSet[i].Contains(prod))
                                        tempBasketCandidateSet[i].Remove(prod);
                                }
                            }
                        }
                        
                    }
                    i++;
                }
                j++;
            }

            j = 0;
            foreach (HashSet<string> currBasketCandidate in rawSet)
            {
                if (tempBasketCandidateSet[j].Count > 0)
                    prunedBasketCandidateSet.Add(currBasketCandidate);
                j++;
            }

            bool isNeeded;
            j = 0;
            //Eliminiere Warenkörbe mit einem Element (atomare Warenkörbe), wenn sie in keine
            //größere Menge eingehen
            foreach (HashSet<string> currBasketCandidate in prunedBasketCandidateSet)
            {


                if (currBasketCandidate.Count == 1)
                {

                    isNeeded = false;
                    i = 0;
                    foreach (HashSet<string> currBasketCandidate2 in prunedBasketCandidateSet)
                    {
                        if (i != j && currBasketCandidate.Count < currBasketCandidate2.Count)
                        {
                            isAllContained = true;

                            foreach (string prod in currBasketCandidate)
                            {
                                if (!currBasketCandidate2.Contains(prod))
                                {
                                    isAllContained = false;
                                    break;
                                }
                            }
                            if (isAllContained)
                            {
                                isNeeded = true;
                                break;
                            }
                        }

                        i++;
                    }
                    if (isNeeded)
                        prunedBasketCandidateSet2.Add(currBasketCandidate);

                }
                else
                    prunedBasketCandidateSet2.Add(currBasketCandidate);
            }

            return prunedBasketCandidateSet2;
        }


        protected void readTransportsNE()
        {
            Optimizer_Data.Excel_Document transpDoc;
            string completeFN;
            
            completeFN = srcPath + Path.DirectorySeparatorChar + transpFileName;

            if (File.Exists(completeFN))
            {

                Console.WriteLine("Lade Transporte Nord-Ost-Europa...");


                transpDoc = new Optimizer_Data.Excel_Document(completeFN);
                transpDoc.OpenDoc();
                histTranspTabNE = loadTableFromExcelDocument(transpDoc, transpNESheetName, transpNEColNames, transpNEColMatchTypes);

                transpDoc.CloseDoc();

                Console.WriteLine("Transporte Nord-Ost-Europa fertig.");
            }
            else
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, transpFileName);
        }

        protected void readTransportsSW()
        {
            Optimizer_Data.Excel_Document transpDoc;
            string completeFN;

            completeFN = srcPath + Path.DirectorySeparatorChar + transpSouthWestEuropeFileName;

            if (File.Exists(completeFN))
            {

                Console.WriteLine("Lade Transporte Süd-West-Europa...");


                transpDoc = new Optimizer_Data.Excel_Document(completeFN);
                transpDoc.OpenDoc();
                histTranspTabSW = loadTableFromExcelDocument(transpDoc, transpSWSheetName, transpSWColNames, transpSWColMatchTypes);

                transpDoc.CloseDoc();

                Console.WriteLine("Transporte Süd-West-Europa fertig");
            }
            else
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, transpFileName);
        }

        protected void readWHSESWEurope()
        {
            string completeFN;
            Optimizer_Data.Excel_Document whseSWEuropeDoc;

            completeFN = srcPath + Path.DirectorySeparatorChar + whseSouthWestEuropeFilename;
            if (File.Exists(completeFN))
            {
                whseSWEuropeDoc = new Optimizer_Data.Excel_Document(completeFN);
                whseSWEuropeDoc.OpenDoc();
                whseSWEuropeTab = loadTableFromExcelDocument(whseSWEuropeDoc, whseSWSheetName, whseSWColNames, whseSWColMatchTypes);

                whseSWEuropeDoc.CloseDoc();
            }
            else
            throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, whseSouthWestEuropeFilename);
        }
        
        protected void prepareCustomerDemands(SNP_WEPA_Instance pI)
        {
            string customerProductKey;
            ArticleLocationDistribution currCustomerProductLocationDistribution;
            DemandValue currCustomerDemand;
            WEPA_Location currCustomerLocationInfo;
            relevantProductsForCustomer = new Dictionary<string, HashSet<string>>();
            bool isExistCustomerLocationsInDemandCountry;
            HashSet<string> currProductsForCustomer;
            string currDemandLocName;
            string currDemandProductName;
            string currProdLoc;
            RawTransportProcess currRawTransProcess;
            HashSet<string> nPFCL;
            double currDemandValue;
            string currDemandPeriodName;
            string currDemandCustomerName;
            bool isTransportAddedForCustomerDemand;
            Product currDemandProduct;
            Location currLocation;
            string currTranspProcName;
            TransportationProcess finalTransProcess;
            double totalNumberPerPallets;
            double totalPotentialPallets = 0, totalPotentialPalletsMP = 0, totalPotentialPalletsP = 0, totalPalletsCD = 0;
            string msg;
            bool hasProductionProcess, hasTransport;
            ProductInformationState currPIS;
            string currPID;

            int i=0;
            //Derive demands related to customer locations
            foreach (KeyValuePair<string, RawDemand> dE in rawDemands)
            {
                hasProductionProcess= hasTransport=false;
                if (dE.Value.ProductName == "36451" || dE.Value.ProductName == "036451")
                    i = i;
                if (productsWithProcesses.Contains(dE.Value.ProductName))
                {
                    customerProductKey = dE.Value.CustomerName + dE.Value.ProductName;

                    if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                    {
                        if (!locationDistributionPerCustomer.ContainsKey(dE.Value.CustomerName))
                            currCustomerProductLocationDistribution = null;
                        else
                            currCustomerProductLocationDistribution = locationDistributionPerCustomer[dE.Value.CustomerName];
                    }
                    else
                        currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                    if (currCustomerProductLocationDistribution != null)
                    {
                        hasProductionProcess = true;
                        isExistCustomerLocationsInDemandCountry = false;
                        currCustomerLocationInfo = null;
                        if (isConsiderCountryInfoOfDemands)
                        {
                            foreach (KeyValuePair<string, double> dE2 in currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation)
                            {
                                currCustomerLocationInfo = wCustomerLocations[dE2.Key];

                                if (currCustomerLocationInfo.locationCountry == dE.Value.CountryName)
                                {
                                    isExistCustomerLocationsInDemandCountry = true;
                                    break;
                                }
                            }
                        }

                        if (!relevantProductsForCustomer.ContainsKey(dE.Value.CustomerName))
                        {
                            currProductsForCustomer = new HashSet<string>();
                            relevantProductsForCustomer.Add(dE.Value.CustomerName, currProductsForCustomer);
                        }
                        else
                            currProductsForCustomer = relevantProductsForCustomer[dE.Value.CustomerName];

                        if (!currProductsForCustomer.Contains(dE.Value.ProductName))
                            currProductsForCustomer.Add(dE.Value.ProductName);

                        totalNumberPerPallets = 0;
                        totalPotentialPallets = 0;
                        foreach (KeyValuePair<string, double> dE2 in currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation)
                        {
                            currCustomerLocationInfo = wCustomerLocations[dE2.Key];
                            currDemandLocName = dE.Value.CustomerName + dE2.Key;
                            currTransportProcessesToDestLocation = transportProcessesToLocation[currDemandLocName];
                            totalPotentialPallets += dE2.Value;
                            foreach (string currTranspName in currTransportProcessesToDestLocation)
                            {
                                
                                currRawTransProcess = transportProcesses[currTranspName];
                                currProdLoc = dE.Value.ProductName + currRawTransProcess.sourceLocId;
                                if (
                                    ((isExistCustomerLocationsInDemandCountry && currCustomerLocationInfo.locationCountry == dE.Value.CountryName) || (!isExistCustomerLocationsInDemandCountry)) &&
                                    ((productLocation.Contains(currProdLoc) && isWarrantFeasiblityWithoutInterplantTransports) || (!isWarrantFeasiblityWithoutInterplantTransports &&
                                                                       wPlants.Contains(currRawTransProcess.sourceLocId))
                                     )
                                   )
                                {
                                    hasTransport = true;
                                    totalNumberPerPallets += dE2.Value;
                                    break;
                                }
                            }
                        }

                        if (totalNumberPerPallets == 0)
                        {
                            totalPotentialPalletsMP += dE.Value.Val;
                        }
                        else
                            totalPotentialPalletsP += dE.Value.Val;

                        foreach (KeyValuePair<string, double> dE2 in currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation)
                        {
                            currCustomerLocationInfo = wCustomerLocations[dE2.Key];
                            currDemandLocName = dE.Value.CustomerName + dE2.Key;
                            currDemandProductName = dE.Value.ProductName;
                            currDemandValue = Math.Ceiling(dE.Value.Val * (dE2.Value / totalNumberPerPallets));

                            currDemandPeriodName = dE.Value.PeriodName;
                            currDemandCustomerName = "";

                            currTransportProcessesToDestLocation = transportProcessesToLocation[currDemandLocName];

                            isTransportAddedForCustomerDemand = false;

                            foreach (string currTranspName in currTransportProcessesToDestLocation)
                            {
                                currRawTransProcess = transportProcesses[currTranspName];
                                currProdLoc = dE.Value.ProductName + currRawTransProcess.sourceLocId;

                                if (
                                    ((isExistCustomerLocationsInDemandCountry && currCustomerLocationInfo.locationCountry == dE.Value.CountryName) || (!isExistCustomerLocationsInDemandCountry)) &&
                                    (   (productLocation.Contains(currProdLoc) && isWarrantFeasiblityWithoutInterplantTransports) || 
                                        (!isWarrantFeasiblityWithoutInterplantTransports && wPlants.Contains(currRawTransProcess.sourceLocId))
                                        )
                                    )
                                {
                                    if (pI.getProduct(currDemandProductName) == null)
                                    {
                                        currDemandProduct = preliminaryProducts[currDemandProductName];
                                        pI.addProduct(currDemandProduct);
                                        
                                    }
                                    IndexedItem<Location> lI;
                                    lI = pI.getCustomerLocation(currDemandLocName);
                                    if (lI == null)//(currLocation == null)
                                    {
                                        currLocation = new Location();

                                        currLocation.LocationName = currDemandLocName;
                                        pI.addCustomerLocation(currLocation);
                                    }

                                    currTranspProcName = currRawTransProcess.sourceLocId + currDemandLocName;

                                    IndexedItem<TransportationProcess> tI;
                                    tI = pI.getCustomerTransportationProcess(currTranspProcName);
                                    if (tI == null)//(finalTransProcess == null)
                                    {
                                        finalTransProcess = new TransportationProcess();
                                        finalTransProcess.StartLocation = currRawTransProcess.sourceLocId;

                                        if (pI.getPlantLocation(currRawTransProcess.sourceLocId) == null)
                                        {
                                            currLocation = new Location();
                                            currLocation.LocationName = currRawTransProcess.sourceLocId;
                                            pI.addPlantLocation(currLocation);
                                        }
                                        finalTransProcess.DestLocation = currDemandLocName;
                                        finalTransProcess.CostPerTruck = currRawTransProcess.avgCostPerTruck;

                                        pI.addCustomerTransportationProcess(finalTransProcess);
                                    }
                                    else
                                        finalTransProcess = tI.item;

                                    currTranspProcName = finalTransProcess.StartLocation + finalTransProcess.DestLocation;
                                    isTransportAddedForCustomerDemand = true;

                                }
                            }

                            if (isTransportAddedForCustomerDemand)
                            {
                                IndexedItem<DemandValue> iD;

                                iD = pI.getDemand(currDemandProductName + currDemandLocName + currDemandPeriodName + currDemandCustomerName);

                                if (iD == null)
                                {
                                    if (!productsForCustomerLocation.ContainsKey(currDemandLocName))
                                    {
                                        nPFCL = new HashSet<string>();
                                        productsForCustomerLocation.Add(currDemandLocName, nPFCL);
                                    }
                                    else
                                        nPFCL = productsForCustomerLocation[currDemandLocName];

                                    if (!nPFCL.Contains(currDemandProductName))
                                        nPFCL.Add(currDemandProductName);


                                    currCustomerDemand = new DemandValue();
                                    currCustomerDemand.ProductName = currDemandProductName;
                                    currCustomerDemand.LocationName = currDemandLocName;
                                    currCustomerDemand.PeriodName = currDemandPeriodName;
                                    currCustomerDemand.Val = 0;

                                    pI.addDemand(currCustomerDemand);
                                }
                                else
                                    currCustomerDemand = iD.item;
                                currCustomerDemand.Val += currDemandValue;
                                totalPalletsCD += currDemandValue;
                            }
                        }

                    }
                }
                else
                {
                    hasProductionProcess = false;
                }

                if (!hasProductionProcess || !hasTransport)
                {
                    currPID = dE.Value.ProductName + dE.Value.CustomerName + dE.Value.CountryName;
                    if (productsNotConsidered.ContainsKey(dE.Value.ProductName))
                    {
                        currPIS = productsNotConsidered[dE.Value.ProductName];
                    }
                    else
                    {
                        currPIS = new ProductInformationState();
                        currPIS.productName = dE.Value.ProductName;
                        currPIS.customerName = dE.Value.CustomerName;
                        currPIS.countryName = dE.Value.CountryName;
                        currPIS.hasNoProductionProcess = currPIS.hasNoTranportationProcess = false;
                        productsNotConsidered.Add(currPID, currPIS);
                    }

                    currPIS.hasNoProductionProcess = !hasProductionProcess;
                    currPIS.hasNoTranportationProcess = !hasTransport;
                }
            }

            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
        }


        protected void prepareBaskets(SNP_WEPA_Instance pI)
        {
            string msg;
            Dictionary<string, List<HashSet<string>>> basketCandidates = new Dictionary<string, List<HashSet<string>>>();
            HashSet<string> currBasketCandidate;
            List<HashSet<string>> currBasketCandidateSet;
            ArticleLocationDistribution currLocDistr;
            TourData currTour;
            string currCustomerLocationName;

            msg = "Bearbeite Warenkörbe...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

            //Analyse der Transportkombinationen um Warenkörbe zu bestimmen
            foreach (KeyValuePair<string, HashSet<string>> customerProductEntry in relevantProductsForCustomer)
            {
                if (toursForCustomer.ContainsKey(customerProductEntry.Key))
                {

                    HashSet<string> currToursForCustomer = toursForCustomer[customerProductEntry.Key];

                    currBasketCandidateSet = new List<HashSet<string>>();

                    foreach (string tourName in currToursForCustomer)
                    {

                        currTour = tours[tourName];


                        currBasketCandidate = null;


                        foreach (string productName in customerProductEntry.Value)
                        {

                            if (currTour.productsOnTour.Contains(productName))
                            {

                                if (currBasketCandidate == null)

                                    currBasketCandidate = new HashSet<string>();

                                currBasketCandidate.Add(productName);

                            }

                        }


                        if (currBasketCandidate != null)
                        {

                            currBasketCandidateSet.Add(currBasketCandidate);

                        }
                    }


                    currBasketCandidateSet = pruneBasketCandidateSet(currBasketCandidateSet);

                    currLocDistr = locationDistributionPerCustomer[customerProductEntry.Key];


                    foreach (KeyValuePair<string, double> custLocDistr in currLocDistr.numberPalletsTransportedPerLocation)
                    {
                        currCustomerLocationName = customerProductEntry.Key + custLocDistr.Key;

                        if (pI.getCustomerLocation(currCustomerLocationName) != null)
                        {
                            string currLocName = customerProductEntry.Key + custLocDistr.Key;

                            List<HashSet<string>> customerBasketSet = new List<HashSet<string>>();

                            foreach (HashSet<string> currBasket in currBasketCandidateSet)
                            {
                                HashSet<string> customerBasket;
                                customerBasket = new HashSet<string>();

                                foreach (string prodName in currBasket)
                                {
                                    if (productsForCustomerLocation[currLocName].Contains(prodName))
                                        customerBasket.Add(prodName);
                                }

                                customerBasketSet.Add(customerBasket);
                            }

                            customerBasketSet = pruneBasketCandidateSet(customerBasketSet);

                            if (customerBasketSet.Count > 0)
                            {
                                BasketSet nBaskets = new BasketSet();
                                nBaskets.LocationName = currLocName;

                                foreach (HashSet<string> currBasket in customerBasketSet)
                                {
                                    nBaskets.addBasket(currBasket);
                                }

                                pI.addBasketSet(nBaskets);
                            }
                        }
                    }
                }
            }

            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

        }

        protected void getInterplantTransports(SNP_WEPA_Instance pI)
        {
            string msg;
            Optimizer_Data.Excel_Document iTranspDoc;
            XLSTable iTranspTab;
            TransportationProcess currTranspP;
            WEPA_Location currSLoc, currDLoc;
            string currCellStr;
            string completeFN;
            string currLocationName;
            string currStartLocation, currDestLocation;
            double currCostPerTruck;
            int endOfFirstWord;
            Location currLoc;
            int i,j;

            msg = "Lese Interplant-Transporte...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
            completeFN = srcPath + Path.DirectorySeparatorChar + interPlantTranspFileName;
            if (File.Exists(completeFN))
            {
                iTranspDoc = new Optimizer_Data.Excel_Document(completeFN);//new Excel_Document2007(ref completeFN, false, false, Excel_Document2007.XLS_Type.XLSX);

                iTranspDoc.OpenDoc();
                iTranspTab = loadTableFromExcelDocument(iTranspDoc, interPlantTranspSheetName, interPlantTranspColNames, interPlantTranspColMatchTypes);

                iTranspDoc.CloseDoc();

                for (i = iTranspTab.firstRow + 1; i < iTranspTab.rawTable.Rows .Count; i++)//for (i = 1; i < iTranspTab.numberOfRows; i++)
                {

                    currLocationName = iTranspTab.rawTable.Rows [i].ItemArray[iTranspTab.colNums[0]].ToString ().Trim();//iTranspTab.cells[i, 0].Trim();
                    if (currLocationName == "")
                        break;
                    if (wPlantLocations.ContainsKey(currLocationName))
                    {
                        currSLoc = wPlantLocations[currLocationName];

                        for (j = iTranspTab.colNums[0] + 1; j < iTranspTab.rawTable.Columns.Count ; j++)
                        {
                            currLocationName = iTranspTab.rawTable.Rows [iTranspTab.firstRow].ItemArray[j].ToString ().Trim();
                            if (wPlantLocations.ContainsKey(currLocationName))
                            {
                                currDLoc = wPlantLocations[currLocationName];

                                currStartLocation = currSLoc.locationCountry + currSLoc.locationPLZ;
                                currDestLocation = currDLoc.locationCountry + currDLoc.locationPLZ;

                                if (currStartLocation != currDestLocation)// && productsAtLocation.ContainsKey(currStartLocation) && productsAtLocation.ContainsKey(currDestLocation))
                                {
                                    currCellStr = iTranspTab.rawTable.Rows [i].ItemArray [j].ToString ().Trim();
                                    currCostPerTruck = 0;
                                    if (currCellStr != "")
                                    {
                                        endOfFirstWord = currCellStr.IndexOf(" ", 0);
                                        if (endOfFirstWord != -1)
                                            currCellStr = currCellStr.Substring(0, endOfFirstWord);

                                        currCostPerTruck = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                    }

                                    if (pI.getPlantLocation(currStartLocation) == null)
                                    {
                                        currLoc = new Location();
                                        currLoc.LocationName = currStartLocation;
                                        pI.addPlantLocation(currLoc);
                                    }

                                    if (pI.getPlantLocation(currDestLocation) == null)
                                    {
                                        currLoc = new Location();
                                        currLoc.LocationName = currDestLocation;
                                        pI.addPlantLocation(currLoc);
                                    }

                                    currTranspP = new TransportationProcess();
                                    currTranspP.StartLocation = currStartLocation;

                                    currTranspP.DestLocation = currDestLocation;
                                    currTranspP.CostPerTruck = currCostPerTruck;

                                    pI.addInterplantTransportationProcess(currTranspP);
                                }
                            }
                        }
                    }
                }

                msg = "Fertig.";
                loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
            }
            else
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, interPlantTranspFileName);
        }

        protected void prepareTransportationProcesses()
        {
            HashSet<string> consideredTours;
            string msg;
            string transpProcKey;
            RawTransportProcess currTransportProcess;
            List<string> currTranspProcToLocation;
            WEPA_Location dummyLoc;
            string currCustomerDummyLocation;
            ArticleLocationDistribution currDummyArtLocDistr;

            msg = "Bearbeite Transportprozesse...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

            transportProcesses = new Dictionary<string, RawTransportProcess>();
            transportProcessesToLocation = new Dictionary<string, List<string>>();

            consideredTours = new HashSet<string>();

            foreach (KeyValuePair<string, TourData> dE in tours)
            {
                transpProcKey = dE.Value.sourceLocId + dE.Value.customer + dE.Value.destLocId;

                if (!transportProcesses.ContainsKey(transpProcKey))
                {
                    currTransportProcess = new RawTransportProcess();
                    currTransportProcess.numberOfHistoricalTrucks = 0;
                    currTransportProcess.avgCostPerTruck = 0;
                    currTransportProcess.sourceLocId = dE.Value.sourceLocId;
                    currTransportProcess.destLocId = dE.Value.customer + dE.Value.destLocId;

                    transportProcesses.Add(transpProcKey, currTransportProcess);

                    if (!transportProcessesToLocation.ContainsKey(dE.Value.customer + dE.Value.destLocId))
                    {
                        currTranspProcToLocation = new List<string>();
                        transportProcessesToLocation.Add(dE.Value.customer + dE.Value.destLocId, currTranspProcToLocation);
                    }
                    else
                        currTranspProcToLocation = transportProcessesToLocation[dE.Value.customer + dE.Value.destLocId];

                    currTranspProcToLocation.Add(transpProcKey);
                }
                else
                    currTransportProcess = transportProcesses[transpProcKey];

                foreach (string currProd in dE.Value.productsOnTour)
                {
                    if (!currTransportProcess.productsTransported.Contains(currProd))
                        currTransportProcess.productsTransported.Add(currProd);
                }

                currTransportProcess.avgCostPerTruck += dE.Value.tourPrice;
                currTransportProcess.numberOfHistoricalTrucks += (int)Math.Ceiling(dE.Value.tourNumberPallets / ((double)NUMBER_PALLETS_PER_TRUCK));
            }

            foreach (KeyValuePair<string, RawTransportProcess> dE in transportProcesses)
            {
                dE.Value.avgCostPerTruck /= dE.Value.numberOfHistoricalTrucks;

                if (dE.Value.avgCostPerTruck < THRESHOLD_ACCEPTANCE_AVG_TRANSPORT_COST && dE.Value.avgCostPerTruck > 0)
                    dE.Value.avgCostPerTruck = 0;
            }


            foreach (string customerName in customersWithDemands)
            {
                if (!locationDistributionPerCustomer.ContainsKey(customerName))
                {
                    
                    currCustomerDummyLocation = customerName + DUMMY_LOCATION_NAME;
                    currDummyArtLocDistr = new ArticleLocationDistribution();
                    currDummyArtLocDistr.numberPalletsTransportedPerLocation.Add(DUMMY_LOCATION_NAME, 1); //Eine symbolische Palette, damit Division durch Palettenanzahl funktioniert.
                    locationDistributionPerCustomer.Add(customerName, currDummyArtLocDistr);
                    if (!wCustomerLocations.ContainsKey(DUMMY_LOCATION_NAME))
                    {
                        dummyLoc = new WEPA_Location();
                        dummyLoc.locationCountry = "DUMMY_COUNTRY";
                        dummyLoc.locationName = DUMMY_LOCATION_NAME;
                        dummyLoc.locationPLZ = "DUMMY_PLZ";
                        wCustomerLocations.Add(DUMMY_LOCATION_NAME, dummyLoc);
                    }
                    foreach (KeyValuePair<string, WEPA_Location> plantInfo in wPlantLocations)
                    {
                        currTransportProcess = new RawTransportProcess();
                        currTransportProcess.numberOfHistoricalTrucks = 1;
                        currTransportProcess.avgCostPerTruck = 0;
                        currTransportProcess.sourceLocId = plantInfo.Value.locationCountry + plantInfo.Value.locationPLZ;
                        currTransportProcess.destLocId = currCustomerDummyLocation;

                        transpProcKey = currTransportProcess.sourceLocId + currCustomerDummyLocation;
                        transportProcesses.Add(transpProcKey, currTransportProcess);

                        if (!transportProcessesToLocation.ContainsKey(currCustomerDummyLocation))
                        {
                            currTranspProcToLocation = new List<string>();
                            transportProcessesToLocation.Add(currCustomerDummyLocation, currTranspProcToLocation);
                        }
                        else
                            currTranspProcToLocation = transportProcessesToLocation[currCustomerDummyLocation];

                        currTranspProcToLocation.Add(transpProcKey);
                    }
                }
            }

        }

       protected void prepareProductionProcesses()
       {
           string msg;
           Dictionary<string, List<string>> productsAtLocation;
           Resource currResource;
           WEPA_Location currWLoc;
           string currResourceLocation;
           string currProductLocation;
           List<string> currProductsAtLocation;
           msg = "Bearbeite Produktionsprozesse...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

            productLocation = new HashSet<string>();
            productsAtLocation=new Dictionary<string,List<string>> ();

            foreach (KeyValuePair <string,ProductionProcess> kP in preliminaryProductionProcesses)
            {
                currResource = preliminaryResources[(string)kP.Value.ResourceName]; 

                currWLoc=wPlantLocations[currResource.LocationName];
                currResourceLocation = currWLoc.locationCountry + currWLoc.locationPLZ;

                currProductLocation = kP.Value.ProductName + currResourceLocation;

               if (!productsAtLocation .ContainsKey ( currResourceLocation))
               {
                    currProductsAtLocation =new List<string>();
                    productsAtLocation.Add(currResourceLocation, currProductsAtLocation);
               }
               else
                   currProductsAtLocation = productsAtLocation[currResourceLocation];

                if (!productLocation.Contains(currProductLocation))
                {
                    productLocation.Add(currProductLocation);
                    currProductsAtLocation.Add(kP.Value.ProductName);
                }
            }

            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
       }
        //Gets:
        //1. Transports with cost
        protected void getTransportData(SNP_WEPA_Instance pI)
        {
            string msg;
            
            HashSet<string> customersWithTransports;
            TourData currTour; //ok: Aktuelle Tour
            ArticleLocationDistribution currCustomerProductLocationDistribution, currCustomerLocationDistribution; //ok:Für aktuelle kundenproduktspezifische bzw. kundenspezifische Verteilungen der Nachfrage eines Kunden
            string tourId, prodId;
            string tourKey, sourceLocId, destLocId,customerId,customerWithDemandId;
            int i, j;
            string customerProductKey;
            double numberPallets, price;
            string firstPartCustomerName;
            int posEndFirstPart;
            string whseId, dateExp, cap, countryDest, whseLocId, destCorpId,corpId;
            string currCustomerLocation;
            string  destLocCountry,destLocPLZ;
            string matchingName;
            HashSet<string> currCList;
            HashSet<string> currCustomerTours;
            List<TransportCustomerEntry> currentTransportCustomers;
            string shortCustomerId, shortCustomerId2;
            CSV_File currMatchingsFile;
            DatTable matchingTab;
            string currTransCustomerName;
            CSV_File currMatchingFile;
            WEPA_Location newCustomerLocation;
            string shortTranspCustomerName;
            
            transportCustomerCustomerChainMatchings = new Dictionary<string, HashSet<string>>();

            msg = "Lese Transporte...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

            wCustomerLocations = new Dictionary<string, WEPA_Location>();
            productsForCustomerLocation = new Dictionary<string, HashSet<string>>();
            toursForCustomer=new Dictionary<string,HashSet<string>> ();
            
            tours = new Dictionary<string, TourData>();
           
            locationDistributionPerCustomerProduct = new Dictionary<string, ArticleLocationDistribution>();
            locationDistributionPerCustomer = new Dictionary<string, ArticleLocationDistribution>();
            customersWithTransports = new HashSet<string>();

            warehouseLocsSWEurope = new Dictionary<string, string>();
           
            for (i = whseSWEuropeTab.firstRow+1; i < whseSWEuropeTab.rawTable.Rows .Count ; i++)
            {
                warehouseLocsSWEurope.Add(whseSWEuropeTab.rawTable.Rows [i].ItemArray [ whseSWEuropeTab.colNums[0]].ToString ().Trim(), 
                    whseSWEuropeTab.rawTable.Rows [i].ItemArray [ whseSWEuropeTab.colNums[1]].ToString ().Trim() + whseSWEuropeTab.rawTable.Rows [i].ItemArray[whseSWEuropeTab.colNums[2]].ToString ().Trim().Replace("-", ""));
            }

            if (File.Exists(matchingFileName))
            {
                currMatchingsFile = new CSV_File(ref matchingFileName);
                currMatchingsFile.setIsHeadLine(false);
                currMatchingsFile.setCSVDelimier(ref csvDelim);
                currMatchingsFile.OpenForReading();
                
                matchingTab = currMatchingsFile.TableToArray();
                
                for (i = 0; i < matchingTab.numberOfRows; i++)
                {
                    currTransCustomerName=matchingTab.cells[i, 0].Trim();
                    if (!transportCustomerCustomerChainMatchings.ContainsKey(currTransCustomerName))
                    {
                        currCList = new HashSet<string>();
                        transportCustomerCustomerChainMatchings.Add(currTransCustomerName, currCList);
                    }
                    else
                        currCList =transportCustomerCustomerChainMatchings[currTransCustomerName];

                    currCList .Add (matchingTab.cells[i, 1].Trim ());
                }

                currMatchingsFile.CloseDoc();

                for (i = histTranspTabNE.firstRow+1; i < histTranspTabNE.rawTable.Rows.Count ; i++)//for (i = 1; i < transpTab.numberOfRows; i++)
                {
                    prodId = histTranspTabNE.rawTable.Rows [i].ItemArray [ histTranspTabNE.colNums [9]].ToString ().Trim();//cells[i, productColumnNum].Trim();
                    sourceLocId = histTranspTabNE.rawTable.Rows [i].ItemArray [ histTranspTabNE.colNums[4]].ToString ().Trim() + histTranspTabNE.rawTable.Rows [i].ItemArray [ histTranspTabNE.colNums[5]].ToString ().Trim().Replace("-", "");
                    if (histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[0]].ToString ().Trim() == "FG" && wPlants.Contains(sourceLocId)) //cells[i, artColumnNum].Trim() == "FG")
                    {
                        if (prodId == "036451" || prodId == "36451")
                            i = i;

                        tourId = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[1]].ToString().Trim();//transpTab.cells[i, tourColumnNum].Trim();
                        sourceLocId = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[4]].ToString().Trim() + histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[5]].ToString().Trim().Replace("-", "");//transpTab.cells[i, sourceCountryColumnNum].Trim() + transpTab.cells[i, sourcePlz1ColumnNum].Trim().Replace("-", "");

                        destLocCountry = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[7]].ToString().Trim();//transpTab.cells[i, destCountryColumnNum].Trim();
                        destLocPLZ = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[8]].ToString().Trim().Replace("-", "");//transpTab.cells[i, destPlz1ColumnNum].Trim().Replace("-", "");
                        destLocId = destLocCountry + destLocPLZ;

                        customerId = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[10]].ToString().Trim();//transpTab.cells[i, customerColumn].Trim();
                        shortCustomerId = customerId.Replace(" ", "").Replace("\t", "").ToUpper(); ;

                        customerWithDemandId = "";

                        if (isConsiderTourChainAssignments)
                        {
                            if (tourChainAssignments.ContainsKey(tourId))
                            {
                                customerWithDemandId = tourChainAssignments[tourId];

                                if ((prodId == "036451" || prodId == "36451") && customerWithDemandId=="ASWATSON")
                                    i = i;

                                if (customerWithDemandId != "" && histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[2]].ToString().Trim() != "" &&
                                    histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[3]].ToString ().Trim() != "")//if (customerWithDemandId != "" && transpTab.cells[i, numberPalletsColumnNum].Trim() != "" && transpTab.cells[i, priceColumnNum].Trim() != "")
                                {
                                    if (isUseGivenCustomerSalesToCustomerTransportRelations && !customersWithTransports.Contains(customerWithDemandId))
                                        customersWithTransports.Add(customerWithDemandId);

                                    tourKey = tourId + sourceLocId + destLocId + customerWithDemandId;

                                    //numberPallets = Convert.ToDouble(transpTab.cells[i, numberPalletsColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                    numberPallets = Convert.ToDouble(histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[2]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                    if (numberPallets > 0)
                                    {
                                        //price = Convert.ToDouble(transpTab.cells[i, priceColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                        price = Convert.ToDouble(histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[3]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                        if (!tours.ContainsKey(tourKey))
                                        {
                                            currTour = new TourData();
                                            currTour.tourId = tourId;
                                            currTour.sourceLocId = sourceLocId;
                                            currTour.destLocId = destLocId;
                                            currTour.tourNumberPallets = 0;
                                            currTour.tourPrice = 0;

                                            currTour.customer = customerWithDemandId;
                                            tours.Add(tourKey, currTour);

                                            if (!toursForCustomer.ContainsKey(customerWithDemandId))
                                            {
                                                currCustomerTours = new HashSet<string>();
                                                toursForCustomer.Add(customerWithDemandId, currCustomerTours);
                                            }
                                            else
                                                currCustomerTours = toursForCustomer[customerWithDemandId];

                                            currCustomerTours.Add(tourKey);

                                            if (!wCustomerLocations.ContainsKey(destLocId))
                                            {
                                                newCustomerLocation = new WEPA_Location();
                                                newCustomerLocation.locationName = destLocId;
                                                newCustomerLocation.locationCountry = destLocCountry;
                                                newCustomerLocation.locationPLZ = destLocPLZ;
                                                wCustomerLocations.Add(destLocId, newCustomerLocation);
                                            }
                                        }
                                        else
                                            currTour = tours[tourKey];

                                        currTour.tourNumberPallets += numberPallets;
                                        currTour.tourPrice += price;

                                        if (!currTour.productsOnTour.Contains(prodId))
                                            currTour.productsOnTour.Add(prodId);

                                        customerProductKey = currTour.customer + prodId;

                                        currCustomerLocation = currTour.customer + currTour.destLocId;

                                        if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                                        {
                                            currCustomerProductLocationDistribution = new ArticleLocationDistribution();
                                            locationDistributionPerCustomerProduct.Add(customerProductKey, currCustomerProductLocationDistribution);
                                        }
                                        else
                                            currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                                        if (!currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                            currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                        else
                                            currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                                currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;

                                        if (!locationDistributionPerCustomer.ContainsKey(currTour.customer))
                                        {
                                            currCustomerLocationDistribution = new ArticleLocationDistribution();
                                            locationDistributionPerCustomer.Add(currTour.customer, currCustomerLocationDistribution);
                                        }
                                        else
                                            currCustomerLocationDistribution = locationDistributionPerCustomer[currTour.customer];

                                        if (!currCustomerLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                            currCustomerLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                        else
                                            currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                                currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;
                                    }
                                }

                            }
                        }
                        else
                        {
                            if (transportCustomerCustomerChainMatchings.ContainsKey(customerId))
                            {
                                currCList = transportCustomerCustomerChainMatchings[customerId];

                                foreach (string customerChain in currCList)
                                {
                                    customerWithDemandId = customerChain;

                                    if (customerWithDemandId != "" && histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[2]].ToString().Trim() != "" &&
                                        histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[3]].ToString ().Trim() != "")//if (customerWithDemandId != "" && transpTab.cells[i, numberPalletsColumnNum].Trim() != "" && transpTab.cells[i, priceColumnNum].Trim() != "")
                                    {
                                        if (isUseGivenCustomerSalesToCustomerTransportRelations && !customersWithTransports.Contains(customerWithDemandId))
                                            customersWithTransports.Add(customerWithDemandId);

                                        tourKey = tourId + sourceLocId + destLocId + customerWithDemandId;

                                        //numberPallets = Convert.ToDouble(transpTab.cells[i, numberPalletsColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                        numberPallets = Convert.ToDouble(histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[2]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                        if (numberPallets > 0)
                                        {
                                            //price = Convert.ToDouble(transpTab.cells[i, priceColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                            price = Convert.ToDouble(histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[3]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                            if (!tours.ContainsKey(tourKey))
                                            {
                                                currTour = new TourData();
                                                currTour.tourId = tourId;
                                                currTour.sourceLocId = sourceLocId;
                                                currTour.destLocId = destLocId;
                                                currTour.tourNumberPallets = 0;
                                                currTour.tourPrice = 0;

                                                currTour.customer = customerWithDemandId;
                                                tours.Add(tourKey, currTour);

                                                if (!toursForCustomer.ContainsKey(customerWithDemandId))
                                                {
                                                    currCustomerTours = new HashSet<string>();
                                                    toursForCustomer.Add(customerWithDemandId, currCustomerTours);
                                                }
                                                else
                                                    currCustomerTours = toursForCustomer[customerWithDemandId];

                                                currCustomerTours.Add(tourKey);

                                                if (!wCustomerLocations.ContainsKey(destLocId))
                                                {
                                                    newCustomerLocation = new WEPA_Location();
                                                    newCustomerLocation.locationName = destLocId;
                                                    newCustomerLocation.locationCountry = destLocCountry;
                                                    newCustomerLocation.locationPLZ = destLocPLZ;
                                                    wCustomerLocations.Add(destLocId, newCustomerLocation);
                                                }
                                            }
                                            else
                                                currTour = tours[tourKey];

                                            currTour.tourNumberPallets += numberPallets;
                                            currTour.tourPrice += price;

                                            if (!currTour.productsOnTour.Contains(prodId))
                                                currTour.productsOnTour.Add(prodId);

                                            customerProductKey = currTour.customer + prodId;

                                            currCustomerLocation = currTour.customer + currTour.destLocId;

                                            if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                                            {
                                                currCustomerProductLocationDistribution = new ArticleLocationDistribution();
                                                locationDistributionPerCustomerProduct.Add(customerProductKey, currCustomerProductLocationDistribution);
                                            }
                                            else
                                                currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                                            if (!currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                                currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                            else
                                                currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                                    currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;

                                            if (!locationDistributionPerCustomer.ContainsKey(currTour.customer))
                                            {
                                                currCustomerLocationDistribution = new ArticleLocationDistribution();
                                                locationDistributionPerCustomer.Add(currTour.customer, currCustomerLocationDistribution);
                                            }
                                            else
                                                currCustomerLocationDistribution = locationDistributionPerCustomer[currTour.customer];

                                            if (!currCustomerLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                                currCustomerLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                            else
                                                currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                                    currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                for (i = histTranspTabSW.firstRow+1 ; i < histTranspTabSW.rawTable.Rows .Count ; i++)// transpTabSWEurope.rawTable.numberOfRows; i++)
                {
                    prodId = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[6]].ToString().Trim();//transpTabSWEurope.cells[i, itemCodeColumnNum].Trim();
                    whseId = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[0]].ToString().Trim();//transpTabSWEurope.cells[i, whseColumnNum].Trim();
                    dateExp = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[1]].ToString().Trim();//transpTabSWEurope.cells[i, dateExpColumnNum].Trim();
                    cap = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[4]].ToString().Trim();//transpTabSWEurope.cells[i, capColumnNum].Trim();
                    countryDest = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[5]].ToString().Trim();//transpTabSWEurope.cells[i, countryDestColumnNum].Trim();
                    destCorpId = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[2]].ToString().Trim();//transpTabSWEurope.cells[i, destCorpColumnNum].Trim();
                    corpId = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[3]].ToString().Trim();//transpTabSWEurope.cells[i, corpColumnNum].Trim();
                    shortCustomerId = destCorpId.Replace(" ", "").Replace("\t", "").ToUpper();
                    shortCustomerId2 = corpId.Replace(" ", "").Replace("\t", "").ToUpper();
                    whseLocId = warehouseLocsSWEurope[whseId];
                    tourId = whseId + dateExp + countryDest;
                    sourceLocId = whseLocId;
                    destLocId = countryDest + cap;

                    if (prodId == "036451" || prodId == "36451")
                        i = i;

                    customerWithDemandId = "";

                    if (histTranspTabSW.colNums[9] != -1)
                    {
                        customerWithDemandId = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[9]].ToString().Trim();
                        if (customerWithDemandId != "" && histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[7]].ToString().Trim() != "")//if (customerWithDemandId != "" && transpTabSWEurope.cells[i, qtyPalletsColumnNum].Trim() != "")
                        {
                            if (isUseGivenCustomerSalesToCustomerTransportRelations && !customersWithTransports.Contains(customerWithDemandId))
                                customersWithTransports.Add(customerWithDemandId);

                            tourKey = tourId + sourceLocId + destLocId + customerWithDemandId;
                            numberPallets = Convert.ToDouble(histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[7]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);//Convert.ToDouble(transpTabSWEurope.cells[i, qtyPalletsColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                            if (numberPallets > 0)
                            {
                                price = Convert.ToDouble(histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[8]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);//Convert.ToDouble(transpTabSWEurope.cells[i, amountColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                if (!tours.ContainsKey(tourKey))
                                {
                                    currTour = new TourData();
                                    currTour.tourId = tourId;
                                    currTour.sourceLocId = sourceLocId;
                                    currTour.destLocId = destLocId;
                                    currTour.tourNumberPallets = 0;
                                    currTour.tourPrice = 0;

                                    currTour.customer = customerWithDemandId;
                                    tours.Add(tourKey, currTour);

                                    if (!toursForCustomer.ContainsKey(customerWithDemandId))
                                    {
                                        currCustomerTours = new HashSet<string>();
                                        toursForCustomer.Add(customerWithDemandId, currCustomerTours);
                                    }
                                    else
                                        currCustomerTours = toursForCustomer[customerWithDemandId];

                                    currCustomerTours.Add(tourKey);
                                    if (!wCustomerLocations.ContainsKey(destLocId))
                                    {
                                        newCustomerLocation = new WEPA_Location();
                                        newCustomerLocation.locationName = destLocId;
                                        newCustomerLocation.locationCountry = countryDest;
                                        newCustomerLocation.locationPLZ = cap;
                                        wCustomerLocations.Add(destLocId, newCustomerLocation);
                                    }
                                }
                                else
                                    currTour = tours[tourKey];

                                currTour.tourNumberPallets += numberPallets;
                                currTour.tourPrice += price;

                                customerProductKey = currTour.customer + prodId;

                                if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                                {
                                    currCustomerProductLocationDistribution = new ArticleLocationDistribution();
                                    locationDistributionPerCustomerProduct.Add(customerProductKey, currCustomerProductLocationDistribution);
                                }
                                else
                                    currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                                if (!currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                    currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                else
                                    currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                        currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;

                                if (!locationDistributionPerCustomer.ContainsKey(currTour.customer))
                                {
                                    currCustomerLocationDistribution = new ArticleLocationDistribution();
                                    locationDistributionPerCustomer.Add(currTour.customer, currCustomerLocationDistribution);
                                }
                                else
                                    currCustomerLocationDistribution = locationDistributionPerCustomer[currTour.customer];

                                if (!currCustomerLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                    currCustomerLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                else
                                    currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                        currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;
                            }
                        }
                    }
                    else
                    {
                        if (transportCustomerCustomerChainMatchings.ContainsKey(destCorpId))
                        {
                            currCList = transportCustomerCustomerChainMatchings[destCorpId];

                            foreach (string customerChain in currCList)
                            {
                                customerWithDemandId = customerChain;

                                if (customerWithDemandId != "" && histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[7]].ToString().Trim() != "")//if (customerWithDemandId != "" && transpTabSWEurope.cells[i, qtyPalletsColumnNum].Trim() != "")
                                {
                                    if (isUseGivenCustomerSalesToCustomerTransportRelations && !customersWithTransports.Contains(customerWithDemandId))
                                        customersWithTransports.Add(customerWithDemandId);

                                    tourKey = tourId + sourceLocId + destLocId + customerWithDemandId;
                                    numberPallets = Convert.ToDouble(histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[7]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);//Convert.ToDouble(transpTabSWEurope.cells[i, qtyPalletsColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                    if (numberPallets > 0)
                                    {
                                        price = Convert.ToDouble(histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[8]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);//Convert.ToDouble(transpTabSWEurope.cells[i, amountColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                        if (!tours.ContainsKey(tourKey))
                                        {
                                            currTour = new TourData();
                                            currTour.tourId = tourId;
                                            currTour.sourceLocId = sourceLocId;
                                            currTour.destLocId = destLocId;
                                            currTour.tourNumberPallets = 0;
                                            currTour.tourPrice = 0;

                                            currTour.customer = customerWithDemandId;
                                            tours.Add(tourKey, currTour);

                                            if (!toursForCustomer.ContainsKey(customerWithDemandId))
                                            {
                                                currCustomerTours = new HashSet<string>();
                                                toursForCustomer.Add(customerWithDemandId, currCustomerTours);
                                            }
                                            else
                                                currCustomerTours = toursForCustomer[customerWithDemandId];

                                            currCustomerTours.Add(tourKey);
                                            if (!wCustomerLocations.ContainsKey(destLocId))
                                            {
                                                newCustomerLocation = new WEPA_Location();
                                                newCustomerLocation.locationName = destLocId;
                                                newCustomerLocation.locationCountry = countryDest;
                                                newCustomerLocation.locationPLZ = cap;
                                                wCustomerLocations.Add(destLocId, newCustomerLocation);
                                            }
                                        }
                                        else
                                            currTour = tours[tourKey];

                                        currTour.tourNumberPallets += numberPallets;
                                        currTour.tourPrice += price;

                                        customerProductKey = currTour.customer + prodId;

                                        if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                                        {
                                            currCustomerProductLocationDistribution = new ArticleLocationDistribution();
                                            locationDistributionPerCustomerProduct.Add(customerProductKey, currCustomerProductLocationDistribution);
                                        }
                                        else
                                            currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                                        if (!currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                            currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                        else
                                            currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                                currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;

                                        if (!locationDistributionPerCustomer.ContainsKey(currTour.customer))
                                        {
                                            currCustomerLocationDistribution = new ArticleLocationDistribution();
                                            locationDistributionPerCustomer.Add(currTour.customer, currCustomerLocationDistribution);
                                        }
                                        else
                                            currCustomerLocationDistribution = locationDistributionPerCustomer[currTour.customer];

                                        if (!currCustomerLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                            currCustomerLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                        else
                                            currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                                currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;
                                    }
                                }
                            }
                        }//****
                    }
                    if (transportCustomerCustomerChainMatchings.ContainsKey(corpId))
                    {
                        currCList = transportCustomerCustomerChainMatchings[corpId];

                        foreach (string customerChain in currCList)
                        {
                            customerWithDemandId = customerChain;

                            if (customerWithDemandId != "" && histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[7]].ToString().Trim() != "")//if (customerWithDemandId != "" && transpTabSWEurope.cells[i, qtyPalletsColumnNum].Trim() != "")
                            {
                                if (isUseGivenCustomerSalesToCustomerTransportRelations && !customersWithTransports.Contains(customerWithDemandId))
                                    customersWithTransports.Add(customerWithDemandId);

                                tourKey = tourId + sourceLocId + destLocId + customerWithDemandId;
                                numberPallets = Convert.ToDouble(histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[7]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                if (numberPallets > 0)
                                {
                                    price = Convert.ToDouble(histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[8]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);//Convert.ToDouble(transpTabSWEurope.cells[i, amountColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                    if (!tours.ContainsKey(tourKey))
                                    {
                                        currTour = new TourData();
                                        currTour.tourId = tourId;
                                        currTour.sourceLocId = sourceLocId;
                                        currTour.destLocId = destLocId;
                                        currTour.tourNumberPallets = 0;
                                        currTour.tourPrice = 0;

                                        currTour.customer = customerWithDemandId;
                                        tours.Add(tourKey, currTour);

                                        if (!toursForCustomer.ContainsKey(customerWithDemandId))
                                        {
                                            currCustomerTours = new HashSet<string>();
                                            toursForCustomer.Add(customerWithDemandId, currCustomerTours);
                                        }
                                        else
                                            currCustomerTours = toursForCustomer[customerWithDemandId];

                                        currCustomerTours.Add(tourKey);
                                        if (!wCustomerLocations.ContainsKey(destLocId))
                                        {
                                            newCustomerLocation = new WEPA_Location();
                                            newCustomerLocation.locationName = destLocId;
                                            newCustomerLocation.locationCountry = countryDest;
                                            newCustomerLocation.locationPLZ = cap;
                                            wCustomerLocations.Add(destLocId, newCustomerLocation);
                                        }
                                    }
                                    else
                                        currTour = tours[tourKey];

                                    currTour.tourNumberPallets += numberPallets;
                                    currTour.tourPrice += price;

                                    customerProductKey = currTour.customer + prodId;

                                    if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                                    {
                                        currCustomerProductLocationDistribution = new ArticleLocationDistribution();
                                        locationDistributionPerCustomerProduct.Add(customerProductKey, currCustomerProductLocationDistribution);
                                    }
                                    else
                                        currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                                    if (!currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                        currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                    else
                                        currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                            currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;

                                    if (!locationDistributionPerCustomer.ContainsKey(currTour.customer))
                                    {
                                        currCustomerLocationDistribution = new ArticleLocationDistribution();
                                        locationDistributionPerCustomer.Add(currTour.customer, currCustomerLocationDistribution);
                                    }
                                    else
                                        currCustomerLocationDistribution = locationDistributionPerCustomer[currTour.customer];

                                    if (!currCustomerLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                        currCustomerLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                    else
                                        currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                            currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;
                                }
                            }
                        }
                    }//**
                }
            }
            else
            {

                int currNumberOfPartialMatches;

                currNumberOfPartialMatches = Int32.MaxValue;
               // List<int> rowsLeftToConsider = new List<int>();

                if (isConsiderTourChainAssignments)
                {
                    for (i = histTranspTabNE.firstRow + 1; i < histTranspTabNE.rawTable.Rows .Count ; i++)//for (i = 1; i < transpTab.numberOfRows; i++)
                    {
                        prodId = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[9]].ToString().Trim();//transpTab.cells[i, productColumnNum].Trim();

                        if (histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[0]].ToString().Trim() == "FG")//(transpTab.cells[i, artColumnNum].Trim() == "FG")
                        {
                            tourId = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[1]].ToString().Trim();//transpTab.cells[i, tourColumnNum].Trim();

                            if (tourId == "10001909200MUE")
                                i = i;

                            sourceLocId = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[4]] + histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[5]].ToString().Replace("-", ""); //transpTab.cells[i, sourceCountryColumnNum].Trim() + transpTab.cells[i, sourcePlz1ColumnNum].Trim().Replace("-", "");

                            destLocCountry = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[7]].ToString ().Trim(); //transpTab.cells[i, destCountryColumnNum].Trim();
                            destLocPLZ = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[8]].ToString().Trim().Replace("-", "");//transpTab.cells[i, destPlz1ColumnNum].Trim().Replace("-", "");
                            destLocId = destLocCountry + destLocPLZ;

                            customerId = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[10]].ToString().Trim();//transpTab.cells[i, customerColumn].Trim();
                            shortCustomerId = customerId.Replace(" ", "").Replace("\t", "").ToUpper(); ;

                            customerWithDemandId = "";
                            matchingName = "";

                            if (tourChainAssignments.ContainsKey(tourId))
                            {
                                customerWithDemandId = tourChainAssignments[tourId];

                                if (customerWithDemandId != "" && histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[2]].ToString().Trim() != "" &&
                                    histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[3]].ToString().Trim() != "")//if (customerWithDemandId != "" && transpTab.cells[i, numberPalletsColumnNum].Trim() != "" && transpTab.cells[i, priceColumnNum].Trim() != "")
                                {
                                    if (!transportCustomerCustomerChainMatchings.ContainsKey(customerId))
                                    {
                                        currCList = new HashSet<string>();

                                        transportCustomerCustomerChainMatchings.Add(customerId, currCList);
                                    }

                                    else
                                        currCList = transportCustomerCustomerChainMatchings[customerId];

                                    if (!currCList.Contains(customerWithDemandId))
                                        currCList.Add(customerWithDemandId);

                                    if (isUseGivenCustomerSalesToCustomerTransportRelations && !customersWithTransports.Contains(customerWithDemandId))
                                        customersWithTransports.Add(customerWithDemandId);

                                    tourKey = tourId + sourceLocId + destLocId + customerWithDemandId;

                                    numberPallets = Convert.ToDouble(histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[2]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);//Convert.ToDouble(transpTab.cells[i, numberPalletsColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                    if (numberPallets > 0)
                                    {
                                        price = Convert.ToDouble(histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[3]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);//Convert.ToDouble(transpTab.cells[i, priceColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                        if (!tours.ContainsKey(tourKey))
                                        {
                                            currTour = new TourData();
                                            currTour.tourId = tourId;
                                            currTour.sourceLocId = sourceLocId;
                                            currTour.destLocId = destLocId;
                                            currTour.tourNumberPallets = 0;
                                            currTour.tourPrice = 0;

                                            currTour.customer = customerWithDemandId;
                                            tours.Add(tourKey, currTour);

                                            if (!toursForCustomer.ContainsKey(customerWithDemandId))
                                            {
                                                currCustomerTours = new HashSet<string>();
                                                toursForCustomer.Add(customerWithDemandId, currCustomerTours);
                                            }
                                            else
                                                currCustomerTours = toursForCustomer[customerWithDemandId];

                                            currCustomerTours.Add(tourKey);

                                            if (!wCustomerLocations.ContainsKey(destLocId))
                                            {
                                                newCustomerLocation = new WEPA_Location();
                                                newCustomerLocation.locationName = destLocId;
                                                newCustomerLocation.locationCountry = destLocCountry;
                                                newCustomerLocation.locationPLZ = destLocPLZ;
                                                wCustomerLocations.Add(destLocId, newCustomerLocation);
                                            }
                                        }
                                        else
                                            currTour = tours[tourKey];

                                        currTour.tourNumberPallets += numberPallets;
                                        currTour.tourPrice += price;

                                        if (!currTour.productsOnTour.Contains(prodId))
                                            currTour.productsOnTour.Add(prodId);

                                        customerProductKey = currTour.customer + prodId;

                                        currCustomerLocation = currTour.customer + currTour.destLocId;

                                        if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                                        {
                                            currCustomerProductLocationDistribution = new ArticleLocationDistribution();
                                            locationDistributionPerCustomerProduct.Add(customerProductKey, currCustomerProductLocationDistribution);
                                        }
                                        else
                                            currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                                        if (!currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                            currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                        else
                                            currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                                currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;

                                        if (!locationDistributionPerCustomer.ContainsKey(currTour.customer))
                                        {
                                            currCustomerLocationDistribution = new ArticleLocationDistribution();
                                            locationDistributionPerCustomer.Add(currTour.customer, currCustomerLocationDistribution);
                                        }
                                        else
                                            currCustomerLocationDistribution = locationDistributionPerCustomer[currTour.customer];

                                        if (!currCustomerLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                            currCustomerLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                        else
                                            currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                                currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;
                                    }
                                }
                            }
                  
                        }
                    }
                }
               
                do
                {
                    if (!isConsiderTourChainAssignments)
                    {
                        foreach (string customerName in customersWithDemands)
                        {
                            msg = "Kunde:" + customerName;

                            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
                            if ((
                                        isUseGivenCustomerSalesToCustomerTransportRelations &&
                                        (customerChainTransportCustomersRelations.ContainsKey(customerName) || currNumberOfPartialMatches == 1) &&
                                        (
                                               (currNumberOfPartialMatches != Int32.MaxValue && !customersWithTransports.Contains(customerName)) || currNumberOfPartialMatches == Int32.MaxValue
                                        )
                                )
                                || !isUseGivenCustomerSalesToCustomerTransportRelations)
                            {

                                if (isUseGivenCustomerSalesToCustomerTransportRelations && currNumberOfPartialMatches > 1)
                                    currentTransportCustomers = customerChainTransportCustomersRelations[customerName];
                                else
                                    currentTransportCustomers = null;


                                for (i = histTranspTabNE.firstRow + 1; i < histTranspTabNE.rawTable.Rows .Count ; i++)//for (i = 1; i < transpTab.numberOfRows; i++)
                                {

                                    prodId = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[9]].ToString().Trim();//transpTab.cells[i, productColumnNum].Trim();

                                    if (histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[0]].ToString().Trim() == "FG")//(transpTab.cells[i, artColumnNum].Trim() == "FG")
                                    {
                                        tourId = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[1]].ToString().Trim();//transpTab.cells[i, tourColumnNum].Trim();
                                        sourceLocId = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[4]] + histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[5]].ToString().Replace("-", ""); //transpTab.cells[i, sourceCountryColumnNum].Trim() + transpTab.cells[i, sourcePlz1ColumnNum].Trim().Replace("-", "");

                                        destLocCountry = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[7]].ToString ().Trim(); //transpTab.cells[i, destCountryColumnNum].Trim();
                                        destLocPLZ = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[8]].ToString().Trim().Replace("-", ""); ;//transpTab.cells[i, destPlz1ColumnNum].Trim().Replace("-", "");
                                        destLocId = destLocCountry + destLocPLZ;

                                        customerId = histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[10]].ToString().Trim();//transpTab.cells[i, customerColumn].Trim();
                                        shortCustomerId = customerId.Replace(" ", "").Replace("\t", "").ToUpper(); ;

                                        customerWithDemandId = "";
                                        matchingName = "";

                                        //if (tourChainAssignments.ContainsKey(tourId))
                                        //{
                                        //    customerWithDemandId = tourChainAssignments[tourId];
                                        //}
                                        //else
                                        //{

                                        if (isUseGivenCustomerSalesToCustomerTransportRelations && currNumberOfPartialMatches > 1)
                                        {

                                            foreach (TransportCustomerEntry transpCustomerEntry in currentTransportCustomers)
                                            {

                                                shortTranspCustomerName = transpCustomerEntry.compactName;

                                                if (currNumberOfPartialMatches == Int32.MaxValue && shortTranspCustomerName.Contains(shortCustomerId))
                                                {
                                                    customerWithDemandId = customerName;
                                                    break;
                                                }
                                                else if (currNumberOfPartialMatches != Int32.MaxValue && !customersWithTransports.Contains(customerName))
                                                {
                                                    if (customerId.ToUpper().Contains(customerName))
                                                    {
                                                        string[] splitCustomerName = customerId.Split(nameSeparators);
                                                        int foundPartialMatches = 0;

                                                        for (j = 0; j < splitCustomerName.Length; j++)
                                                        {
                                                            string currPart = splitCustomerName[j].ToUpper();
                                                            if (transpCustomerEntry.nameParts.Contains(currPart) && currPart != customerName)
                                                                foundPartialMatches++;
                                                        }

                                                        if (foundPartialMatches >= currNumberOfPartialMatches)
                                                            customerWithDemandId = customerName;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //Try full name of customer given in sales
                                            if (customerId.ToUpper().Contains(customerName.ToUpper())) //Matchen von Kundennamen !
                                            {
                                                customerWithDemandId = customerName;
                                            }

                                            if (customerWithDemandId == "")
                                            {
                                                //Try first part of the name of customer given in sales
                                                posEndFirstPart = customerName.IndexOf(" ");

                                                if (posEndFirstPart != -1)
                                                    firstPartCustomerName = customerName.Substring(0, posEndFirstPart);
                                                else
                                                    firstPartCustomerName = customerName;

                                                if (customerId.ToUpper().Contains(firstPartCustomerName.ToUpper())) //Matchen von Kundennamen !
                                                {
                                                    customerWithDemandId = customerName;
                                                }
                                            }

                                        }
                                        //}
                                        if (customerWithDemandId != "" && histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[2]].ToString().Trim() != "" &&
                                            histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[3]].ToString().Trim() != "")//if (customerWithDemandId != "" && transpTab.cells[i, numberPalletsColumnNum].Trim() != "" && transpTab.cells[i, priceColumnNum].Trim() != "")
                                        {
                                            if (!transportCustomerCustomerChainMatchings.ContainsKey(customerId))
                                            {
                                                currCList = new HashSet<string>();

                                                transportCustomerCustomerChainMatchings.Add(customerId, currCList);
                                            }

                                            else
                                                currCList = transportCustomerCustomerChainMatchings[customerId];

                                            if (!currCList.Contains(customerWithDemandId))
                                                currCList.Add(customerWithDemandId);

                                            if (isUseGivenCustomerSalesToCustomerTransportRelations && !customersWithTransports.Contains(customerWithDemandId))
                                                customersWithTransports.Add(customerWithDemandId);

                                            tourKey = tourId + sourceLocId + destLocId + customerWithDemandId;

                                            numberPallets = Convert.ToDouble(histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[2]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);//Convert.ToDouble(transpTab.cells[i, numberPalletsColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                            if (numberPallets > 0)
                                            {
                                                price = Convert.ToDouble(histTranspTabNE.rawTable.Rows[i].ItemArray[histTranspTabNE.colNums[3]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);//Convert.ToDouble(transpTab.cells[i, priceColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                                if (!tours.ContainsKey(tourKey))
                                                {
                                                    currTour = new TourData();
                                                    currTour.tourId = tourId;
                                                    currTour.sourceLocId = sourceLocId;
                                                    currTour.destLocId = destLocId;
                                                    currTour.tourNumberPallets = 0;
                                                    currTour.tourPrice = 0;

                                                    currTour.customer = customerWithDemandId;
                                                    tours.Add(tourKey, currTour);

                                                    if (!toursForCustomer.ContainsKey(customerWithDemandId))
                                                    {
                                                        currCustomerTours = new HashSet<string>();
                                                        toursForCustomer.Add(customerWithDemandId, currCustomerTours);
                                                    }
                                                    else
                                                        currCustomerTours = toursForCustomer[customerWithDemandId];

                                                    currCustomerTours.Add(tourKey);

                                                    if (!wCustomerLocations.ContainsKey(destLocId))
                                                    {
                                                        newCustomerLocation = new WEPA_Location();
                                                        newCustomerLocation.locationName = destLocId;
                                                        newCustomerLocation.locationCountry = destLocCountry;
                                                        newCustomerLocation.locationPLZ = destLocPLZ;
                                                        wCustomerLocations.Add(destLocId, newCustomerLocation);
                                                    }
                                                }
                                                else
                                                    currTour = tours[tourKey];

                                                currTour.tourNumberPallets += numberPallets;
                                                currTour.tourPrice += price;

                                                if (!currTour.productsOnTour.Contains(prodId))
                                                    currTour.productsOnTour.Add(prodId);

                                                customerProductKey = currTour.customer + prodId;

                                                currCustomerLocation = currTour.customer + currTour.destLocId;

                                                if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                                                {
                                                    currCustomerProductLocationDistribution = new ArticleLocationDistribution();
                                                    locationDistributionPerCustomerProduct.Add(customerProductKey, currCustomerProductLocationDistribution);
                                                }
                                                else
                                                    currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                                                if (!currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                                    currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                                else
                                                    currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                                        currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;

                                                if (!locationDistributionPerCustomer.ContainsKey(currTour.customer))
                                                {
                                                    currCustomerLocationDistribution = new ArticleLocationDistribution();
                                                    locationDistributionPerCustomer.Add(currTour.customer, currCustomerLocationDistribution);
                                                }
                                                else
                                                    currCustomerLocationDistribution = locationDistributionPerCustomer[currTour.customer];

                                                if (!currCustomerLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                                    currCustomerLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                                else
                                                    currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                                        currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;
                                            }
                                        }
                                    }
                                }//ggg
                            }
                        }
                    }
                    foreach (string customerName in customersWithDemands)
                    {
                        msg = "Kunde:" + customerName;
                        loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

                        if (
                                (
                                    isUseGivenCustomerSalesToCustomerTransportRelations &&
                                    (customerChainTransportCustomersRelations.ContainsKey(customerName) || currNumberOfPartialMatches == 1) &&
                                    (
                                        (currNumberOfPartialMatches != Int32.MaxValue && !customersWithTransports.Contains(customerName)) ||
                                        currNumberOfPartialMatches == Int32.MaxValue)
                                )
                                ||
                                !isUseGivenCustomerSalesToCustomerTransportRelations
                           )
                        {
                            if (isUseGivenCustomerSalesToCustomerTransportRelations && currNumberOfPartialMatches > 1)
                                currentTransportCustomers = customerChainTransportCustomersRelations[customerName];
                            else
                                currentTransportCustomers = null;
                           
                            for (i = histTranspTabSW.firstRow+1; i < histTranspTabSW.rawTable.Rows .Count ; i++)//for (i = 1; i < transpTabSWEurope.numberOfRows; i++)
                            {
                                prodId = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[6]].ToString().Trim();//transpTabSWEurope.cells[i, itemCodeColumnNum].Trim();
                                whseId = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[0]].ToString().Trim();//transpTabSWEurope.cells[i, whseColumnNum].Trim();
                                dateExp = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[1]].ToString().Trim();//transpTabSWEurope.cells[i, dateExpColumnNum].Trim();
                                cap = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[4]].ToString().Trim();//transpTabSWEurope.cells[i, capColumnNum].Trim();
                                countryDest = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[5]].ToString().Trim();//transpTabSWEurope.cells[i, countryDestColumnNum].Trim();
                                destCorpId = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[2]].ToString().Trim();//transpTabSWEurope.cells[i, destCorpColumnNum].Trim();
                                corpId = histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[3]].ToString().Trim();//transpTabSWEurope.cells[i, corpColumnNum].Trim();

                                shortCustomerId = destCorpId.Replace(" ", "").Replace("\t", "").ToUpper();
                                shortCustomerId2 = corpId.Replace(" ", "").Replace("\t", "").ToUpper();
                                whseLocId = warehouseLocsSWEurope[whseId];
                                tourId = whseId + dateExp + countryDest;
                                sourceLocId = whseLocId;
                                destLocId = countryDest + cap;

                                customerWithDemandId = "";
                                matchingName = "";
                                
                                    if (isUseGivenCustomerSalesToCustomerTransportRelations && currNumberOfPartialMatches > 1)
                                    {
                                        foreach (TransportCustomerEntry transpCustomerEntry in currentTransportCustomers)
                                        {
                                            shortTranspCustomerName = transpCustomerEntry.fullName.Replace(" ", "").Replace("\t", "");

                                            if (currNumberOfPartialMatches == Int32.MaxValue && (shortTranspCustomerName.Contains(shortCustomerId)))
                                            {
                                                customerWithDemandId = customerName;
                                                matchingName = destCorpId;
                                                break;
                                            }
                                            else if (currNumberOfPartialMatches == Int32.MaxValue && ((shortTranspCustomerName.Contains(shortCustomerId2) && shortCustomerId2.Length > 0)))
                                            {
                                                customerWithDemandId = customerName;
                                                matchingName = corpId;
                                                break;
                                            }
                                            else if (currNumberOfPartialMatches != Int32.MaxValue && !customersWithTransports.Contains(customerName))
                                            {
                                                if (destCorpId.ToUpper().Contains(customerName))
                                                {
                                                    string[] splitCustomerName = destCorpId.Split(nameSeparators);

                                                    int foundPartialMatches = 0;
                                                    for (j = 0; j < splitCustomerName.Length; j++)
                                                    {
                                                        string currPart = splitCustomerName[j].ToUpper();
                                                        if (transpCustomerEntry.nameParts.Contains(currPart) && customerName != currPart)
                                                            foundPartialMatches++;
                                                    }

                                                    if (foundPartialMatches >= currNumberOfPartialMatches)
                                                    {
                                                        customerWithDemandId = customerName;
                                                        matchingName = destCorpId;
                                                    }
                                                }

                                                if (customerWithDemandId == "")
                                                {
                                                    if (corpId.ToUpper().Contains(customerName))
                                                    {
                                                        string[] splitCustomerName = corpId.Split(nameSeparators);


                                                        int foundPartialMatches = 0;
                                                        for (j = 0; j < splitCustomerName.Length; j++)
                                                        {
                                                            string currPart = splitCustomerName[j].ToUpper();
                                                            if (transpCustomerEntry.nameParts.Contains(currPart) && customerName != currPart)
                                                                foundPartialMatches++;
                                                        }

                                                        if (foundPartialMatches >= currNumberOfPartialMatches)
                                                        {
                                                            customerWithDemandId = customerName;
                                                            matchingName = corpId;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //Try full name of customer given in sales
                                        if (destCorpId.ToUpper().Contains(customerName.ToUpper())) //Matchen von Kundennamen !
                                        {
                                            customerWithDemandId = customerName;
                                            matchingName = destCorpId;
                                        }
                                        else if (corpId.ToUpper().Contains(customerName.ToUpper())) //Matchen von Kundennamen !
                                        {
                                            customerWithDemandId = customerName;
                                            matchingName = corpId;
                                        }

                                        if (customerWithDemandId == "")
                                        {
                                            //Try first part of the name of customer given in sales
                                            posEndFirstPart = customerName.IndexOf(" ");

                                            if (posEndFirstPart != -1)
                                                firstPartCustomerName = customerName.Substring(0, posEndFirstPart);
                                            else
                                                firstPartCustomerName = customerName;

                                            if (destCorpId.ToUpper().Contains(firstPartCustomerName.ToUpper())) //Matchen von Kundennamen !
                                            {
                                                customerWithDemandId = customerName;
                                                matchingName = destCorpId;
                                            }
                                            else if (corpId.ToUpper().Contains(firstPartCustomerName.ToUpper())) //Matchen von Kundennamen !
                                            {
                                                customerWithDemandId = customerName;
                                                matchingName = corpId;
                                            }
                                        }
                                    }

                                    if (customerWithDemandId != "" && histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[7]].ToString().Trim() != "")//if (customerWithDemandId != "" && transpTabSWEurope.cells[i, qtyPalletsColumnNum].Trim() != "")
                                {
                                    if (!transportCustomerCustomerChainMatchings.ContainsKey(matchingName))
                                    {
                                        currCList =new HashSet<string> ();
                                        transportCustomerCustomerChainMatchings.Add(matchingName, currCList);
                                    }
                                    else
                                        currCList = transportCustomerCustomerChainMatchings[matchingName];

                                    if (!currCList.Contains (customerWithDemandId))
                                        currCList .Add (customerWithDemandId);
                                 
                                    if (isUseGivenCustomerSalesToCustomerTransportRelations && !customersWithTransports.Contains(customerWithDemandId))
                                        customersWithTransports.Add(customerWithDemandId);

                                    tourKey = tourId + sourceLocId + destLocId + customerWithDemandId;
                                    numberPallets = Convert.ToDouble(histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[7]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);//Convert.ToDouble(transpTabSWEurope.cells[i, qtyPalletsColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                    if (numberPallets > 0)
                                    {
                                        price = Convert.ToDouble(histTranspTabSW.rawTable.Rows[i].ItemArray[histTranspTabSW.colNums[8]].ToString().Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);// Convert.ToDouble(transpTabSWEurope.cells[i, amountColumnNum].Trim().Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                                        if (!tours.ContainsKey(tourKey))
                                        {
                                            currTour = new TourData();
                                            currTour.tourId = tourId;
                                            currTour.sourceLocId = sourceLocId;
                                            currTour.destLocId = destLocId;
                                            currTour.tourNumberPallets = 0;
                                            currTour.tourPrice = 0;

                                            currTour.customer = customerWithDemandId;
                                            tours.Add(tourKey, currTour);

                                            if (!toursForCustomer.ContainsKey(customerWithDemandId))
                                            {
                                                currCustomerTours = new HashSet<string>();
                                                toursForCustomer.Add(customerWithDemandId, currCustomerTours);
                                            }
                                            else
                                                currCustomerTours = toursForCustomer[customerWithDemandId];

                                            currCustomerTours.Add(tourKey);
                                            if (!wCustomerLocations.ContainsKey(destLocId))
                                            {
                                                newCustomerLocation = new WEPA_Location();
                                                newCustomerLocation.locationName = destLocId;
                                                newCustomerLocation.locationCountry = countryDest;
                                                newCustomerLocation.locationPLZ = cap;
                                                wCustomerLocations.Add(destLocId, newCustomerLocation);
                                            }
                                        }
                                        else
                                            currTour = tours[tourKey];

                                        currTour.tourNumberPallets += numberPallets;
                                        currTour.tourPrice += price;

                                        customerProductKey = currTour.customer + prodId;

                                        if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                                        {
                                            currCustomerProductLocationDistribution = new ArticleLocationDistribution();
                                            locationDistributionPerCustomerProduct.Add(customerProductKey, currCustomerProductLocationDistribution);
                                        }
                                        else
                                            currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                                        if (!currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                            currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                        else
                                            currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                                currCustomerProductLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;

                                        if (!locationDistributionPerCustomer.ContainsKey(currTour.customer))
                                        {
                                            currCustomerLocationDistribution = new ArticleLocationDistribution();
                                            locationDistributionPerCustomer.Add(currTour.customer, currCustomerLocationDistribution);
                                        }
                                        else
                                            currCustomerLocationDistribution = locationDistributionPerCustomer[currTour.customer];

                                        if (!currCustomerLocationDistribution.numberPalletsTransportedPerLocation.ContainsKey(currTour.destLocId))
                                            currCustomerLocationDistribution.numberPalletsTransportedPerLocation.Add(currTour.destLocId, numberPallets);
                                        else
                                            currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] =
                                                currCustomerLocationDistribution.numberPalletsTransportedPerLocation[currTour.destLocId] + numberPallets;
                                    }
                                }
                            }
                        }
                    }

                    if (isUseGivenCustomerSalesToCustomerTransportRelations)
                    {
                        if (currNumberOfPartialMatches == Int32.MaxValue)
                            currNumberOfPartialMatches = MAX_PARTIAL_TRANSPORT_CUSTOMER_NAME_MATCHES;
                        else if (currNumberOfPartialMatches > MIN_PARTIAL_TRANSPORT_CUSTOMER_NAME_MATCHES)
                            currNumberOfPartialMatches--;
                        else
                            currNumberOfPartialMatches = 0; 
                    }
                    else
                        currNumberOfPartialMatches = 0;
                }
                while (currNumberOfPartialMatches > 0);
            }

            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
           
            if (!File.Exists(matchingFileName))
            {
                msg = "Schreibe Matchings...";
                loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

                currMatchingFile = new CSV_File(ref matchingFileName);
                
                matchingTab = new DatTable();

                matchingTab.numberOfRows = 0;

                foreach (KeyValuePair<string, HashSet<string>> m in transportCustomerCustomerChainMatchings)
                {
                    matchingTab.numberOfRows += m.Value.Count;
                }
                
                matchingTab.numberOfColumns = 2;
                matchingTab.cells = new string[matchingTab.numberOfRows, matchingTab.numberOfColumns];
                i = 0;
                foreach (KeyValuePair<string, HashSet<string>> m in transportCustomerCustomerChainMatchings)
                {
                    foreach (string transpCustomerName in m.Value)
                    {
                        matchingTab.cells[i, 0] = m.Key;
                        matchingTab.cells[i, 1] = transpCustomerName;
                        i++;
                    }
                }

                currMatchingFile.setCSVDelimier(ref csvDelim);
                currMatchingFile.setHeadline(ref matchingHL);
                currMatchingFile.setIsHeadLine(true);
                currMatchingFile.OpenForWriting(false);
                currMatchingFile.ArrayToTable(matchingTab);
                currMatchingFile.CloseDoc();

                msg = "Fertig.";
                loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
            }         
        }

        protected void getDemandData(SNP_WEPA_Instance pI)
        {
            Optimizer_Data.Excel_Document demDoc;
            string completeFN;
            XLSTable tab;
            int i, j;
            int currProductIndex;
            int numberPeriods;
            string currCellStr;
            string currArtName;
            string currHistoricalResource;
            int endOfFirstWord;
            Product currProduct;
            Period currPeriod;
            RawDemand currDemand;
            HistProductionEntry currHPE;
            string msg;

            preliminaryProductsNames = new Dictionary<string, string>();
            completeFN = srcPath + Path.DirectorySeparatorChar + demandFileName;

            if (File.Exists(completeFN))
            {

                msg = "Lese Bedarfe...";
                loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

                rawDemands = new Dictionary<string, RawDemand>();
                customersWithDemands = new HashSet<string>();
                preliminaryProducts = new Dictionary<string, Product>();

                historicalProductionProcesses = new HashSet<string>();
                preliminaryHistoricalProductions = new Dictionary<string, HistProductionEntry>();
                articlesWithDemandsOfCustomer = new Dictionary<string, HashSet<string>>();
                currProductIndex = 0;


                demDoc = new Optimizer_Data.Excel_Document(completeFN);

                demDoc.OpenDoc();

                tab = loadTableFromExcelDocument(demDoc, demandSheetName, demandColNames, demandColMatchTypes);//demDoc.TableToArray();
                demDoc.CloseDoc();

                numberPeriods = 0;

                string currPeriodName;

                int startPeriod;
                int monthNum;
                startPeriod = Convert.ToInt32(tab.rawTable.Rows[tab.firstRow].ItemArray[tab.colNums[4]].ToString().Substring(1));
                monthNum = firstMonth;
                for (i = tab.colNums[4]; i < tab.rawTable.Columns .Count ; i++)
                {
                    currPeriodName = "M" + Convert.ToString(i - tab.colNums[4] + startPeriod);
                    if (tab.rawTable.Rows[tab.firstRow].ItemArray [i].ToString ().Trim () != currPeriodName)
                        break;

                    currPeriod = new Period();
                    currPeriod.PeriodName = MONTHS[monthNum]; //currPeriodName;//currCellStr;
                    pI.addPeriod(ref currPeriod);
                    numberPeriods++;
                    monthNum++;
                    if (monthNum == MONTHS.Length)
                        monthNum = 0;
                }

                for (i = tab.firstRow + 1; i < tab.rawTable.Rows .Count ; i++)
                {
                    currArtName = tab.rawTable.Rows[i].ItemArray[tab.colNums[0]].ToString().Trim();//tab.cells[i, articleColumnPosition].Trim();
                    currCellStr = tab.rawTable.Rows[i].ItemArray[tab.colNums[3]].ToString().Trim();//tab.cells[i, historicalResourceColumnPosition].Trim();

                    endOfFirstWord = currCellStr.IndexOf(" ");
                    if (endOfFirstWord != -1)
                        currHistoricalResource = currCellStr.Substring(0, endOfFirstWord);
                    else
                        currHistoricalResource = currCellStr;

                    if (currHistoricalResource != "HW")
                    {
                        if (!preliminaryProducts.ContainsKey(currArtName))
                        {
                            currProduct = new Product();
                            currProduct.ProductName = currArtName;
                            currProduct.PalletsPerTon = 1000 / DEFAULT_KG_PER_PALLET;

                            preliminaryProducts.Add(currArtName, currProduct);

                            preliminaryProductsNames.Add(currArtName, tab.rawTable.Rows [i].ItemArray[tab.colNums[5]].ToString ().Trim ());

                            currProductIndex++;
                        }
                        else
                            currProduct = preliminaryProducts[currArtName];

                        monthNum = firstMonth ;
                        for (j = 0; j < numberPeriods; j++)
                        {
                            currCellStr = tab.rawTable.Rows[i].ItemArray[j + tab.colNums[4]].ToString().Trim(); //tab.cells[i, j + firstDemandColumnPosition];
                            if (currCellStr != "")
                            {
                                double currDemVal = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                if (currDemVal > 0)
                                {
                                    string currDemandKey, currCustomerName, currHistProdKey, currCountryKey;
                                    currCustomerName = tab.rawTable.Rows[i].ItemArray[tab.colNums[1]].ToString().Trim();//tab.cells[i, customerColumnPosition].Trim();
                                    currPeriodName = MONTHS[monthNum];//tab.rawTable.cells[tab.firstRow, j + tab.colNums[4]].Trim();//tab.cells[1, j + firstDemandColumnPosition].Trim();
                                    currCountryKey = getCountryAbbrevISO3166Alpha2(tab.rawTable.Rows[i].ItemArray[tab.colNums[2]].ToString().Trim());//getCountryAbbrevISO3166Alpha2(tab.cells[i, countryColumnPosition].Trim());

                                    currDemandKey = currProduct.ProductName + currCustomerName + currPeriodName;
                                    if (isConsiderCountryInfoOfDemands)
                                        currDemandKey = currDemandKey + currCountryKey;


                                    if (!customersWithDemands.Contains(currCustomerName))
                                        customersWithDemands.Add(currCustomerName);

                                    HashSet<string> currProductsOfCustomers;
                                    if (!articlesWithDemandsOfCustomer.ContainsKey(currCustomerName))
                                    {
                                        currProductsOfCustomers = new HashSet<string>();
                                        articlesWithDemandsOfCustomer.Add(currCustomerName, currProductsOfCustomers);
                                    }
                                    else
                                        currProductsOfCustomers = articlesWithDemandsOfCustomer[currCustomerName];

                                    if (!currProductsOfCustomers.Contains(currProduct.ProductName))
                                        currProductsOfCustomers.Add(currProduct.ProductName);

                                    if (!rawDemands.ContainsKey(currDemandKey))
                                    {
                                        currDemand = new RawDemand();
                                        currDemand.ProductName = currProduct.ProductName;
                                        currDemand.PeriodName = currPeriodName;//tab.cells[1, j + firstDemandColumnPosition].Trim();
                                        currDemand.CustomerName = currCustomerName;//tab.cells[i, customerColumnPosition].Trim();
                                        currDemand.Val = 0;
                                        if (isConsiderCountryInfoOfDemands)
                                            currDemand.CountryName = currCountryKey;
                                        rawDemands.Add(currDemandKey, currDemand);
                                    }
                                    else
                                        currDemand = rawDemands[currDemandKey];

                                    currDemand.Val += currDemVal;

                                    currHistProdKey = currProduct.ProductName + currHistoricalResource + currPeriodName;
                                    if (!preliminaryHistoricalProductions.ContainsKey(currHistProdKey))
                                    {
                                        currHPE = new HistProductionEntry();
                                        currHPE.PeriodName = currPeriodName;
                                        currHPE.ProcessName = currProduct.ProductName + currHistoricalResource;
                                        currHPE.Quantity = 0;

                                        if (!historicalProductionProcesses.Contains(currHPE.ProcessName))
                                            historicalProductionProcesses.Add(currHPE.ProcessName);

                                        preliminaryHistoricalProductions.Add(currHistProdKey, currHPE);
                                    }
                                    else
                                        currHPE = preliminaryHistoricalProductions[currHistProdKey];

                                    currHPE.Quantity += currDemVal;
                                }
                            }
                            monthNum++;
                            if (monthNum == MONTHS.Length)
                                monthNum = 0;
                        }
                    }
                }

                msg = "Fertig.";
                loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
            }
            else
            throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, demandFileName);
        }

        protected void getTonnageData(SNP_WEPA_Instance pI)
        {
            Optimizer_Data.Excel_Document tnDoc;
            string completeFN;
            XLSTable tab;
            string currResourceName, lastResourceName;
            string currAutomatName,currLocName,currArtName;
            ProductionProcess currProductionProcess;
            int i, j;
            int firstConversionPosition, numberOfConversionPositions;
            string currCellStr;
            object tempObj;
            string msg;

             completeFN=srcPath + Path.DirectorySeparatorChar + tonnageFileName;

             if (File.Exists(completeFN))
             {
                 msg = "Lese Historische Tonnagen...";
                 loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

                 tonnageProductionProcesses = new Dictionary<string, ProductionProcess>();//new CSVector();

                 tnDoc = new Optimizer_Data.Excel_Document(completeFN);

                 tnDoc.OpenDoc();

                 tab = loadTableFromExcelDocument(tnDoc, tonnageSheetName, tonnageColNames, tonnageColMatchTypes);//tnDoc.TableToArray ();

                 firstConversionPosition = 0;
                 numberOfConversionPositions = 0;

                 for (i = tab.colNums[0]; i < tab.rawTable.Columns .Count; i++)
                 {
                     currCellStr = tab.rawTable.Rows[tab.rowNums[0]].ItemArray[i].ToString().Trim();
                     if (currCellStr == tonnageColNames[0])//"converting in ton")
                     {
                         if (numberOfConversionPositions == 0)
                             firstConversionPosition = i;

                         numberOfConversionPositions++;
                     }
                 }

                 currResourceName = "";
                 for (j = tab.firstRow + 1; j < tab.rawTable.Rows .Count ; j++)//for (j = 2; j < tab.numberOfRows; j++)
                 {
                     currArtName = tab.rawTable.Rows [j].ItemArray [ tab.colNums[3]].ToString ().Trim();

                     if (currArtName != "all items")
                     {
                         for (i = 0; i < numberOfConversionPositions; i++)
                         {
                             currCellStr = tab.rawTable.Rows[j].ItemArray[i + tab.colNums[0]].ToString().Trim();
                             if (currCellStr != "")
                                 break;
                         }

                         lastResourceName = currResourceName;

                         if (i < numberOfConversionPositions)
                         {
                             currAutomatName = tab.rawTable.Rows[j].ItemArray[tab.colNums[1]].ToString().Trim();  //tab.cells [j,resourceColumnPosition].Trim () ;
                             currLocName = tab.rawTable.Rows[j].ItemArray[tab.colNums[2]].ToString().Trim();//tab.cells [j,locationColumnPosition].Trim ();

                             if (!articlesWithTonnages.Contains(currArtName))
                                 articlesWithTonnages.Add(currArtName);

                             currResourceName = currAutomatName;
                             tempObj = currResourceName;

                             if (preliminaryResources.ContainsKey(currResourceName))//(currResource != null)
                             {
                                 currProductionProcess = new ProductionProcess();

                                 currProductionProcess.ProductName = currArtName;
                                 currProductionProcess.ResourceName = currAutomatName;
                                 currProductionProcess.LocationName = currLocName;

                                 tonnageProductionProcesses.Add((string)currProductionProcess.Key, currProductionProcess);
                             }
                         }
                     }
                 }
                 msg = "Fertig.";
                 loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
             }
             else
                 throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, tonnageFileName);
        }

        protected void getBaseDataAndWorkingPlans(SNP_WEPA_Instance pI)
        {
            Optimizer_Data.Excel_Document bDWPDoc, aBDWPDoc;
            string completeFN1, completeFN2;
            XLSTable tab;
            int i, j;
            string currTitleCellStr, currCellStr;
            string currProcessName;
            string currAutomatName, currArtName;
            double kgPerPallet;
            double currTonPerShift;
            object tempObj;
            ProductionProcess currProcess;
            Product currProduct;
            Resource currResource;
            string msg;
            StreamWriter pOutputFile;
            string currPlantName;
            int endOfAutomatName;
            bool isResource, isTonnageNecessary, isTonnageAvailable;
            ProcessInformationState currPCIS;

            completeFN1 = srcPath + Path.DirectorySeparatorChar + baseDataAndWorkingPlansFileName;

            if (!File.Exists(completeFN1))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, baseDataAndWorkingPlansFileName);
            completeFN2 = srcPath + Path.DirectorySeparatorChar + additionalBaseDataAndWorkingPlansFileName;
            if (!File.Exists(completeFN2))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, additionalBaseDataAndWorkingPlansFileName);

            msg = "Lese Arbeitspläne...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

            pOutputFile = new StreamWriter("productsWithoutHistoricalProduction.csv");

            preliminaryProductionProcesses = new Dictionary<string, ProductionProcess>();//new CSVector();

            productsWithProcesses = new HashSet<string>();

            bDWPDoc = new Optimizer_Data.Excel_Document(completeFN1);
            bDWPDoc.OpenDoc();
            tab = loadTableFromExcelDocument(bDWPDoc, workingPlansSheetName, workingPlansColNames, workingPlansColMatchTypes);//bDWPDoc.TableToArray();
            bDWPDoc.CloseDoc();

            for (i = tab.firstRow + 1; i < tab.rawTable.Rows .Count ; i++)//for (i = 1; i < tab.numberOfRows; i++)
            {
                currArtName = tab.rawTable.Rows[i].ItemArray[tab.colNums[0]].ToString().Trim();//tab.cells [i,articleColumnPosition].Trim ();

                if (preliminaryProducts.ContainsKey(currArtName))
                {
                    currProduct = preliminaryProducts[currArtName];

                    currCellStr = tab.rawTable.Rows[i].ItemArray[tab.colNums[1]].ToString().Trim();//tab.cells[i, kgPerPalletColumnPosition].Trim();

                    if (currCellStr != "")
                        kgPerPallet = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                    else
                        kgPerPallet = 0;

                    if (kgPerPallet > 0)
                    {
                        currProduct.PalletsPerTon = 1000 / kgPerPallet;

                        for (j = tab.colNums[2]; j < tab.rawTable.Columns .Count; j++)//for (j = firstAutomatPosition; j < tab.numberOfColumns; j++)
                        {
                            currTitleCellStr = tab.rawTable.Rows[tab.rowNums[2]].ItemArray[j].ToString().Trim();
                            currCellStr = tab.rawTable.Rows[i].ItemArray[j].ToString().Trim();

                            if (currTitleCellStr.Substring(0, workingPlansColNames[2].Length) == workingPlansColNames[2])
                            {
                                currAutomatName = currTitleCellStr.Substring(workingPlansColNames[2].Length + 1).Trim();
                                currProcessName = currArtName + currAutomatName;

                                isResource = isTonnageNecessary = isTonnageAvailable = false;
                                
                               // tempObj = currAutomatName;

                                if (preliminaryResources.ContainsKey(currAutomatName))//(currResource != null) //Process is considered only if the resource exists with larger than zero capacity in the planning horizon
                                {
                                    isResource = true;
                                    currResource = preliminaryResources[currAutomatName];
                                    currProcessName = currArtName + currAutomatName;
                                   // object k = currProcessName;
                                    if (articlesWithTonnages.Contains(currArtName))
                                        isTonnageNecessary = true;
                                    if (tonnageProductionProcesses.ContainsKey(currProcessName))
                                    {
                                        currProcess = tonnageProductionProcesses[currProcessName];
                                        isTonnageAvailable = true;
                                    }
                                    else
                                        currProcess = null;

                                    if (currCellStr != "" && currCellStr != "#VALUE!")
                                        currTonPerShift = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                    else
                                        currTonPerShift = 0;

                                    if (currTonPerShift > 0)
                                    {
                                        if (currProcess == null
                                               && (isUseHistoricalProcessIfNoTonnages || !articlesWithTonnages.Contains(currArtName)) &&
                                            (
                                                (isUseHistoricalProcessIfNoTonnages && historicalProductionProcesses.Contains(currArtName + currAutomatName)) ||
                                                !isUseHistoricalProcessIfNoTonnages
                                                )
                                            )
                                        {
                                            pOutputFile.WriteLine("Article without historical tonnages " + currArtName + ": Assigned machine " + currAutomatName);
                                            currProcess = new ProductionProcess();
                                            currProcess.ResourceName = currAutomatName;
                                            currProcess.ProductName = currArtName;
                                        }

                                        if (currProcess != null) //Process is only added to the final set of processes if 1) in case of an article with past production the process already has been applied 2) the article does not have past production
                                        {
                                            if (!usedPreliminaryResources.Contains(currAutomatName))
                                                usedPreliminaryResources.Add(currAutomatName);
                                            currProcess.TonPerShift = currTonPerShift;
                                            currPlantName = wPlantLocsForMachines[currAutomatName];
                                            if (isUseCostPerShift)
                                            {
                                                if (wCostPerShiftForMachines.ContainsKey(currAutomatName))
                                                    currProcess.CostPerShift = wCostPerShiftForMachines[currAutomatName];
                                                else
                                                    currProcess.CostPerShift = 0;
                                            }
                                            else
                                            {
                                                if (wCostPerShiftForMachines.ContainsKey(currAutomatName))
                                                    currProcess.CostPerShift = wCostPerShiftForMachines[currAutomatName];
                                                else
                                                    currProcess.CostPerShift = 0;
                                            }
                                            currProcess.LocationName = wPlantLocations[currPlantName].locationCountry + wPlantLocations[currPlantName].locationPLZ;

                                            if (!preliminaryProductionProcesses.ContainsKey(currProcess.Key))
                                            {
                                                preliminaryProductionProcesses.Add(currProcess.Key, currProcess);
                                                if (!productsWithProcesses.Contains(currArtName))
                                                    productsWithProcesses.Add(currArtName);
                                            }
                                        }
                                        else
                                        {

                                        }
                                    }
                                }

                                if (!isResource || (!isTonnageAvailable && isTonnageNecessary))
                                {
                                    if (!processesNotConsidered.ContainsKey(currProcessName))
                                    {
                                        currPCIS = new ProcessInformationState();
                                        currPCIS.isHistoricalProductionNeeded = isTonnageNecessary;
                                        currPCIS.isNotExistHistoricalProduction = !isTonnageAvailable;
                                        currPCIS.hasNoResourceWithCapacity = !isResource;
                                        currPCIS.productName = currArtName;
                                        currPCIS.resourceName = currAutomatName;
                                        processesNotConsidered.Add(currProcessName, currPCIS);

                                        currPCIS = processesNotConsidered[currProcessName];
                                    }
                                    
                                }

                            }
                        }
                    }
                }
            }

            aBDWPDoc = new Optimizer_Data.Excel_Document(completeFN2);
            aBDWPDoc.OpenDoc();
           
            tab = loadTableFromExcelDocument(aBDWPDoc, addWorkingPlansSheetName, addWorkingPlansColNames, addWorkingPlansColMatchTypes);//aBDWPDoc.TableToArray();
            aBDWPDoc.CloseDoc();

            for (i = tab.firstRow + 1; i < tab.rawTable.Rows .Count ; i++)//for (i = 1; i < tab.numberOfRows; i++)
            {
                currArtName = tab.rawTable.Rows[i].ItemArray[tab.colNums[0]].ToString().Trim();//tab.cells[i, artNumColumnNum].Trim();
                currAutomatName = tab.rawTable.Rows[i].ItemArray[tab.colNums[3]].ToString().Trim();//tab.cells[i, automatColumnNum].Trim();
                endOfAutomatName = currAutomatName.IndexOf(" ");
                if (endOfAutomatName != -1)
                    currAutomatName = currAutomatName.Substring(0, endOfAutomatName);

                tempObj = currAutomatName;

                if (preliminaryResources.ContainsKey(currAutomatName))//(currResource != null)
                {
                    currResource = preliminaryResources[currAutomatName];
                    currProcessName = currArtName + currAutomatName;
                    object k = currProcessName;


                    if (tonnageProductionProcesses.ContainsKey(currProcessName))
                        currProcess = tonnageProductionProcesses[currProcessName];//(ProductionProcess)tonnageProductionProcesses.getObj(ref k);
                    else
                        currProcess = null;

                    try
                    {
                        currCellStr = tab.rawTable.Rows[i].ItemArray[tab.colNums[2]].ToString().Trim();//tab.cells[i, tonPerShiftColumnNum].Trim();
                        currTonPerShift = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);

                        if (currTonPerShift == 0)
                        {
                            try
                            {
                                currCellStr = tab.rawTable.Rows[i].ItemArray[tab.colNums[1]].ToString().Trim();//tab.cells[i, oldTonPerShiftColumnNum].Trim();
                                currTonPerShift = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                            }
                            catch (Exception e2)
                            {
                                currTonPerShift = 0;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            currCellStr = tab.rawTable.Rows[i].ItemArray[tab.colNums[1]].ToString().Trim();//tab.cells[i, oldTonPerShiftColumnNum].Trim();
                            currTonPerShift = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                        }
                        catch (Exception e2)
                        {
                            currTonPerShift = 0;
                        }
                    }

                    if (currTonPerShift > 0)
                    {
                        if (currProcess == null && (!articlesWithTonnages.Contains(currArtName) &&
                                            (
                                                (isUseHistoricalProcessIfNoTonnages && historicalProductionProcesses.Contains(currProcessName)) ||
                                                !isUseHistoricalProcessIfNoTonnages
                                                )
                                            )
                        )
                        {
                            pOutputFile.WriteLine("Article without historical tonnages " + currArtName + ": Assigned machine " + currAutomatName);
                            currProcess = new ProductionProcess();
                            currProcess.ResourceName = currAutomatName;
                            currProcess.ProductName = currArtName;
                        }

                        if (currProcess != null) //Process is only added to the final set of processes if 1) in case of an article with past production the process already has been applied 2) the article does not have past production
                        {
                            if (!usedPreliminaryResources.Contains(currAutomatName))
                                usedPreliminaryResources.Add(currAutomatName);

                            currProcess.TonPerShift = currTonPerShift;

                            if (isUseCostPerShift)
                            {
                                if (wCostPerShiftForMachines.ContainsKey(currAutomatName))
                                    currProcess.CostPerShift = wCostPerShiftForMachines[currAutomatName];
                                else
                                    currProcess.CostPerShift = 0;
                            }
                            else
                            {
                                if (wCostPerShiftForMachines.ContainsKey(currAutomatName))
                                    currProcess.CostPerShift = wCostPerShiftForMachines[currAutomatName];
                                else
                                    currProcess.CostPerShift = 0;
                            }

                            currPlantName = wPlantLocsForMachines[currAutomatName];
                            currProcess.LocationName = wPlantLocations[currPlantName].locationCountry + wPlantLocations[currPlantName].locationPLZ;

                            if (!preliminaryProductionProcesses.ContainsKey(currProcess.Key))//(pIProdProc == null)
                            {
                                preliminaryProductionProcesses.Add(currProcess.Key, currProcess);//.add(currProcess);
                                if (!productsWithProcesses.Contains(currArtName))
                                    productsWithProcesses.Add(currArtName);
                            }
                            else
                            {
                            }
                        }
                    }
                }
            }

            pOutputFile.Close();

            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
        }

        //protected int getMonthNum(ref string monthName)
        //{
        //    int i;

        //    for (i = 0; i < MONTHS.Length; i++)
        //    {
        //        if (monthName == MONTHS[i])
        //            return i;
        //    }
        //    return -1;
        //}

        protected void getShiftData(SNP_WEPA_Instance pI)
        {
            Optimizer_Data.Excel_Document shiftsDoc;
            string completeFN;
            string currAutomatName;
            Resource currResource;
            XLSTable tab;
            CapacityValue currCapVal;
            int i, j,spaceCharPos,monthI;
           
            string currTitleCellStr, currCellStr,periodName;
            double totalCapVal;
            object tempObj;
            string msg;
            IndexedItem<Period> currPI;

            completeFN = srcPath + Path.DirectorySeparatorChar + shiftsFileName;
            if (!File.Exists(completeFN))
                throw new OptimizerException(OptimizerException.OptimizerExceptionType.InputFileNotFound, shiftsFileName);

            msg = "Lese Schichtdaten...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);

            preliminaryResources = new Dictionary<string, Resource>();

            shiftsDoc = new Optimizer_Data.Excel_Document(completeFN);
            shiftsDoc.OpenDoc();
            
            tab = loadTableFromExcelDocument(shiftsDoc, shiftDataSheetName, shiftDataColNames,shiftDataColMatchTypes );//shiftsDoc.TableToArray();
            
            for (j = tab.colNums[1]; j < tab.rawTable.Columns.Count; j++)//for (j = 0; j < tab.numberOfColumns; j++)
            {
                currTitleCellStr = tab.rawTable.Rows[tab.rowNums[1]].ItemArray[j].ToString().Trim();//tab.cells[0, j].Trim();
                if (currTitleCellStr != "")
                {
                    spaceCharPos=currTitleCellStr.IndexOf(" ");
                    if (spaceCharPos != -1)
                        currAutomatName = currTitleCellStr.Substring(0, spaceCharPos);
                    else
                        currAutomatName = currTitleCellStr;

                   tempObj= currAutomatName;
                  
                    if (wPlantLocsForMachines.ContainsKey (currAutomatName))
                    {
                        currResource = new Resource();
                        currResource.ResourceName = currAutomatName;
                        currResource.LocationName = wPlantLocsForMachines[currAutomatName];
                  
                        totalCapVal = 0;

                        for (i = tab.rowNums[0] ; i < tab.rawTable.Rows .Count ; i++)//for (i = 1; i < tab.numberOfRows; i++)
                        {
                            periodName = tab.rawTable.Rows[i].ItemArray[tab.colNums[0]].ToString().Trim();//tab.cells[i, monthColumnPos].Trim();
                            if (periodName != "")
                            {
                                //periodName = "M" + Convert.ToString(monthI + 1, currC);

                                     currPI = pI.getPeriod(periodName);
                                     if (currPI != null)
                                     {
                                         currCellStr = tab.rawTable.Rows[i].ItemArray[j].ToString().Trim();//tab.cells[i, j].Trim();

                                       if (currCellStr != "")
                                            totalCapVal += Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                    }
                                //monthI = getMonthNum(ref currCellStr);
                                //if (monthI != -1)
                                //{
                                //     periodName = "M" + Convert.ToString(monthI + 1, currC);
                                     
                                //     currPI = pI.getPeriod(periodName);
                                //     if (currPI != null)
                                //    {
                                //        currCellStr = tab.rawTable.cells[i, j].Trim();//tab.cells[i, j].Trim();

                                //        if (currCellStr != "")
                                //            totalCapVal += Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                //    }
                                //}
                            }
                        }

                        if (totalCapVal > 0)
                        {
                            for (i = tab.rowNums[0]; i < tab.rawTable.Rows .Count ; i++)//for (i = 1; i < tab.numberOfRows; i++)
                            {
                                periodName = tab.rawTable.Rows[i].ItemArray[tab.colNums[0]].ToString().Trim();

                                //currCellStr = tab.rawTable.cells[i, tab.colNums[0]].Trim();//tab.cells[i, monthColumnPos].Trim();
                                if (periodName != "")
                                {
                                //    monthI = getMonthNum(ref currCellStr);
                                //    if (monthI != -1)
                                //    {
                                //        periodName = "M" + Convert.ToString(monthI + 1, currC);

                                        currPI = pI.getPeriod(periodName);
                                       if (currPI != null)
                                       {
                                            currCapVal = new CapacityValue();
                                            currCapVal.PeriodName = periodName;

                                            currCellStr = tab.rawTable.Rows[i].ItemArray[j].ToString().Trim();//tab.cells[i, j].Trim();
                                            if (currCellStr != "")
                                                currCapVal.Val = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                            else
                                                currCapVal.Val = 0;

                                            currResource.addCapacity(currCapVal);
                                        }
                                }

                                //currCellStr = tab.rawTable.cells[i, tab.colNums[0]].Trim();//tab.cells[i, monthColumnPos].Trim();
                                //if (currCellStr != "")
                                //{
                                //    monthI = getMonthNum(ref currCellStr);
                                //    if (monthI != -1)
                                //    {
                                //        periodName = "M" + Convert.ToString(monthI + 1, currC);

                                //        currPI = pI.getPeriod(periodName);
                                //        if (currPI != null)
                                //        {
                                //            currCapVal = new CapacityValue();
                                //            currCapVal.PeriodName = periodName;

                                //            currCellStr = tab.rawTable.cells[i, j].Trim();//tab.cells[i, j].Trim();
                                //            if (currCellStr != "")
                                //                currCapVal.Val = Convert.ToDouble(currCellStr.Replace(currC.NumberFormat.NumberGroupSeparator, ""), currC);
                                //            else
                                //                currCapVal.Val = 0;

                                //            currResource.addCapacity(currCapVal);
                                //        }
                                //    }
                                //}
                            }
                           
                            preliminaryResources.Add((string)currResource.Key, currResource);
                        }
                    }
                }
            }

            usedPreliminaryResources = new HashSet<string>();

            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, ref msg, 0);
            
        }
    }
}