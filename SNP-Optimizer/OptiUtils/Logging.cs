﻿using System.Diagnostics;
using System.Collections.Generic;
using System.Data;
using Optimizer_Data;
using Optimizer_Messages;
using System.Threading;
using System;
namespace Logging
{

    public enum LoggerMsgType
    {
        INFO_MSG,
        WARNING_MSG,
          ERROR_MSG
    }

    public interface Logger
    {   
        void outputMessage(LoggerMsgType type , string msg, int msgId);
    }

    public class LoggerCollection : Logger
    {
        protected int numberOfLoggers;

        protected List<Logger> lC;//Logger [];

        public LoggerCollection ()
        {
            lC=new List<Logger>();
        }

        public void addLogger(Logger l)
        {
            lC.Add(l);
            
        }

        public void outputMessage(LoggerMsgType type, string msg, int msgId) 
        {
            foreach (Logger l in lC)
            {
               lock (l)
                {
                    l.outputMessage(type, msg, msgId);
                }
            }
        }
    }

    public class ScreenLogger :Logger
    {
        protected Mutex mut;
     
        public ScreenLogger ()
        {
            mut = new Mutex();
        }

        public void outputMessage(LoggerMsgType type , string msg, int msgId)
        {

            mut.WaitOne();

            switch (type)
            {
                case LoggerMsgType.INFO_MSG:
                    Console.WriteLine(msg);
                    break;
                case LoggerMsgType.WARNING_MSG:
                    Console.WriteLine("Warning: " + msg);
                    break;
                case LoggerMsgType.ERROR_MSG:
                    Console.WriteLine("Error:" + msg);
                    break;
            }

            mut.ReleaseMutex();
            
        }
    }

    public class SystemEventLogger :Logger
    {
       UserMessageGenerator msgGen ;

        EventLog eLog;

        string sName;
       string lName;

        public SystemEventLogger (string sourceName, string logName, UserMessageGenerator mG)
        {
            
            sName = sourceName;
            lName = logName;

            eLog = new EventLog();

            eLog.Source = sourceName;
            eLog.Log = logName;
            msgGen = mG;
        }

        public void createSource()
        {
             if(!System.Diagnostics.EventLog.SourceExists(sName))
             {
                 System.Diagnostics.EventLog.CreateEventSource(sName, lName);
             }

        }

        public void deleteSource()
        {
            if(!System.Diagnostics.EventLog.SourceExists(sName))
            {
                System.Diagnostics.EventLog.DeleteEventSource(sName);
            }
        }

        public void outputMessage(LoggerMsgType type, string msg, int msgId)
        {
           switch(type)
           {
               case LoggerMsgType.INFO_MSG:
                    eLog.WriteEntry(msg);
                   break;
               case LoggerMsgType.WARNING_MSG:
                    eLog.WriteEntry("Warning: " + msg);
                   break;
               case LoggerMsgType.ERROR_MSG:
                    eLog.WriteEntry("Error:" + msg);
                   break;
            }
        }
    }


    public class FileLogger:Logger
    {
        private UserMessageGenerator msgGen;
        private CSV_File logFile;
       // private DataTable msgTab;

        public FileLogger(string fileName, UserMessageGenerator mG)
        {
            int i;
            logFile = new CSV_File(fileName);
            logFile.IsHeadLine=false;
            logFile.openForWriting(true);
           
            //msgTab = new DataTable();
           
            //for (i=0;i<3;i++)
            //    msgTab.Columns .Add ();

            msgGen = mG;
        }

       
        public void outputMessage(LoggerMsgType type , string msg, int msgId)
        {
           string currentTime;
            string [] msgRow=new string[3];
            currentTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
           msgRow[0] = currentTime;
            switch (type)
            {
                case LoggerMsgType.INFO_MSG:
                     msgRow[1] = msg;
                    break;
                case LoggerMsgType.WARNING_MSG:
                     msgRow[1] = "Warning: " +msg;
                    break;
                case LoggerMsgType.ERROR_MSG:
                    msgRow[1] = "Error:" +msg;
                    break;
            }
           msgRow[2] = Convert.ToString(msgId);
            logFile.writeData(msgRow);
            //logFile.ArrayToTable(msgTab)
        }

        public void close()
        {
            if (logFile!=null)
            {
                logFile.closeDoc ();
            }
        }
    }

    public class TableLogger
    {
        private UserMessageGenerator msgGen;

        public class LogEntry
        {
            public DateTime timeStamp;
            public LoggerMsgType msgType;
            public int msgId;
            public string msg;
        }

        public struct CurrentLog
        {
            LogEntry[] log;
            int numberOfLogEntries;
        }

        List<LogEntry> log;

        public TableLogger(string fileName, UserMessageGenerator mG)
        {
            log = new List<LogEntry>();
            msgGen = mG;
        }


        public void outputMessage(LoggerMsgType type, string msg, int msgId)
        {
            LogEntry currLEntry;

            currLEntry = new LogEntry();

            currLEntry.msgId = msgId;
            currLEntry.msgType = type;
            currLEntry.msg = msg;
            currLEntry.timeStamp = DateTime.Now;
            log.Add(currLEntry);
        }


        public List<LogEntry> clearLog()
        {
            List<LogEntry> cL=new List<LogEntry>();

            cL.AddRange (log);
            log.Clear ();
            return cL;
        }

        public List<LogEntry> peekLog()
        {
            return log;
        }

        public DataTable peekLogAsTable()
        {
            int i;
            string [] lRow;
            DataTable currTab=new DataTable ();

            for (i = 0; i < 4; i++)
                currTab.Columns.Add();

            lRow =new string[4];
            
            foreach (LogEntry lE in log)
            {
                lRow[0] = lE.timeStamp.ToString("yyyy-MM-ddTHH:mm:ss");
                lRow[1] = Convert.ToString(lE.msgType);
                lRow[2] = Convert.ToString(lE.msgId);
                lRow[3] = lE.msg;

                currTab.Rows.Add(lRow);
            }

            return currTab;
        }
    }
}



